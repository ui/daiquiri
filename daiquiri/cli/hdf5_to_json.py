#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import argparse
from daiquiri.core.components.hdf5 import hdf5_to_dict


def file_and_path(string):
    """Expect a filename string as provided by `silx.io`: `filename.h5::path`"""
    if "::" in string:
        return string.split("::", 1)
    return string, None


def main():
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(
        description="HDF5 to JSON as provided by the hdf5 component server"
    )
    parser.add_argument(
        "input",
        type=file_and_path,
        default=None,
        help="Link to the HDF5 file and data path. filename.h5::path",
    )
    parser.add_argument(
        "--load_data",
        action="store_true",
        default=False,
        help="If defined, the data of the datasets will be included. Else it is set to None",
    )

    options = parser.parse_args()
    filename, path = options.input
    result = hdf5_to_dict(filename, path, load_data=options.load_data)
    print(json.dumps(result))

    return 0


if __name__ == "__main__":
    result = main()
    sys.exit(result)
