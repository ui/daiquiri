#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    import bliss  # noqa F401

    # Importing bliss patches gevent which in turn patches python
except ImportError:
    from gevent.monkey import patch_all

    patch_all(thread=False)

# TODO: Horrible hack to avoid:
#   ImportError: dlopen: cannot load any more object with static TLS
#   https://github.com/pytorch/pytorch/issues/2575
#   https://github.com/scikit-learn/scikit-learn/issues/14485
from silx.math import colormap  # noqa F401

import time
import os
import json
import argparse
import pydantic
from ruamel.yaml import YAML
from flask import Flask, jsonify
from flask_socketio import SocketIO
from flask_apispec import FlaskApiSpec
from flask_restful import abort
from flask import send_from_directory
from webargs.flaskparser import parser
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

import daiquiri
from daiquiri.core.authenticator import Authenticator
from daiquiri.core.session import Session
from daiquiri.core.queue import Queue
from daiquiri.core.metadata import MetaData
from daiquiri.core.components import Components
from daiquiri.core.hardware import Hardware
from daiquiri.core.schema import Schema
from daiquiri.core.layout import Layout
from daiquiri.core.saving import Saving
from daiquiri.resources import utils
from daiquiri.core.logging import log
from daiquiri.core.responses import nocache
from daiquiri.core.options import ServerOptions

import logging

logger = logging.getLogger(__name__)
DAIQUIRI_ROOT = os.path.dirname(daiquiri.__file__)


def init_server(
    options: ServerOptions,
    testing: bool = False,
):
    """Instantiate a Flask application

    :param options: Options use to setup the server
    :param testing: Attaches core components to the `app` so that can be retrieved in tests
    :returns: Flask, SocketIO
    """
    start = time.time()
    for resource_folder in reversed(options.resource_folders):
        if not resource_folder:
            continue
        if not os.path.isdir(resource_folder):
            raise ValueError(f"Resource folder '{resource_folder}' does not exist")
        utils.add_resource_root(resource_folder)
        logger.info("Added resource folder: %s", resource_folder)

    static_folder = options.static_folder
    if static_folder.startswith("static."):
        provider = utils.get_resource_provider()
        try:
            static_folder = provider.get_resource_path(static_folder, "")
        except utils.ResourceNotAvailable:
            raise ValueError(
                f"Static resource '{static_folder}' does not exist"
            ) from None
    if not os.path.isdir(static_folder):
        raise ValueError(f"Static folder '{static_folder}' does not exist")
    if options.hardware_folder:
        if not os.path.isdir(options.hardware_folder):
            raise ValueError(
                f"Hardware folder '{options.hardware_folder}' does not exist"
            )
        os.environ["HWR_ROOT"] = options.hardware_folder

    config = utils.ConfigDict("app.yml")

    if not config.get("versions"):
        config["versions"] = []

    # Allow CLI to override implementors
    if options.implementors:
        config["implementors"] = options.implementors

    log.start(config=config)
    logger.info(f"Starting daiquiri version: {daiquiri.__version__}")

    app = Flask(__name__, static_folder=static_folder, static_url_path="/")

    app.config["APISPEC_FORMAT_RESPONSE"] = None
    app.config["APISPEC_TITLE"] = "daiquiri"

    security_definitions = {
        "bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}
    }

    app.config["APISPEC_SPEC"] = APISpec(
        title="daiquiri",
        version="v1",
        openapi_version="2.0",
        plugins=[MarshmallowPlugin()],
        securityDefinitions=security_definitions,
    )

    if not config.get("swagger"):
        app.config["APISPEC_SWAGGER_URL"] = None
        app.config["APISPEC_SWAGGER_UI_URL"] = None

    docs = FlaskApiSpec(app)

    sio_kws = {}
    if config["cors"]:
        from flask_cors import CORS

        sio_kws["cors_allowed_origins"] = "*"
        CORS(app)

    app.config["SECRET_KEY"] = config["iosecret"]
    socketio = SocketIO(app, **sio_kws)

    log.init_sio(socketio)

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify(error=str(e)), 404

    @app.errorhandler(405)
    def method_not_allowed(e):
        return jsonify(message="The method is not allowed for the requested URL."), 405

    # @app.errorhandler(422)
    # def unprocessable(e):
    #     return jsonify(error=str(e)), 422

    @parser.error_handler
    def handle_request_parsing_error(
        err, req, schema, error_status_code, error_headers
    ):
        abort(422, description=err.messages)

    if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        schema = Schema(app=app, docs=docs, socketio=socketio)
        ses = Session(
            config=config, app=app, docs=docs, socketio=socketio, schema=schema
        )
        schema.set_session(ses)

        Authenticator(config=config, app=app, session=ses, docs=docs, schema=schema)

        # TODO
        # This is BLISS specific, move to where bliss is handled ?
        if config.get("controls_session_type", None) == "bliss":
            # This is not very elegant but a solution until this section
            # have been moved to an appropriate place
            from daiquiri.core.hardware.bliss.session import BlissSession

            BlissSession(config["controls_session_name"])

        hardware = Hardware(
            base_config=config, app=app, socketio=socketio, docs=docs, schema=schema
        )
        # TODO: implement get_core_component API
        app.hardware = hardware

        queue = Queue(
            config=config,
            app=app,
            session=ses,
            docs=docs,
            socketio=socketio,
            schema=schema,
        )

        Layout(
            config, app=app, session=ses, docs=docs, socketio=socketio, schema=schema
        )

        metadata = MetaData(
            config, app=app, session=ses, docs=docs, socketio=socketio, schema=schema
        ).init()
        ses.set_metadata(metadata)

        saving = Saving(
            config,
            app=app,
            session=ses,
            docs=docs,
            socketio=socketio,
            schema=schema,
            metadata=metadata,
        ).init()

        components = Components(
            base_config=config,
            app=app,
            socketio=socketio,
            docs=docs,
            schema=schema,
            hardware=hardware,
            session=ses,
            metadata=metadata,
            queue=queue,
            saving=saving,
        )

    if options.save_spec_file is not None:
        logger.info("Writing API spec and exiting")
        save_spec_dir = os.path.dirname(options.save_spec_file)
        if not os.path.exists(save_spec_dir):
            os.mkdir(save_spec_dir)
        with open(options.save_spec_file, "w") as spec:
            json.dump(docs.spec.to_dict(), spec)

        exit()

    if config["debug"] is True:
        app.debug = True

    if testing:
        app.queue = queue
        app.metadata = metadata
        app.components = components
        app.session = ses
        app.socketio = socketio
        app.saving = saving

        def close():
            components.close()

        app.close = close

    @app.route("/manifest.json")
    def manifest():
        return app.send_static_file("manifest.json")

    @app.route("/meta.json")
    @nocache
    def meta():
        return app.send_static_file("meta.json")

    @app.route("/favicon.ico")
    def favicon():
        return app.send_static_file("favicon.ico")

    if options.static_resources_folder:
        default_static_resources = os.path.join(static_folder, "resources")
        static_resources_folder = os.path.abspath(options.static_resources_folder)

        @app.route("/resources/<path:path>")
        def resources(path):
            """Serve a resource file from static_resources_folder if it exists.

            Else fall back to the original static directory.
            """
            if os.path.isfile(os.path.join(static_resources_folder, path)):
                return send_from_directory(static_resources_folder, path)
            return send_from_directory(default_static_resources, path)

    @app.route("/", defaults={"path": ""})
    @app.route("/<string:path>")
    @app.route("/<path:path>")
    @nocache
    def index(path):
        return app.send_static_file("index.html")

    took = round(time.time() - start, 2)
    logger.info(f"Server ready, startup took {took}s", extra={"startup_time": took})

    return app, socketio


def get_certs_file_path(resource):
    """Get a file path from a resource path

    This can be:
    - foobar.txt  # read file from resource provides "certs"
    - /etc/foobar.txt  # read file from absolute path
    """
    if resource.startswith("/"):
        return os.path.abspath(resource)
    provider = utils.get_resource_provider()
    return provider.get_resource_path("certs", resource)


def get_ssl_context(options: ServerOptions):
    """Build an ssl context for serving over HTTPS.

    The options get the priority on the configuration.

    The configurations should be dropped at some point for security.
    """
    config = utils.ConfigDict("app.yml")

    use_ssl = options.ssl or config.get("ssl", False)
    if not use_ssl:
        return None

    import ssl

    crt = get_certs_file_path(options.ssl_cert or config["ssl_cert"])
    key = get_certs_file_path(options.ssl_key or config["ssl_key"])

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    try:
        context.load_cert_chain(crt, key)
    except Exception:
        logger.exception("Could not load certificate chain", exc_info=True)
        raise

    return context


def run_server(options: ServerOptions):
    """Runs REST server

    :param int port:
    :param kwargs: see `init_server`
    """
    app, socketio = init_server(options)

    server_args = {}
    context = get_ssl_context(options)
    if context:
        server_args["ssl_context"] = context

    socketio.run(app, host="0.0.0.0", port=options.port, **server_args)  # nosec


def add_pydantic_model(parser: argparse.ArgumentParser, model: pydantic.BaseModel):
    "Add Pydantic model to an ArgumentParser"
    fields = model.__fields__
    for name, field in fields.items():
        extra = field.json_schema_extra or {}
        name_or_flags = []
        if "flag" in extra:
            name_or_flags += [extra["argparse_flag"]]
        if "argname" in extra:
            name_or_flags += [extra["argparse_name"]]
        else:
            name_or_flags += [f"--{name.replace('_', '-')}"]

        parser.add_argument(
            *name_or_flags,
            dest=name,
            nargs=extra.get("argparse_nargs"),
            type=extra.get("argparse_type") or field.annotation,
            default=None,  # let pydantic handle the default later
            help=field.description,
        )


def parse_options() -> ServerOptions:
    """Parse the server options.

    Read the command line arguments and the optional configuration file.
    """
    parser = argparse.ArgumentParser(description="REST server for a beamline GUI")
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default=None,
        help="File/resource which is used to setup the server (instead of the command line argument)",
    )
    add_pydantic_model(parser, ServerOptions)
    cmd_args = parser.parse_args()

    config_args = {}
    if cmd_args.config:
        logging.info("Read config file: %s", cmd_args.config)
        yaml = YAML(typ="safe")
        with open(cmd_args.config, mode="rt") as f:
            config_args = yaml.load(f)

    merged_args = {}
    merged_args.update(config_args)
    # Only override command line arguments which are set
    merged_args.update({k: v for k, v in vars(cmd_args).items() if v is not None})

    options = ServerOptions(**merged_args)

    # Sounds like flask except an absolute path
    if options.static_folder is not None:
        options.static_folder = os.path.abspath(options.static_folder)

    return options


def main():
    """Runs REST server with CLI configuration"""
    options = parse_options()
    run_server(options)


if __name__ == "__main__":
    main()
