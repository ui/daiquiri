# -*- coding: utf-8 -*-
import re
import logging
import json

from flask import g

from requests import Session, post, get
from requests.auth import HTTPBasicAuth
from zeep import Client
from zeep.transports import Transport

import xmltodict
from urllib.parse import urljoin
from bliss import current_session

from marshmallow import fields, Schema, EXCLUDE
from daiquiri.core import CoreResource, marshal
from daiquiri.core.metadata.user import User
from daiquiri.core.metadata import MetaDataHandler

# from daiquiri.core.schema.biosaxscollect.biosaxs import (
#     SCCollectSample,37
#     SCCollectBuffer,
#     SCIspybMetadataArgumentSchema
# )

logger = logging.getLogger(__name__)


class ISPyBConfigSchema(Schema):
    meta_url = fields.Str()
    meta_user = fields.Str()
    meta_password = fields.Str()
    meta_beamline = fields.Str()
    meta_staff = fields.Str()
    rest_meta_user = fields.Str()
    rest_meta_password = fields.Str()


class ProposalResource(CoreResource):
    @marshal(inp={"code": fields.Str(), "number": fields.Str()})
    def get(self, code, number):
        """Get current proposal"""
        proposal = self._parent.find_proposal(code, number)

        if proposal:
            return proposal, 200
        else:
            return {"error": "No such propsoal"}, 404


class ListExperimentsResource(CoreResource):
    def get(self):
        """Get a list of all experiments for current proposal"""
        experiments = json.loads(self._parent.list_experiments())

        if experiments:
            return experiments, 200
        else:
            return {"error": "Could not find any experiment for proposal"}, 404


class GetExperimentResource(CoreResource):
    @marshal(inp={"experiment_id": fields.Str()})
    def get(self, experiment_id):
        """Get experiment with experiment id: <experiment_id>"""
        experiment = json.loads(self._parent.get_experiment(experiment_id))

        if experiment:
            return experiment, 200
        else:
            return {"error": "Could not find any experiment for proposal"}, 404


# ----------- rest web socket ------------------
# store  list of experiment  from UI to ISPYB
class StoreSCTableRestWSResource(CoreResource):
    @marshal(
        inp={
            # "data": fields.Nested(SCIspybMetadataArgumentSchema)})
            "data": fields.Raw(required=True)
        }
    )
    def post(self, data):
        """Store experiment with data <data>"""
        success = False
        try:
            self._parent.create_rest_ws_templete_experiment(data)
            success = True
        except Exception:
            return {"error": "Could not store experiment"}, 404

        if success:
            return {"Succeed": "Successfully "}, 200

        return {"error": "Could not store experiment"}, 404


# Get List of Experiment and send them to UI
class GetSCExperimentRestWSResource(CoreResource):
    def get(self):
        """Get experiment with experiment id"""
        experiment = self._parent.get_rest_ws_experiments()
        if experiment:
            return {"data": experiment}, 200
        else:
            return {"error": "Could not find any experiment for proposal"}, 404


# ------------------------ END rest web socket ---------------------------------


class IspybMetaDataHandler(MetaDataHandler):
    def __init__(self, *args, **kwargs):
        self._config = ISPyBConfigSchema().load(
            kwargs.get("config", {}), unknown=EXCLUDE
        )
        super().__init__(*args, **kwargs)

        self._current_experiment = None
        self._current_measurement = None
        self._current_proposal = None

    def _get_prop(self):
        prop = re.findall(r"[^\W\d_]+|\d+", self._current_proposal)

        try:
            pcode, pnumber = prop
        except ValueError:
            pcode, pnumber = ("", "")

        return pcode, pnumber

    def setup(self, *args, **kwargs):
        logger.debug("Loaded: {c}".format(c=self.__class__.__name__))

        self.register_route(GetExperimentResource, "/get-experiment")

        #  Rest Web Services
        self.register_route(GetSCExperimentRestWSResource, "/list-experiments")
        self.register_route(StoreSCTableRestWSResource, "/store_sc_experiment")

        self.rest_ws_host = self._config.get("rest_meta_host")

        host = self._config.get("meta_host")

        session = Session()
        session.auth = HTTPBasicAuth(
            self._config.get("meta_user"), self._config.get("meta_password")
        )

        self._shipping_client = Client(
            host + "ToolsForShippingWebService?wsdl",
            transport=Transport(session=session),
        )

        self._collection_client = Client(
            host + "ToolsForCollectionWebService?wsdl",
            transport=Transport(session=session),
        )

        self._biosaxs_client = Client(
            host + "ToolsForBiosaxsWebService?wsdl",
            transport=Transport(session=session),
        )

        self._generic_biosaxs_client = Client(
            host + "GenericSampleChangerBiosaxsWebService?wsdl",
            transport=Transport(session=session),
        )

        super().setup()

    def find_sessions(self, code, number):
        return self._collection_client.service.findSessionsByProposalAndBeamLine(
            code + number, self._config.get("beamline")
        )

    def find_proposal(self, code, number):
        return self._shipping_client.service.findProposal(code, number)

    def get_user(self, **kwargs):
        self._current_proposal = g.login

        u = {
            "givenname": "",
            "familyname": "",
            "fullname": "",
            "personid": 1,
            "login": self._current_proposal,
            "groups": [],
            "permissions": ["staff"],
        }

        user = User(**u)
        user["is_staff"] = user.permission(self._config["meta_staff"])

        return user

    def set_session(self, session):
        if self.verify_session(session):
            self._session.update({"blsession": session})
            return True

    def verify_session(self, session):
        return self.get_sessions(session=session)

    def get_proposals(self, proposal=None, **kwargs):
        code, number = re.findall(r"[^\W\d_]+|\d+", proposal)
        p = self.find_proposal(code, number)
        proposal = {}

        if p:
            proposal = {
                "proposalid": p.proposalId,
                "proposalcode": p.code,
                "proposalnumber": p.number,
                "proposal": p.code + p.number,
            }

            self.current_proposal = code, number

        return proposal

    def get_sessions(self, session=None, **kwargs):
        session = {
            "sessionid": 1,
            "proposalid": 1,
            "proposal": g.login,
            "visit_number": 1,
            "session": "s1",
        }

        return session

    # ---------------------------------------------------------------------------------------------------
    def get_rest_ws_token(self):
        auth_url = urljoin(self.rest_ws_host, "authenticate?site=" + "ESRF")
        # self._config.get("meta_user"), self._config.get("meta_password")
        try:
            data = {
                "login": self._config.get("rest_meta_user"),
                "password": self._config.get("rest_meta_password"),
            }
            response = post(auth_url, data=data, timeout=900)
            rest_token = response.json().get("token")
        except Exception as ex:
            msg = "POST to %s failed reason %s" % (auth_url, str(ex))
            logging.getLogger("ispyb_client").exception(msg)
            return ""

        return rest_token

    def get_rest_ws_sample_info(self):
        """Get all sample information"""
        rest_token = self.get_rest_ws_token()
        current_user = current_session.scan_saving.proposal.name
        sample_info = get(
            self.rest_ws_host + rest_token + "/proposal/" + current_user + "/info/get",
            timeout=900,
        )
        return sample_info.json()

    def get_rest_ws_list_experiment(self):
        rest_token = self.get_rest_ws_token()
        current_user = current_session.scan_saving.proposal.name
        experiment_list = get(
            self.rest_ws_host
            + rest_token
            + "/proposal/"
            + current_user
            + "/saxs/experiment/list",
            timeout=900,
        )

        return experiment_list.json()

    def get_rest_ws_experiments(self):
        """Get all TEMPLATE experiments by experiment id"""
        experiments = []
        rest_token = self.get_rest_ws_token()
        experiment_list = self.get_rest_ws_list_experiment()
        current_user = current_session.scan_saving.proposal.name
        for experiment in experiment_list:
            experimentid = experiment["experimentId"]
            if experiment["experimentType"] == "TEMPLATE":
                experimentbyid = get(
                    self.rest_ws_host
                    + rest_token
                    + "/proposal/"
                    + current_user
                    + "/saxs/experiment/"
                    + str(experimentid)
                    + "/samplechanger"
                    + "/type/Sample"
                    "/template",
                    timeout=900,
                )
                experimentbyid = experimentbyid.text.replace("\n", "")
                if experimentbyid != "":
                    data = json.loads(json.dumps(xmltodict.parse(experimentbyid)))
                    data["bsxcube"].update(
                        experiment
                    )  # this is not elegant, but necessary
                    experiments.append(data)
        print(current_user)
        print(len(experiments))
        return experiments

    def create_rest_ws_templete_experiment(self, data):
        rest_token = self.get_rest_ws_token()
        current_user = current_session.scan_saving.proposal.name
        try:
            url = (
                self.rest_ws_host
                + rest_token
                + "/proposal/"
                + current_user
                + "/saxs/experiment/save"
            )
            data = {
                "name": data["name"],
                "comments": data["comments"],
                "measurements": str(data["measurements"]),
            }
            response = post(url, data, timeout=900)
            print(current_user)
            print(response)
            return response
        except Exception as ex:
            msg = "POST to %s failed reason %s" % (url, str(ex))
            logging.getLogger("ispyb_client").exception(msg)

    # ------------------------------------------------------------------------------------------------------------------

    def create_new_sc_experiment(self, exp_name):
        """On Collect started Created a new empty Experiment"""
        pcode, pnumber = self._get_prop()
        experiment_name = exp_name
        experiment = self._generic_biosaxs_client.service.createEmptyExperiment(
            pcode, pnumber, experiment_name
        )
        self._current_experiment = json.loads(experiment)

    def store_sc_measurement(self, item, run_number):
        experiment_id = self._current_experiment.get("experimentId", None)
        _type = "SAMPLE" if "Sample" in type(item).__name__ else "BUFFER"
        name = item.name
        buffer_name = item.buffer_name if "Sample" in type(item).__name__ else item.name
        measurement = (
            self._generic_biosaxs_client.service.appendMeasurementToExperiment(
                experiment_id,
                run_number,
                _type,
                item.plate,
                ord(item.row) - 64,
                item.column,
                name,
                buffer_name,
                item.concentration,
                item.seu_temperature,
                item.viscosity,
                item.volume,
                item.volume,
                item.wait,
                item.transmission,
                item.comment,
            )
        )
        self._current_measurement = measurement
        measurementId = measurement.measurementId
        measurement_info = {
            "measurement_id": measurementId,
            "experiment_id": experiment_id,
            "run_number": run_number,
        }

        self._generic_biosaxs_client.service.addRun(
            experiment_id,
            run_number,
            item.seu_temperature,
            item.storage_temperature,
            item.exposure_time,
            "None",
            "None",
            item.energy,
            "detectorDistance",
            "snapshotCapillary",
            "current",
            "bmX",
            "bmY",
            "rR",
            "rA",
            "pxX",
            "pxY",
            "normalization",
            item.transmission,
        )

        return measurement_info

    def create_hplc_experiment(self, experiment_name):
        pcode, pnumber = self._get_prop()

        self._current_experiment = self._biosaxs_client.service.createHPLC(
            pcode, pnumber, experiment_name
        )

    def store_hplc_frames(self, run_number):
        experiment_id = self._current_experiment
        measurement = self._biosaxs_client.service.storeHPLC(
            experiment_id,
            "",
            None,
        )
        measurement_info = {
            "measurement_id": measurement,
            "experiment_id": experiment_id,
            "run_number": run_number,
        }
        self._current_measurement = measurement
        return measurement_info
