# -*- coding: utf-8 -*-
from datetime import datetime
import json
import os

from flask import g
from PIL import Image
import sqlalchemy
from sqlalchemy import orm
from sqlalchemy.sql.expression import cast, func, distinct, and_

from daiquiri.core.metadata.ispyalchemy.handler import IspyalchemyHandler


class SampleHandler(IspyalchemyHandler):
    exported = [
        "get_components",
        "add_component",
        "update_component",
        "get_samples",
        "add_sample",
        "update_sample",
        "queue_sample",
        "unqueue_sample",
        "get_subsamples",
        "add_subsample",
        "update_subsample",
        "remove_subsample",
        "queue_subsample",
        "unqueue_subsample",
        "get_sample_tags",
        "get_sampleimages",
        "add_sampleimage",
        "add_container_inspection",
    ]

    def get_components(self, componentid=None, **kwargs):
        with self.session_scope() as ses:
            components = (
                ses.query(
                    self.Protein.proteinid.label("componentid"),
                    self.Protein.name,
                    self.Protein.acronym,
                    self.Protein.description,
                    cast(self.Protein.molecularmass, sqlalchemy.Float).label(
                        "molecularmass"
                    ),
                    self.Protein.density,
                    self.Protein.sequence,
                    func.count(distinct(self.BLSample.blsampleid)).label("samples"),
                    func.count(distinct(self.DataCollection.datacollectionid)).label(
                        "datacollections"
                    ),
                )
                .outerjoin(
                    self.Crystal, self.Crystal.proteinid == self.Protein.proteinid
                )
                .outerjoin(self.BLSample)
                .outerjoin(
                    self.DataCollectionGroup,
                    self.BLSample.blsampleid == self.DataCollectionGroup.blsampleid,
                )
                .outerjoin(
                    self.DataCollection,
                    self.DataCollectionGroup.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
                .group_by(self.Protein.proteinid)
            )

            if not kwargs.get("no_context"):
                components = components.filter(
                    self.Protein.proposalid == g.blsession.get("proposalid")
                )

            if kwargs.get("sampleid"):
                components = components.filter(
                    self.BLSample.blsampleid == kwargs["sampleid"]
                )

            if componentid:
                components = components.filter(self.Protein.proteinid == componentid)
                component = components.first()
                if component:
                    return component._asdict()

            else:
                components = [r._asdict() for r in components.all()]
                return {"total": len(components), "rows": components}

    def add_component(self, **kwargs):
        with self.session_scope() as ses:
            component = self.Protein(
                proposalid=g.blsession.get("proposalid"),
                name=kwargs.get("name"),
                acronym=kwargs.get("acronym"),
                molecularmass=kwargs.get("molecularmass"),
                sequence=kwargs.get("sequence"),
                density=kwargs.get("density"),
            )

            ses.add(component)
            ses.commit()

            return self.get_components(componentid=component.proteinid)

    def update_component(self, componentid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_components(componentid=componentid)
            if chk:
                component = (
                    ses.query(self.Protein)
                    .filter(self.Protein.proteinid == componentid)
                    .first()
                )

                updatable = ["name", "acronym", "molecularmass", "density", "sequence"]
                for k, v in kwargs.items():
                    if k in updatable:
                        setattr(component, k, v)

                ses.commit()

                return self.get_components(componentid=componentid)

    def get_samples(self, sampleid=None, **kwargs):
        with self.session_scope() as ses:
            dc2 = orm.aliased(self.DataCollection)
            samples = (
                ses.query(
                    self.BLSample.blsampleid.label("sampleid"),
                    self.BLSample.name,
                    self.BLSample.comments,
                    self.BLSample.extrametadata,
                    self.Container.containerid,
                    cast(self.BLSample.location, sqlalchemy.Integer).label("location"),
                    cast(self.Position.posx, sqlalchemy.Integer).label("offsetx"),
                    cast(self.Position.posy, sqlalchemy.Integer).label("offsety"),
                    self.Protein.acronym.label("component"),
                    self.Protein.proteinid.label("componentid"),
                    func.count(distinct(self.BLSubSample.blsubsampleid)).label(
                        "subsamples"
                    ),
                    func.count(distinct(self.DataCollection.datacollectionid)).label(
                        "datacollections"
                    ),
                    func.IF(
                        func.count(self.ContainerQueueSample.containerqueuesampleid)
                        > func.count(dc2.datacollectionid),
                        True,
                        False,
                    ).label("queued"),
                )
                .join(self.Crystal, self.Crystal.crystalid == self.BLSample.crystalid)
                .join(self.Protein)
                .join(self.Proposal)
                .join(self.Container)
                .outerjoin(
                    self.Position, self.Position.positionid == self.BLSample.positionid
                )
                .outerjoin(
                    self.DataCollectionGroup,
                    self.DataCollectionGroup.blsampleid == self.BLSample.blsampleid,
                )
                .outerjoin(
                    self.DataCollection,
                    self.DataCollection.datacollectiongroupid
                    == self.DataCollectionGroup.datacollectiongroupid,
                )
                .outerjoin(
                    self.BLSubSample,
                    self.BLSubSample.blsampleid == self.BLSample.blsampleid,
                )
                .outerjoin(
                    self.ContainerQueueSample,
                    self.BLSample.blsampleid == self.ContainerQueueSample.blsampleid,
                )
                .outerjoin(
                    dc2,
                    self.ContainerQueueSample.datacollectionplanid
                    == dc2.datacollectionplanid,
                )
                .filter(
                    and_(
                        self.Container.beamlinelocation
                        == self._config["meta_beamline"],
                        self.Container.samplechangerlocation is not None,
                        self.Container.containerstatus == "processing",
                    )
                )
                .group_by(self.BLSample.blsampleid)
            )

            if not kwargs.get("no_context"):
                samples = samples.filter(
                    self.Proposal.proposalid == g.blsession.get("proposalid")
                )

            if kwargs.get("containerid"):
                samples = samples.filter(
                    self.Container.containerid == kwargs.get("containerid")
                )

            if sampleid:
                samples = samples.filter(self.BLSample.blsampleid == sampleid)
                sample = samples.first()
                if sample:
                    return sample._asdict()

            else:
                samples = [r._asdict() for r in samples.all()]
                return {"total": len(samples), "rows": samples}

    def add_sample(self, **kwargs):
        if kwargs.get("containerid") is None:
            containerid = self._ensure_default_container()
        else:
            containerid = kwargs["containerid"]

        if kwargs.get("componentid") is None:
            proteinid = self._ensure_default_component()
            chk = self.get_components(componentid=proteinid)
            if not chk:
                return

        else:
            proteinid = kwargs["componentid"]

        samples = self.get_samples(containerid=containerid)["rows"]
        max_loc = 0
        for s in samples:
            if s["location"] > max_loc:
                max_loc = s["location"]

        with self.session_scope() as ses:
            pos = self.Position(posx=kwargs["offsetx"], posy=kwargs["offsety"])
            ses.add(pos)
            ses.commit()

            crystal = self.Crystal(proteinid=proteinid)

            ses.add(crystal)
            ses.commit()

            sample = self.BLSample(
                containerid=containerid,
                crystalid=crystal.crystalid,
                location=max_loc + 1,
                positionid=pos.positionid,
                name=kwargs.get("name"),
                comments=kwargs.get("comments"),
            )

            ses.add(sample)
            ses.commit()

            return self.get_samples(sampleid=sample.blsampleid)

    def update_sample(self, sampleid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_samples(sampleid=sampleid)
            if chk:
                sample = (
                    ses.query(self.BLSample)
                    .filter(self.BLSample.blsampleid == sampleid)
                    .first()
                )

                updatable = ["name", "comments", "offsetx", "offsety", "extrametadata"]
                for k, v in kwargs.items():
                    if k in updatable:
                        setattr(sample, k, v)

                if "componentid" in kwargs:
                    crystal = (
                        ses.query(self.Crystal)
                        .filter(self.Crystal.crystalid == sample.crystalid)
                        .first()
                    )

                    crystal.proteinid = kwargs["componentid"]

                ses.commit()

                return self.get_samples(sampleid=sampleid)

    def get_subsamples(self, subsampleid=None, **kwargs):
        dc2 = orm.aliased(self.DataCollection)
        position2 = orm.aliased(self.Position)

        with self.session_scope() as ses:
            subsamples = (
                ses.query(
                    self.BLSubSample.blsubsampleid.label("subsampleid"),
                    self.BLSample.blsampleid.label("sampleid"),
                    self.BLSample.name.label("sample"),
                    self.BLSubSample.type,
                    self.BLSubSample.source,
                    self.BLSubSample.comments,
                    self.BLSubSample.extrametadata,
                    self.Container.containerid,
                    cast(self.Position.posx, sqlalchemy.Integer).label("x"),
                    cast(self.Position.posy, sqlalchemy.Integer).label("y"),
                    cast(position2.posx, sqlalchemy.Integer).label("x2"),
                    cast(position2.posy, sqlalchemy.Integer).label("y2"),
                    func.count(distinct(self.DataCollection.datacollectionid)).label(
                        "datacollections"
                    ),
                    func.IF(
                        func.count(self.ContainerQueueSample.containerqueuesampleid)
                        > func.count(dc2.datacollectionid),
                        True,
                        False,
                    ).label("queued"),
                    func.group_concat(
                        distinct(
                            func.concat(
                                self.Positioner.positioner, ":", self.Positioner.value
                            )
                        )
                    ).label("positions"),
                )
                .join(
                    self.BLSample,
                    self.BLSample.blsampleid == self.BLSubSample.blsampleid,
                )
                .join(self.Crystal)
                .join(self.Protein)
                .join(self.Proposal)
                .join(self.Container)
                .outerjoin(
                    self.Position,
                    self.Position.positionid == self.BLSubSample.positionid,
                )
                .outerjoin(
                    position2, position2.positionid == self.BLSubSample.position2id
                )
                .outerjoin(
                    self.DataCollection,
                    self.DataCollection.blsubsampleid == self.BLSubSample.blsubsampleid,
                )
                .outerjoin(
                    self.ContainerQueueSample,
                    self.BLSubSample.blsubsampleid
                    == self.ContainerQueueSample.blsubsampleid,
                )
                .outerjoin(
                    dc2,
                    self.ContainerQueueSample.datacollectionplanid
                    == dc2.datacollectionplanid,
                )
                .outerjoin(self.BLSubSample_has_Positioner)
                .outerjoin(
                    self.Positioner,
                    self.BLSubSample_has_Positioner.positionerid
                    == self.Positioner.positionerid,
                )
                .group_by(self.BLSubSample.blsubsampleid)
            )

            if not kwargs.get("no_context"):
                subsamples = subsamples.filter(
                    self.Proposal.proposalid == g.blsession.get("proposalid")
                )

            if kwargs.get("sampleid"):
                subsamples = subsamples.filter(
                    self.BLSample.blsampleid == kwargs.get("sampleid")
                )

            if subsampleid:
                subsamples = subsamples.filter(
                    self.BLSubSample.blsubsampleid == subsampleid
                )
                subsample = subsamples.first()
                if subsample:
                    subs = subsample._asdict()
                    subs["positions"] = self._pos_to_dict(subs["positions"])
                    return subs

            else:
                subs = [r._asdict() for r in subsamples.all()]

                for s in subs:
                    s["positions"] = self._pos_to_dict(s["positions"])

                return {"total": len(subs), "rows": subs}

    def _pos_to_dict(self, positions):
        ps = {}
        if positions:
            for p in positions.split(","):
                k, v = p.split(":")
                ps[k] = v

        return ps

    def add_subsample(self, **kwargs):
        with self.session_scope() as ses:
            sample = self.get_samples(sampleid=kwargs["sampleid"])
            if sample:
                position = self.Position(posx=kwargs["x"], posy=kwargs["y"])
                ses.add(position)
                ses.commit()

                position2 = None
                if kwargs.get("x2"):
                    position2 = self.Position(posx=kwargs["x2"], posy=kwargs["y2"])
                    ses.add(position2)
                    ses.commit()

                subsample = self.BLSubSample(
                    blsampleid=kwargs["sampleid"],
                    type=kwargs["type"],
                    comments=kwargs.get("comments"),
                    positionid=position.positionid,
                    position2id=position2.positionid if position2 else None,
                )

                ses.add(subsample)
                ses.commit()

                if kwargs.get("positions"):
                    for k, v in kwargs["positions"].items():
                        positioner = self.Positioner(positioner=k, value=v)
                        ses.add(positioner)
                        ses.commit()

                        blshaspos = self.BLSubSample_has_Positioner(
                            blsubsampleid=subsample.blsubsampleid,
                            positionerid=positioner.positionerid,
                        )
                        ses.add(blshaspos)
                        ses.commit()

                return self.get_subsamples(subsampleid=subsample.blsubsampleid)

    def update_subsample(self, subsampleid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_subsamples(subsampleid=subsampleid)
            if chk:
                position2 = orm.aliased(self.Position)
                subsample, position, position2 = (
                    ses.query(self.BLSubSample, self.Position, position2)
                    .outerjoin(
                        self.Position,
                        self.Position.positionid == self.BLSubSample.positionid,
                    )
                    .outerjoin(
                        position2, position2.positionid == self.BLSubSample.position2id
                    )
                    .filter(self.BLSubSample.blsubsampleid == subsampleid)
                    .first()
                )

                additional = (
                    ses.query(self.Positioner)
                    .join(self.BLSubSample_has_Positioner)
                    .filter(
                        self.BLSubSample_has_Positioner.blsubsampleid == subsampleid
                    )
                    .all()
                )

                if kwargs.get("positions"):
                    for pos in additional:
                        if pos.positioner in kwargs["positions"]:
                            pos.value = kwargs["positions"][pos.positioner]

                if subsample:
                    updatable = ["comments", "extrametadata"]
                    for k, v in kwargs.items():
                        if k in updatable:
                            setattr(subsample, k, v)

                    if kwargs.get("x"):
                        position.posx = kwargs["x"]

                    if kwargs.get("y"):
                        position.posy = kwargs["y"]

                    if position2:
                        if kwargs.get("x2"):
                            position2.posx = kwargs["x2"]

                        if kwargs.get("y2"):
                            position2.posy = kwargs["y2"]

                    ses.commit()

                    return self.get_subsamples(subsampleid=subsampleid)

    def remove_subsample(self, subsampleid):
        with self.session_scope() as ses:
            chk = self.get_subsamples(subsampleid=subsampleid)
            if chk:
                positions = (
                    ses.query(self.Positioner)
                    .join(self.BLSubSample_has_Positioner)
                    .filter(
                        self.BLSubSample_has_Positioner.blsubsampleid == subsampleid
                    )
                    .all()
                )

                bls_has_pos = (
                    ses.query(self.BLSubSample_has_Positioner)
                    .filter(
                        self.BLSubSample_has_Positioner.blsubsampleid == subsampleid
                    )
                    .all()
                )
                for blp in bls_has_pos:
                    ses.delete(blp)
                ses.commit()

                for p in positions:
                    ses.delete(p)
                ses.commit()

                position2 = orm.aliased(self.Position)
                subsample, position, position2 = (
                    ses.query(self.BLSubSample, self.Position, position2)
                    .outerjoin(
                        self.Position,
                        self.Position.positionid == self.BLSubSample.positionid,
                    )
                    .outerjoin(
                        position2, position2.positionid == self.BLSubSample.position2id
                    )
                    .filter(self.BLSubSample.blsubsampleid == subsampleid)
                    .first()
                )

                if subsample:
                    ses.delete(position)

                    if position2:
                        ses.delete(position2)

                    ses.delete(subsample)

                    ses.commit()

                    return True

    def queue_subsample(self, subsampleid, datacollectionplanid=None, **kwargs):
        with self.session_scope() as ses:
            subsample = self.get_subsamples(subsampleid=subsampleid)
            if subsample:
                if not datacollectionplanid:
                    dp = self.add_datacollectionplan(no_context=True, **kwargs)
                    datacollectionplanid = dp["datacollectionplanid"]

                cq = (
                    ses.query(self.ContainerQueue.containerqueueid)
                    .join(
                        self.BLSample,
                        self.BLSample.containerid == self.ContainerQueue.containerid,
                    )
                    .join(
                        self.BLSubSample,
                        self.BLSample.blsampleid == self.BLSubSample.blsampleid,
                    )
                    .filter(self.BLSubSample.blsubsampleid == subsampleid)
                    .first()
                )

                if not cq:
                    cq = self.ContainerQueue(
                        containerid=subsample["containerid"],
                        personid=g.user["personid"],
                    )
                    ses.add(cq)
                    ses.commit()

                cqs = self.ContainerQueueSample(
                    containerqueueid=cq.containerqueueid,
                    datacollectionplanid=datacollectionplanid,
                    blsubsampleid=subsampleid,
                )

                ses.add(cqs)
                ses.commit()

                return cqs.containerqueuesampleid, datacollectionplanid

    def unqueue_subsample(self, subsampleid, **kwargs):
        with self.session_scope() as ses:
            if not kwargs.get("containerqueuesampleid"):
                raise AttributeError("missing containerqueuesampleid")

            subsample = self.get_subsamples(subsampleid=subsampleid)
            if subsample:
                cqs, dp = (
                    ses.query(self.ContainerQueueSample, self.DiffractionPlan)
                    .filter(
                        self.ContainerQueueSample.datacollectionplanid
                        == self.DiffractionPlan.diffractionplanid
                    )
                    .filter(
                        self.ContainerQueueSample.containerqueuesampleid
                        == kwargs["containerqueuesampleid"]
                    )
                    .first()
                )

                if cqs:
                    ses.delete(cqs)
                    ses.commit()

                    if dp:
                        ses.delete(dp)
                        ses.commit()

                    return True

    def queue_sample(self, sampleid, datacollectionplanid=None, **kwargs):
        with self.session_scope() as ses:
            sample = self.get_samples(sampleid=sampleid)
            if sample:
                if not datacollectionplanid:
                    dp = self.add_datacollectionplan(no_context=True, **kwargs)
                    datacollectionplanid = dp["datacollectionplanid"]

                cq = (
                    ses.query(self.ContainerQueue.containerqueueid)
                    .join(
                        self.BLSample,
                        self.BLSample.containerid == self.ContainerQueue.containerid,
                    )
                    .filter(self.BLSample.blsampleid == sampleid)
                    .first()
                )

                if not cq:
                    cq = self.ContainerQueue(
                        containerid=sample["containerid"],
                        personid=g.user["personid"],
                    )
                    ses.add(cq)
                    ses.commit()

                cqs = self.ContainerQueueSample(
                    containerqueueid=cq.containerqueueid,
                    datacollectionplanid=datacollectionplanid,
                    blsampleid=sampleid,
                )

                ses.add(cqs)
                ses.commit()

                return cqs.containerqueuesampleid, datacollectionplanid

    def unqueue_sample(self, sampleid, **kwargs):
        with self.session_scope() as ses:
            if not kwargs.get("containerqueuesampleid"):
                raise AttributeError("missing containerqueuesampleid")

            sample = self.get_samples(
                sampleid=sampleid, no_context=kwargs.get("no_context")
            )
            if sample:
                cqs, dp = (
                    ses.query(self.ContainerQueueSample, self.DiffractionPlan)
                    .filter(
                        self.ContainerQueueSample.datacollectionplanid
                        == self.DiffractionPlan.diffractionplanid
                    )
                    .filter(
                        self.ContainerQueueSample.containerqueuesampleid
                        == kwargs["containerqueuesampleid"]
                    )
                    .first()
                )

                if cqs:
                    ses.delete(cqs)
                    ses.commit()

                    if dp:
                        sample_has_datacollectionplan = (
                            ses.query(self.BLSample_has_DataCollectionPlan)
                            .filter(
                                self.BLSample_has_DataCollectionPlan.datacollectionplanid
                                == dp.diffractionplanid,
                            )
                            .first()
                        )

                        # If this plan is associated to a sample it was created using the plans routes
                        # rather than automatically, unqueuing should preserve the plan
                        if not sample_has_datacollectionplan:
                            ses.delete(dp)
                            ses.commit()

                    return True

    def get_sample_tags(self, type: str = None):
        with self.session_scope() as ses:
            sample_tags = []
            print("sample tags", type)
            if (type and type.value == "sample") or type is None:
                sample_tags = (
                    ses.query(
                        func.json_extract(self.BLSample.extrametadata, "$.tags").label(
                            "tags"
                        )
                    )
                    .join(self.Crystal)
                    .join(self.Protein)
                    .join(self.Proposal)
                    .filter(self.Proposal.proposalid == g.blsession.get("proposalid"))
                    .group_by(self.BLSample.blsampleid)
                )
                sample_tags = [r._asdict() for r in sample_tags.all()]

            subsample_tags = []
            if (type and type.value == "subsample") or type is None:
                subsample_tags = (
                    ses.query(
                        func.json_extract(
                            self.BLSubSample.extrametadata, "$.tags"
                        ).label("tags")
                    )
                    .join(
                        self.BLSample,
                        self.BLSample.blsampleid == self.BLSubSample.blsampleid,
                    )
                    .join(self.Crystal)
                    .join(self.Protein)
                    .join(self.Proposal)
                    .filter(self.Proposal.proposalid == g.blsession.get("proposalid"))
                    .group_by(self.BLSubSample.blsubsampleid)
                )

                subsample_tags = [r._asdict() for r in subsample_tags.all()]

            tag_list = []
            for tags in [sample_tags, subsample_tags]:
                for tag in tags:
                    if tag["tags"]:
                        tag_list.extend(json.loads(tag["tags"]))

            tag_list.sort()
            return {"tags": list(set(tag_list))}

    def get_sampleimages(self, sampleimageid=None, **kwargs):
        with self.session_scope() as ses:
            sampleimages = (
                ses.query(
                    self.BLSampleImage.blsampleimageid.label("sampleimageid"),
                    self.BLSampleImage.imagefullpath.label("file"),
                    (self.BLSampleImage.micronsperpixelx * 1000).label("scalex"),
                    (self.BLSampleImage.micronsperpixely * 1000).label("scaley"),
                    self.BLSampleImage.offsetx,
                    (self.BLSampleImage.offsety * -1).label("offsety"),
                    func.concat(
                        f"/{self._base_url}/samples/images/",
                        self.BLSampleImage.blsampleimageid,
                    ).label("url"),
                    func.group_concat(
                        distinct(
                            func.concat(
                                self.Positioner.positioner, ":", self.Positioner.value
                            )
                        )
                    ).label("positions"),
                )
                .join(self.BLSample)
                .join(self.Container)
                .join(self.Crystal)
                .join(self.Protein)
                .outerjoin(self.BLSampleImage_has_Positioner)
                .outerjoin(
                    self.Positioner,
                    self.BLSampleImage_has_Positioner.positionerid
                    == self.Positioner.positionerid,
                )
                .group_by(self.BLSampleImage.blsampleimageid)
            )

            if not kwargs.get("no_context"):
                sampleimages = sampleimages.filter(
                    self.Protein.proposalid == g.blsession.get("proposalid")
                )

            if kwargs.get("containerid"):
                sampleimages = sampleimages.filter(
                    self.Container.containerid == kwargs.get("containerid")
                )

            if kwargs.get("sampleid"):
                sampleimages = sampleimages.filter(
                    self.BLSampleImage.blsampleid == kwargs.get("sampleid")
                )

            if sampleimageid:
                sampleimages = sampleimages.filter(
                    self.BLSampleImage.blsampleimageid == sampleimageid
                )
                sampleimage = sampleimages.first()
                if sampleimage:
                    si = sampleimage._asdict()
                    si["positions"] = self._pos_to_dict(si["positions"])
                    return si

            else:
                sampleimages = [r._asdict() for r in sampleimages.all()]
                for s in sampleimages:
                    s["positions"] = self._pos_to_dict(s["positions"])
                    if os.path.exists(s["file"]):
                        image = Image.open(s["file"])
                        s["width"], s["height"] = image.size

                return {"total": len(sampleimages), "rows": sampleimages}

    def add_sampleimage(self, **kwargs):
        with self.session_scope() as ses:
            sample = self.get_samples(
                sampleid=kwargs["sampleid"], no_context=kwargs.get("no_context")
            )
            if sample:
                if "containerinspectionid" not in kwargs:
                    containerinspectionid = self._ensure_default_inspection(
                        sample["containerid"]
                    )
                else:
                    containerinspectionid = kwargs["containerinspectionid"]

                sampleimage = self.BLSampleImage(
                    blsampleid=kwargs.get("sampleid"),
                    bltimestamp=datetime.now(),
                    imagefullpath=kwargs.get("file"),
                    micronsperpixelx=kwargs.get("scalex") / 1000,
                    micronsperpixely=kwargs.get("scaley") / 1000,
                    offsetx=kwargs.get("offsetx"),
                    offsety=-1 * kwargs.get("offsety"),
                    containerinspectionid=containerinspectionid,
                )

                ses.add(sampleimage)
                ses.commit()

                if kwargs.get("positions"):
                    for k, v in kwargs["positions"].items():
                        positioner = self.Positioner(positioner=k, value=v)
                        ses.add(positioner)
                        ses.commit()

                        blshasimagepos = self.BLSampleImage_has_Positioner(
                            blsampleimageid=sampleimage.blsampleimageid,
                            positionerid=positioner.positionerid,
                            value=v,
                        )
                        ses.add(blshasimagepos)
                        ses.commit()

                return self.get_sampleimages(
                    sampleimageid=sampleimage.blsampleimageid,
                    no_context=kwargs.get("no_context"),
                )

    def add_container_inspection(self, containerid):
        with self.session_scope() as ses:
            containerinspection = self.ContainerInspection(
                containerid=containerid,
                inspectiontypeid=1,
                manual=1,
                scheduledtimestamp=datetime.now(),
                completedtimestamp=datetime.now(),
            )

            ses.add(containerinspection)
            ses.commit()

            return containerinspection.containerinspectionid

    def _ensure_default_inspection(self, containerid):
        with self.session_scope() as ses:
            containerinspection = (
                ses.query(self.ContainerInspection.containerinspectionid)
                .filter(self.ContainerInspection.containerid == containerid)
                .first()
            )

            if not containerinspection:
                return self.add_container_inspection(containerid)

            return containerinspection.containerinspectionid

    def _ensure_default_container(self):
        with self.session_scope() as ses:
            shipment_name = g.blsession.get("session") + "_Shipment1"
            shipment = (
                ses.query(self.Shipping.shippingid)
                .filter(self.Shipping.proposalid == g.blsession.get("proposalid"))
                .filter(self.Shipping.shippingname == shipment_name)
                .first()
            )

            if not shipment:
                shipment = self.Shipping(
                    shippingname=shipment_name,
                    proposalid=g.blsession.get("proposalid"),
                    creationdate=datetime.now(),
                )

                ses.add(shipment)
                ses.commit()

            dewar_name = g.blsession.get("session") + "_Dewar1"
            dewar = (
                ses.query(self.Dewar.dewarid)
                .filter(self.Dewar.shippingid == shipment.shippingid)
                .filter(self.Dewar.code == dewar_name)
                .first()
            )

            if not dewar:
                dewar = self.Dewar(
                    shippingid=shipment.shippingid,
                    code=dewar_name,
                    dewarstatus="processing",
                )
                ses.add(dewar)
                ses.commit()

            container_name = g.blsession.get("session") + "_Container1"
            container = (
                ses.query(self.Container.containerid)
                .filter(self.Container.dewarid == dewar.dewarid)
                .filter(self.Container.code == container_name)
                .first()
            )

            if not container:
                container = self.Container(
                    dewarid=dewar.dewarid,
                    code=container_name,
                    containertype="Box",
                    capacity=self._config.get("meta_capacity", 25),
                    beamlinelocation=self._config["meta_beamline"],
                    samplechangerlocation=1,
                    bltimestamp=datetime.now(),
                    containerstatus="processing",
                )
                ses.add(container)
                ses.commit()

                containerhistory = self.ContainerHistory(
                    containerid=container.containerid,
                    status="processing",
                    location=1,
                    beamlinename=self._config["meta_beamline"],
                )

                ses.add(containerhistory)
                ses.commit()

            return container.containerid

    def _ensure_default_component(self):
        with self.session_scope() as ses:
            component_name = g.blsession.get("session") + "_Component1"
            component_acronym = g.blsession.get("session") + "_c1"

            component = (
                ses.query(self.Protein.proteinid)
                .filter(self.Protein.proposalid == g.blsession.get("proposalid"))
                .filter(self.Protein.name == component_name)
                .first()
            )

            if not component:
                component = self.Protein(
                    name=component_name,
                    acronym=component_acronym,
                    proposalid=g.blsession.get("proposalid"),
                )

                ses.add(component)
                ses.commit()

            return component.proteinid
