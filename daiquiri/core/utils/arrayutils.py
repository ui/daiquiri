import numpy
from typing import Tuple, Union


def parse_slice(slice_string) -> Tuple[Union[slice, int], ...]:
    """Parse a slicing sequence and return an associated tuple.

    It supports a sequence of `...`, `:`, and integers separated by a coma.
    """

    def str_to_slice(string):
        if string == "...":
            return Ellipsis
        elif ":" in string:
            if string == ":":
                return slice(None)
            else:

                def get_value(my_str):
                    if my_str in ("", None):
                        return None
                    else:
                        return int(my_str)

                sss = string.split(":")
                start = get_value(sss[0])
                stop = get_value(sss[1] if len(sss) > 1 else None)
                step = get_value(sss[2] if len(sss) > 2 else None)
                return slice(start, stop, step)
        else:
            return int(string)

    if slice_string == "":
        raise ValueError("An empty slice is not valid")

    tokens = slice_string.split(",")
    data_slice = []
    for t in tokens:
        try:
            data_slice.append(str_to_slice(t))
        except ValueError:
            raise ValueError("'%s' is not a valid slicing" % t)
    return tuple(data_slice)


def to_safe_js_dtype(dtype: numpy.dtype) -> numpy.dtype:
    """Convert dtype to a dtype supported by js-numpy-parser.
    See https://github.com/ludwigschubert/js-numpy-parser

    raises:
        ValueError: For unsupported array dtype
    """
    if dtype.kind not in ("f", "i", "u"):
        raise ValueError("Unsupported array type")

    # Convert to little endian
    result = dtype.newbyteorder("<")

    if result.kind == "i" and result.itemsize > 4:
        return numpy.dtype("<i4")  # int64 -> int32

    if result.kind == "u" and result.itemsize > 4:
        return numpy.dtype("<u4")  # uint64 -> uint32

    if result.kind == "f":
        if result.itemsize < 4:
            return numpy.dtype("<f4")
        if result.itemsize > 8:
            return numpy.dtype("<f8")

    return result
