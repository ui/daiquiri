"""
Helper to deal with image data
"""

from silx.math import colormap


def array_to_image(array, autoscale="none", norm="linear", lut="gray"):
    """Convert an array into a displayable array (uint8, RGBA)

    Arguments:
        array: An intensity numpy array
        norm: One of 'linear', 'log', 'arcsinh', 'sqrt'
        lut: One of 'gray', 'gray_r', 'viridis', 'cividis'...
        autoscale: One of 'none', 'minmax', 'stddev3'
    """
    if autoscale == "none":
        vmin, vmax = 0, 255
    else:
        vmin, vmax = None, None
    return colormap.apply_colormap(
        array, colormap=lut, norm=norm, autoscale=autoscale, vmin=vmin, vmax=vmax
    )
