from typing import Optional, List
from pydantic import BaseModel, Field


def _dir_list(string) -> List[str]:
    """Returns a list of directories from a comma separater list"""
    folders = string.split(",")
    folders = [s.strip() for s in folders]
    return folders


class ServerOptions(BaseModel):
    """Options used to setup the Daiquiri server"""

    resource_folders: Optional[List[str]] = Field(
        default=[],
        description="Server resources directories (first has priority)",
        json_schema_extra=dict(argparse_type=_dir_list),
    )

    static_folder: str = Field(
        default="static.default",
        description="Actor implementors module",
    )

    static_resources_folder: Optional[str] = Field(
        default=None,
        description="Extra folder to override `static_folder/resources`",
        json_schema_extra=dict(argparse_type=str),
    )

    hardware_folder: str = Field(
        default="",
        description="Hardware folder",
    )

    port: int = Field(
        default=8080,
        description="Web server port",
        json_schema_extra=dict(argparse_flag="-p"),
    )

    save_spec_file: Optional[str] = Field(
        default=None,
        description="If defined, save the current API spec into the specified file, and exit",
        json_schema_extra=dict(
            argparse_name="--save-spec",
            argparse_nargs="?",
            argparse_flag="-s",
            argparse_type=str,
        ),
    )

    implementors: Optional[str] = Field(
        default=None,
        description="Actor implementors module",
        json_schema_extra=dict(argparse_type=str),
    )

    ssl: Optional[bool] = Field(
        default=None,
        description="Enable the SSL protocol",
        json_schema_extra=dict(argparse_type=bool),
    )

    ssl_cert: Optional[str] = Field(
        default=None,
        description="Location of the SSL certificat",
        json_schema_extra=dict(argparse_type=str),
    )

    ssl_key: Optional[str] = Field(
        default=None,
        description="Location of the SSL key",
        json_schema_extra=dict(argparse_type=str),
    )
