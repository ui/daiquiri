#!/usr/bin/env python
# -*- coding: utf-8 -*-
from io import StringIO
from abc import ABC, abstractmethod

import ruamel.yaml


class PrettyException(Exception, ABC):
    @abstractmethod
    def pretty(self):
        pass


class SyntaxErrorYAML(PrettyException):
    """YAML Syntax Error

    Args:
        error (obj): The YAMLError instance

    """

    def pretty(self):
        arg = self.args[0]
        message = "Invalid YAML Syntax\n"
        for line in str(arg["error"]).split("\n"):
            message += f"  {line}\n"
        return message


def indent(text, spaces=2):
    return "\n".join([f"{' ' * spaces}{line}" for line in text.split("\n")])


class InvalidYAML(PrettyException):
    """InvalidYAML Exception

    Args:
        message (str): Error message
        file (str): The yaml file name
        obj (dict): Parsed yaml dict
        errors (dict, optional): Error dict
    """

    def dump(self, obj):
        """Long discussion about dumping to stream using ruamel:
        https://stackoverflow.com/questions/47614862/best-way-to-use-ruamel-yaml-to-dump-yaml-to-string-not-to-stream
        """
        yaml = ruamel.yaml.YAML()
        string_stream = StringIO()
        yaml.dump(obj, string_stream)
        output_str = string_stream.getvalue()
        string_stream.close()
        return output_str

    def pretty(self):
        arg = self.args[0]
        message = f"Invalid YAML Definition in {arg['file']}\n"
        message += f"  {arg['message']}\n"
        message += indent(self.dump(dict(arg["obj"])), 4)

        if "errors" in arg:
            message += "  Errors were:"
            for k, v in arg["errors"].items():
                message += f"    {k}:\n"
                if isinstance(v, list):
                    message += indent(self.dump(v), 6)
                else:
                    message += indent(self.dump(dict(v)), 6)

        return message
