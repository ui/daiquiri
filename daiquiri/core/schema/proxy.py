#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.component import ComponentSchema
from daiquiri.core.schema.validators import OneOf


METHODS = "get", "post", "patch", "delete"


class RouteSchema(Schema):
    name = fields.Str(required=True)
    methods = fields.List(OneOf(METHODS))
    headers = fields.Dict()


class ProxySchema(Schema):
    name = fields.Str(required=True)
    openapi = fields.Str(required=True)
    target = fields.Str(required=True)
    routes = fields.Nested(RouteSchema, many=True)


class ProxyConfigSchema(ComponentSchema):
    proxies = fields.Nested(ProxySchema, many=True)
