#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.component import ComponentSchema
from daiquiri.core.schema.validators import OneOf


class TaskSchema(Schema):
    status = OneOf(["reserved", "pending", "running"])
    name = fields.Str()
    id = fields.Str()
    time_start: fields.Float()
    args = fields.List(fields.Raw())
    kwargs = fields.Dict()


class WorkersSchema(Schema):
    host = fields.Str()
    stats = fields.Dict()
    tasks = fields.Nested(TaskSchema, many=True)


class TaskConfigSchema(Schema):
    actor = fields.Str(required=True)
    description = fields.Str()


class ExecutedTasksSchema(Schema):
    job_id = fields.Str()
    name = fields.Str()
    received = fields.Int()
    started = fields.Int()
    finished = fields.Int()
    status = fields.Str()
    args = fields.Str()
    kwargs = fields.Str()
    result = fields.Field()
    uris = fields.Dict()
    exception = fields.Str()
    traceback = fields.Str()


class CeleryConfigSchema(ComponentSchema):
    ewoks_config = fields.Str()
    broker_url = fields.Str()
    backend_url = fields.Str()
    broker_dlq = fields.Bool()
    broker_queue = fields.Str()
    beamline_queue = fields.Str()
    monitor = fields.Bool()
    ewoks_events_backend_url = fields.Str()
    tasks = fields.Nested(TaskConfigSchema, many=True)
