#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields, validates_schema, ValidationError, post_load

from daiquiri.core.schema.validators import ValidatedRegexp, Any

import logging

logger = logging.getLogger(__name__)


class HardwareObjectBaseSchema(Schema):
    """Hardware as exposed by the server to the web client"""

    id = fields.Str()
    """Unique identifier for this object"""

    protocol = fields.Str()
    """Protocol, like 'bliss', 'tango'..."""

    type = fields.Str()
    """Normalized daiquiri hardware type"""

    online = fields.Bool()
    """True if the hardware is online"""

    errors = fields.List(fields.Dict())
    """A list of errors collected during the execution of the hardware"""

    name = fields.Str()
    """A fullname which identify the hardware for users"""

    alias = fields.Str()
    """A short name which can be used to identify the hardware for users"""

    require_staff = fields.Bool()
    """True if any action on the hardware requires staff privilege"""

    callables = fields.List(fields.Str())
    """Available commands provided by the hardware"""

    properties = fields.Dict()
    """Exposed state of the hardware"""

    user_tags = fields.List(fields.Str())
    """List of user tags in which the hardware is categorized"""

    locked = fields.Str(allow_none=True)
    """If not None, the device is locked for this reason, and no actions can be requested."""


class SetObjectProperty(Schema):
    property = ValidatedRegexp("word", required=True)
    value = Any(required=True, allow_none=True)


class CallObjectFunction(Schema):
    function = ValidatedRegexp("word", required=True)
    value = Any()


class HardwareGroupSchema(Schema):
    groupid = fields.Str(required=True)
    name = fields.Str(required=True)
    description = fields.Str()
    objects = fields.List(fields.Str(), required=True)
    state = fields.Bool()


class HardwareTypeSchema(Schema):
    type = fields.Str()
    schema = fields.Str()


class HOConfigAttribute(Schema):
    """The Hardware Object Config Attribute Schema"""

    id = fields.Str(required=True, metadata={"description": "Attribute id"})
    name = fields.Str(metadata={"description": "Attribute name (can be customised)"})
    ui_schema = fields.Dict(
        metadata={"description": "Define how the UI renders this attribute"}
    )
    type = fields.Str(
        metadata={
            "enum": ["float", "int", "bool", "str"],
            "description": "Attribute type",
        }
    )
    step = fields.Float(metadata={"description": "Step size for attribute"})
    min = fields.Float(metadata={"description": "Minimum value for attribute"})
    max = fields.Float(metadata={"description": "Maximum value for attribute"})


class HOConfigSchema(Schema):
    """HardwareObject base configuration schema"""

    name = fields.Str(metadata={"description": "Object name"})
    id = fields.Str(metadata={"description": "Object id"})
    auto_id = fields.Bool(
        metadata={"description": "Whether the id was automatically generated"}
    )
    protocol = fields.Str(metadata={"description": "Protocol handler to use"})
    url = fields.Str(
        metadata={"description": "Url of the device including which protocol to use"}
    )
    require_staff = fields.Bool(
        metadata={"description": "Whether this object requires staff to modify"}
    )
    attributes = fields.Nested(
        HOConfigAttribute,
        many=True,
        metadata={"description": "Attribute configuration for run time schemas"},
    )

    @validates_schema
    def schema_validate(self, data, **kwargs):
        if not (data.get("protocol") or data.get("url")):
            raise ValidationError(
                "Object must have either a `protocol` or `url` defined"
            )

    @post_load
    def populate(self, data, **kwargs):
        if data.get("url"):
            protocol, rest = data["url"].split("://")
            data["protocol"] = protocol

            if not data.get("id"):
                parts = rest.split("/")
                data["id"] = parts[-1]
                data["auto_id"] = True

        if not data.get("name"):
            data["name"] = data.get("id")

        return data


class HardwareSchema(Schema):
    def read_only(self, prop):
        return self.fields[prop].metadata.get("readOnly", False)

    def __contains__(self, val):
        return val in self.fields

    def __iter__(self):
        self.__iter = iter(self.fields.keys())
        return self.__iter

    def __next__(self):
        return next(self.__iter)

    def validate(self, prop, value):
        data = {}
        data[prop] = value
        valid = self.load(data)

        return valid[prop]
