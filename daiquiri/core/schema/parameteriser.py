#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.component import ComponentSchema


# Config Schema
class ParameterSchema(Schema):
    actor = fields.Str(required=True)
    require_staff = fields.Bool()


class ParameteriserConfigSchema(ComponentSchema):
    root = fields.Str(metadata={"title": "Root directory"})
    parametertypes = fields.Dict(
        keys=fields.Str(required=True), values=fields.Nested(ParameterSchema, many=True)
    )


# Endpoint Schema
class AvailableParametersSchema(Schema):
    parameter_type = fields.Str(metadata={"title": "Parameter type"})
    instance_type = fields.Str(metadata={"title": "Instance class"})
    name = fields.Str(metadata={"title": "Instance name"})
    file_name = fields.Str(metadata={"title": "File name"})
    directory = fields.Str(metadata={"title": "File directory"})
    parameters = fields.Dict()
