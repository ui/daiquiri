#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import Schema, fields

logger = logging.getLogger(__name__)


class SourceMarking(Schema):
    markingid = fields.Int()
    sourceid = fields.Int()
    scale = fields.Dict()
    name = fields.Str()
    sizex = fields.Float()
    sizey = fields.Float()
    origin = fields.Bool()


class UpdateSourceMarking(Schema):
    scalelabel = fields.Str()
    x = fields.Int()
    y = fields.Int()


class ImageSourceConfig(Schema):
    fine_fixed = fields.Bool()
    allow_fixed_axes = fields.Bool()


class SourceSettings(Schema):
    has_fine = fields.Bool(
        metadata={"description": "Whether this origin has fine axes"}
    )
    fine_fixed = fields.Bool(
        metadata={"description": "Whether the fine axis is fixed during movement"}
    )
    coarse_fixed = fields.Bool(
        metadata={"description": "Whether the coarse axis is fixed during movement"}
    )
    config = fields.Nested(ImageSourceConfig)


class AdditionalSourceUrl(Schema):
    name = fields.String()
    url = fields.String()
    scale = fields.Float()


class ImageSource(Schema):
    sourceid = fields.Int()

    url = fields.String(required=True)
    additional_urls = fields.List(fields.Nested(AdditionalSourceUrl))
    name = fields.String(required=True)
    type = fields.String(required=True)
    origin = fields.Bool()
    scale = fields.Float()

    additional = fields.Dict(keys=fields.Str(), values=fields.Float())

    center = fields.List(fields.Float(), metadata={"length": 2})
    markings = fields.Dict()
    reference = fields.Dict()
    pixelsize = fields.List(fields.Float(), metadata={"length": 2})
    polylines = fields.Dict(
        keys=fields.Str(),
        values=fields.List(fields.List(fields.Float(), metadata={"length": 2})),
    )


class MoveToCoords(Schema):
    x = fields.Int()
    y = fields.Int()


class MapAdditionalSchema(Schema):
    datacollectionid = fields.Int(required=True)
    scalars = fields.List(fields.Str(), required=True, metadata={"minItems": 1})


class MapSettings(Schema):
    during_scan = fields.Bool(
        metadata={"description": "Whether to generate maps during a scan"}
    )
    scalar_maps = fields.List(
        fields.Str(),
        metadata={"description": "List of scalars to automatically create maps from"},
    )


class MoveToReferenceSchema(Schema):
    moveid = fields.Str()
    positions = fields.Dict()


class SelectMatrixSchema(Schema):
    matrixid = fields.Str()


class ExportCropRegionSchema(Schema):
    x = fields.List(fields.Int)
    y = fields.List(fields.Int)


class ExportReferenceSchema(Schema):
    sampleactionid = fields.Int(required=True, metadata={"title": "Sample Action"})
    crop = fields.Nested(
        ExportCropRegionSchema, metadata={"title": "Region to crop reference image to"}
    )
