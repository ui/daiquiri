#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

import logging

logger = logging.getLogger(__name__)


class TomoAcquisitionSchema(Schema):
    scanid = fields.Int()
    frame_no = fields.Int()


class TomoDetectorsSchema(Schema):
    detector_id = fields.Str()
    tomo_detector_id = fields.Str()
    node_name = fields.Str()
    dark = fields.Int(required=False)
    flat = fields.Int(required=False)
    data = fields.Nested(TomoAcquisitionSchema, required=False)
