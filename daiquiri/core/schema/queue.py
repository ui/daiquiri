#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields
from daiquiri.core.schema.validators import Any

import logging

logger = logging.getLogger(__name__)


class QueueItemSchema(Schema):
    args = fields.Dict(metadata={"description": "Arguments passed to the queue item"})
    cls = fields.Str(
        metadata={"description": "The class of which this is an instance of"}
    )
    desc = fields.Str(metadata={"description": "Description of the item"})
    name = fields.Str(metadata={"description": "Name of the item"})
    running = fields.Bool(metadata={"description": "Whether the queue item is running"})
    uid = fields.Str(metadata={"description": "The unique identifier"})
    created = fields.Float(metadata={"description": "Time added to queue"})
    started = fields.Float(metadata={"description": "Time started"})
    finished = fields.Float(metadata={"description": "Time finished"})
    estimate = fields.Float(metadata={"description": "Estimated time required"})
    status = fields.Str()
    stdout = fields.Str()


class QueueStatusSchema(Schema):
    running = fields.Bool(metadata={"description": "Whether the queue is running"})
    ready = fields.Bool(
        metadata={"description": "Whether the queue is ready to process"}
    )
    stack = fields.Nested(
        QueueItemSchema,
        many=True,
        metadata={"description": "A list of queue items in the stack"},
    )
    current = fields.Nested(
        QueueItemSchema, metadata={"description": "The currently running queue item"}
    )
    all = fields.Nested(
        QueueItemSchema,
        many=True,
        metadata={"description": "A list of all queue items"},
    )
    pause_on_fail = fields.Bool(
        metadata={"description": "Whether to pause the queue if an actor fails"}
    )


class ChangeQueueStatusSchema(Schema):
    state = fields.Bool(required=True, metadata={"description": "The new queue state"})


class ChangeQueueSettingSchema(Schema):
    setting = fields.Str(
        required=True, metadata={"description": "The new queue setting"}
    )
    value = Any(required=True, metadata={"description": "The queue setting value"})


class MoveQueueItemSchema(Schema):
    uid = fields.Str(metadata={"description": "The queue item uuid"})
    position = fields.Int(metadata={"description": "The new position"})


class ClearQueueItemsSchema(Schema):
    finished = fields.Bool(metadata={"description": "Clear the finished queue items"})


class KillQueueSchema(Schema):
    stop = fields.Bool(
        metadata={"description": "Whether to Stop the queue if an actor is killed"}
    )
