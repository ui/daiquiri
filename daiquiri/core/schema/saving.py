#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields


class SavingSchema(Schema):
    arguments = fields.Dict(
        keys=fields.Str,
        values=fields.Str,
        required=True,
        metadata={"title": "Arguments"},
    )

    class Meta:
        uiorder = ["arguments"]
