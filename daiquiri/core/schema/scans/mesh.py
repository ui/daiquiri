from typing import List

from decimal import Decimal, getcontext, ROUND_DOWN
from marshmallow import fields, validate, validates_schema, ValidationError

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import ComponentActorSchema


def set_decimal_precision(precision: int = 8):
    getcontext().prec = precision


def detectors_factory(detectors: List[str], defaults: List[str] = None):
    return fields.List(
        OneOf(detectors),
        metadata={
            "uniqueItems": True,
            "minItems": 1,
            "title": "Detectors",
        },
        required=True,
        dump_default=defaults,
    )


class MeshSchema(ComponentActorSchema):
    subsampleid = fields.Int(required=True)
    steps_x = fields.Str(metadata={"title": "Horizontally", "readOnly": True})
    steps_x = fields.Str(metadata={"title": "Horizontally", "readOnly": True})
    step_size_x = fields.Float(
        required=True,
        metadata={"title": "Size Horiz", "unit": "um"},
        validate=validate.Range(min=0.1),
    )
    steps_y = fields.Str(metadata={"title": "Vertically", "readOnly": True})
    step_size_y = fields.Float(
        required=True,
        metadata={"title": "Size Vert", "unit": "um"},
        validate=validate.Range(min=0.1),
    )
    dwell = fields.Float(
        required=True,
        metadata={
            "title": "Dwell time",
            "unit": "s",
        },
        validate=validate.Range(min=0.001, max=60),
    )
    enqueue = fields.Bool(metadata={"title": "Queue Scan"}, dump_default=True)
    estimate = fields.Float(dump_default=0)
    continuous = fields.Bool(metadata={"title": "Continuous Scan"}, dump_default=True)
    fast_axis = OneOf(
        ["Horizontal", "Vertical"],
        metadata={"title": "Fast Axis"},
        dump_default="Horizontal",
    )

    def _steps(self, start, end, step_size, base_unit=1e-9, places=5):
        steps = abs(
            (Decimal(end) - Decimal(start))
            * Decimal(base_unit)
            / Decimal(step_size)
            / Decimal(1e-6)
        )
        # Places must be less than `getcontext().prec`` above
        # https://stackoverflow.com/questions/65240853/python-decimal-decimal-invalidoperation
        return steps.quantize(Decimal(10) ** -places, rounding=ROUND_DOWN)

    def calc_steps_size(self, axes, init_step_size_x: float, init_step_size_y: float):
        steps_x = int(
            round(
                self._steps(
                    axes["x"]["destination"][0],
                    axes["x"]["destination"][1],
                    init_step_size_x,
                    base_unit=axes["x"]["unit_exponent"],
                )
            )
        )
        steps_y = int(
            round(
                self._steps(
                    axes["y"]["destination"][0],
                    axes["y"]["destination"][1],
                    init_step_size_y,
                    base_unit=axes["y"]["unit_exponent"],
                )
            )
        )

        step_size_x = (
            axes["x"]["destination"][1] - axes["x"]["destination"][0]
        ) / steps_x
        step_size_y = (
            axes["y"]["destination"][1] - axes["y"]["destination"][0]
        ) / steps_y

        return steps_x, steps_y, step_size_x, step_size_y

    @validates_schema
    def schema_validate(self, data, **kwargs):
        intervals = [["step_size_x", "x", "x2"], ["step_size_y", "y", "y2"]]

        for keys in intervals:
            if data.get("objects"):
                for obj in data.get("objects"):
                    steps = self._steps(obj[keys[1]], obj[keys[2]], data[keys[0]])
                    if not steps == steps.to_integral_value():
                        raise ValidationError(
                            f"{keys[0]} must be an integer value: {steps}"
                        )

    def calculated(self, data, **kwargs):
        calculated = {}

        intervals = {
            "steps_x": ["step_size_x", "x", "x2"],
            "steps_y": ["step_size_y", "y", "y2"],
        }

        for iv, keys in intervals.items():
            if data.get(keys[0]):
                steps = []
                if data.get("objects"):
                    for obj in data["objects"]:
                        step = self._steps(obj[keys[1]], obj[keys[2]], data[keys[0]])
                        step = (
                            step.to_integral_value()
                            if step.to_integral_value() == step
                            else round(step, 2)
                        )

                        steps.append(step)

                calculated[iv] = ", ".join(map(str, steps))

        calculated["estimate"] = self.time_estimate(data)
        return calculated

    def time_estimate(self, data):
        fudge = 1.5
        if data.get("step_size_x") and data.get("step_size_y"):
            if data.get("objects"):
                for obj in data["objects"]:
                    steps_x = (obj["x2"] - obj["x"]) * 1e-9 / data["step_size_x"] / 1e-6
                    steps_y = (obj["y2"] - obj["y"]) * 1e-9 / data["step_size_y"] / 1e-6
                    return data["dwell"] * steps_x * steps_y * fudge

    class Meta:
        uiorder = [
            "subsampleid",
            "dwell",
            "step_size_x",
            "step_size_y",
            "steps_x",
            "steps_y",
            "continuous",
            "fast_axis",
            "enqueue",
            "estimate",
        ]
        uischema = {
            "subsampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "estimate": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }
        uigroups = [
            "subsampleid",
            "dwell",
            {"Steps": ["step_size_x", "step_size_y", "steps_x", "steps_y"]},
            "continuous",
            "fast_axis",
            "enqueue",
            "estimate",
        ]


class MeshPatchSchema(MeshSchema):
    _FINE_RANGE_X = 100
    _FINE_RANGE_Y = 100

    patches_x = fields.Int(metadata={"title": "Horizontally"}, required=True)
    patches_y = fields.Int(metadata={"title": "Vertically"}, required=True)
    patch_size_x = fields.Str(
        metadata={"title": "Size Horiz", "readOnly": True, "unit": "um"}
    )
    patch_size_y = fields.Str(
        metadata={"title": "Size Vert", "readOnly": True, "unit": "um"}
    )

    @validates_schema
    def schema_validate(self, data, **kwargs):
        super().schema_validate(data, **kwargs)
        intervals = [["step_size_x", "x", "x2"], ["step_size_y", "y", "y2"]]

        if data.get("objects"):
            for obj in data.get("objects"):
                size_x = (obj["x2"] - obj["x"]) * 1e-9 / 1e-6
                size_y = (obj["y2"] - obj["y"]) * 1e-9 / 1e-6

                if size_x <= self._FINE_RANGE_X and size_y <= self._FINE_RANGE_Y:
                    raise ValidationError(
                        f"Object {obj['subsampleid']} is smaller than fine range, use roi scan instead"
                    )

        for keys in intervals:
            if data.get("objects"):
                for obj in data.get("objects"):
                    patch_size = (obj[keys[2]] - obj[keys[1]]) / data[
                        f"patches_{keys[1]}"
                    ]

                    patch_size_um = patch_size * 1e-9 / 1e-6
                    if patch_size_um > self._FINE_RANGE_X:
                        raise ValidationError(
                            f"Patch size {keys[1]}: {patch_size_um} is bigger than piezo range, increase the number of patches"
                        )

    class Meta:
        uiorder = [
            *MeshSchema.Meta.uiorder,
            "patches_x",
            "patches_y",
            "patch_size_x",
            "patch_size_y",
        ]

        uischema = {
            **MeshSchema.Meta.uischema,
        }

        uigroups = [
            "subsampleid",
            "dwell",
            {"Patches": ["patches_x", "patches_y", "patch_size_x", "patch_size_y"]},
            {"Steps": ["step_size_x", "step_size_y", "steps_x", "steps_y"]},
            "continuous",
            "fast_axis",
            "enqueue",
            "estimate",
        ]

    def calculated(self, data, **kwargs):
        calculated = {}

        intervals = {
            "steps_x": ["step_size_x", "x", "x2"],
            "steps_y": ["step_size_y", "y", "y2"],
        }

        for iv, keys in intervals.items():
            if data.get(keys[0]):
                patch_sizes = []
                steps = []
                if data.get("objects"):
                    for obj in data["objects"]:
                        patch_size = (obj[keys[2]] - obj[keys[1]]) / data[
                            f"patches_{keys[1]}"
                        ]
                        step = self._steps(
                            obj[keys[1]], obj[keys[1]] + patch_size, data[keys[0]]
                        )
                        patch_sizes.append(patch_size * (1e-9 / 1e-6))
                        step = (
                            step.to_integral_value()
                            if step.to_integral_value() == step
                            else round(step, 2)
                        )

                        steps.append(step)

                calculated[iv] = ", ".join(map(str, steps))
                calculated[f"patch_size_{keys[1]}"] = ", ".join(map(str, patch_sizes))

        return calculated

    def calc_patch_steps_size(
        self,
        axes,
        init_patches_x: int,
        init_patches_y: int,
        init_step_size_x: int,
        init_step_size_y: int,
        fast_axis: str = "Horizontal",
    ):
        patch_size_x = (
            axes["x"]["destination"][1] - axes["x"]["destination"][0]
        ) / init_patches_x
        patch_size_y = (
            axes["y"]["destination"][1] - axes["y"]["destination"][0]
        ) / init_patches_y

        steps_x = int(
            round(
                self._steps(
                    axes["x"]["destination"][0],
                    axes["x"]["destination"][0] + patch_size_x,
                    init_step_size_x,
                    base_unit=axes["x"]["unit_exponent"],
                )
            )
        )
        steps_y = int(
            round(
                self._steps(
                    axes["y"]["destination"][0],
                    axes["y"]["destination"][0] + patch_size_y,
                    init_step_size_y,
                    base_unit=axes["y"]["unit_exponent"],
                )
            )
        )

        print("input steps", steps_x, steps_y)

        step_size_x = (axes["x"]["destination"][1] - axes["x"]["destination"][0]) / (
            steps_x * init_patches_x
        )
        step_size_y = (axes["y"]["destination"][1] - axes["y"]["destination"][0]) / (
            steps_y * init_patches_y
        )

        fast = "x" if fast_axis == "Horizontal" else "y"

        offset_y = step_size_y / 2
        offset_x = step_size_x / 2

        orientation = "horizontal"
        if fast == "y":
            slow = "x"
            orientation = "vertical"
        else:
            slow = "y"

        return (
            patch_size_x,
            patch_size_y,
            steps_x,
            steps_y,
            offset_x,
            offset_y,
            orientation,
            fast,
            slow,
        )
