#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.laser import Laser as AbstractLaser
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Laser(BlissObject, AbstractLaser):
    PROPERTY_MAP = {
        "power": HardwareProperty("power"),
        "state": HardwareProperty("state"),
    }

    CALLABLE_MAP = {"on": "on", "off": "off"}
