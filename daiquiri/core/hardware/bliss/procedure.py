#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.procedure import Procedure as AbstractProcedure
from daiquiri.core.hardware.bliss.object import BlissObject
from tomo.procedures.base_procedure import ProcedureState
from tomo.procedures.base_procedure import ProcedureExecusionState
from daiquiri.core.hardware.bliss.object import EnumProperty
from daiquiri.core.hardware.bliss.object import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Procedure(BlissObject, AbstractProcedure):
    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=ProcedureState),
        "previous_run_state": EnumProperty(
            "previous_run_state", enum_type=ProcedureExecusionState
        ),
        "previous_run_exception": HardwareProperty("previous_run_exception"),
        "parameters": HardwareProperty("parameters"),
    }

    CALLABLE_MAP = {
        "start": "start",
        "abort": "abort",
        "clear": "clear",
        "validate": "validate",
    }


Default = Procedure
