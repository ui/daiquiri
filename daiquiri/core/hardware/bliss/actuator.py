#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.actuator import Actuator as AbstractActuator
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Actuator(BlissObject, AbstractActuator):
    PROPERTY_MAP = {"state": HardwareProperty("state")}

    CALLABLE_MAP = {"move_in": "open", "move_out": "close", "toggle": "toggle"}
