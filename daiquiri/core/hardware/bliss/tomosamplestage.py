#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomosamplestage import (
    TomoSampleStage as AbstractTomoSampleStage,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.objectref import ObjectRefProperty
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class TomoSampleStage(BlissObject, AbstractTomoSampleStage):
    def _get_x_axis_focal_pos(self):
        return self._object._x_axis_focal_pos

    PROPERTY_MAP = {
        "sx": ObjectRefProperty("x_axis", compose=True),
        "sy": ObjectRefProperty("y_axis", compose=True),
        "sz": ObjectRefProperty("z_axis", compose=True),
        "somega": ObjectRefProperty("rotation_axis", compose=True),
        "sampx": ObjectRefProperty("sample_x_axis", compose=True),
        "sampy": ObjectRefProperty("sample_y_axis", compose=True),
        "sampu": ObjectRefProperty("sample_u_axis", compose=True),
        "sampv": ObjectRefProperty("sample_v_axis", compose=True),
        "pusher": ObjectRefProperty("pusher", compose=True),
        "air_bearing_x": ObjectRefProperty("air_bearing_x", compose=True),
        "x_axis_focal_pos": HardwareProperty(
            "x_axis_focal_pos", getter=_get_x_axis_focal_pos
        ),
        "detector_center": HardwareProperty("detector_center"),
    }


Default = TomoSampleStage
