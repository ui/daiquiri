#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.light import Light as AbstractLight, LightStates
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Volpi(BlissObject, AbstractLight):
    def _get_state(self):
        return LightStates[0]

    def _get_temperature(self):
        return 0

    PROPERTY_MAP = {
        "intensity": HardwareProperty("intensity"),
        "state": HardwareProperty("state", getter=_get_state),
        "temperature": HardwareProperty("temperature", getter=_get_temperature),
    }
