#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomoimaging import (
    TomoImaging as AbstractTomoImaging,
)
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import EnumProperty
from tomo.controllers.tomo_imaging import TomoImagingState

import logging

logger = logging.getLogger(__name__)


class TomoImaging(BlissObject, AbstractTomoImaging):
    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=TomoImagingState),
        "update_on_move": HardwareProperty("update_on_move"),
        "exposure_time": HardwareProperty("exposure_time"),
        "settle_time": HardwareProperty("settle_time"),
    }

    CALLABLE_MAP = {
        "take_proj": "take_proj",
        "take_dark": "take_dark",
        "take_flat": "take_flat",
    }


Default = TomoImaging
