#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.pusher import (
    Pusher as AbstractPusher,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import EnumProperty
from tomo.controllers.pusher import PusherState

import logging

logger = logging.getLogger(__name__)


class Pusher(BlissObject, AbstractPusher):
    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=PusherState),
    }

    CALLABLE_MAP = {
        "move_in": "move_in",
        "move_out": "move_out",
        "sync_hard": "sync_hard",
        "push": "push",
    }
