#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomoholo import (
    TomoHolo as AbstractTomoHolo,
)
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import EnumProperty
from tomo.controllers.holotomo import HolotomoState, HolotomoDistance

import logging

logger = logging.getLogger(__name__)


class _DistancesProperty(HardwareProperty):
    def translate_from(self, values: list[HolotomoDistance]):
        if values is None:
            return None
        result = []
        for v in values:
            result.append(v._asdict())
        return result


class TomoHolo(BlissObject, AbstractTomoHolo):
    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=HolotomoState),
        "pixel_size": HardwareProperty("pixel_size"),
        "nb_distances": HardwareProperty("nb_distances"),
        "settle_time": HardwareProperty("settle_time"),
        "distances": _DistancesProperty("distances"),
    }

    CALLABLE_MAP = {
        "move": "move",
    }


Default = TomoHolo
