from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.processor import (
    Processor as AbstractProcessor,
    ProcessorCallablesSchema,
    ProcessorPropertiesSchema,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class DynamicPropertiesMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        dynamic_parameters = {}
        for (
            parameter_key,
            parameter_schema,
        ) in self._object.parameters_schema._declared_fields.items():
            dynamic_parameters[parameter_key] = parameter_schema
            self._property_map[parameter_key] = HardwareProperty(parameter_key)
            self._connect_event(self._property_map[parameter_key])

        dynamic_parameters["Meta"] = self._object.parameters_schema.Meta

        properties_schema = type(
            self.id() + "PropertiesSchema",
            (ProcessorPropertiesSchema,),
            dynamic_parameters,
        )
        self._properties = properties_schema()

        callables_schema = type(
            self.id() + "CallablesSchema", (ProcessorCallablesSchema,), {}
        )
        self._callables = callables_schema()

    def schema_name(self):
        return self.id()


class Processor(DynamicPropertiesMixin, BlissObject, AbstractProcessor):
    _type = "processor"

    PROPERTY_MAP = {
        "state": HardwareProperty("state"),
        "enabled": HardwareProperty("enabled"),
    }
    CALLABLE_MAP = {"reprocess": "reprocess"}
