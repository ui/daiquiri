#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.objectref import Objectref as AbstractObjectref
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import ObjectRefProperty

import logging

logger = logging.getLogger(__name__)


class Objectref(BlissObject, AbstractObjectref):
    PROPERTY_MAP = {"ref": ObjectRefProperty("ref")}
