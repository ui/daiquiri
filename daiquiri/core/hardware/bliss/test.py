#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.test import Test as AbstractTest, TestStates
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class StateProperty(HardwareProperty):
    def translate_from(self, value):
        if value == 1:
            return TestStates[0]

        return ["UNKNOWN"]


class Test(BlissObject, AbstractTest):
    PROPERTY_MAP = {
        "number": HardwareProperty("number"),
        "string": HardwareProperty("string"),
        "option": HardwareProperty("option"),
        "state": StateProperty("state"),
        "read_only": HardwareProperty("read_only"),
    }
