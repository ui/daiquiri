#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomodetector import (
    TomoDetector as AbstractTomoDetector,
)
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import ObjectRefProperty
from daiquiri.core.hardware.bliss.object import EnumProperty
from tomo.controllers.tomo_detector import TomoDetectorState
from tomo.controllers.tomo_detector import TomoDetectorPixelSizeMode

import logging

logger = logging.getLogger(__name__)


class TomoDetector(BlissObject, AbstractTomoDetector):
    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=TomoDetectorState),
        "detector": ObjectRefProperty("detector", compose=True),
        "actual_size": HardwareProperty("actual_size"),
        "camera_pixel_size": HardwareProperty("camera_pixel_size"),
        "optic": ObjectRefProperty("optic", compose=True),
        "sample_pixel_size_mode": EnumProperty(
            "sample_pixel_size_mode", enum_type=TomoDetectorPixelSizeMode
        ),
        "user_sample_pixel_size": HardwareProperty("user_sample_pixel_size"),
        "sample_pixel_size": HardwareProperty("sample_pixel_size"),
        "source_distance": HardwareProperty("source_distance"),
        "field_of_view": HardwareProperty("field_of_view"),
        "acq_mode": HardwareProperty("acq_mode"),
    }


Default = TomoDetector
