#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.airbearing import (
    AirBearing as AbstractAirBearing,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import EnumProperty
from tomo.controllers.air_bearing import AirBearingState

import logging

logger = logging.getLogger(__name__)


class Airbearing(BlissObject, AbstractAirBearing):
    PROPERTY_MAP = {
        "state": EnumProperty("state", enum_type=AirBearingState),
    }

    CALLABLE_MAP = {
        "on": "on",
        "off": "off",
        "sync_hard": "sync_hard",
    }
