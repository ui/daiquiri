#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.attenuator import (
    Attenuator as AbstractAttenuator,
    AttenuatorStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject


class Attenuator_Wago(BlissObject, AbstractAttenuator):
    def _get_state(self):
        return AttenuatorStates[0]

    def _get_factor(self):
        return self._object.factor()

    def _get_thickness(self):
        return self._object.thickness()

    PROPERTY_MAP = {
        "state": HardwareProperty("state", getter=_get_state),
        "factor": HardwareProperty("factor", getter=_get_factor),
        "thickness": HardwareProperty("thickness", getter=_get_thickness),
    }
