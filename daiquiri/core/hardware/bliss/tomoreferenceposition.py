#!/usr/bin/env python
# -*- coding: utf-8 -*-

from daiquiri.core.hardware.abstract.tomoreferenceposition import (
    TomoReferencePosition as AbstractTomoReferencePosition,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class TomoReferencePosition(BlissObject, AbstractTomoReferencePosition):
    PROPERTY_MAP = {}

    CALLABLE_MAP = {
        "move_to_reference": "move_to_reference",
    }


Default = TomoReferencePosition
