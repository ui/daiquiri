#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.remotemotor import (
    Remotemotor as AbstractRemotemotor,
    RemoteMotorStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Remotemotor(BlissObject, AbstractRemotemotor):
    def _get_state(self):
        return RemoteMotorStates[0]

    PROPERTY_MAP = {
        "resolution": HardwareProperty("resolution"),
        "state": HardwareProperty("state", getter=_get_state),
    }

    CALLABLE_MAP = {"enable": "enable", "disable": "disable"}
