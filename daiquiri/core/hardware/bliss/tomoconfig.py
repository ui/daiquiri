#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomoconfig import TomoConfig as AbstractTomoConfig
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.objectref import ObjectRefProperty
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class TomoConfig(BlissObject, AbstractTomoConfig):
    PROPERTY_MAP = {
        "sample_stage": ObjectRefProperty("sample_stage", compose=True),
        "imaging": ObjectRefProperty("tomo_imaging", compose=True),
        "sxbeam": ObjectRefProperty("tracked_x_axis", compose=True),
        "detectors": ObjectRefProperty("detectors", compose=True),
        "reference_position": ObjectRefProperty("reference_position", compose=True),
        "energy": HardwareProperty("energy"),
        "flat_motion": ObjectRefProperty("reference", compose=True),
        "holotomo": ObjectRefProperty("holotomo", compose=True),
        "latency_time": HardwareProperty("latency_time"),
    }


Default = TomoConfig
