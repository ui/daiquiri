#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.transmission import (
    Transmission as AbstractTransmission,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Transmission(BlissObject, AbstractTransmission):
    PROPERTY_MAP = {
        "datafile": HardwareProperty("datafile"),
        "transmission_factor": HardwareProperty("get"),
    }

    CALLABLE_MAP = {"get": "get", "set": "set"}
