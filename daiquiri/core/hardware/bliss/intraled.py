#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.light import Light as AbstractLight, LightStates
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class StateProperty(HardwareProperty):
    def translate_from(self, value):
        for s in LightStates:
            if s == value.upper():
                return s

        return ["UNKNOWN"]


class Intraled(BlissObject, AbstractLight):
    PROPERTY_MAP = {
        "intensity": HardwareProperty("intensity"),
        "temperature": HardwareProperty("temperature"),
        "state": StateProperty("modus"),
    }
