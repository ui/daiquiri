#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations
import math
import gevent

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.motor import Motor as AbstractMotor, MotorStates
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import CouldBeNotImplementedProperty
from daiquiri.core.logging import log
from bliss.common.axis import AxisState


import logging

logger = logging.getLogger(__name__)


class LimitsProperty(HardwareProperty):
    def translate_from(self, value):
        if value is None:
            return [None, None]

        value = list(value)
        for i, v in enumerate(value):
            if math.isinf(v):
                if v < 0:
                    value[i] = -999999999
                else:
                    value[i] = 999999999
        if value[0] > value[1]:
            # BLISS returns the biggest value first, when the sign is negative
            return value[::-1]
        return value


# Convert BLISS state into Daiquiri state.
# Other state names already matches.
STATE_MAPPING = {
    "LIMPOS": "HIGHLIMIT",
    "LIMNEG": "LOWLIMIT",
}


class StateProperty(HardwareProperty):
    def translate_from(self, value: AxisState | None):
        if value is None:
            return ["UNKNOWN"]

        states = []
        for name in value.current_states_names:
            normalized = STATE_MAPPING.get(name, name)
            if normalized in MotorStates:
                states.append(normalized)
            else:
                desc = value._state_desc[name]
                states.append(f"_{name}:{desc}")

        if len(states):
            return states

        # It's "not READY"
        return []


class PositionProperty(HardwareProperty):
    def translate_from(self, value):
        if value is None:
            return None

        if math.isnan(value):
            return None

        return value


class NoneIfNotImplementedProperty(CouldBeNotImplementedProperty):
    """Acceleration and velocity can not be exposed in case of a
    CalcController"""

    def notImplementedValue(self):
        """Returned if the property is not implemented in the hardware"""
        return None


class Motor(BlissObject, AbstractMotor):
    PROPERTY_MAP = {
        "position": PositionProperty("position"),
        "target": PositionProperty("_set_position"),
        "tolerance": HardwareProperty("tolerance"),
        "acceleration": NoneIfNotImplementedProperty("acceleration"),
        "velocity": NoneIfNotImplementedProperty("velocity"),
        "limits": LimitsProperty("limits"),
        "state": StateProperty("state"),
        "unit": HardwareProperty("unit"),
        "offset": HardwareProperty("offset"),
        "sign": HardwareProperty("sign"),
        "display_digits": HardwareProperty("display_digits"),
    }

    CALLABLE_MAP = {"stop": "stop", "wait": "wait_move"}

    def _call_move(self, value, **kwargs):
        logger.debug(f"_call_move {self.name()} {value} {kwargs}")
        bliss_axis = self._object
        bliss_axis.move(value, wait=False)

        def propagate_motion_exception():
            try:
                bliss_axis.wait_move()
            except Exception:
                log.get("user").error(
                    f"move {self.name()} {value} {kwargs} failed",
                    type="hardware",
                    exc_info=True,
                )

        gevent.spawn(propagate_motion_exception)

    def _call_rmove(self, value, **kwargs):
        logger.debug(f"_call_rmove {self.name()} {value} {kwargs}")
        bliss_axis = self._object
        bliss_axis.move(value, wait=False, relative=True)

        def propagate_motion_exception():
            try:
                bliss_axis.wait_move()
            except Exception:
                log.get("user").error(
                    f"rmove {self.name()} {value} {kwargs} failed",
                    type="hardware",
                    exc_info=True,
                )

        gevent.spawn(propagate_motion_exception)
