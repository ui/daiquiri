#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bliss.config.static import get_config
from bliss.common import event
from bliss import global_map
from collections.abc import MutableSequence

from daiquiri.core.hardware.abstract import (
    MappedHardwareObject,
    HardwareObject,
    HardwareProperty,
    AbstractHardwareProperty2,
)
from daiquiri.core.utils import get_nested_attr

import logging

logger = logging.getLogger(__name__)


class ObjectRefProperty(HardwareProperty):
    """Attribute read from BLISS as another BLISS object and exposed to Daiquiri
    as an object name

    Attributes:
        name: Name of the attribute in the remote hardware device
        compose: If true the referenced object is part of this component and will
                 automatically be exposed registered in Daiquiri and exposed to
                 the front end.
    """

    def __init__(self, name, compose: bool = False):
        HardwareProperty.__init__(self, name)
        self._compose = compose

    @property
    def compose(self) -> bool:
        """If true the referenced object compose this hardware object.

        As result if this object is registered in Daiquiri, the referenced
        object will be registered too.
        """
        return self._compose

    def translate_from(self, value):
        if value is None:
            name = ""
        elif isinstance(value, str):
            name = value
        else:
            name = value.name
        return "hardware:" + name


class ObjectRefListProperty(HardwareProperty):
    """Attribute read from BLISS as another BLISS object and exposed to Daiquiri
    as a list of object name

    Attributes:
        name: Name of the attribute in the remote hardware device
        compose: If true the referenced object is part of this component and will
                 automatically be exposed registered in Daiquiri and exposed to
                 the front end.

    """

    def __init__(self, name, compose: bool = False):
        HardwareProperty.__init__(self, name)
        self._compose = compose

    @property
    def compose(self) -> bool:
        """If true the referenced object compose this hardware object.

        As result if this object is registered in Daiquiri, the referenced
        object will be registered too.
        """
        return self._compose

    def _device_to_name(self, device):
        if device is None:
            name = ""
        elif isinstance(device, str):
            name = device
        else:
            name = device.name
        return "hardware:" + name

    def translate_from(self, value):
        if value is None:
            return []
        return [self._device_to_name(v) for v in value]


class EnumProperty(HardwareProperty):
    """Attribute read from BLISS as a python enum and exposed to Daiquiri as a
    name
    """

    def __init__(self, name: str, enum_type, getter: callable = None):
        HardwareProperty.__init__(self, name, getter=getter)
        self.__enum_type = enum_type

    def translate_from(self, value):
        if isinstance(value, str):
            value = self.__enum_type(value)
        state = value.name
        return state.upper()


class CouldBeNotImplementedProperty(HardwareProperty):
    """Attribute which can not be implemented in the hardware object.

    In this case a default value is returned.
    name
    """

    def notImplementedValue(self):
        """Returned value when the property is not implemented."""
        return None


class BlissObject(MappedHardwareObject):
    _protocol = "bliss"
    _online = True

    def __init__(self, obj=None, **kwargs):
        super().__init__(**kwargs)

        if obj is None:
            c = get_config()
            self._object = c.get(kwargs["address"])
        else:
            self._object = obj

        aliases = global_map.aliases
        self._alias = aliases.get_alias(obj)

        user_tags = []
        if hasattr(obj, "config"):
            config = obj.config
            tags = config.get("user_tag")
            if tags is not None:
                if isinstance(tags, str):
                    user_tags.append(tags)
                elif isinstance(tags, MutableSequence):
                    for tag in tags:
                        user_tags.append(tag)
                else:
                    raise ValueError("Unsupported BLISS tag from object %s", obj.name)
        self._user_tags = user_tags

        logger.debug("Connecting to object %s", self._object.name)
        for name, prop in self._property_map.items():
            logger.debug("            - Property %s", name)
            self._connect_event(prop)

    def _connect_event(self, prop: HardwareProperty):
        if isinstance(prop, AbstractHardwareProperty2):
            prop.connect_hardware(self._object)
        else:
            event.connect(self._object, prop.name, self._event)

    def __repr__(self) -> str:
        return f"<Bliss: {self.name()} ({self.__class__.__name__}/{self._object.__class__.__name__})>"

    def _event(self, value, *args, signal=None, **kwargs):
        for name, prop in self._property_map.items():
            if signal == prop.name:
                self._update(name, prop, value)
                break

    def _do_set(self, prop: HardwareProperty, value):
        obj = self._object
        if isinstance(prop, AbstractHardwareProperty2):
            prop.write_hardware(obj, value)
        else:
            return setattr(obj, prop.name, value)

    def _do_get(self, prop: HardwareProperty):
        obj = self._object
        if isinstance(prop, AbstractHardwareProperty2):
            try:
                return prop.read_hardware(obj)
            except (NotImplementedError, RuntimeError):
                logger.info(
                    f"Could not get property {prop.name} from {self.name()}",
                    exc_info=True,
                )
                return None

        getter = prop.getter
        if getter is not None:
            return getter(self)
        try:
            try:
                return get_nested_attr(obj, prop.name)
            except NotImplementedError:
                if isinstance(prop, CouldBeNotImplementedProperty):
                    return prop.notImplementedValue()
                raise
        except (NotImplementedError, RuntimeError):
            logger.info(
                f"Could not get property {prop.name} from {self.name()}", exc_info=True
            )
            return None

    def _do_call(self, function, value, **kwargs):
        fn = getattr(self._object, self._callable_map[function])
        if value is None:
            return fn(**kwargs)
        else:
            return fn(value, **kwargs)

    def _create_config_from_ref_property(self, name):
        obj_name = self.get(name)
        if not obj_name.startswith("hardware:"):
            return None
        obj_name = obj_name[len("hardware:") :]
        if obj_name == "":
            return None
        return {
            "protocol": "bliss",
            "id": obj_name,
            "name": obj_name,
            "address": obj_name,
        }

    def _create_config_from_ref_list_property(self, name):
        obj_names = self.get(name)
        obj_names = [
            n[len("hardware:") :] for n in obj_names if n.startswith("hardware:")
        ]
        obj_names = [n for n in obj_names if n != ""]
        return [
            {
                "protocol": "bliss",
                "id": obj_name,
                "name": obj_name,
                "address": obj_name,
            }
            for obj_name in obj_names
        ]

    def get_subobject_configs(self):
        """
        Create a list of configuration for each object reference composition
        this object.

        It is based on properties `ObjectRefProperty` and `ObjectRefListProperty`
        with the `compose` attribute to true.
        """
        configs = []
        for name, prop in self._property_map.items():
            if isinstance(prop, ObjectRefListProperty) and prop.compose:
                configs.extend(self._create_config_from_ref_list_property(name))
            if isinstance(prop, ObjectRefProperty) and prop.compose:
                configs.append(self._create_config_from_ref_property(name))

        return [c for c in configs if c is not None]


class BlissDummyObject(HardwareObject):
    """Dummy Bliss Object

    Used when an object cannot be retrieved from Beacon
    """

    _type = "unknown"
    _protocol = "bliss"
    _online = False

    def _call(self, *args, **kwargs):
        pass

    def _get(self, *args, **kwargs):
        pass

    def _set(self, *args, **kwargs):
        pass
