#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
import numpy as np
import math

from bliss.config.settings import scan as rdsscan

try:
    from blissdata.data.nodes.lima import LimaImageChannelDataNode
    from blissdata.data.node import get_or_create_node, get_nodes
    from blissdata.data.scan import watch_session_scans
    from blissdata.data.nodes.scan_group import GroupScanNode
except ImportError:
    from bliss.data.nodes.lima import LimaImageChannelDataNode
    from bliss.data.node import get_or_create_node, get_nodes
    from bliss.data.scan import watch_session_scans
    from bliss.data.nodes.scan_group import GroupScanNode
from bliss.scanning.scan import ScanState

try:
    from silx.utils.retry import RetryTimeoutError
except ImportError:
    # For older silx version
    class RetryTimeoutError(Exception):
        pass


from daiquiri.core.hardware.bliss.helpers import get_data_from_file

from daiquiri.core.hardware.abstract.scansource import ScanSource, ScanStates
from daiquiri.core.utils import get_start_end, make_json_safe

import logging

logger = logging.getLogger(__name__)


STATE_MAPPING = {
    # TODO: This should not be needed anymore with BLISS 1.11
    0: 0,
    1: 1,
    2: 2,
    3: 2,
    4: 3,
    5: 4,
    6: 5,
    # BLISS >= 1.11
    ScanState.IDLE.name: 0,
    ScanState.PREPARING.name: 1,
    ScanState.STARTING.name: 2,
    ScanState.STOPPING.name: 2,
    ScanState.DONE.name: 3,
    ScanState.USER_ABORTED.name: 4,
    ScanState.KILLED.name: 5,
}


class BlissScans(ScanSource):
    def __init__(
        self,
        config,
        app=None,
        source_config={},
    ):
        super().__init__(config, app=app, source_config=source_config)
        logger.info("Initialising Bliss Scan Watcher")

        self._session = get_or_create_node(self._session_name, node_type="session")
        self._task = gevent.spawn(
            watch_session_scans,
            self._session_name,
            self._scan_start,
            self._scan_child,
            self._scan_data,
            self._scan_end,
            watch_scan_group=True,
            exclude_existing_scans=True,
        )

    def _scan_start(self, info, *args, **kwargs):
        self._npoints = {}
        scanid = self._create_scanid(info["node_name"])
        self._emit_new_scan_event(
            scanid, info.get("type", "none"), info["title"], metadata=info
        )

    def _scan_end(self, info, *args, **kwargs):
        scanid = self._create_scanid(info["node_name"])
        self._emit_end_scan_event(scanid, metadata=info)

    def _scan_child(self, *args, **kwargs):
        pass

    def _scan_data(self, dims, master, details):
        if not ("data" in details):
            return

        scanid = self._create_scanid(details["scan_info"]["node_name"])

        if dims == "0d":
            # Data is accumulated
            channel_name = "0d"
            channel_size = min(len(arr) for arr in details["data"].values())
            for k, v in details["data"].items():
                self._emit_new_scan_data_0d_event(
                    scanid, channel_name=k, channel_size=len(v)
                )
        else:
            channel_node = details["channel_data_node"]
            if isinstance(channel_node, LimaImageChannelDataNode):
                channel_size = details["description"]["last_image_ready"] + 1
            else:
                channel_size = details["index"] + len(details["data"])
            channel_name = details["channel_name"]

        if channel_size <= self._npoints.get(channel_name, 0):
            # Skip dup updates, could be:
            # - trigger from different 0d
            # - trigger from different lima field update last_image_acquired/save
            return
        self._npoints[channel_name] = channel_size
        min_points = min(n for n in self._npoints.values())

        try:
            expected_scan_size = details["scan_info"]["npoints"]
        except Exception:
            expected_scan_size = 1

        try:
            progress = 100 * min_points / expected_scan_size
        except ZeroDivisionError:
            progress = 0

        try:
            channel_progress = 100 * channel_size / expected_scan_size
        except ZeroDivisionError:
            channel_progress = 0

        self._emit_new_scan_data_event(
            scanid, master, progress, channel_name, channel_size, channel_progress
        )

    def _get_scan_nodes(self):
        db_names = rdsscan(
            f"{self._session.name}:*_children_list",
            count=1000000,
            connection=self._session.db_connection,
        )
        return (
            node
            for node in get_nodes(
                *(db_name.replace("_children_list", "") for db_name in db_names)
            )
            if node is not None and node.type in ["scan", "scan_group"]
        )

    def _get_state(self, info):
        """Convert bliss scans states to abstract scan state"""
        state = info.get("state")
        if state is None:
            return "UNKNOWN"

        try:
            return ScanStates[STATE_MAPPING[state]]
        except KeyError:
            logger.error(f"No state mapping for scan state {state}")
            return "UNKNOWN"

    def _get_shape(self, info):
        """Return scan shape

        This is very bliss specific, maybe generalise in the future
        """
        shape = {}
        for k in ["npoints1", "npoints2", "dim", "requests"]:
            shape[k] = info.get(k)
        return shape

    def get_scans(self, scanid=None, **kwargs):
        scans = []

        if self._session is None:
            return scans

        nodes = list(self._get_scan_nodes())
        nodes = sorted(nodes, key=lambda k: k.info.get("start_timestamp", 0))
        nodes.reverse()
        paging = get_start_end(kwargs, points=len(nodes))

        if scanid:
            filtered = nodes
        else:
            filtered = nodes[paging["st"] : paging["en"]]

        for scan in filtered:
            if scanid:
                if self._create_scanid(scan.db_name) != scanid:
                    continue

            info = scan.info.get_all()
            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            if scanid:
                if self._create_scanid(info["node_name"]) != scanid:
                    continue

            sobj = {"scanid": self._create_scanid(info["node_name"])}
            for k in [
                "count_time",
                "node_name",
                "npoints",
                "filename",
                "end_timestamp",
                "start_timestamp",
                "title",
                "type",
            ]:
                sobj[k] = info.get(k)

            sobj["status"] = self._get_state(info)
            sobj["shape"] = self._get_shape(info)
            sobj["scan_number"] = info.get("scan_nb")

            if isinstance(scan, GroupScanNode):
                sobj["group"] = True

                children: list[dict] = []
                # Iterate a custom channel on the sequence called "child_nodes"
                child_channel = "child_nodes"
                data = self.get_scan_data(sobj["scanid"], scalars=[child_channel])
                if child_channel in data["data"]:
                    child_node_names = [
                        # bytes from h5, str from redis?
                        s.decode("utf-8") if isinstance(s, bytes) else s
                        for s in data["data"][child_channel]["data"]
                    ]
                    if child_node_names:
                        for child_scan in nodes:
                            if child_scan.db_name in child_node_names:
                                child_scan_info = child_scan.info.get_all()
                                children.append(
                                    {
                                        "scanid": self._scanid(child_scan.db_name),
                                        "type": child_scan_info.get("type"),
                                        "node": child_scan.db_name,
                                    }
                                )

                    children = sorted(
                        children,
                        key=lambda child: child_node_names.index(child["node"]),
                    )

                sobj["children"] = children

            else:
                try:
                    sobj["estimated_time"] = info["estimation"]["total_time"]
                except KeyError:
                    sobj["estimated_time"] = 0

                xs = []
                ys = {"images": [], "scalars": [], "spectra": []}
                for k, el in info["acquisition_chain"].items():
                    mast = el["master"]
                    for t in ["images", "scalars", "spectra"]:
                        xs.extend(mast.get(t, []))

                    for t in ["images", "scalars", "spectra"]:
                        ys[t].extend(el.get(t, []))

                sobj["axes"] = {"xs": xs, "ys": ys}

            scans.append(make_json_safe(sobj))

        if scanid:
            if scans:
                return scans[0]
        else:
            return {"total": len(nodes), "rows": scans}

    def get_scan_data(
        self, scanid, json_safe=True, scalars=None, all_scalars=False, **kwargs
    ):
        if self._session is None:
            return {}

        # get the expected scan
        for scan in self._get_scan_nodes():
            if self._create_scanid(scan.db_name) == scanid:
                break
            continue
        else:
            return {}

        sobj = {"data": {}, "info": {}}
        info = scan.info.get_all()

        try:
            info["node_name"]
        except KeyError:
            logger.exception(f"No node_name for scan {scan.db_name}")
            return {}

        min_points = self._get_available_points(scan)
        sobj["npoints_avail"] = min_points

        paging = get_start_end(kwargs, points=min_points, last=True)
        sobj["page"] = paging["page"]
        sobj["pages"] = paging["pages"]
        sobj["per_page"] = paging["per_page"]
        sobj["scanid"] = self._create_scanid(scan.db_name)
        sobj["npoints"] = info.get("npoints")
        sobj["shape"] = self._get_shape(info)

        xs = []
        ys = {"images": [], "scalars": [], "spectra": []}
        for el in info["acquisition_chain"].values():
            mast = el["master"]
            for t in ["images", "scalars", "spectra"]:
                xs.extend(mast.get(t, []))

            for t in ["images", "scalars", "spectra"]:
                ys[t].extend(el.get(t, []))

        sobj["axes"] = {"xs": xs, "ys": ys}

        scalarid = 0
        if scalars is None:
            scalars = []
        hdf5_data = None

        for node in scan.walk(include_filter="channel", wait=False):
            sobj["data"][node.name] = {
                "name": node.name,
                "shape": node.info["shape"],
                "size": len(node),
                "dtype": np.dtype(node.dtype).str,
            }

            data = np.array([])
            if len(node.info["shape"]) == 0 and paging["en"] > paging["st"]:
                if (
                    node.name not in scalars
                    and node.name not in xs
                    and not all_scalars
                    and (
                        (
                            len(scalars) == 0
                            and (node.name.startswith("timer") or scalarid >= 2)
                        )
                        or len(scalars) > 0
                    )
                ):
                    continue

                try:
                    if hdf5_data is not None:
                        raise RuntimeError("Getting all data from hdf5")

                    data = node.get_as_array(paging["st"], paging["en"])
                except (RuntimeError, IndexError, TypeError):
                    if hdf5_data is None:
                        try:
                            hdf5_data, points = get_data_from_file(scan.name, info)
                            logger.info("Retrieving data from hdf5")
                            logger.debug(f"Available keys: {hdf5_data.keys()}")
                            sobj["npoints_avail"] = points
                            paging = get_start_end(kwargs, points=points, last=True)
                        except (OSError, RetryTimeoutError):
                            hdf5_data = {}
                            logger.exception("Could not read hdf5 file")
                    if node.name in hdf5_data:
                        data = hdf5_data[node.name][paging["st"] : paging["en"]]
                    else:
                        log_failure = (
                            logger.exception if self._app.debug else logger.info
                        )
                        log_failure(
                            f"Couldnt get paged scan data for {node.db_name}. Requested {paging['st']} to {paging['en']}, node length {len(node)}"
                        )

            # TODO: convert nan -> None
            # TODO: Make sure the browser doesnt interpret as infinity (1e308)
            data = np.nan_to_num(data, posinf=1e200, neginf=-1e200)
            sobj["data"][node.name]["data"] = data

            if not node.name.startswith("timer") and node.name not in xs:
                scalarid += 1

        # Channels are now deleted from redis after a short TTL (~15min)
        # In the case no channels are available in a scan try to read from h5
        if not sobj["data"]:
            try:
                hdf5_data, points = get_data_from_file(scan.name, info)
                logger.info("Retrieving data from hdf5")
                logger.debug(f"Available keys: {hdf5_data.keys()}")

            except (OSError, RetryTimeoutError):
                hdf5_data = {}
                logger.exception("Could not read hdf5 file")
            else:
                paging = get_start_end(kwargs, points=points, last=True)
                for channel in hdf5_data.keys():
                    sobj["data"][channel] = {
                        "name": channel,
                        "shape": [],  # scalar is an enpty shape
                        "data": np.array([]),
                        "size": len(hdf5_data[channel]),
                        "dtype": hdf5_data[channel].dtype.str,
                    }

                    if channel in xs or channel in scalars or all_scalars:
                        data = hdf5_data[channel][paging["st"] : paging["en"]]
                        data = np.nan_to_num(data, posinf=1e200, neginf=-1e200)
                        sobj["data"][channel]["data"] = data

        if json_safe:
            sobj = make_json_safe(sobj)
        return sobj

    def _get_available_points(self, scan):
        """TODO: This is problematic because len(node) actually has to
        retrieve the data, for a scan with ~2000 ish points it takes
        of the order of 500ms
        """
        import time

        start = time.time()
        shortest = None
        min_points = math.inf
        for node in scan.walk(include_filter="channel", wait=False):
            if len(node) < min_points:
                shortest = node.name
                min_points = len(node)

        if min_points == math.inf:
            min_points = 0

        took = time.time() - start
        logger.debug(f"_get_available_points {shortest} {min_points} took: {took} s")

        return min_points

    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        log_failure = logger.exception if self._app.debug else logger.info
        for scan in self._get_scan_nodes():
            if self._create_scanid(scan.db_name) != scanid:
                continue

            info = scan.info.get_all()
            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            min_points = self._get_available_points(scan)

            hdf5_data = None
            spectra = {}
            for node in scan.walk(include_filter="channel", wait=False):
                if not node.info.get("shape"):
                    continue

                if len(node.info["shape"]) > 0:
                    data = np.array([])
                    try:
                        if hdf5_data is not None:
                            raise RuntimeError("Getting all spectra from hdf5")

                        data = (
                            node.get_as_array(0, to_index=int(info.get("npoints", 0)))
                            if allpoints
                            else np.array([node.get_as_array(point)])
                        )
                    except (RuntimeError, IndexError, TypeError):
                        if hdf5_data is None:
                            try:
                                hdf5_data, _points = get_data_from_file(
                                    scan.name, info, type="spectrum"
                                )
                                logger.info(
                                    f"Retrieving data from hdf5: {hdf5_data.keys()}"
                                )
                            except (OSError, RetryTimeoutError):
                                hdf5_data = {}
                                logger.exception("Could not read hdf5 file")

                        if node.name in hdf5_data:
                            if allpoints:
                                data = hdf5_data[node.name]
                            else:
                                if point < len(hdf5_data[node.name]):
                                    data = np.array([hdf5_data[node.name][point]])
                                else:
                                    log_failure(
                                        f"Couldnt get scan spectra for {node.db_name}. Requested point {point} outside range {len(hdf5_data[node.name])}"
                                    )
                                    return None
                        else:
                            log_failure(
                                f"Couldnt get scan spectra for {node.db_name}. Requested 0 to {info.get('npoints')}, node length {len(node)}"
                            )
                            return None

                    spectra[node.name] = {"data": data, "name": node.name}

            # Channels are now deleted from redis after a short TTL (~15min)
            # In the case no channels are available in a scan try to read from h5
            if not spectra:
                try:
                    hdf5_data, _ = get_data_from_file(scan.name, info, type="spectrum")
                except (OSError, RetryTimeoutError):
                    hdf5_data = {}
                    logger.exception("Could not read hdf5 file")
                else:
                    for channel in hdf5_data.keys():
                        data = {}
                        if allpoints:
                            data = hdf5_data[channel]
                        else:
                            if point < len(hdf5_data[channel]):
                                data = np.array([hdf5_data[channel][point]])
                            else:
                                log_failure(
                                    f"Couldnt get scan spectra for {channel}. Requested point {point} outside range {len(hdf5_data[channel])}"
                                )
                                return None

                        spectra[channel] = {"data": data, "name": channel}

            return make_json_safe(
                {
                    "scanid": scanid,
                    "data": spectra,
                    "npoints": info.get("npoints"),
                    "npoints_avail": min_points,
                    "conversion": self.get_conversion(),
                }
            )

    def get_scan_image(self, scanid, node_name, image_no):
        for scan in self._get_scan_nodes():
            if self._create_scanid(scan.db_name) != scanid:
                continue

            info = scan.info.get_all()
            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            for node in scan.walk(include_filter="lima", wait=False):
                if node_name != node.name:
                    continue

                view = node.get(image_no, image_no)
                return view.get_image(image_no)

            for node in scan.walk(include_filter="channel", wait=False):
                if node_name != node.name:
                    continue
                shape = node.info.get("shape")
                if len(shape) != 2:
                    continue
                return node.get_as_array(image_no)

        raise RuntimeError(
            f"Data scanid={scanid} node_name={node_name} image_no={image_no} not found"
        )
