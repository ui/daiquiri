# from blinker import signal
import logging
from daiquiri.core.hardware.abstract import MappedHardwareObject, HardwareProperty
from ._httpclient import Multivisor

logger = logging.getLogger(__name__)


class MultivisorObject(MappedHardwareObject):
    """
    Base object to handle hardware object from Multivisor.

    An hardware object can be defined such way in Daiquiri, from the
    Multivisor UID of the service.

    .. code-block: yaml

       - url: multivisor://mainstation:id00:metadata

    Arguments:
        app: Link to flask
        address: Address of the object inside Multivisor (it is the mulivisor uid)
        multivisor: Access to the Multivisor API
    """

    _protocol = "multivisor"

    def __init__(self, app, address: str, multivisor: Multivisor, **kwargs):
        self._app = app
        self._uid = address
        self._multivisor = multivisor
        super().__init__(**kwargs)
        self._connect()

    def __repr__(self) -> str:
        return f"<Multivisor: {self._uid} ({self.__class__.__name__})>"

    def _connect(self):
        """Connect this daiquiri hardware to the remote service"""
        self._multivisor.register_state(self._uid, self._state_updated)
        state = self._multivisor.get_state(self._uid)
        self._state_updated(self._uid, state)

    def _state_updated(self, name, state):
        prop = self._property_map["state"]
        self.set_online(state != "UNKNOWN")
        self._update("state", prop, state)

    def _disconnect(self):
        """Disconnect this daiquiri hardware from the remote service"""
        self._multivisor.unregister_state(self._uid, self._state_updated)
        self.set_online(False)

    def _do_set(self, prop: HardwareProperty, value):
        raise Exception("Unsupported")

    def _do_get(self, prop: HardwareProperty):
        if prop.name == "state":
            state = self._multivisor.get_state(self._uid)
            return state
        raise Exception("Unsupported")

    def _do_call(self, function, value, **kwargs):
        if function == "start":
            self._multivisor.start(self._uid)
        elif function == "restart":
            self._multivisor.restart(self._uid)
        elif function == "stop":
            self._multivisor.stop(self._uid)
