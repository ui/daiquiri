#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.service import Service as AbstractService
from daiquiri.core.hardware.multivisor.object import MultivisorObject

import logging

logger = logging.getLogger(__name__)


class Service(MultivisorObject, AbstractService):
    PROPERTY_MAP = {
        "state": HardwareProperty("state"),
    }
    CALLABLE_MAP = {"start": "start", "stop": "stop", "restart": "restart"}
