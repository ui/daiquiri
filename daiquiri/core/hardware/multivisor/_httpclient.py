import json

import requests
import typing
import gevent
import logging

logger = logging.getLogger(__name__)


class _MultivisorApi:
    """Remote access to Multivisor service based on HTTP.

    It's a raw copy-paste from https://github.com/tiagocoutinho/multivisor/blob/develop/multivisor/client/http.py
    With small changes.

    Under GNU GENERAL PUBLIC LICENSE
    """

    def __init__(self, url):
        self.url = url
        self._status = None
        self.notifications = []
        self.on_process_changed = None

    def stop_processes(self, *names):
        return self.post("/api/process/stop", data=dict(uid=[",".join(names)]))

    def restart_processes(self, *names):
        return self.post("/api/process/restart", data=dict(uid=[",".join(names)]))

    @property
    def status(self):
        if self._status is None:
            self._status = self._get_status()
        return self._status

    @staticmethod
    def _update_status_stats(status):
        supervisors, processes = status["supervisors"], status["processes"]
        s_stats = dict(
            running=sum((s["running"] for s in status["supervisors"].values())),
            total=len(supervisors),
        )
        s_stats["stopped"] = s_stats["total"] - s_stats["running"]
        p_stats = dict(
            running=sum((p["running"] for p in status["processes"].values())),
            total=len(processes),
        )
        p_stats["stopped"] = p_stats["total"] - p_stats["running"]
        stats = dict(supervisors=s_stats, processes=p_stats)
        status["stats"] = stats
        return stats

    def _get_status(self):
        status = self.get("/api/data").json()
        # reorganize status per process
        status["processes"] = processes = {}
        for supervisor in status["supervisors"].values():
            processes.update(supervisor["processes"])
        self._update_status_stats(status)
        return status

    def refresh_status(self):
        self._status = None
        return self.status

    def get(self, url, params=None, **kwargs):
        result = requests.get(self.url + url, params=params, timeout=10, **kwargs)
        result.raise_for_status()
        return result

    def post(self, url, data=None, json=None, **kwargs):
        result = requests.post(
            self.url + url, data=data, json=json, timeout=10, **kwargs
        )
        result.raise_for_status()
        return result

    def __getitem__(self, item):
        return self.get(item).json()

    def __setitem__(self, item, value):
        self.post(item, data=value)

    def events(self):
        stream = self.get("/api/stream", stream=True)
        for line in stream.iter_lines():
            if line:
                line = line.decode("utf-8")
                if line.startswith("data:"):
                    line = line[5:]
                    try:
                        yield json.loads(line)
                    except ValueError:
                        print("error", line)

    def run(self):
        for event in self.events():
            status = self.status
            name, payload = event["event"], event["payload"]
            if name == "process_changed":
                status["processes"][payload["uid"]].update(payload)
                self.on_process_changed(payload)
                self._update_status_stats(status)
            elif name == "notification":
                # self.notifications.append(payload)
                pass


class Multivisor:
    def __init__(self, url):
        self.__url = url
        self.__monitor = None
        self.__callbacks: typing.Dict[str, typing.List[typing.Callable]] = {}
        self._api = _MultivisorApi(url)
        self._api.on_process_changed = self._on_process_changed
        self._api.refresh_status()
        self._spawn()

    def _spawn(self):
        self.__monitor = gevent.spawn(self._api.run)

    def disconnect(self):
        self.__monitor.kill()
        self.__monitor = None
        self.__callbacks = None

    @property
    def url(self):
        return self.__url

    def start(self, name):
        # There is no start, use restart
        self._api.restart_processes(name)

    def restart(self, name):
        self._api.restart_processes(name)

    def stop(self, name):
        self._api.stop_processes(name)

    def _on_process_changed(self, payload):
        uid = payload["uid"]
        statename = payload.get("statename")
        if statename is not None:
            callbacks = self.__callbacks.get(uid)
            if callbacks:
                for c in callbacks:
                    c(name=uid, state=statename)

    def register_state(self, name, callback):
        callbacks = self.__callbacks.get(name)
        if callbacks is None:
            callbacks = []
            self.__callbacks[name] = callbacks
        callbacks.append(callback)

    def unregister_state(self, name, callback):
        callbacks = self.__callbacks.get(name)
        if callbacks is None:
            raise ValueError(f"Service uid {name} was not registered")
        callbacks.remove(callback)

    def get_state(self, name):
        status = self._api.status
        meta = status["processes"].get(name, {"statename": "UNKNOWN"})
        return meta.get("statename", "UNKNOWN")
