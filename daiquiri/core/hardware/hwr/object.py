#!/usr/bin/env python
# -*- coding: utf-8 -*-
import typing
from daiquiri.core.hardware.abstract import MappedHardwareObject, HardwareProperty

import logging

logger = logging.getLogger(__name__)


class HWRProperty(HardwareProperty):
    def __init__(self, name: str, setter: str = None, getter: str = None):
        super(HWRProperty, self).__init__(name=name)
        self._setter = setter
        self._getter = getter

    @property
    def setter(self) -> typing.Optional[str]:
        return self._setter

    @property
    def getter(self) -> typing.Optional[str]:
        return self._getter


class HWRObject(MappedHardwareObject):
    _protocol = "hwr"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._object = kwargs.get("obj")
        for p in self.property_getter_map.values():
            logger.debug("connecting to {p} {obj}".format(p=p, obj=self._object))
            # connect to hwr events here

    # function to call when a HWR event happens
    def _event(self, value, *args, **kwargs):
        logger.debug("HWRObject._event {v} {kw}".format(v=value, kw=kwargs))
        # for p, v in self._property_map.items():
        #    pass
        # some magic
        # self._update(p, v, value)

    def _do_set(self, prop: HardwareProperty, value):
        """Set a property on the child object using the setter map

        Args:
            prop (str): The property to set.
            value: Its value.
        """
        assert isinstance(prop, HWRProperty)  # nosec
        if prop in self.property_setter_map:
            return setattr(self._object, prop.setter)(value)
        else:
            raise KeyError(f"Couldnt find a setter for property {prop}")

    def _do_get(self, prop: HWRProperty):
        """Get a property on the child object using the getter map

        Args:
            prop (str): The property to set.

        Returns:
            The property value
        """
        assert isinstance(prop, HWRProperty)  # nosec
        if prop in self.property_getter_map:
            return getattr(self._object, prop.getter)()
        else:
            raise KeyError(f"Couldnt find a getter for property {prop}")
