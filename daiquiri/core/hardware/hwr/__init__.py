# -*- coding: utf-8 -*-
""" Protocol handler for HardwareRepsitory HardwareObjects"""
import os
import sys
import logging

from marshmallow import ValidationError, fields

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.utils import loader

logger = logging.getLogger(__name__)


class HwrHOConfigSchema(HOConfigSchema):
    address = fields.Str(required=True)
    type = fields.Str(required=True)


class HwrHandler(ProtocolHandler):
    def __init__(self, *args, **kwargs):
        super(HwrHandler, self).__init__(args, kwargs)

        hwr_root = os.path.abspath(os.environ.get("HWR_ROOT", ""))

        if hwr_root:
            logging.info("Using HardwareRepository from %s", hwr_root)
            sys.path.insert(0, hwr_root)

        try:
            # pylint: disable=import-error
            from HardwareRepository import HardwareRepository as hwr

            logging.info("Using system HardwareRepository from %s", hwr.__file__)
        except (ImportError, ModuleNotFoundError):
            raise

        self._hwr = hwr.getHardwareRepository(kwargs.get("path", None))
        self._hwr.connect()

        hwr_logger = logging.getLogger("HWR")
        hwr_logger.setLevel(logging.DEBUG)

    def get(self, *args, **kwargs):
        try:
            HwrHOConfigSchema().load(kwargs)
        except ValidationError as ex:
            raise Exception("HWR Hardware Object definition is invalid %s" % str(ex))

        kwargs["obj"] = self._hwr.getHardwareObject(kwargs.get("address"))

        try:
            return loader(
                "daiquiri.core.hardware.hwr", "", kwargs.get("type"), **kwargs
            )
        except Exception as e:
            logger.error(f"Could not find class for HWR object {kwargs.get('type')}")
            raise e
