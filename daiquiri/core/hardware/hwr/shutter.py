#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.shutter import Shutter as AbstractShutter
from daiquiri.core.hardware.hwr.object import HWRObject, HWRProperty

import logging

logger = logging.getLogger(__name__)


class Shutter(HWRObject, AbstractShutter):
    property_getter_map = {
        "state": HWRProperty("state", setter="set_state"),
        "valid": HWRProperty("is_valid"),
        "open_text": HWRProperty("open_text"),
        "closed_text": HWRProperty("closed_text"),
    }

    CALLABLE_MAP = {"open": "open", "close": "close"}

    def _call_toggle(self):
        if self._object.state() == self._object.STATE.OPEN.name:
            self._object.close()
        elif self._object.state() == self._object.STATE.CLOSED.name:
            self._object.open()
