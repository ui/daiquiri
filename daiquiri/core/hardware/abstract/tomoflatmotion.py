#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

TomoFlatMotion = ["READY"]


class _MotionSchema(HardwareSchema):
    axisRef = fields.Str()
    relative_position = fields.Number(allow_none=True)
    absolute_position = fields.Number(allow_none=True)


class _UpdateParamSchema(HardwareSchema):
    axisRef = fields.Str()
    position = fields.Number(allow_none=True)


class _PropertiesSchema(HardwareSchema):
    state = OneOf(TomoFlatMotion, metadata={"readOnly": True})
    settle_time = fields.Number(metadata={"readOnly": True})
    power_onoff = fields.Bool(metadata={"readOnly": True})
    move_in_parallel = fields.Bool(metadata={"readOnly": True})
    available_axes = fields.List(fields.Str(), metadata={"readOnly": True})
    motion = fields.List(fields.Nested(_MotionSchema), metadata={"readOnly": True})


class _CallablesSchema(HardwareSchema):
    update_axis = fields.Dict()


class TomoFlatMotion(HardwareObject):
    _type = "tomoflatmotion"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()

    def update_axis(self, value):
        self.call("update_axis", value)
