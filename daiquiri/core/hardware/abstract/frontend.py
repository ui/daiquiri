#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

FrontendStates = ["OPEN", "RUNNING", "STANDBY", "CLOSED", "UNKNOWN", "FAULT"]
FrontendItlkStates = ["ON", "FAULT"]


class FrontendPropertiesSchema(HardwareSchema):
    state = OneOf(FrontendStates, metadata={"readOnly": True})
    status = fields.Str(metadata={"readOnly": True})

    automatic = fields.Bool(metadata={"readOnly": True})
    frontend = fields.Str(metadata={"readOnly": True})

    current = fields.Float(metadata={"readOnly": True})
    refill = fields.Float(metadata={"readOnly": True})
    mode = fields.Str(metadata={"readOnly": True})
    message = fields.Str(metadata={"readOnly": True})

    feitlk = OneOf(FrontendItlkStates, metadata={"readOnly": True})
    pssitlk = OneOf(FrontendItlkStates, metadata={"readOnly": True})
    expitlk = OneOf(FrontendItlkStates, metadata={"readOnly": True})


class FrontendCallablesSchema(HardwareSchema):
    open = RequireEmpty()
    close = RequireEmpty()
    reset = RequireEmpty()


class Frontend(HardwareObject):
    _type = "frontend"
    _state_ok = [FrontendStates[0], FrontendStates[1]]

    _properties = FrontendPropertiesSchema()
    _callables = FrontendCallablesSchema()
