#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

PresetmanagerStates = ["READY", "ERROR"]


class PresetmanagerPropertiesSchema(HardwareSchema):
    state = OneOf(PresetmanagerStates, metadata={"readOnly": True})
    presets = fields.List(fields.Str())


class PresetmanagerCallablesSchema(HardwareSchema):
    apply = fields.Str(required=True, metadata={"description": "The preset to apply"})


class Presetmanager(HardwareObject):
    _type = "presetmanager"
    _state_ok = [PresetmanagerStates[0]]

    _properties = PresetmanagerPropertiesSchema()
    _callables = PresetmanagerCallablesSchema()
