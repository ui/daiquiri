#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from typing import Any, Dict
import logging

import mmh3

logger = logging.getLogger(__name__)

ScanStates = ["CREATED", "PREPARING", "RUNNING", "FINISHED", "ABORTED", "FAILED"]


class ScanSource(ABC):
    """The scan source interface"""

    def __init__(
        self,
        config: Dict[str, Any],
        app=None,
        source_config: Dict[str, Any] = {},
    ):
        self._app = app
        self._config = config
        self._source_config = source_config
        self._session_name = source_config.get("session")

        self._new_scan_watchers = []
        self._new_data_watchers = []
        self._end_scan_watchers = []
        self._new_0d_data_watchers = []

    def close(self):
        """Clean up the service at the end.

        After this call, the component should not be accessed anymore
        """
        pass

    @abstractmethod
    def get_scans(self, scanid=None):
        """Get a list of scans

        Returns a list of scan dicts, can be filtered to only scanid to return
        details of a specific scan

        Args:
            scanid (int): Scan id to return

        Returns:
            scans (list): A list of valid `ScanSchema` dicts
        """
        pass

    # @marshal_with(ScanDataSchema())
    @abstractmethod
    def get_scan_data(self, scanid, per_page=25, page=1):
        """Return scan data for the specific scan

        Args:
            scanid (int): The scan id
            per_page (int): Number of items to return per page
            page (int): Page of data to return

        Returns:
            data (dict): A valid `ScanDataSchema` dict

        """
        pass

    @abstractmethod
    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        """Return a scan specific for the specific scan

        Args:
            scanid (int): The scan id
            point (int): The image number to return

        Returns:
            data (dict): A valid `ScanSpectraSchema` dict

        """
        pass

    @abstractmethod
    def get_scan_image(self, scanid, node_name, image_no):
        """Return a scan image for the specific scan

        Args:
            scanid (int): The scan id
            node_name (str): The node for the requested image
            image_no (int): The image number to return

        Returns:
            data (dict): A valid `ScanDataSchema` dict

        """
        pass

    def watch_new_scan(self, fn):
        """Register a callback for when a new scan is started"""
        if not callable(fn):
            raise AttributeError("Callback function must be callable")

        if not (fn in self._new_scan_watchers):
            self._new_scan_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to new scan".format(f=fn)
            )

    def watch_new_data(self, fn):
        """Register a callback for when there is new data for a scan"""
        if not callable(fn):
            raise AttributeError("Callback function must be callable")

        if not (fn in self._new_data_watchers):
            self._new_data_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to new data".format(f=fn)
            )

    def watch_new_0d_data(self, fn):
        """Register a callback for when there is new 0d data for a scan"""
        if not callable(fn):
            raise AttributeError("Callback function must be callable")

        if not (fn in self._new_0d_data_watchers):
            self._new_0d_data_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to new 0d data".format(f=fn)
            )

    def watch_end_scan(self, fn):
        """Register a callback for a scan ends"""
        if not callable(fn):
            raise AttributeError("Callback function must be callable")

        if not (fn in self._end_scan_watchers):
            self._end_scan_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to end scan".format(f=fn)
            )

    def _emit_new_scan_event(
        self, scanid: int, type: str, title: str, metadata: Dict[str, Any]
    ) -> None:
        """Emit an event when a new scan starts

        Kwargs:
            scanid: The id of the new scan
            type: The scan type, ascan, amesh, etc
            title: A short description of the scan
            metadata: Any related metadata (not currently used)
        """
        for cb in self._new_scan_watchers:
            try:
                cb(scanid, type, type, metadata=metadata)
            except Exception:
                logger.error("Error during scan start callback", exc_info=True)

    def _emit_end_scan_event(self, scanid: int, metadata: Dict[str, Any]) -> None:
        """Emit an event when a scan ends

        Kwargs:
            scanid: The id of the scan that has ended
            metadata: Any related metadata (not currently used)
        """
        for cb in self._end_scan_watchers:
            try:
                cb(scanid, metadata=metadata)
            except Exception:
                logger.error("Error during scan end callback", exc_info=True)

    def _emit_new_scan_data_0d_event(
        self, scanid: int, channel_name: str, channel_size: int, continue_: bool = False
    ) -> None:
        """Emit an event when there is new 0d scan data

        Kwargs:
            scanid: The id of the scan in progress
            channel_name: The channel that has new data
            channel_size: The length of the new channel
        """
        for cb in self._new_0d_data_watchers:
            try:
                cb(scanid, channel_name=channel_name, channel_size=channel_size)
            except Exception:
                logger.error("Error during 0d data callback", exc_info=True)
                if continue_:
                    continue

    def _emit_new_scan_data_event(
        self,
        scanid: int,
        master_channel: str,
        progress: float,
        channel_name: str,
        channel_size: int,
        channel_progress: float,
    ) -> None:
        """Emit an event when there is new scan data

        Kwargs:
            scanid: The id of the new scan
            master_channel: The master channel of the scan
            progress: A value between 0 and 100 for the scan progress
            channel_name: The channel that has new data
            channel_size: The length of the new channel
            channel_progress: The progres between 0 and 100 for the triggering channel
        """
        for cb in self._new_data_watchers:
            try:
                cb(
                    scanid,
                    master_channel,
                    progress,
                    channel_name,
                    channel_size,
                    channel_progress,
                )
            except Exception:
                logger.error("Error during data callback", exc_info=True)

    def _create_scanid(self, scan_key: str) -> int:
        """Construct a valid DB key from the scan key

        Converts any string based scan id into a hashed number for database compatibility

        Kwargs:
            scan_key: The scan engine scan key
        """
        return mmh3.hash(scan_key) & 0xFFFFFFFF

    def get_conversion(self) -> dict:
        """Get the mca conversion factors to convert bins to energy

        energy = zero + bin * scale
        """
        if "mca" not in self._config:
            return {}
        return {
            "zero": self._config["mca"]["conversion"]["zero"],
            "scale": self._config["mca"]["conversion"]["scale"],
        }
