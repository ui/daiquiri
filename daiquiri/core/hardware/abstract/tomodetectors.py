#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    state = fields.Str(metadata={"readOnly": True})
    detectors = fields.List(fields.Str(), metadata={"readOnly": True})
    active_detector = fields.Str(metadata={"readOnly": True})


class _CallablesSchema(HardwareSchema):
    mount = fields.Str()


class TomoDetectors(HardwareObject):
    _type = "tomodetectors"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()

    def mount(self, value):
        self.call("mount", value)
