#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    sx = fields.Str(metadata={"readOnly": True}, allow_none=True)
    sy = fields.Str(metadata={"readOnly": True})
    sz = fields.Str(metadata={"readOnly": True})
    somega = fields.Str(metadata={"readOnly": True})
    sampx = fields.Str(metadata={"readOnly": True})
    sampy = fields.Str(metadata={"readOnly": True})
    sampu = fields.Str(metadata={"readOnly": True}, allow_none=True)
    sampv = fields.Str(metadata={"readOnly": True}, allow_none=True)
    pusher = fields.Str(metadata={"readOnly": True}, allow_none=True)
    air_bearing_x = fields.Str(metadata={"readOnly": True}, allow_none=True)
    x_axis_focal_pos = fields.Number(allow_none=True, metadata={"readOnly": True})
    detector_center = fields.List(
        fields.Number(allow_none=True),
        metadata={"readOnly": True},
        required=True,
    )


class _CallablesSchema(HardwareSchema):
    pass


class TomoSampleStage(HardwareObject):
    _type = "tomosamplestage"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
