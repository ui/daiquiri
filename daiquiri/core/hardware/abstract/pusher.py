#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

PusherStates = ["UNKNOWN", "RETRACTED", "IN_TOUCH", "MOVING_IN", "MOVING_OUT"]


class PusherPropertiesSchema(HardwareSchema):
    state = OneOf(PusherStates, metadata={"readOnly": True})


class PusherCallablesSchema(HardwareSchema):
    sync_hard = RequireEmpty()
    move_in = RequireEmpty()
    move_out = RequireEmpty()
    push = fields.Float(metadata={"readOnly": True})


class Pusher(HardwareObject):
    _type = "pusher"
    _state_ok = [PusherStates[1], PusherStates[2]]

    _properties = PusherPropertiesSchema()
    _callables = PusherCallablesSchema()
