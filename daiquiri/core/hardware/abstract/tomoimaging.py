#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

TomoImagingStatues = ["READY", "BLOCKED", "ACQUIRING", "UNKNOWN"]


class _PropertiesSchema(HardwareSchema):
    state = OneOf(TomoImagingStatues, metadata={"readOnly": True})
    update_on_move = fields.Bool()
    exposure_time = fields.Number()
    settle_time = fields.Number()


class _CallablesSchema(HardwareSchema):
    take_proj = RequireEmpty()
    take_dark = RequireEmpty()
    take_flat = RequireEmpty()


class TomoImaging(HardwareObject):
    _type = "tomoimaging"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
