#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

PumpStates = ["ON", "OFF", "UNKNOWN", "ERROR"]


class PumpPropertiesSchema(HardwareSchema):
    state = OneOf(PumpStates, metadata={"readOnly": True})
    status = fields.Str(metadata={"readOnly": True})
    pressure = fields.Float(metadata={"readOnly": True})
    voltage = fields.Float(metadata={"readOnly": True})
    current = fields.Float(metadata={"readOnly": True})


class PumpCallablesSchema(HardwareSchema):
    on = RequireEmpty()
    off = RequireEmpty()


class Pump(HardwareObject):
    _type = "pump"
    _state_ok = [PumpStates[0], PumpStates[1]]

    _properties = PumpPropertiesSchema()
    _callables = PumpCallablesSchema()
