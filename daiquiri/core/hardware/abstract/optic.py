#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class OpticPropertiesSchema(HardwareSchema):
    state = fields.String()
    magnification = fields.Float()
    available_magnifications = fields.List(fields.Float())
    target_magnification = fields.Float()
    magnification_range = fields.List(fields.Float(), metadata={"length": 2})


class OpticCallablesSchema(HardwareSchema):
    move = fields.Float()


class Optic(HardwareObject):
    _type = "optic"

    _properties = OpticPropertiesSchema()
    _callables = OpticCallablesSchema()

    def move(self, value):
        self.call("move", value)
