#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    ref = fields.Str(metadata={"readOnly": True})


class _CallablesSchema(HardwareSchema):
    pass


class Objectref(HardwareObject):
    _type = "objectref"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
