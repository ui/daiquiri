#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    imaging = fields.Str(metadata={"readOnly": True})
    sample_stage = fields.Str(metadata={"readOnly": True}, allow_none=False)
    sxbeam = fields.Str(metadata={"readOnly": True}, allow_none=True)
    detectors = fields.Str(metadata={"readOnly": True})
    reference_position = fields.Str(metadata={"readOnly": True}, allow_none=True)
    energy = fields.Float()
    flat_motion = fields.Str(metadata={"readOnly": True})
    holotomo = fields.Str(metadata={"readOnly": True})
    latency_time = fields.Float()
    flat_motion = fields.Str(metadata={"readOnly": True})


class _CallablesSchema(HardwareSchema):
    pass


class TomoConfig(HardwareObject):
    _type = "tomoconfig"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
