#!/usr/bin/env python
# -*- coding: utf-8 -*-

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

AirBearingStates = ["UNKNOWN", "ON", "OFF", "MOVING_ON", "MOVING_OFF", "FAULT"]


class AirBearingPropertiesSchema(HardwareSchema):
    state = OneOf(AirBearingStates, metadata={"readOnly": True})


class AirBearingCallablesSchema(HardwareSchema):
    sync_hard = RequireEmpty()
    on = RequireEmpty()
    off = RequireEmpty()


class AirBearing(HardwareObject):
    _type = "airbearing"
    _state_ok = [AirBearingStates[1], AirBearingStates[2]]

    _properties = AirBearingPropertiesSchema()
    _callables = AirBearingCallablesSchema()
