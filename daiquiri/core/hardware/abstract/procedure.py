#!/usr/bin/env python
# -*- coding: utf-8 -*-

from marshmallow import fields
from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)

ProcedureStates = [
    "STANDBY",
    "DISABLED",
    "RUNNING",
    "ABORTING",
    "AWAITING_USER_INPUT",
    "UNKNOWN",
]


class ProcedurePropertiesSchema(HardwareSchema):
    state = OneOf(ProcedureStates, metadata={"readOnly": True})
    previous_run_state = fields.String()
    previous_run_exception = fields.String()
    parameters = fields.Dict()


class ProcedureCallablesSchema(HardwareSchema):
    start = RequireEmpty()
    abort = RequireEmpty()
    clear = RequireEmpty()
    validate = fields.Dict(allow_none=True)


class Procedure(HardwareObject):
    _type = "procedure"
    _state_ok = ["STANDBY", "RUNNING"]

    _properties = ProcedurePropertiesSchema()
    _callables = ProcedureCallablesSchema()
