#!/usr/bin/env python
# -*- coding: utf-8 -*-

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

ServiceStates = [
    "STOPPED",
    # The process has been stopped due to a stop request or has never been started.
    "STARTING",
    # The process is starting due to a start request.
    "RUNNING",
    # The process is running.
    "BACKOFF",
    # The process entered the STARTING state but subsequently exited too quickly
    # to move to the RUNNING state.
    "STOPPING",
    # The process is stopping due to a stop request.
    "EXITED",
    # The process exited from the RUNNING state (expectedly or unexpectedly).
    "FATAL",
    # The process could not be started successfully.
    "UNKNOWN",
    # The process is in an unknown state (supervisord programming error).
]
"""See http://supervisord.org/subprocess.html#process-states"""


class ServicePropertiesSchema(HardwareSchema):
    state = OneOf(ServiceStates, metadata={"readOnly": True})


class ServiceCallablesSchema(HardwareSchema):
    start = RequireEmpty()
    stop = RequireEmpty()
    restart = RequireEmpty()


class Service(HardwareObject):
    _type = "service"
    _state_ok = ["STARTING", "RUNNING"]

    _properties = ServicePropertiesSchema()
    _callables = ServiceCallablesSchema()
