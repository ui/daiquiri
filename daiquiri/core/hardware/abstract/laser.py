#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf, RequireEmpty

import logging

logger = logging.getLogger(__name__)

LaserStates = ["ON", "OFF", "ERROR", "UNKNOWN"]


class LaserPropertiesSchema(HardwareSchema):
    state = OneOf(LaserStates, metadata={"readOnly": True})
    power = fields.Float()


class LaserCallablesSchema(HardwareSchema):
    on = RequireEmpty()
    off = RequireEmpty()


class Laser(HardwareObject):
    _type = "laser"
    _state_ok = [LaserStates[0]]

    _properties = LaserPropertiesSchema()
    _callables = LaserCallablesSchema()
