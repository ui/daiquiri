#!/usr/bin/env python
# -*- coding: utf-8 -*-

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    pass


class _CallablesSchema(HardwareSchema):
    move_to_reference = RequireEmpty()


class TomoReferencePosition(HardwareObject):
    _type = "tomoreferenceposition"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
