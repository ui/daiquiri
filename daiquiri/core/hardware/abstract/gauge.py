#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

GaugeStates = ["ON", "OFF", "UNKNOWN", "ERROR"]


class GaugePropertiesSchema(HardwareSchema):
    state = OneOf(GaugeStates, metadata={"readOnly": True})
    status = fields.Str(metadata={"readOnly": True})
    pressure = fields.Float(metadata={"readOnly": True})


class GaugeCallablesSchema(HardwareSchema):
    on = RequireEmpty()
    off = RequireEmpty()


class Gauge(HardwareObject):
    _type = "gauge"
    _state_ok = [GaugeStates[0], GaugeStates[1]]

    _properties = GaugePropertiesSchema()
    _callables = GaugeCallablesSchema()
