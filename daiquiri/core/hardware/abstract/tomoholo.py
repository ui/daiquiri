#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

TomoHoloStatues = [
    "READY",
    "MOVING",
    "STABILIZING",
    "INVALID",
    "INVALID_DETECTOR_BINNING_CHANGED",
    "INVALID_DETECTOR_DISTANCE_CHANGED",
    "INVALID_DETECTOR_CHANGED",
    "UNKNOWN",
]


class _DistanceSchema(HardwareSchema):
    position = fields.Number()
    z1 = fields.Number()
    pixel_size = fields.Number()


class _PropertiesSchema(HardwareSchema):
    state = OneOf(TomoHoloStatues, metadata={"readOnly": True})
    pixel_size = fields.Number(allow_none=True)
    nb_distances = fields.Integer()
    settle_time = fields.Number()
    distances = fields.List(fields.Nested(_DistanceSchema), metadata={"readOnly": True})


class _CallablesSchema(HardwareSchema):
    move = fields.Str()


class TomoHolo(HardwareObject):
    _type = "tomoholo"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
