#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)

MotorStates = [
    "READY",
    "MOVING",
    "FAULT",
    "UNKNOWN",
    "DISABLED",
    "LOWLIMIT",
    "HIGHLIMIT",
    "HOME",
    "OFF",
]


class MotorPropertiesSchema(HardwareSchema):
    position = fields.Float()
    target = fields.Float()
    tolerance = fields.Float()
    acceleration = fields.Float()
    velocity = fields.Float()

    limits = fields.List(fields.Float(), metadata={"length": 2})
    """Limits for the position, such as limits[0] <= position <= limits[1]"""

    state = fields.List(
        fields.Str(metadata={"enum": MotorStates}), metadata={"readOnly": True}
    )
    """List of actual state of the motor.

    Extra unofficial state can also be exposed with the following template:

    - `_MYSTATE:The state description`

    It have to be prefixed with an underscore, and can have a colon
    separator with a description.
    """

    unit = fields.Str(metadata={"readOnly": True})
    offset = fields.Float()
    sign = fields.Int()
    display_digits = fields.Int(allow_none=True, metadata={"readOnly": True})


class MotorCallablesSchema(HardwareSchema):
    move = fields.Float()
    rmove = fields.Float()
    stop = RequireEmpty()
    wait = RequireEmpty()


class Motor(HardwareObject):
    _type = "motor"
    _state_ok = [MotorStates[0], MotorStates[1]]

    _properties = MotorPropertiesSchema()
    _callables = MotorCallablesSchema()

    def rmove(self, value):
        self.call("rmove", value)

    def move(self, value):
        self.call("move", value)

    def stop(self):
        self.call("stop", None)

    def wait(self):
        self.call("wait", None)
