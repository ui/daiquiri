#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields, Schema

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)

CameraStatuses = ["READY", "ACQUIRING", "CONFIGURATION", "FAULT", "UNKNOWN", "OFFLINE"]


class _LimaStaticPropertiesSchema(Schema):
    """Static information valid during the whole device life cycle.

    Properties from https://lima1.readthedocs.io/en/latest/applications/tango/python/doc/index.html
    """

    lima_version = fields.Str()
    """The lima core library version number"""

    lima_type = fields.Str()
    """Name of the camera plugin: Maxipix, Pilatus, Frelon, Pco, Basler, Simulator..."""

    camera_type = fields.Str()
    """Type of the camera as exposed by the camera plugin."""

    camera_model = fields.Str()
    """Model of the camera as exposed by the camera plugin: 5x1- TPX1"""

    camera_pixelsize = fields.List(fields.Float())
    """The camera pixel size in x and y dimension, in micron.

    Despit the Lima Tango API, this value is returned in micron instead of meter.
    """

    image_max_dim = fields.List(fields.Int())
    """Maximum image dimension, width and height in pixel"""


class LimaPropertiesSchema(HardwareSchema):
    state = fields.Str(metadata={"readOnly": True})
    static = fields.Nested(
        _LimaStaticPropertiesSchema,
        allow_none=True,
        metadata={"readOnly": True},
    )
    rotation = fields.Int(allow_none=True)
    binning = fields.List(fields.Int(), allow_none=True)
    raw_roi = fields.List(
        fields.Int(),
        metadata={"readOnly": True},
    )
    roi = fields.List(
        fields.Int(),
        metadata={"readOnly": True},
    )
    flip = fields.List(fields.Bool(), allow_none=True)
    size = fields.List(fields.Int(), allow_none=True, metadata={"readOnly": True})
    acc_max_expo_time = fields.Float(allow_none=True)


class LimaCallablesSchema(HardwareSchema):
    pass


class Lima(HardwareObject):
    _type = "lima"
    _state_ok = [CameraStatuses[0], CameraStatuses[1]]

    _properties = LimaPropertiesSchema()
    _callables = LimaCallablesSchema()
