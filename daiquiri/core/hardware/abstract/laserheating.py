#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields
from marshmallow.validate import Length

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf, RequireEmpty

import logging

logger = logging.getLogger(__name__)

LaserheatingStates = ["READY", "ERROR", "UNKNOWN"]


class LaserheatingPropertiesSchema(HardwareSchema):
    state = OneOf(LaserheatingStates, metadata={"readOnly": True})
    exposure_time = fields.Float()
    background_mode = OneOf(["ON", "OFF", "ALWAYS"])
    fit_wavelength = fields.List(fields.Int(), validate=Length(2, 2))
    current_calibration = fields.Str()


class LaserheatingCallablesSchema(HardwareSchema):
    measure = RequireEmpty()


class Laserheating(HardwareObject):
    _type = "laserheating"
    _state_ok = [LaserheatingStates[0]]

    _properties = LaserheatingPropertiesSchema()
    _callables = LaserheatingCallablesSchema()
