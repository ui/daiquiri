#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)

ProcessorStates = ["READY", "PROCESSING", "OFFLINE", "UNKNOWN"]


class ProcessorPropertiesSchema(HardwareSchema):
    state = fields.Str()
    state_ok = fields.Bool()
    enabled = fields.Bool()


class ProcessorCallablesSchema(HardwareSchema):
    reprocess = fields.Dict(keys=fields.Str())


class Processor(HardwareObject):
    _type = "processor"
    _state_ok = [ProcessorStates[0], ProcessorStates[1]]

    _properties = ProcessorPropertiesSchema()
    _callables = ProcessorCallablesSchema()
