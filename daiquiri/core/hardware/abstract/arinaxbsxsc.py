# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareObject

import logging

logger = logging.getLogger(__name__)


class ArinaxBSXSC(HardwareObject):
    _type = "samplechanger"

    _properties = None
    _callables = None
