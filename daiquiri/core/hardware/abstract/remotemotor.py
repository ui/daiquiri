#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf, RequireEmpty

import logging

logger = logging.getLogger(__name__)

RemoteMotorStates = ["ON"]


class RemotemotorPropertiesSchema(HardwareSchema):
    state = OneOf(RemoteMotorStates, metadata={"readOnly": True})
    resolution = fields.Int()


class RemotemotorCallablesSchema(HardwareSchema):
    enable = RequireEmpty()
    disable = RequireEmpty()


class Remotemotor(HardwareObject):
    _type = "remotemotor"
    _state_ok = [RemoteMotorStates[0]]

    _properties = RemotemotorPropertiesSchema()
    _callables = RemotemotorCallablesSchema()
