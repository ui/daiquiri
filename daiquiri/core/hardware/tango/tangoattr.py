#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.tangoattr import TangoAttr as AbstractTangoAttr
from daiquiri.core.hardware.tango.object import (
    TangoObject,
    TangoStateProperty,
    TangoAttrProperty,
)

import logging

logger = logging.getLogger(__name__)


class TangoAttr(TangoObject, AbstractTangoAttr):
    def __init__(self, attrname: str, **kwargs):
        self.__attrname = attrname
        TangoObject.__init__(self, **kwargs)

    def _get_attrname(self):
        return self.__attrname

    PROPERTY_MAP = {
        "state": TangoStateProperty("state"),
        "status": HardwareProperty("status"),
        "value": TangoAttrProperty("value", attrname=_get_attrname),
        "quality": TangoAttrProperty("quality", attrname=_get_attrname),
        "name": TangoAttrProperty("name", attrname=_get_attrname),
        "label": TangoAttrProperty("label", attrname=_get_attrname),
        "description": TangoAttrProperty("description", attrname=_get_attrname),
        "unit": TangoAttrProperty("unit", attrname=_get_attrname),
        "display_unit": TangoAttrProperty("display_unit", attrname=_get_attrname),
        "format": TangoAttrProperty("format", attrname=_get_attrname),
        "data_type": TangoAttrProperty("data_type", attrname=_get_attrname),
    }


Tangoattr = TangoAttr
