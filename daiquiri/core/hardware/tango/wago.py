#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.generic import Generic as AbstractGeneric
from daiquiri.core.hardware.rtschemamixin import RTSchemaMixin
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class WagoStateProperty(HardwareProperty):
    def translate_from(self, value):
        val_map = {DevState.ON: "ON", DevState.OFF: "OFF", DevState.UNKNOWN: "UNKNOWN"}
        for k, v in val_map.items():
            if k == value:
                return v
        return "UNKNOWN"


class Wago(RTSchemaMixin, TangoObject, AbstractGeneric):
    _type = "wago"
    PROPERTY_MAP = {"state": WagoStateProperty("state")}
