#!/usr/bin/env python
# -*- coding: utf-8 -*-
import struct
import numpy
import typing
import enum
from dataclasses import dataclass

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.camera import Camera as AbstractCamera
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)

try:
    import cv2
except ImportError:
    logger.warning("opencv not available, will not be able to bayer decode")
    cv2 = None


VIDEO_HEADER_FORMAT = "!IHHqiiHHHH"
VIDEO_MAGIC = struct.unpack(">I", b"VDEO")[0]
HEADER_SIZE = struct.calcsize(VIDEO_HEADER_FORMAT)


class ImageFormatNotSupported(Exception):
    """Raised when the RAW data from a Lima device can't be decoded as an RGB numpy array."""


class VIDEO_MODES(enum.Enum):
    # From https://github.com/esrf-bliss/Lima/blob/master/common/include/lima/Constants.h#L118
    Y8 = 0
    Y16 = 1
    Y32 = 2
    Y64 = 3
    RGB555 = 4
    RGB565 = 5
    RGB24 = 6
    RGB32 = 7
    BGR24 = 8
    BGR32 = 9
    BAYER_RG8 = 10
    BAYER_RG16 = 11
    BAYER_BG8 = 12
    BAYER_BG16 = 13
    I420 = 14
    YUV411 = 15
    YUV422 = 16
    YUV444 = 17
    YUV411PACKED = 18
    YUV422PACKED = 19
    YUV444PACKED = 20


@dataclass
class VideoCodec:
    dtype: numpy.dtype
    shape: typing.Callable
    opencv_code: int


VIDEO_CODECS = {}

VIDEO_CODECS[VIDEO_MODES.Y8] = VideoCodec(numpy.uint8, lambda w, h: (w, h), None)
VIDEO_CODECS[VIDEO_MODES.Y16] = VideoCodec(numpy.uint16, lambda w, h: (w, h), None)
VIDEO_CODECS[VIDEO_MODES.Y32] = VideoCodec(numpy.uint32, lambda w, h: (w, h), None)
VIDEO_CODECS[VIDEO_MODES.Y64] = VideoCodec(numpy.uint64, lambda w, h: (w, h), None)

VIDEO_CODECS[VIDEO_MODES.RGB24] = VideoCodec(numpy.uint8, lambda w, h: (w, h, 3), None)
VIDEO_CODECS[VIDEO_MODES.RGB32] = VideoCodec(numpy.uint8, lambda w, h: (w, h, 4), None)

if cv2:
    VIDEO_CODECS[VIDEO_MODES.BAYER_RG16] = VideoCodec(
        numpy.uint16, lambda w, h: (w, h), cv2.COLOR_BayerRG2BGR
    )
    VIDEO_CODECS[VIDEO_MODES.BAYER_BG16] = VideoCodec(
        numpy.uint16, lambda w, h: (w, h), cv2.COLOR_BayerBG2BGR
    )
    VIDEO_CODECS[VIDEO_MODES.YUV422PACKED] = VideoCodec(
        numpy.uint8, lambda w, h: (w, h, 2), cv2.COLOR_YUV2RGB_Y422
    )
    VIDEO_CODECS[VIDEO_MODES.I420] = VideoCodec(
        numpy.uint8, lambda w, h: (w, h + h // 2), cv2.COLOR_YUV2RGB_I420
    )
    VIDEO_CODECS[VIDEO_MODES.BAYER_BG8] = VideoCodec(
        numpy.uint8, lambda w, h: (w, h), cv2.COLOR_BayerRG2RGB
    )
    VIDEO_CODECS[VIDEO_MODES.BAYER_RG8] = VideoCodec(
        numpy.uint8, lambda w, h: (w, h), cv2.COLOR_BayerRG2BGR
    )


def parse_video_frame(
    raw_data: bytes, bpp: int = None, rotation: int = 0
) -> numpy.ndarray:
    """Parse a lima video frame and convert to rgb

    Lima frame decoding in bliss
    https://gitlab.esrf.fr/bliss/bliss/-/blob/master/bliss/data/lima_image.py

    Available lima pixel formats:
    https://gitlab.esrf.fr/limagroup/lima/-/blob/master/common/include/lima/VideoUtils.h

    OpenCV formats:
    https://docs.opencv.org/master/d1/d4f/imgproc_2include_2opencv2_2imgproc_8hpp.html
    """
    (
        magic,
        header_version,
        image_mode,
        image_frame_number,
        image_width,
        image_height,
        endian,
        header_size,
        pad0,
        pad1,
    ) = struct.unpack(VIDEO_HEADER_FORMAT, raw_data[:HEADER_SIZE])

    if magic != VIDEO_MAGIC:
        raise ImageFormatNotSupported(f"Magic header not supported (found {magic:0x})")

    if header_version != 1:
        raise ImageFormatNotSupported(
            f"Image header version not supported (found {header_version})"
        )
    if image_frame_number < 0:
        raise IndexError("Image from lima video_live interface not available yet.")

    try:
        mode = VIDEO_MODES(image_mode)
    except KeyError:
        raise ImageFormatNotSupported(f"Video format unsupported (found {image_mode})")

    try:
        codec = VIDEO_CODECS[mode]
    except KeyError:
        raise ImageFormatNotSupported(f"Video codec unsupported (found {mode})")

    data = numpy.frombuffer(raw_data[HEADER_SIZE:], dtype=codec.dtype)

    # flip w and h as this is how images are saved
    shape = list(codec.shape(image_width, image_height))
    shape[0], shape[1] = shape[1], shape[0]
    data.shape = shape

    if bpp is not None:
        if codec.dtype == numpy.uint16:
            if bpp < 16:
                logger.debug(f"Scaling to 16bit from {bpp}")
                data = data << (16 - bpp)

        if codec.dtype == numpy.uint8:
            if bpp > 8:
                logger.warning(f"Scaling to 8bit from {bpp}, trimming {16 - bpp} bits")
                data = data >> (bpp - 8)

    if cv2:
        if codec.opencv_code is not None:
            logger.debug(
                f"Decoding frame with mode {image_mode} using {codec.opencv_code}"
            )
            data = cv2.cvtColor(data, codec.opencv_code)

    if rotation != 0:
        ninties = rotation / 90
        data = numpy.rot90(data, -ninties)

    return data


class LimaStateProperty(HardwareProperty):
    def translate_from(self, value):
        vals = {"Ready": "READY", "Running": "ACQUIRING"}

        if value in vals:
            return vals[value]

        return "UNKNOWN"


class Lima(TangoObject, AbstractCamera):
    PROPERTY_MAP = {
        "state": LimaStateProperty("acq_status"),
        "exposure": HardwareProperty("video_exposure"),
        "gain": HardwareProperty("video_gain"),
        "mode": HardwareProperty("video_mode"),
        "live": HardwareProperty("video_live"),
        "height": HardwareProperty("image_height"),
        "width": HardwareProperty("image_width"),
    }

    def frame(self) -> numpy.ndarray:
        _, raw_data = self._object.video_last_image
        if len(raw_data) > HEADER_SIZE:
            try:
                rotation = int(self._object.image_rotation)
            except ValueError:
                rotation = 0
            return parse_video_frame(
                raw_data, int(self._object.image_type.replace("Bpp", "")), rotation
            )
