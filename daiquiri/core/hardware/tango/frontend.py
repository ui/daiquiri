#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.frontend import Frontend as AbstractFrontend
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class ItlkStateProperty(HardwareProperty):
    def translate_from(self, value):
        val_map = {DevState.ON: "ON", DevState.FAULT: "FAULT"}

        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class FrontendStateProperty(HardwareProperty):
    def translate_from(self, value):
        val_map = {
            DevState.OPEN: "OPEN",
            DevState.RUNNING: "RUNNING",
            DevState.CLOSE: "CLOSED",
            DevState.STANDBY: "STANDBY",
            DevState.FAULT: "FAULT",
            DevState.UNKNOWN: "UNKNOWN",
        }
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Frontend(TangoObject, AbstractFrontend):
    PROPERTY_MAP = {
        "state": FrontendStateProperty("state"),
        "status": HardwareProperty("status"),
        "automatic": HardwareProperty("Automatic_Mode"),
        "frontend": HardwareProperty("FE_State"),
        "current": HardwareProperty("SR_Current"),
        "mode": HardwareProperty("SR_Mode"),
        "refill": HardwareProperty("SR_Refill_Countdown"),
        "message": HardwareProperty("SR_Operator_Mesg"),
        "feitlk": ItlkStateProperty("FE_Itlk_State"),
        "pssitlk": ItlkStateProperty("PSS_Itlk_State"),
        "expitlk": ItlkStateProperty("EXP_Itlk_State"),
    }

    CALLABLE_MAP = {"open": "Open", "close": "Close", "reset": "Reset"}
