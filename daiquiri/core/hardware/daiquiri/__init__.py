#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import INCLUDE, ValidationError, fields

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.hardware.daiquiri.object import DaiquiriObject
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.utils import loader

import logging

logger = logging.getLogger(__name__)


class DaiquiriHOConfig(HOConfigSchema):
    type = fields.Str(required=True, metadata={"description": "The object type"})


class DaiquiriHandler(ProtocolHandler):
    def get(self, **kwargs) -> DaiquiriObject:
        try:
            kwargs = DaiquiriHOConfig().load(kwargs, unknown=INCLUDE)
        except ValidationError as err:
            raise InvalidYAML(
                {
                    "message": "Daiquiri hardware object definition is invalid",
                    "file": "hardware.yml",
                    "obj": kwargs,
                    "errors": err.messages,
                }
            ) from err

        try:
            return loader(
                "daiquiri.core.hardware.daiquiri",
                "",
                kwargs["type"],
                app=self._app,
                **kwargs
            )
        except ModuleNotFoundError:
            logger.error(
                "Could not find class for daiquiri object {type}".format(
                    type=kwargs["type"]
                )
            )
            raise
