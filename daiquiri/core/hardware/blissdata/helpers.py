def get_scan_key(scan):
    import bliss.release
    from packaging.version import Version

    if Version(bliss.release.version) < Version("2.0.dev"):
        return scan.node.db_name
    else:
        from bliss.scanning.group import Sequence

        # Legacy `Sequence` has none standard interface
        if isinstance(scan, Sequence):
            return scan.scan._scan_data.key
        return scan._scan_data.key
