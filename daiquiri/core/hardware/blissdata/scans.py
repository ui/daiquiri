#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
from typing import Any, Dict
import dateutil.parser

import gevent
import numpy
import logging
import lru
import time

from blissdata.scan import Scan
from blissdata.redis_engine.exceptions import NoScanAvailable
from blissdata.beacon.data import BeaconData
from blissdata.redis_engine.store import DataStore
from blissdata.redis_engine.exceptions import EndOfStream
from blissdata.redis_engine.scan import ScanState
from daiquiri.core.hardware.abstract.scansource import ScanSource, ScanStates
from daiquiri.core.utils import get_start_end, make_json_safe

# blissdata 2
# https://gitlab.esrf.fr/bliss/bliss/-/commit/dfbe24b9bee401eabd296664466df93b6f9844d1
try:
    from blissdata.streams.base import CursorGroup

    use_blissdata_2_api = True
# blissdata < 2
except ModuleNotFoundError:
    from blissdata.redis_engine.stream import StreamingClient
    from blissdata.redis_engine.scan import Scan as RedisScan

    use_blissdata_2_api = False

_logger = logging.getLogger(__name__)

STATE_MAPPING = {
    ScanState.CREATED: 0,
    ScanState.PREPARED: 1,
    ScanState.STARTED: 2,
    ScanState.STOPPED: 3,
    ScanState.CLOSED: 5,
}

TERMINATION_MAPPING = {
    "SUCCESS": 3,
    "USER_ABORT": 4,
    "FAILURE": 5,
    "DELETION": 5,
}


class BlissdataScans(ScanSource):
    UPDATE_PERIOD = 0.1
    """Limiting update rate."""

    def __init__(self, config: dict, app=None, source_config={}):
        super().__init__(config, app=app, source_config=source_config)
        self._key_mapping: lru.LRU = lru.LRU(256)

        url = source_config.get("redis_url")
        if not url:
            url = BeaconData().get_redis_data_db()
        self._store: DataStore = DataStore(url)

        _logger.info("Initialising Blissdata Scan Watcher")

        self._service_running: bool = True
        self._task = gevent.spawn(self._listen_scans)
        self._task.link_exception(self._gevent_exception)

    def close(self):
        self._service_running = False
        self._task.join()
        self._task = None
        self._store = None

    def _gevent_exception(self, greenlet):
        """Process gevent exception"""
        try:
            greenlet.get()
        except Exception:
            _logger.error(
                "Error while executing greenlet %s", greenlet.name, exc_info=True
            )

    def _listen_scans(self):
        timestamp_cursor = None
        while self._service_running:
            try:
                timestamp_cursor, scan_key = self._store.get_next_scan(
                    since=timestamp_cursor, block=True, timeout=1
                )
            except NoScanAvailable:
                continue
            self._key_mapping[self._create_scanid(scan_key)] = scan_key
            scan = self._store.load_scan(scan_key)
            if scan.session == self._session_name:
                # Only watch scans from this session
                # FIXME: would be good to close safely this greenlet at service termination
                g = gevent.spawn(self._listen_scan_state, scan)
                g.link_exception(self._gevent_exception)

    def _listen_scan_state(self, scan: Scan):
        while scan.state < ScanState.STARTED:
            scan.update()

        self._scan_start(scan.key, scan.info)
        try:
            if use_blissdata_2_api:
                _data_greenlet = gevent.spawn(self._listen_scan_streams2, scan)
            else:
                _data_greenlet = gevent.spawn(self._listen_scan_streams, scan)
            _data_greenlet.link_exception(self._gevent_exception)
            while scan.state < ScanState.CLOSED:
                scan.update()
        finally:
            if _data_greenlet is not None:
                _data_greenlet.join()
                _data_greenlet = None
        self._scan_end(scan.key, scan.info)

    def _listen_scan_streams2(self, scan: Scan):
        raw_scan = self._store.load_scan(scan.key)
        cursor_group = CursorGroup(raw_scan.streams)
        scanid = self._create_scanid(scan.key)

        info = scan.info
        expected_scan_size = info.get("scan_info", {}).get("npoints", 1)

        while self._service_running:
            try:
                views = cursor_group.read(last_only=True)
            except EndOfStream:
                break

            for stream, view in views.items():
                channel_name = stream.name
                if stream.kind == "array":
                    ndim = len(stream.info["shape"])
                    channel_size = view.index + len(view)
                    if ndim == 0:
                        self._emit_new_scan_data_0d_event(
                            scanid,
                            channel_name=channel_name,
                            channel_size=channel_size,
                            continue_=True,
                        )
                elif stream.kind == "scan":
                    # Scan sequences should be handled at some point
                    continue
                else:
                    _logger.warning("Unsupported stream kind %s", stream.kind)
                    continue

                min_points = self._get_available_points(scan)

                try:
                    progress = 100 * min_points / expected_scan_size
                except ZeroDivisionError:
                    progress = 0

                try:
                    channel_progress = 100 * channel_size / expected_scan_size
                except ZeroDivisionError:
                    channel_progress = 0

                self._emit_new_scan_data_event(
                    scanid,
                    "root",
                    progress,
                    channel_name,
                    channel_size,
                    channel_progress,
                )

            # Rate limit requests to blissdata
            time.sleep(0.2)

    def _listen_scan_streams(self, scan: Scan):
        # Pure redis streams to enable parallel reading
        raw_scan = self._store.load_scan(scan.key, RedisScan)
        client = StreamingClient(raw_scan.streams)
        scanid = self._create_scanid(scan.key)

        info = scan.info
        expected_scan_size = info.get("scan_info", {}).get("npoints", 1)

        while self._service_running:
            try:
                data = client.read(count=-1)
            except EndOfStream:
                break

            for stream, entries in data.items():
                channel_name = stream.name
                encoding_type = stream.encoding["type"]
                index, messages = entries
                if encoding_type == "numeric":
                    ndim = len(stream.encoding["shape"])
                    channel_size = index + len(messages)
                    if ndim == 0:
                        self._emit_new_scan_data_0d_event(
                            scanid,
                            channel_name=channel_name,
                            channel_size=channel_size,
                            continue_=True,
                        )
                elif encoding_type == "json":
                    json_format = stream.info.get("format")
                    if json_format == "lima_v1":
                        channel_size = messages[-1].get("last_index") + 1
                    elif json_format == "lima_v2":
                        channel_size = messages[-1].get("last_index") + 1
                    elif json_format == "subscan":
                        # This should be handled at some point
                        continue
                    else:
                        _logger.warning("Unsupported json format type %s", json_format)
                        continue
                else:
                    _logger.warning("Unsupported encoding type %s", encoding_type)
                    continue

                min_points = self._get_available_points(scan)

                try:
                    progress = 100 * min_points / expected_scan_size
                except ZeroDivisionError:
                    progress = 0

                try:
                    channel_progress = 100 * channel_size / expected_scan_size
                except ZeroDivisionError:
                    channel_progress = 0

                self._emit_new_scan_data_event(
                    scanid,
                    "root",
                    progress,
                    channel_name,
                    channel_size,
                    channel_progress,
                )

            # Rate limit requests to blissdata
            time.sleep(0.2)

    def _scan_start(self, scan_key: str, info: dict):
        scanid = self._create_scanid(scan_key)
        self._emit_new_scan_event(
            scanid, info.get("type", "none"), info["title"], metadata=info
        )

    def _scan_end(self, scan_key: str, info: dict):
        scanid = self._create_scanid(scan_key)
        self._emit_end_scan_event(scanid, metadata=info)

    def _get_state(self, state: ScanState, scan_info: dict):
        """Convert blissdata scans states to abstract scan state"""
        if state is None:
            return "UNKNOWN"
        if state == ScanState.CLOSED:
            termination = scan_info.get("end_reason", None)
            iresult = TERMINATION_MAPPING.get(termination)
            if iresult is None:
                _logger.error(
                    f"No state termination mapping for scan state {termination}"
                )
                return "UNKNOWN"
        else:
            iresult = STATE_MAPPING.get(state)
            if iresult is None:
                _logger.error(f"No state mapping for scan state {state}")
                return "UNKNOWN"
        return ScanStates[iresult]

    def _get_shape(self, info):
        """Return scan shape

        This is very bliss specific, maybe generalise in the future
        """
        shape = {}
        for k in ["npoints1", "npoints2", "dim", "requests"]:
            shape[k] = info.get(k)
        return shape

    def get_scans(self, scanid: int | None = None, **kwargs):
        def scan_to_result(scan: Scan):
            info = scan.info
            sobj: dict = {"scanid": self._create_scanid(scan.key)}
            for k in [
                "count_time",
                "npoints",
                "filename",
                "title",
                "type",
            ]:
                sobj[k] = info.get(k)

            sobj["scan_number"] = info.get("scan_nb")
            sobj["status"] = self._get_state(scan.state, info)
            sobj["shape"] = self._get_shape(info)
            sobj["start_timestamp"] = (
                dateutil.parser.parse(info["start_time"]).timestamp()
                if info.get("start_time")
                else None
            )
            sobj["end_timestamp"] = (
                dateutil.parser.parse(info["end_time"]).timestamp()
                if info.get("end_time")
                else None
            )

            if info.get("is_scan_sequence", False):
                sobj["group"] = True

                children: list[dict] = []
                child_channel_values = list(scan.streams["SUBSCANS"])
                for child_scan in child_channel_values:
                    # In bliss 2.2 blissdata plugins automatically resolve child_scans to their `Scan` object
                    if not isinstance(child_scan, Scan):
                        child_scan = self._store.load_scan(child_scan["key"])
                    children.append(
                        {
                            "scanid": self._create_scanid(child_scan.key),
                            "type": child_scan.info.get("type"),
                            "node": child_scan.key,
                        }
                    )
                sobj["children"] = children

            else:
                try:
                    sobj["estimated_time"] = info["estimation"]["total_time"]
                except KeyError:
                    sobj["estimated_time"] = 0

                xs = []
                ys: dict = {"images": [], "scalars": [], "spectra": []}
                for k, el in info["acquisition_chain"].items():
                    mast = el["master"]
                    for t in ["images", "scalars", "spectra"]:
                        xs.extend(mast.get(t, []))

                    for t in ["images", "scalars", "spectra"]:
                        ys[t].extend(el.get(t, []))

                sobj["axes"] = {"xs": xs, "ys": ys}
            return sobj

        if scanid is not None:
            # Special case for a single scan
            # FIXME: A dedicated `ScanSource.get_scan` API should be exposed instead
            scan = self._get_scan_from_scanid(scanid)
            if scan is None:
                return None
            sobj = scan_to_result(scan)
            return make_json_safe(sobj)

        _timestamp, scan_keys = self._store.search_existing_scans(
            session=self._session_name
        )
        # NOTE: sorting keys is the same as sorting by datetime of the creation
        # NOTE: The `reversed` is a copy-paste from bliss connector
        scan_keys = list(reversed(sorted(scan_keys)))
        paging = get_start_end(kwargs, points=len(scan_keys))
        filtered = scan_keys[paging["st"] : paging["en"]]

        scans = []
        for scan_key in filtered:
            scan = self._store.load_scan(scan_key)
            sobj = scan_to_result(scan)
            scans.append(make_json_safe(sobj))
        return {"total": len(filtered), "rows": scans}

    def _get_scankey_from_scanid(self, scanid: int) -> str | None:
        if scanid not in self._key_mapping:
            # FIXME: i would be better to use scan_key in the daiquiri client
            _timestamp, scan_keys = self._store.search_existing_scans(
                session=self._session_name
            )
            for scan_key in scan_keys:
                # We could use all this keys to update the cache
                if self._create_scanid(scan_key) == scanid:
                    break
            else:
                # That could be a problem not to update the cache with None
                return None
            self._key_mapping[scanid] = scan_key
        else:
            scan_key = self._key_mapping[scanid]

        return scan_key

    def _get_scan_from_scanid(self, scanid: int) -> Scan | None:
        scan_key = self._get_scankey_from_scanid(scanid)
        if scan_key is None:
            return None
        return self._store.load_scan(scan_key)

    def get_scan_data(
        self, scanid, json_safe=True, scalars=None, all_scalars=False, **kwargs
    ):
        scan = self._get_scan_from_scanid(scanid)
        if scan is None:
            return {}

        sobj: Dict[str, Any] = {"data": {}, "info": {}}
        info = scan.info

        min_points = self._get_available_points(scan)
        sobj["npoints_avail"] = min_points

        paging = get_start_end(kwargs, points=min_points, last=True)
        sobj["page"] = paging["page"]
        sobj["pages"] = paging["pages"]
        sobj["per_page"] = paging["per_page"]
        sobj["scanid"] = scanid
        sobj["npoints"] = info.get("npoints")
        sobj["shape"] = self._get_shape(info)

        xs = []
        ys: dict = {"images": [], "scalars": [], "spectra": []}
        for el in info["acquisition_chain"].values():
            mast = el["master"]
            for t in ["images", "scalars", "spectra"]:
                xs.extend(mast.get(t, []))

            for t in ["images", "scalars", "spectra"]:
                ys[t].extend(el.get(t, []))

        sobj["axes"] = {"xs": xs, "ys": ys}

        scalarid = 0
        if scalars is None:
            scalars = []

        for stream in scan.streams.values():
            if stream.name == "SUBSCANS":
                # That's a special stream, better to skip it
                continue
            sobj["data"][stream.name] = {
                "name": stream.name,
                "shape": stream.info.get("shape"),
                "size": len(stream),
                "dtype": (
                    numpy.dtype(stream.info["dtype"]).str
                    if stream.info.get("dtype")
                    else None
                ),
            }

            data = numpy.array([])
            if len(stream.info["shape"]) == 0 and paging["en"] > paging["st"]:
                if (
                    stream.name not in scalars
                    and stream.name not in xs
                    and not all_scalars
                    and (
                        (
                            len(scalars) == 0
                            and (stream.name.startswith("timer") or scalarid >= 2)
                        )
                        or len(scalars) > 0
                    )
                ):
                    continue

                # NOTE: Data size could be smaller than what it is requested
                data = stream[paging["st"] : paging["en"]]

            # TODO: convert nan -> None
            # TODO: Make sure the browser doesnt interpret as infinity (1e308)
            data = numpy.nan_to_num(data, posinf=1e200, neginf=-1e200)
            sobj["data"][stream.name]["data"] = data

            if not stream.name.startswith("timer") and stream.name not in xs:
                scalarid += 1

        if json_safe:
            sobj = make_json_safe(sobj)
        return sobj

    def _get_available_points(self, scan: Scan) -> int:
        """TODO: This is problematic because len(node) actually has to
        retrieve the data, for a scan with ~2000 ish points it takes
        of the order of 500ms
        """
        min_points = None
        for stream in scan.streams.values():
            nb = len(stream)
            if min_points is None or nb < min_points:
                min_points = nb

        if min_points is None:
            return 0
        return min_points

    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        log_failure = _logger.exception if self._app.debug else _logger.info
        scan = self._get_scan_from_scanid(scanid)
        if scan is None:
            return None

        info = scan.info

        min_points = self._get_available_points(scan)

        spectra = {}
        for stream in scan.streams.values():
            shape = stream.info.get("shape")
            if stream is None:
                continue

            if len(shape) == 1:
                data = numpy.array([])
                try:
                    data = (
                        stream[0 : int(info.get("npoints", 0))]
                        if allpoints
                        else numpy.array([stream[point]])
                    )
                except (RuntimeError, IndexError, TypeError):
                    log_failure(
                        f"Couldnt get scan spectra for {stream.name}. Requested 0 to {info.get('npoints')}, node length {len(stream)}"
                    )
                    return None

                spectra[stream.name] = {"data": data, "name": stream.name}

        return make_json_safe(
            {
                "scanid": scanid,
                "data": spectra,
                "npoints": info.get("npoints"),
                "npoints_avail": min_points,
                "conversion": self.get_conversion(),
            }
        )

    def get_scan_image(self, scanid: int, node_name: str, image_no: int):
        scan = self._get_scan_from_scanid(scanid)
        if scan is None:
            return None
        stream = scan.streams.get(node_name)
        if stream is None:
            return None
        return stream[image_no]
