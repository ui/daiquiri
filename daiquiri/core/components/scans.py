#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from marshmallow import fields
from PIL import Image
import numpy

from daiquiri.core import marshal
from daiquiri.core.logging import log
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.components.scan import (
    ScanSchema,
    ScanStatusSchema,
    ScanDataSchema,
    ScanSpectraSchema,
)
from daiquiri.core.schema.components.h5grove import ValueField
from daiquiri.core.responses import image_response, gzipped
from daiquiri.core.utils import make_json_safe, worker
from daiquiri.core.utils import imageutils
from daiquiri.core.utils import arrayutils

import logging

logger = logging.getLogger(__name__)


class ScansResource(ComponentResource):
    @marshal(out=[[200, paginated(ScanSchema), "List of scans info"]], paged=True)
    def get(self, **kwargs):
        """Get a list of all scans and their info"""
        return self._parent.get_scans(**kwargs), 200


class ScanStatusResource(ComponentResource):
    @marshal(
        out=[[200, ScanStatusSchema(), "Scan status"]],
    )
    def get(self, **kwargs):
        """Get current scan status

        i.e. if there is a running scan
        """
        return self._parent.get_scan_status()


class ScanResource(ComponentResource):
    @marshal(
        out=[
            [200, ScanSchema(), "List of scans"],
            [404, ErrorSchema(), "Scan not found"],
        ]
    )
    def get(self, scanid, **kwargs):
        """Get info for a specific scan"""
        scan = self._parent.get_scans(scanid=scanid)
        if scan:
            return scan, 200
        else:
            return {"error": "No such scan"}, 404


class ScanDataResource(ComponentResource):
    @marshal(
        inp={"scalars": fields.List(fields.Str())},
        out=[
            [200, ScanDataSchema(), "Scan data"],
            [404, ErrorSchema(), "Scan not found"],
        ],
        paged=True,
    )
    def get(self, scanid, **kwargs):
        """Get the data for a specific scan"""
        scan = self._parent.get_scan_data(scanid=scanid, **kwargs)
        if scan:

            def gzip():
                return gzipped(scan)

            return worker(gzip)
        else:
            return {"error": "No such scan"}, 404


class ScanDataBinaryResource(ComponentResource):
    @marshal(
        inp={
            "channel": fields.Str(required=True),
            "dtype": fields.Str(),
            "selection": fields.Str(),
        },
        out=[
            [200, ValueField(), "Get scalar data from a scan in binary format"],
            [404, ErrorSchema(), "Scan not found"],
        ],
    )
    def get(self, scanid, channel, dtype=None, selection=None, **kwargs):
        """Get all the data for a specific scan scalar in binary format

        Arguments:
            scanid: Identifier of the scan
            channel: Name of the data channel
            dtype: One of "origin" or "safe" or numpy string like "float32" or "<f16"
            selection: A string representation to slice the data (`1,2::2`)
        """
        if selection:
            selection = arrayutils.parse_slice(selection)
        else:
            selection = None

        scan_info = self._parent.get_scan_data(
            scanid=scanid, json_safe=False, scalars=[channel], **kwargs
        )
        if not scan_info:
            return {"error": "No such scan"}, 404

        shape = scan_info["data"][channel]["shape"]
        if len(shape) == 2:

            def supported_selection(selection) -> bool:
                if selection is None:
                    return False
                if len(selection) == 0:
                    return False
                if isinstance(selection[0], int):
                    return True
                if selection[0].start != selection[0].stop - 1:
                    return False
                return True

            if not supported_selection(selection):
                raise RuntimeError(
                    f"Such selection selection={selection} is not supported by the actual image API"
                )
            image_no = selection[0]
            if not isinstance(image_no, int):
                image_no = image_no.start
            array = self._parent.get_scan_image(
                scanid=scanid, node_name=channel, image_no=image_no
            )
            if len(selection) > 1:
                sub_selection = selection[1:]
                array = array[sub_selection]
        else:
            scan = self._parent.get_scan_data(
                scanid=scanid,
                json_safe=False,
                scalars=[channel],
                page=1,
                per_page=scan_info["data"][channel]["size"],
                **kwargs,
            )
            if not scan:
                return {"error": "No such scan"}, 404

            data = scan["data"].get(channel)

            if data is None:
                return {"error": f"No such scalar '{channel}'"}, 404
            array = data["data"]

            if selection is not None:
                array = array[selection]

        if dtype in ["origin", None]:
            pass
        elif dtype == "safe":
            safe_dtype = arrayutils.to_safe_js_dtype(array.dtype)
            array = array.astype(safe_dtype)
        else:
            array = array.astype(dtype)

        def gzip():
            return gzipped(array)

        return worker(gzip)


class ScanSpectraResource(ComponentResource):
    @marshal(
        inp={
            "point": fields.Int(
                required=True,
                metadata={"description": "The point to return a spectrum for"},
            )
        },
        out=[
            [200, ScanSpectraSchema(), "Scan spectra"],
            [404, ErrorSchema(), "Scan not found"],
        ],
    )
    def get(self, scanid, **kwargs):
        """Get the spectra for a specific scan"""
        spectra = self._parent.get_scan_spectra(scanid=scanid, point=kwargs["point"])
        if spectra is not None:
            return spectra, 200
        else:
            return {"error": "No such scan"}, 404


class ScanImageResource(ComponentResource):
    @marshal(
        inp={
            "node_name": fields.Str(
                metadata={"description": "The scan node name to get images from"}
            ),
            "image_no": fields.Int(
                metadata={"description": "The image number to load"}
            ),
            "raw": fields.Bool(
                metadata={
                    "description": "Return the raw data rather than an image (gzipped)"
                }
            ),
            "norm": fields.String(
                metadata={
                    "description": "Normalization of the image, can be 'linear', 'log', 'arcsinh', 'sqrt'"
                }
            ),
            "autoscale": fields.String(
                metadata={
                    "description": "Autoscale for the domain of the image, can be 'none', 'minmax', 'stddev3'"
                }
            ),
            "lut": fields.String(
                metadata={
                    "description": "LUT for the colors, can be 'gray', 'gray_r', 'viridis', 'cividis'"
                }
            ),
        },
        out=[
            # [200, ScanDataSchema(), 'Scan data'],
            [
                400,
                ErrorSchema(),
                "Error retrieving image",
                404,
                ErrorSchema(),
                "Scan not found",
            ]
        ],
    )
    def get(
        self,
        scanid,
        node_name,
        image_no,
        raw=False,
        norm="linear",
        autoscale="none",
        lut="gray",
        **kwargs,
    ):
        """Get the image for a specific scan"""
        try:
            arr = self._parent.get_scan_image(
                scanid=scanid, node_name=node_name, image_no=image_no
            )
        except Exception as e:
            return {"error": str(e)}, 400

        if arr is None:
            return {"error": "No such image"}, 404

        def generate():
            nonlocal arr, raw, lut, autoscale, norm
            if raw:
                flat = arr.flatten()
                return gzipped(
                    make_json_safe(
                        {
                            "domain": [numpy.amin(flat), numpy.amax(flat)],
                            "shape": arr.shape,
                            "data": flat,
                        }
                    )
                )
            else:
                arr = imageutils.array_to_image(arr, autoscale, norm, lut)
                im = Image.fromarray(arr)
                if im.mode != "RGB":
                    im = im.convert("RGB")

                return image_response(im)

        return worker(generate)


class Scans(Component):
    """Scan Component

    The scan component loads a scan source as defined in scans.yml, and then provides
    access to this data via a flask resource.

    It also subscribes to the scan sources new scan, new data, and scan end watchers,
    and emits these changes via socketio
    """

    _config_export = ["mca"]

    def setup(self, *args, **kwargs):
        self._last_new_data = 0
        self._scan_sources = []
        self._scan_status = {"scanid": None, "progress": 0}

        for source in self._config["sources"]:
            src = self._create_source_from_config(source, self._config)
            src.watch_new_scan(self._new_scan)
            src.watch_end_scan(self._end_scan)
            src.watch_new_data(self._new_data)
            self._scan_sources.append(src)

        self.register_route(ScansResource, "")
        self.register_route(ScanStatusResource, "/status")
        self.register_route(ScanResource, "/<int:scanid>")
        self.register_route(ScanDataResource, "/data/<int:scanid>")
        self.register_route(ScanDataBinaryResource, "/data/binary/<int:scanid>")
        self.register_route(ScanSpectraResource, "/spectra/<int:scanid>")
        self.register_route(ScanImageResource, "/image/<int:scanid>")

    def close(self):
        """Clean up the service at the end.

        After this call, the component should not be accessed anymore
        """
        for s in self._scan_sources:
            s.close()
        self._scan_sources = []

    def _create_source_from_config(self, source_config: dict, config: dict):
        source_type = source_config["type"]
        if source_type == "bliss":
            import bliss.release
            from packaging.version import Version

            if Version(bliss.release.version) < Version("2.0.dev"):
                # Use old bliss connector
                from daiquiri.core.hardware.bliss.scans import BlissScans

                scan_connector_class = BlissScans
            else:
                # Use blissdata anyway
                from daiquiri.core.hardware.blissdata.scans import BlissdataScans

                scan_connector_class = BlissdataScans
        elif source_type == "blissdata":
            from daiquiri.core.hardware.blissdata.scans import BlissdataScans

            scan_connector_class = BlissdataScans
        else:
            raise ValueError(f"Unsupported {type} scan component type")

        src = scan_connector_class(config, app=self._app, source_config=source_config)
        logger.debug("Registered %s scan source", source_type)
        return src

    def get_scans(self, scanid=None, **kwargs):
        """Get scans from a scan source

        Args:
            scanid (int): A specific scanif to return

        Returns:
            scans (dict): A dict of total => number of scans, scans => paginated list of scan infos if scanid is None
            scan (dict): A scan dict if scanid is not None
        """
        scans = {}
        if len(self._scan_sources):
            scans = self._scan_sources[0].get_scans(scanid=scanid, **kwargs)

        return scans

    def get_scan_data(self, scanid, **kwargs):
        """Get the data for a scan

        Args:
            scanid (int): The scanid

        Returns:
            data (dict): Returns
        """
        scan = {}
        if len(self._scan_sources):
            scan = self._scan_sources[0].get_scan_data(scanid=scanid, **kwargs)

        return scan

    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        """Get the data for a scan

        Args:
            scanid (int): The scanid
            point (int): The point to return
            allpoints (bool): Return all available points

        Returns:
            data (dict): Returns
        """
        scan = {}
        if len(self._scan_sources):
            scan = self._scan_sources[0].get_scan_spectra(
                scanid=scanid, point=point, allpoints=allpoints
            )

        return scan

    def get_scan_image(self, scanid, node_name, image_no):
        """Get the data for a scan

        Args:
            scanid (int): The scanid
            node_name (str): The node to return data for
            image_no (int): The image to return

        Returns:
            data (dict): Returns
        """
        image = None
        if len(self._scan_sources):
            image = self._scan_sources[0].get_scan_image(
                scanid=scanid, node_name=node_name, image_no=image_no
            )

        return image

    def get_scan_status(self):
        return self._scan_status

    def _new_scan(self, scanid, type, title, metadata):
        self._scan_status["scanid"] = scanid

        log.get("user").info(
            f"New scan started with id {scanid} and title {title}", type="scan"
        )
        self.emit(
            "new_scan",
            {"scanid": scanid, "type": type, "title": title},
        )

    def _end_scan(self, scanid, metadata):
        if self._scan_status["scanid"] == scanid:
            self._scan_status["scanid"] = None

        log.get("user").info(f"Scan {scanid} finished", type="scan")
        self.emit("end_scan", {"scanid": scanid})

    def _new_data(
        self, scanid, master, progress, channel_name, channel_size, channel_progress
    ):
        if self._scan_status["scanid"] == scanid:
            self._scan_status["progress"] = progress

        now = time.time()
        if (now - self._last_new_data) < 1:
            return

        self._last_new_data = now

        # logger.info(f"_new_data debounced {scanid} {channel}")
        self.emit(
            "new_data",
            {"scanid": scanid, "channel": master, "progress": progress},
        )
