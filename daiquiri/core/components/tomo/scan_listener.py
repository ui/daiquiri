#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import typing
import mmh3

from daiquiri.core.components import Component

try:
    from daiquiri.core.hardware.bliss.scans import BlissScans
except ImportError:
    pass

from .datatype import TomoScan
from .scans.tomo_scan import IncompatibleScanData
from .scans.sequence_tomo_scan import SequenceTomoScan
from .scans.standard_tomo_scan import StandardTomoScan
from .scans.tiling_sequence_tomo_scan import TilingSequenceTomoScan


logger = logging.getLogger(__name__)


class TomoScanListener:
    def __init__(self, component: Component, source):
        self._scans: typing.Dict[int, TomoScan] = {}
        """Alive scans containing tomo data

        FIXME: Use LRU structure to avoid any memory leak
        """

        self.__component: Component = component
        self.__source: BlissScans = source

    @property
    def source(self):
        return self.__source

    @property
    def component(self):
        return self.__component

    def _scanid(self, node):
        return mmh3.hash(node) & 0xFFFFFFFF

    def new_scan(self, scanid, type, title, metadata):
        """Triggered when a scan start"""

        supported_scan_class = [
            SequenceTomoScan,
            StandardTomoScan,
            TilingSequenceTomoScan,
        ]
        for scan_class in supported_scan_class:
            try:
                scan = scan_class(self.component, self.source, scanid, metadata)
                break
            except IncompatibleScanData:
                logger.debug("Scanid %s is not a %s", scanid, scan_class)
            except Exception:
                logger.error("Error while parsing scanid %s", scanid, exc_info=True)
        else:
            scan = None

        if scan is not None:
            logger.debug("Scanid %s interpreted as %s", scanid, scan_class)
            self._scans[scanid] = scan
            logger.debug("Scanid %s on_scan_started: %s", scanid, (metadata,))
            scan.on_scan_started(metadata)

        group = metadata.get("group")
        if group is not None:
            groupid = self._scanid(group)
            scan_group = self._scans.get(groupid)
            if scan_group is not None:
                scan_group.on_subscan_started(scanid, scan)

    def new_0d_data(self, scanid, channel_name, channel_size):
        """Triggered during scan on data updated"""
        scan = self._scans.get(scanid)
        if scan is not None:
            logger.debug(
                "Scanid %s new_0d_data: %s", scanid, (channel_name, channel_size)
            )
            scan.on_0d_data_received(channel_name, channel_size)

    def new_data(
        self, scanid, master, progress, channel_name, channel_size, channel_progress
    ):
        """Triggered during scan on data updated"""
        scan = self._scans.get(scanid)
        if scan is not None:
            logger.debug(
                "Scanid %s on_data_received: %s",
                scanid,
                (master, progress, channel_name, channel_size, channel_progress),
            )
            scan.on_data_received(
                master, progress, channel_name, channel_size, channel_progress
            )

    def end_scan(self, scanid, metadata):
        """Triggered when a scan ended"""
        scan = self._scans.pop(scanid, None)
        if scan is not None:
            logger.debug("Scanid %s on_scan_terminated: %s", scanid, (metadata,))
            scan.on_scan_terminated(metadata)
