#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations

import logging
from marshmallow import fields
from PIL import Image
import pint
import numpy

from daiquiri.core import marshal
from daiquiri.core.components import ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.responses import image_response, ndarray_response, iff_response
from daiquiri.core.utils import worker
from daiquiri.core.utils import imageutils

from .datatype import SampleStageMetadata


logger = logging.getLogger(__name__)


class TomoImageResource(ComponentResource):
    @marshal(
        inp={
            "scanid": fields.Str(
                metadata={"description": "Identifier of the scan to get images from"}
            ),
            "detectorid": fields.Str(
                metadata={"description": "Name of the detector to get images from"}
            ),
            "process": fields.Str(
                metadata={
                    "description": "Name of the process retrieving the data, can be 'flat', 'dark', 'proj', 'flatfield'"
                }
            ),
            "node_name": fields.Str(
                metadata={"description": "The scan node name to get images from"}
            ),
            "image_no": fields.Int(
                metadata={"description": "The image number to load"}
            ),
            "encoding": fields.String(
                metadata={
                    "description": "Format to use for the resulting data: default is `img`"
                }
            ),
            "profiles": fields.String(
                metadata={
                    "description": "Image profiles which can be used from high to low priority"
                }
            ),
            "norm": fields.String(
                metadata={
                    "description": "Normalization of the image, can be 'linear', 'log', 'arcsinh', 'sqrt'"
                }
            ),
            "autoscale": fields.String(
                metadata={
                    "description": "Autoscale for the domain of the image, can be 'none', 'minmax', 'stddev3'"
                }
            ),
            "vmin": fields.Float(
                metadata={
                    "description": "Manual vmin level for manual scale normalization"
                }
            ),
            "vmax": fields.Float(
                metadata={
                    "description": "Manual vmax level for manual scale normalization"
                }
            ),
            "lut": fields.String(
                metadata={
                    "description": "LUT for the colors, can be 'gray', 'gray_r', 'viridis', 'cividis'"
                }
            ),
            "histogram": fields.Bool(
                metadata={"description": "Include or not an histogram in the response"}
            ),
        },
        out=[
            # [200, ScanDataSchema(), 'Scan data'],
            [404, ErrorSchema(), "No such image"],
            [404, ErrorSchema(), "No data projection"],
        ],
    )
    def get(
        self,
        detectorid: str,
        process: str,
        node_name: str | None = None,
        scanid: str | None = None,
        image_no: int | None = None,
        encoding: str | None = None,
        profiles: str | None = None,
        norm: str = "linear",
        autoscale: str = "none",
        vmin: float | None = None,
        vmax: float | None = None,
        lut: str = "gray",
        histogram: bool = False,
        **kwargs,
    ):
        """Get the image for a specific scan"""
        detector = self._parent.get_detector(detectorid)
        if detector is None:
            return {"error": "No such detector"}, 404

        if encoding is None:
            return {"error": "No encoding was specified"}, 400

        def get_data(detector, process):
            kind = None
            # FIXME: There is sanitization to do when proj/flat/dark size do not match
            if process == "dark":
                if detector.dark is None:
                    raise RuntimeError("No dark data")
                kind = process
                data = detector.dark.data
            elif process == "flat":
                if detector.flat is None:
                    raise RuntimeError("No flat data")
                kind = process
                data = detector.flat.data
            elif process == "proj":
                if detector.proj is None:
                    raise RuntimeError("No data projection")
                kind = process
                data = detector.proj.data
            elif process == "flatfield":
                d = detector
                if d.proj is None:
                    raise RuntimeError("No data projection")
                proj = d.proj.normalized
                if d.dark is None and d.flat is None:
                    kind = "proj_norm"
                    data = proj
                elif d.dark is None:
                    kind = "proj_flat_norm"
                    with numpy.errstate(divide="ignore", invalid="ignore"):
                        data = proj / d.flat.normalized
                    data[numpy.logical_not(numpy.isfinite(data))] = 0
                elif d.flat is None:
                    kind = "proj_dark_norm"
                    data = proj - d.dark.normalized
                else:
                    kind = "flatfield_norm"
                    with numpy.errstate(divide="ignore", invalid="ignore"):
                        data = (proj - d.dark.normalized) / (
                            d.flat.normalized - d.dark.normalized
                        )
                    data[numpy.logical_not(numpy.isfinite(data))] = 0
            else:
                raise RuntimeError(f"Process {process} unknown")
            return data, kind

        def generate():
            nonlocal detector, encoding, lut, autoscale, norm, process, profiles, vmin, vmax, histogram

            try:
                data, kind = get_data(detector, process)
            except RuntimeError as e:
                return {"error": e.args[0]}, 404

            assert data is not None  # nosec

            extra_headers = {}
            if detector.proj:
                extra_headers = self.format_extra_headers(
                    detector.proj.sample_stage_meta
                )
            extra_headers["DQR-datakind"] = kind

            if encoding == "png":
                # Supported for debugging purpose only
                d = imageutils.array_to_image(data, autoscale, norm, lut)
                im = Image.fromarray(d)
                if im.mode != "RGB":
                    im = im.convert("RGB")
                return image_response(im, img_format="PNG", extra_headers=extra_headers)
            elif encoding == "bin":
                return ndarray_response(data, extra_headers=extra_headers)
            elif encoding == "iff":
                if profiles is None:
                    profiles = "raw"
                try:
                    return iff_response(
                        data,
                        extra_headers=extra_headers,
                        extra_header_type=b"TOMO",
                        profiles=profiles,
                        autoscale=autoscale,
                        norm=norm,
                        vmin=vmin,
                        vmax=vmax,
                        histogram=histogram,
                    )
                except Exception:
                    logger.error(
                        "Error which create IFF response (autoscale:%s, norm:%s, vmin:%s, vmax:%s)",
                        autoscale,
                        norm,
                        vmin,
                        vmax,
                        exc_info=True,
                    )
                    return {"error": "Problem to format the response"}, 404
            else:
                return {"error": f"Unsupported encoding '{encoding}'"}, 404

        return worker(generate)

    def format_extra_headers(self, meta: SampleStageMetadata):
        extra_headers = {}
        if meta is None:
            return extra_headers

        def feed_quantity(key, quantity: pint.Quantity):
            if quantity is None:
                return
            # formatting with short unit (mm, deg...)
            extra_headers[key] = f"{quantity:~}"

        feed_quantity("DQR-sy", meta.sy)
        feed_quantity("DQR-sz", meta.sz)
        feed_quantity("DQR-sampy", meta.sampy)
        feed_quantity("DQR-somega", meta.somega)
        feed_quantity("DQR-detcy", meta.detcy)
        feed_quantity("DQR-detcz", meta.detcz)
        feed_quantity("DQR-pixelsize", meta.pixel_size)
        return extra_headers
