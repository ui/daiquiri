#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import logging
import typing


logger = logging.getLogger(__name__)


class IncompatibleScanData(Exception):
    pass


class TomoScan:
    """Base class to dispatch, receive events from scans, and expose specific
    scan state

    Raises:
        IncompatibleScanData: If the metadata is not compatible with this
                              constructor
    """

    def __init__(self, component, source, scanid, metadata):
        self.__scanid = scanid
        self.__source = source
        self.__component = component
        self._init(metadata)

    @property
    def scanid(self):
        return self.__scanid

    @property
    def source(self):
        return self.__source

    def to_rest(self) -> dict:
        return None

    @property
    def component(self):
        return self.__component

    def on_scan_started(self, metadata):
        pass

    def on_data_received(
        self, master, progress, channel_name, channel_size, channel_progress
    ):
        pass

    def on_0d_data_received(self, channel_name, channel_size):
        pass

    def on_subscan_started(self, scanid, scan: typing.Optional[TomoScan]):
        pass

    def on_scan_terminated(self, metadata):
        pass

    def _get_daiquiri_scan_state(self, scan_info: dict) -> str:
        """Convert blissdata scans states to abstract scan state"""
        # BLISS 1.11
        if "state" in scan_info:
            return scan_info["state"]

        # BLISS 2.0
        from daiquiri.core.hardware.blissdata import scans

        termination = scan_info.get("end_reason", None)
        if termination is not None:
            iresult = scans.TERMINATION_MAPPING.get(termination)
            if iresult is None:
                logger.error(
                    f"No state termination mapping for scan state {termination}"
                )
                return "UNKNOWN"
        return scans.ScanStates[iresult]
