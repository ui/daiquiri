#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import typing
import enum
from ..datatype import TomoScan
from .tomo_scan import IncompatibleScanData
from .. import tomovis_client


logger = logging.getLogger(__name__)


class TilingSequenceTomoScan(TomoScan):
    def _init(self, metadata):
        tomotype = metadata.get("tomotype", None)
        if tomotype != "tilingseq":
            raise IncompatibleScanData("Not a tomo scan")

        self._state: enum.Enum = None
        self._tomovisId: int = None
        self._nb_started_subscans = 0

    def __start_tomovis_processing(self, metadata):
        tomovis_uri = self.component.tomovis_uri
        if tomovis_uri:
            filename = metadata.get("filename", None)
            scan_nb = metadata.get("scan_nb", 1)
            dataset = f"{scan_nb}.1"
            if filename is None or dataset is None:
                logger.error(
                    "Missing metadata from tiling scan sequence. Call to Tomovis service skipped."
                )
            else:
                self._tomovisId = tomovis_client.create_tiling(
                    tomovis_uri, filename, dataset
                )
                logger.info(f"Tomovis reconstruction created: {self._tomovisId}")

    def to_rest(self) -> dict:
        return {
            "scanid": self.scanid,
            "state": self._state or "UNKNOWN",
            "tomovisId": self._tomovisId,
        }

    def on_scan_started(self, metadata):
        scaninfos = self.component.get_scaninfos()
        scaninfos.last_tiling = self

        # This is not anymore available in BLISS 2.0
        # FIXME: The API have to be adapted
        state = metadata.get("state", "RUNNING")
        self._state = state

        self.__start_tomovis_processing(metadata)

        self.component.emit(
            "update_scan_info",
            {"id": "lasttiling", "data": self.to_rest()},
        )

    def on_subscan_started(self, scanid, scan: typing.Optional[TomoScan]):
        self._nb_started_subscans += 1

    def on_scan_terminated(self, metadata):
        state = self._get_daiquiri_scan_state(metadata)
        self._state = state
        self.component.emit(
            "update_scan_info",
            {"id": "lasttiling", "data": self.to_rest()},
        )
