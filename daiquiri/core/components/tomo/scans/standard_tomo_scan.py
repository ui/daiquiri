#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import typing
from .tomo_scan import TomoScan
from .tomo_scan import IncompatibleScanData
from ..datatype import DetectorLiveStorage
from ..datatype import MonitoredScan
from ..datatype import Projection


logger = logging.getLogger(__name__)


class StandardTomoScan(TomoScan):
    def _init(self, metadata):
        info = self._create_tomo_scan(self.scanid, metadata)
        if info is None:
            raise IncompatibleScanData("Not a tomo scan")
        self._info = info

    @property
    def info(self):
        return self._info

    def _get_involved_detectors(self, metadata) -> typing.List[DetectorLiveStorage]:
        """Returns the list of involved detectors.

        Empty if none.
        """
        channels = metadata.get("channels", None)
        if channels is None:
            logger.error(
                "Unexpected scan content. Channels field is mandatory. BLISS scan specification have changed."
            )
            return []
        result = []
        for detector in self.component.get_detectors():
            node_name = detector.detector_node_name
            if node_name in channels:
                result.append(detector)
        return result

    def _create_tomo_scan(self, scanid, metadata) -> typing.Optional[MonitoredScan]:
        """Create a scan structure from metadata.

        Else None, if the scan is not supported (not a tomo scan)
        """
        technique = metadata.get("technique", {})

        detectors = self._get_involved_detectors(metadata)
        if len(detectors) == 0:
            # No need to monitor that scan
            return None

        def read_exposure_time_in_second(meta):
            value = meta["exposure_time"]
            unit = scan.get("exposure_time@units", "s")
            coefs = {"s": 1, "ms": 0.001}
            if unit not in coefs:
                raise RuntimeError(f"Unsupported exposure_time unit '{unit}' in scan")
            return value * coefs[unit]

        if "dark" in technique:
            scan = technique["dark"]
            tomo_scan = "dark"
            nb_frames = scan["dark_n"]
            exposure_time = read_exposure_time_in_second(scan)
        elif "flat" in technique:
            scan = technique["flat"]
            tomo_scan = "flat"
            nb_frames = scan["flat_n"]
            exposure_time = read_exposure_time_in_second(scan)
        elif "proj" in technique:
            scan = technique["proj"]
            tomo_scan = "projection"
            nb_frames = scan["proj_n"]
            exposure_time = read_exposure_time_in_second(scan)
        elif "scan" in technique and "tomo_n" in technique["scan"]:
            scan = technique["scan"]
            tomo_scan = "projection"
            nb_frames = scan["tomo_n"]
            exposure_time = scan["exposure_time"] / 1000
        else:
            image_key = technique.get("image_key", None)
            if image_key == -1:
                tomo_scan = "return"
            else:
                tomo_scan = None
            # NOTE: BLISS 1.9 expose mesh scan npoints as numpy int64
            # cast it to avoid further json serialization fail
            nb_frames = int(metadata.get("npoints"))
            exposure_time = metadata.get("count_time")

        scan = MonitoredScan(
            scan_id=scanid,
            detectors=detectors,
            nb_frames=nb_frames,
            tomo_scan=tomo_scan,
            exposure_time=exposure_time,
        )
        return scan

    def on_scan_started(self, metadata):
        monitored_scan = self._info

        detector_ids = [d.detector_id for d in monitored_scan.detectors]
        node_names = [d.detector_node_name for d in monitored_scan.detectors]
        self.component.emit(
            "new_scan",
            {
                "scanid": self.scanid,
                "detector_ids": detector_ids,
                "node_names": node_names,
                "frame_no": monitored_scan.nb_frames - 1,
            },
        )

    def on_data_received(
        self, master, progress, channel_name, channel_size, channel_progress
    ):

        monitored_scan = self._info

        # FIXME: This could be mapped to speed up the check
        detectors = [
            d for d in monitored_scan.detectors if d.detector_node_name == channel_name
        ]
        if len(detectors) != 0:
            assert len(detectors) == 1, "Problem on the internal logic"  # nosec
            self._new_data_frame(
                monitored_scan, detectors[0], channel_size - 1, channel_progress
            )

    def _new_data_frame(
        self, monitored_scan, detector, channel_index, channel_progress
    ):
        """Triggered when a new frame was received"""

        monitored_scan.frame_id_received[detector] = channel_index

        if monitored_scan.tomo_scan not in ["dark", "flat"]:
            if detector.update_emit_time(self.component.min_refresh_period):
                monitored_scan.frame_id_sent[detector] = channel_index
                self._proj_was_taken(
                    monitored_scan, detector, channel_index, channel_progress
                )

    def on_scan_terminated(self, metadata):
        monitored_scan = self._info

        if monitored_scan.tomo_scan == "dark":
            for detector in monitored_scan.detectors:
                self._dark_was_taken(monitored_scan, detector)
        elif monitored_scan.tomo_scan == "flat":
            for detector in monitored_scan.detectors:
                self._flat_was_taken(monitored_scan, detector)
        else:
            for detector in monitored_scan.detectors:
                newer_frame_id = monitored_scan.newer_received_frame_id(detector)
                if newer_frame_id is not None:
                    detector.update_emit_time(None)
                    self._proj_was_taken(
                        monitored_scan,
                        detector,
                        frame_id=newer_frame_id,
                        progress=100,
                    )

        detector_ids = [d.detector_id for d in monitored_scan.detectors]
        node_names = [d.detector_node_name for d in monitored_scan.detectors]
        self.component.emit(
            "end_scan",
            {
                "scanid": self.scanid,
                "detector_ids": detector_ids,
                "node_names": node_names,
                "frame_no": monitored_scan.nb_frames - 1,
            },
        )

    def _dark_was_taken(
        self, monitored_scan: MonitoredScan, detector: DetectorLiveStorage
    ):
        """Triggered at the end of a scan, when a dark was taken"""
        frame_id = monitored_scan.nb_frames - 1
        data = self.source.get_scan_image(
            monitored_scan.scan_id, detector.detector_node_name, frame_id
        )
        if data is None:
            # FIXME: Could it be retrieved later?
            # Not for ct, could be for sct
            logger.error(
                "Data from scan %s frame %s was None", monitored_scan.scan_id, frame_id
            )
            return
        detector.invalidate_proj(data)
        detector.dark = Projection(
            data=data,
            exposure_time=monitored_scan.exposure_time,
            scan_id=monitored_scan.scan_id,
            frame_no=frame_id,
        )
        self.component.emit(
            "new_dark",
            {
                "scanid": monitored_scan.scan_id,
                "detector_id": detector.detector_id,
                "node_name": detector.detector_node_name,
                "has_flat": detector.flat is not None,
                "has_proj": detector.proj is not None,
            },
        )

    def _flat_was_taken(
        self, monitored_scan: MonitoredScan, detector: DetectorLiveStorage
    ):
        """Triggered at the end of a scan, when a flat was taken"""
        frame_id = monitored_scan.nb_frames - 1
        data = self.source.get_scan_image(
            monitored_scan.scan_id, detector.detector_node_name, frame_id
        )
        if data is None:
            # FIXME: Could it be retrieved later?
            # Not for ct, could be for sct
            logger.error(
                "Data from scan %s frame %s was None", monitored_scan.scan_id, frame_id
            )
            return

        detector.invalidate_proj(data)
        detector.flat = Projection(
            data=data,
            exposure_time=monitored_scan.exposure_time,
            scan_id=monitored_scan.scan_id,
            frame_no=frame_id,
        )
        self.component.emit(
            "new_flat",
            {
                "scanid": monitored_scan.scan_id,
                "detector_id": detector.detector_id,
                "node_name": detector.detector_node_name,
                "has_dark": detector.dark is not None,
                "has_proj": detector.proj is not None,
            },
        )

    def _proj_was_taken(
        self,
        monitored_scan: MonitoredScan,
        detector: DetectorLiveStorage,
        frame_id,
        progress,
    ):
        data = self.source.get_scan_image(
            monitored_scan.scan_id, detector.detector_node_name, frame_id
        )
        if data is None:
            # FIXME: Could it be retrieved later?
            # Not for ct, could be for sct
            logger.error(
                "Data from scan %s frame %s was None", monitored_scan.scan_id, frame_id
            )
            return

        sample_stage_meta = self.component.get_sample_stage_meta(detector.detector_id)
        detector.invalidate_proj(data)
        detector.proj = Projection(
            data=data,
            exposure_time=monitored_scan.exposure_time,
            scan_id=monitored_scan.scan_id,
            frame_no=frame_id,
            sample_stage_meta=sample_stage_meta,
        )
        self.component.emit(
            "new_data",
            {
                "scanid": monitored_scan.scan_id,
                "detector_id": detector.detector_id,
                "node_name": detector.detector_node_name,
                "progress": progress,
                "frame_no": frame_id,
                "has_dark": detector.dark is not None,
                "has_flat": detector.flat is not None,
            },
        )
