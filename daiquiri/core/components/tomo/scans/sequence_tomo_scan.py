#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import typing
import time
import datetime
import os.path
from ..datatype import ScanInfo
from ..datatype import Sinogram
from ..datatype import TomoScan
from .tomo_scan import IncompatibleScanData


logger = logging.getLogger(__name__)


class SequenceTomoScan(TomoScan):
    def _init(self, metadata):
        info = self._create_tomo_sequence_scan(self.scanid, metadata)
        if info is None:
            raise IncompatibleScanData("Not a tomo scan")
        self._info = info
        self._slice_reconstruction_already_requested = False

    def _create_tomo_sequence_scan(self, scanid, metadata):
        is_sequence = metadata.get("is_scan_sequence", metadata.get("is-scan-sequence"))
        if not is_sequence:
            return None

        technique = metadata.get("technique", {})
        detectors = technique.get("detector", {})
        detectors = list(detectors.keys())
        if len(detectors) != 1:
            return None
        detector = detectors[0]

        channels = metadata.get("channels", {})
        sinogram_chan = channels.get("sinogram")
        rotation_chan = channels.get("rotation")
        translation_chan = channels.get("translation")
        if sinogram_chan and rotation_chan and translation_chan:
            try:
                expected_nb_points = translation_chan["points"]
                translation_range = translation_chan["start"], translation_chan["stop"]
                translation_axis_points = translation_chan.get(
                    "axis_points", translation_chan.get("axis-points")
                )
                rotation_range = rotation_chan["start"], rotation_chan["stop"]
                rotation_axis_points = rotation_chan.get(
                    "axis_points", rotation_chan.get("axis-points")
                )
            except Exception:
                logger.error("Error while reading the sinogram meta", exc_info=True)
                logger.error("Sinogram ignored")
                sinogram = None
            else:
                sinogram = Sinogram(
                    expected_nb_points=expected_nb_points,
                    rotation_range=rotation_range,
                    translation_range=translation_range,
                    translation_axis_points=translation_axis_points,
                    rotation_axis_points=rotation_axis_points,
                )
        else:
            sinogram = None

        last_emit = None

        def on_sinogram_updated(nbpoints):
            nonlocal last_emit
            current = time.time()
            if last_emit is None or (current - last_emit) > 1:
                # Reduce the emit frequency
                last_emit = current
                self.component.emit(
                    "update_scan_info_sinogram",
                    {"id": "lastgroup", "scanid": scanid, "actualnbpoints": nbpoints},
                )

        if sinogram:
            sinogram.connect_sinogram_updated(on_sinogram_updated)

        def on_active_subscan_updated(active_subscan):
            self.component.emit(
                "update_scan_info_subscan",
                {"id": "lastgroup", "scanid": scanid, "activesubscan": active_subscan},
            )

        daiquiri_meta = metadata.get("daiquiri", {})
        if len(daiquiri_meta) == 0:
            logger.warning("The scan sequence does not contain any daiquiri metadata")
        datacollectionid = daiquiri_meta.get("datacollectionid")
        datacollectiongroupid = daiquiri_meta.get("datacollectiongroupid")
        sampleid = daiquiri_meta.get("sampleid")
        sessionid = daiquiri_meta.get("sessionid")
        start_time = datetime.datetime.fromisoformat(metadata["start_time"])

        def read_subscans(technique):
            subscans = technique.get("subscans", None)
            if subscans is None:
                # Assume there is no information
                return None
            try:
                result = [(int(k[4:]), v["type"]) for k, v in subscans.items()]
                result = sorted(result)
                result = [i[1] for i in result]
                return result
            except Exception:
                logger.error("Error while parsing subscan info", exc_info=True)
                return []

        subscans = read_subscans(technique)

        # This is not anymore available in BLISS 2.0
        # FIXME: The API have to be adapted
        state = metadata.get("state", "RUNNING")
        info = ScanInfo(
            scan_id=scanid,
            group_id=None,
            state=state,
            detector_id=detector,
            sinogram=sinogram,
            data_collection_id=datacollectionid,
            data_collection_group_id=datacollectiongroupid,
            sample_id=sampleid,
            session_id=sessionid,
            subscans=subscans,
            start_time=start_time,
        )
        info.connect_active_subscan_updated(on_active_subscan_updated)
        return info

    def to_rest(self) -> dict:
        return self._info.to_rest()

    def on_scan_started(self, metadata):
        monitored_sequence = self._info

        if monitored_sequence.data_collection_id is None:
            has_daiquiri_info = (
                monitored_sequence.session_id is not None
                and monitored_sequence.sample_id is not None
            )
            if not has_daiquiri_info:
                # FIXME: this have to be handled at some point, to me it's critical
                logger.warning("The scan can't be registered inside Daiquiri")
                monitored_sequence.handle_datacollection = False
            else:
                kwargs = {}
                kwargs["sessionid"] = monitored_sequence.session_id
                kwargs["datacollectionnumber"] = self.scanid
                if monitored_sequence.data_collection_group_id is not None:
                    kwargs[
                        "datacollectiongroupid"
                    ] = monitored_sequence.data_collection_group_id
                # FIXME: This is definitaly wrong if launched from BLISS
                kwargs["imagecontainersubpath"] = "1.1/measurement"
                kwargs["experimenttype"] = "Tomo"
                kwargs["starttime"] = monitored_sequence.start_time
                if monitored_sequence.sample_id:
                    kwargs["sampleid"] = monitored_sequence.sample_id

                filename = metadata["filename"]
                kwargs["imagedirectory"] = os.path.dirname(filename)
                kwargs["filetemplate"] = os.path.basename(filename)
                dc = self.component._metadata.add_datacollection(**kwargs)
                datacollectionid = dc["datacollectionid"]
                for client_name in ["stomp", "celery"]:
                    client = self.component.get_component(client_name)
                    if client:
                        client.send_event(datacollectionid, "start")
                monitored_sequence.data_collection_id = datacollectionid
                monitored_sequence.handle_datacollection = True

        scaninfos = self.component.get_scaninfos()
        scaninfos.last_group = self

        self.component.emit(
            "update_scan_info",
            {"id": "lastgroup", "data": self.to_rest()},
        )

    def on_subscan_started(self, scanid, scan: typing.Optional[TomoScan]):
        monitored_sequence = self._info
        monitored_sequence.increment_active_subscan()
        if scan is not None:
            try:
                tomo_scan = scan.info.tomo_scan
            except Exception:
                logger.error("Unsupported scan", exc_info=True)
                tomo_scan = None
            if tomo_scan == "return":
                self._request_slice_reconstruction_once("return")

    def on_0d_data_received(self, channel_name, channel_size):
        self._info.channel_updated(channel_name, channel_size)

    def on_scan_terminated(self, metadata):
        monitored_sequence = self._info
        state = self._get_daiquiri_scan_state(metadata)
        monitored_sequence.state = state

        if monitored_sequence.handle_datacollection is not None:
            if monitored_sequence.data_collection_id is not None:
                self.component._metadata.update_datacollection(
                    datacollectionid=monitored_sequence.data_collection_id,
                    no_context=True,
                    endtime=datetime.datetime.now(),
                    runstatus="Successful",
                )

                celery = self.component.get_component("celery")
                if celery:
                    celery.send_event(monitored_sequence.data_collection_id, "end")

        sinogram = monitored_sequence.sinogram
        if sinogram:
            self.component.emit(
                "update_scan_info_sinogram",
                {
                    "id": "lastgroup",
                    "scanid": self.scanid,
                    "actualnbpoints": sinogram.actual_nb_points,
                },
            )
        self.component.emit(
            "update_scan_info",
            {"id": "lastgroup", "data": self.to_rest()},
        )
        self._request_slice_reconstruction_once("terminated")

    def _request_slice_reconstruction_once(self, event):
        """Request slice reconstruction if it was not already done."""
        if self._slice_reconstruction_already_requested:
            return
        if event not in self.component.slice_reconstruction_triggers:
            return
        self._slice_reconstruction_already_requested = True

        monitored_sequence = self._info
        if monitored_sequence.data_collection_id is not None:
            if monitored_sequence.sinogram is not None:
                parameters = {}
                deltabeta = self.component.last_delta_beta
                if deltabeta is not None:
                    parameters["deltabeta"] = deltabeta
                celery = self.component.get_component("celery")
                if celery:
                    celery.execute_graph(
                        "tomo-sinogram-reconstruction",
                        monitored_sequence.data_collection_id,
                        parameters=parameters,
                    )
