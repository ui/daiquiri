#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

from daiquiri.core import marshal
from daiquiri.core.components import ComponentResource

from .datatype import TomoScan


logger = logging.getLogger(__name__)


class TomoScanInfoResource(ComponentResource):
    @marshal(
        # out=[[200, paginated(TomoDetectorsSchema), "List of tomo scan info"]],
        # paged=True,
    )
    def get(self, **kwargs):
        """Get a list of all last scan info"""
        scaninfo = self._parent.get_scaninfos()

        def scan_info_meta(si: TomoScan):
            if si is None:
                return {}
            return si.to_rest()

        result = {
            "lastgroup": scan_info_meta(scaninfo.last_group),
            "lastproj": scan_info_meta(scaninfo.last_proj),
            "lastflat": scan_info_meta(scaninfo.last_flat),
            "lastdark": scan_info_meta(scaninfo.last_dark),
            "lastref": scan_info_meta(scaninfo.last_ref),
            "lasttiling": scan_info_meta(scaninfo.last_tiling),
        }
        return result, 200
