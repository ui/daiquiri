#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations
import numpy
import logging
import dataclasses
import time
import datetime
import pint
from .scans.tomo_scan import TomoScan


logger = logging.getLogger(__name__)


@dataclasses.dataclass
class SampleStageMetadata:
    """Store information relative to the sample stage"""

    sz: pint.Quantity

    sy: pint.Quantity

    sampy: pint.Quantity

    somega: pint.Quantity
    """We have to think about if we need user+dial"""

    detcy: pint.Quantity
    """Detector center in the sample stage referencial"""

    detcz: pint.Quantity
    """Detector center in the sample stage referencial"""

    pixel_size: pint.Quantity


@dataclasses.dataclass
class Sinogram:
    expected_nb_points: int
    """Expected number of points in the sinogram"""

    rotation_range: tuple[float, float] | None
    """Range on the rotation axis"""

    rotation_axis_points: int
    """Number of points in the rotation axis"""

    translation_range: tuple[float, float] | None
    """Range on the translation axis"""

    translation_axis_points: int
    """Number of points in the translation axis"""

    _actual_nb_points: int = 0
    """Actual number of points in the sinogram"""

    _actual_channel_points = [0, 0, 0]

    _sinogram_updated = None

    @property
    def actual_nb_points(self):
        return self._actual_nb_points

    def connect_sinogram_updated(self, callback):
        assert self._sinogram_updated is None  # nosec
        self._sinogram_updated = callback

    def channel_updated(self, channel_name, channel_size):
        channel_names = ["sinogram", "rotation", "translation"]
        try:
            index = channel_names.index(channel_name)
        except ValueError:
            return
        self._actual_channel_points[index] = channel_size
        nb = min(self._actual_channel_points)
        if nb <= self._actual_nb_points:
            return
        self._actual_nb_points = nb
        if self._sinogram_updated:
            self._sinogram_updated(nb)

    def to_rest(self):
        result = {
            "actualnbpoints": self._actual_nb_points,
            "expectednbpoints": self.expected_nb_points,
            "rotationrange": self.rotation_range,
            "translationrange": self.translation_range,
            "rotationaxispoints": self.rotation_axis_points,
            "translationaxispoints": self.translation_axis_points,
        }
        return result


@dataclasses.dataclass
class ScanInfo:
    scan_id: int
    """Daiquiri identifier of the scan"""

    group_id: int | None
    """Daiquiri identifier of the group scan, if one"""

    start_time: datetime.datetime | None = None
    """Start time of this scan"""

    frame_no: int | None = None
    """Identifier of the frame in this scan"""

    state: str | None = None
    """State of the scan

    Which is the name if state from `bliss.scanning.scan.ScanState`
    """

    detector_id: str | None = None
    """Name id of the detector"""

    sinogram: Sinogram | None = None
    """Dedicated information related to the sinogram"""

    handle_datacollection: bool = False
    """If true the tomo component have to handle the live cycle of the datacollection"""

    data_collection_id: int | None = None
    """Data collection id of this scan if some from Daiquiri"""

    data_collection_group_id: int | None = None
    """Data collection group id of this scan if some from Daiquiri"""

    sample_id: int | None = None
    """Sample id from Daiquiri"""

    session_id: int | None = None
    """session id from Daiquiri"""

    subscans: list[str] | None = None
    """List containing the expected sequence of scans with dedicated roles"""

    active_subscan = -1
    """Index of the active subscan"""

    _active_subscan_updated = None

    def connect_active_subscan_updated(self, callback):
        assert self._active_subscan_updated is None  # nosec
        self._active_subscan_updated = callback

    def increment_active_subscan(self):
        self.active_subscan += 1
        if self._active_subscan_updated is not None:
            self._active_subscan_updated(self.active_subscan)

    def channel_updated(self, channel_name, channel_size):
        if self.sinogram is not None:
            self.sinogram.channel_updated(channel_name, channel_size)

    def to_rest(self):
        state = self.state
        if not isinstance(state, str):
            state = "UNKNOWN"

        result = {
            "scanid": self.scan_id,
            "frame_no": self.frame_no,
            "groupid": self.group_id,
            "detectorid": self.detector_id,
            "state": state,
            "datacollectionid": self.data_collection_id,
            "datacollectiongroupid": self.data_collection_group_id,
            "subscans": self.subscans,
            "activesubscan": self.active_subscan,
        }
        sinogram = self.sinogram
        if sinogram is not None:
            result["sinogram"] = sinogram.to_rest()
        return result


@dataclasses.dataclass
class Projection:
    """Store information from a single projection"""

    data: numpy.NdArray | None
    """Raw data from Lima detector"""

    scan_id: int
    """Daiquiri identifier of the scan"""

    frame_no: int
    """Identifier of the frame in this scan"""

    exposure_time: float
    """Exposure time used by the actual proj."""

    sample_stage_meta: SampleStageMetadata | None = None
    """Metadata saved from the sample stage at the time of this projection"""

    _normalized: numpy.NdArray | None = None
    """Normalized data by exposure time"""

    @property
    def normalized(self):
        if self._normalized is None:
            self._normalized = self.data.astype(numpy.float32) / self.exposure_time
        return self._normalized


@dataclasses.dataclass
class DetectorLiveStorage:
    """Store data relative to a detector during the live of the server"""

    detector_id: str
    """Identifier of the Lima detector"""

    tomo_detector_id: str
    """Identifier of the tomo detector BLISS controller"""

    detector_node_name: str
    """Identifier of the detector channel

    In BLISS theoretically this name could be changed. Let assume it's fixed.
    """

    proj: Projection = None
    """
    Hold data used a projection (not a dark, not a flat)
    """

    flat: Projection = None
    """
    Hold data used as flat for flat field correction.
    """

    dark: Projection = None
    """
    Image used as dark for flat field correction.

    Float 2D data normalized by integration time."""

    last_emit_time = 0
    """Time of the last emit frame through client"""

    def __hash__(self):
        return self.detector_id.__hash__()

    def invalidate_proj(self, data: numpy.NdArray):
        """Invalidate every proj which does not match the new data size"""
        shape = data.shape
        if self.dark is not None:
            if self.dark.data.shape != shape:
                self.dark = None
        if self.flat is not None:
            if self.flat.data.shape != shape:
                self.flat = None
        if self.proj is not None:
            if self.proj.data.shape != shape:
                self.proj = None

    def update_emit_time(self, min_delay):
        """Update the emit time if min_delay is respected.

        Returns True if a new emit can be done.

        Arguments:
            min_delay: Minimal delay between 2 signals. If None, there is
                       no limitation.
        """
        now = time.time()
        if min_delay is not None:
            if (now - self.last_emit_time) < min_delay:
                return False
        self.last_emit_time = now
        return True

    def to_rest(self):
        result = {
            "detector_id": self.detector_id,
            "tomo_detector_id": self.tomo_detector_id,
            "node_name": self.detector_node_name,
        }
        if self.dark is not None:
            result["dark"] = 1
        if self.flat is not None:
            result["flat"] = 1
        if self.proj is not None:
            result["data"] = {
                "scanid": self.proj.scan_id,
                "frame_no": self.proj.frame_no,
            }
        return result


@dataclasses.dataclass
class ScanInfoStorage:
    last_group: TomoScan = None
    last_flat: TomoScan = None
    last_dark: TomoScan = None
    last_ref: TomoScan = None
    last_proj: TomoScan = None
    last_tiling: TomoScan = None


@dataclasses.dataclass
class MonitoredScan:
    """Store data relative to a specific scan"""

    scan_id: int
    """Identifier of the scan"""

    exposure_time: float
    """Exposure time for a single frame in second"""

    nb_frames: int
    """Number of frames taken by this scan"""

    tomo_scan: str
    """Content of the tomo_scan field"""

    detectors: list[DetectorLiveStorage]
    """Node names containing tomo image"""

    last_frame_no_received: dict[DetectorLiveStorage, int] | None = None
    """Frames received from the scan"""

    frame_id_received: dict[DetectorLiveStorage, int] | None = None
    """Frames received from the scan"""

    frame_id_sent: dict[DetectorLiveStorage, int] | None = None
    """Frames sent to the client as event"""

    def __post_init__(self):
        self.last_frame_no_received = {}
        self.frame_id_received = {}
        self.frame_id_sent = {}

    def newer_received_frame_id(self, detector):
        """Returns the newer frame than the one already emitted, if one

        Else returns None
        """
        received = self.frame_id_received.get(detector, -1)
        if received == -1:
            return None
        if received <= self.frame_id_sent.get(detector, -1):
            return None
        return received
