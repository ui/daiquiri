#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import fields

from daiquiri.core import marshal
from daiquiri.core.components import ComponentResource
from daiquiri.core.schema import ErrorSchema


logger = logging.getLogger(__name__)


class SliceReconstructionResource(ComponentResource):
    @marshal(
        inp={
            "datacollectionid": fields.Number(
                metadata={"description": "Data collection id to process"}
            ),
            "axisposition": fields.Number(
                metadata={"description": "Position of the axis, in pixel"}
            ),
            "deltabeta": fields.Number(
                metadata={"description": "Delta/beta value for the reconstruction"}
            ),
        },
        out=[
            # [200, ScanDataSchema(), 'Scan data'],
            [404, ErrorSchema(), "Data collection id is not available"]
        ],
    )
    def get(
        self,
        datacollectionid=None,
        axisposition=None,
        deltabeta=None,
        **kwargs,
    ):
        """Get a list of all available detectors and their info"""
        if datacollectionid is None:
            return {
                "error": "Data collection id '%s' is not available" % datacollectionid
            }, 404

        tomo_component = self._parent
        graph_name = "tomo-sinogram-reconstruction"
        if deltabeta is not None:
            tomo_component.set_last_delta_beta(deltabeta)

        try:
            celery = tomo_component.get_component("celery")
            if celery:
                celery.execute_graph(
                    graph_name,
                    datacollectionid,
                    parameters={"axisposition": axisposition, "deltabeta": deltabeta},
                )
        except Exception:
            logger.error(
                "Error while triggering '%s' workflow", graph_name, exc_info=True
            )
            return {
                "error": f"Problem occurred during {graph_name} workflow triggering"
            }, 404
        return "OK", 200
