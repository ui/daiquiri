from .component import TomoComponent


# This expose the component to be used in yml files
# The class name is used as the service name
class Tomo(TomoComponent):
    pass
