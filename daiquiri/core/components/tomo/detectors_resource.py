#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

from daiquiri.core import marshal
from daiquiri.core.components import ComponentResource
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.components.tomo import TomoDetectorsSchema


logger = logging.getLogger(__name__)


class TomoDetectorsResource(ComponentResource):
    @marshal(
        out=[[200, paginated(TomoDetectorsSchema), "List of tomo detectors info"]],
        paged=True,
    )
    def get(self, **kwargs):
        """Get a list of all available detectors and their info"""
        detectors = self._parent.get_detectors()
        results = [d.to_rest() for d in detectors]
        return {"rows": results, "total": len(results)}, 200
