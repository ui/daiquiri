#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import collections
import math

from abc import abstractmethod
from typing import Dict, List, Optional, OrderedDict, Tuple, Type, Union
import gevent
import time

import numpy as np
import h5py

import h5grove
from h5grove.content import DatasetContent
from h5grove.utils import get_array_stats

from flask import Response
from marshmallow import Schema, fields

from fabio.cbfimage import CbfImage

from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.responses import gzipped
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.utils import worker
from daiquiri.resources.utils import YamlDict


import logging

CacheEntry = Tuple[np.ndarray, dict]


logger = logging.getLogger(__name__)


class NodeSchema(Schema):
    """Node Schema"""

    path = fields.Str()
    name = fields.Str()
    type = fields.Str()
    ext = fields.Str()
    hdr = fields.Dict()
    last_modified = fields.Float()


class FileBrowserDirectorySchema(Schema):
    """File Schema"""

    abs_path = fields.Str()
    rows = fields.Nested(NodeSchema, many=True)
    total = fields.Int()


class FileBrowserDirectoryResource(ComponentResource):
    @marshal(
        inp={"path": fields.Str()},
        out=[
            [
                200,
                FileBrowserDirectorySchema,
                "Directory listing for current resources",
            ]
        ],
    )
    def get(self, path: str, **kwargs):
        """Get the directory listing"""
        return self._parent.get_directory(path, **kwargs)


class LoadFileResource(ComponentResource):
    @marshal(
        inp={"path": fields.Str()},
        out=[
            [200, NodeSchema(), "Read file contents"],
            [400, ErrorSchema(), "Could not read file contents"],
        ],
    )
    def get(self, path: str, **kwargs):
        """Get a file"""
        file = self._parent.fb_load_file(path, **kwargs)

        if file is None:
            return {"message": "Could not read file"}, 400
        return file


class FileResource(ComponentResource):
    @marshal(
        inp={"path": fields.Str()},
        out=[[400, ErrorSchema(), "Could not read file contents"]],
    )
    def get(self, path: str, **kwargs) -> Union[Response, Tuple[dict, int]]:
        """Get a file"""
        file = self._parent.get_file_content(path, **kwargs)
        if file is None:
            return {"message": "Could not read file"}, 400

        def gzip():
            return gzipped(file)

        return worker(gzip)


class HistResource(ComponentResource):
    @marshal(
        inp={"path": fields.Str()},
        out=[[400, ErrorSchema(), "Could not read hist data"]],
    )
    def get(self, path: str, **kwargs) -> Union[Response, Tuple[dict, int]]:
        data = self._parent.get_histogram_data(path, **kwargs)
        if data is None:
            return {"message": "Could not read hist data"}, 400

        def gzip():
            return gzipped(data)

        return worker(gzip)


class HistMetaResource(ComponentResource):
    @marshal(
        inp={"path": fields.Str()},
        out=[[400, ErrorSchema(), "Could not read file contents"]],
    )
    def get(self, path: str, **kwargs) -> Union[Response, Tuple[dict, int]]:
        metadata = self._parent.get_histogram_meta(path, **kwargs)
        if metadata is None:
            return {"message": "Could not read hist metadata"}, 400

        return metadata


class Filebrowser(Component):
    _base_url = "filebrowser"
    _require_blsession = False
    _CACHE_LIMIT = 100

    def setup(self):
        assert isinstance(self._config, YamlDict)  # nosec
        self.register_route(FileBrowserDirectoryResource, "/directory")
        self.register_route(LoadFileResource, "/loadfile")
        self.register_route(FileResource, "/file")
        self.register_route(HistMetaResource, "/loadhist")
        self.register_route(HistResource, "/hist")
        self._root_path: str = os.path.normpath(self._config.get("root", "/"))
        self._file_types: List[str] = self._config.get("file_types", [])
        self._show_hidden: bool = self._config.get("show_hidden", False)
        self._container_file_types: List[str] = self._config.get(
            "container_file_types", []
        )
        self._file_cache: OrderedDict[str, CacheEntry] = collections.OrderedDict()
        self._container_handlers: Dict[str, ContainerHandler] = {
            **{
                ext: FSContainerHandler(
                    self._root_path, self._file_types, self._show_hidden
                )
                for ext in FSContainerHandler.EXT
            },
            **{
                ext: HDF5ContainerHandler(
                    self._root_path, self._file_types, self._show_hidden
                )
                for ext in HDF5ContainerHandler.EXT
            },
        }
        self._format_handlers: Dict[str, Type[FormatHandler]] = {
            **{ext: HDF5FormatHandler for ext in HDF5FormatHandler.EXT},
            **{ext: CBFFormatHandler for ext in CBFFormatHandler.EXT},
        }

        self.current_folder_watcher = gevent.spawn(
            self.watch_file_creation, self._root_path
        )

    def watch_file_creation(self, folder):
        ext = get_file_ext(folder)

        # Disable watching for HDF5 "folders"
        if ext in HDF5ContainerHandler.EXT:
            return

        old_listing = {f: None for f in os.listdir(folder)}
        while True:
            time.sleep(2)
            new_listing = {f: None for f in os.listdir(folder)}
            new_files = [f for f in new_listing if f not in old_listing]
            for f in new_files:
                self.emit(
                    "new_file_in_dir",
                    {"dir_path": os.path.relpath(folder, self._root_path), "file": f},
                )
            old_listing = new_listing

    def _add_to_cache(self, path: str, data: CacheEntry):
        if len(self._file_cache) > self._CACHE_LIMIT:
            self._file_cache.popitem(last=False)

        self._file_cache[path] = data

    def _get_root_path(self, path: str) -> str:
        abs_path = os.path.normpath(os.path.join(self._root_path, path))

        # Restrict listing to root folder
        if (
            os.path.commonpath([self._root_path, abs_path]) != self._root_path
            or self._root_path == abs_path
        ):
            abs_path = self._root_path

        return abs_path

    def get_directory(
        self, path: str
    ) -> Optional[Dict[str, Union[int, list, str, float]]]:
        abs_path = self._get_root_path(path)

        if not os.path.exists(abs_path):
            logger.warning(f"Path {abs_path} does not exist")
            return None

        raw_ext = get_file_ext(abs_path)

        # Default to file system listing if no container handler is found
        ext = raw_ext if raw_ext != "" else "FS"

        nodes = (
            self._container_handlers[ext].list_entry(abs_path)
            if ext in self._container_handlers
            else []
        )

        self.current_folder_watcher.kill()
        self.current_folder_watcher = gevent.spawn(self.watch_file_creation, abs_path)

        return {"total": len(nodes), "rows": nodes, "abs_path": abs_path}

    def _make_path_absolute(self, path: str) -> str:
        path = os.path.normpath(path)
        abs_path = os.path.normpath(os.path.join(self._root_path, path))

        if (
            os.path.commonpath([self._root_path, abs_path]) != self._root_path
            or self._root_path == abs_path
        ):
            raise IOError(f"Can't access {abs_path}")

        return abs_path

    def fb_load_file(self, path: str) -> Optional[dict]:
        try:
            abs_path = self._make_path_absolute(path)
        except IOError as e:
            logging.warning(e)
            return None

        ext = get_file_ext(abs_path)

        if abs_path not in self._file_cache:
            try:
                format_handler = self._format_handlers[ext]
            except KeyError:
                logger.warning(f"File empty or format not handled {abs_path}")
                return

            try:
                hdr, data, _ = format_handler.preload(abs_path)
            except RuntimeError:
                logger.warning(f"File {abs_path} is not a file")
                return

            self._add_to_cache(abs_path, (data, hdr))
        else:
            data, hdr = self._file_cache[abs_path]

        return {
            "path": abs_path,
            "name": os.path.basename(abs_path),
            "type": "file",
            "ext": ext,
            "hdr": hdr,
            "last_modified": os.path.getmtime(
                abs_path if os.path.isfile(abs_path) else os.path.dirname(abs_path)
            ),  # File can be a h5py dataset. In this case, take last modified data of the parent file,
        }

    def get_file_content(self, path: str) -> np.ndarray:
        abs_path = self._make_path_absolute(path)

        try:
            data, _ = self._file_cache[abs_path]
            return data
        except KeyError:
            logger.warning(f"File {abs_path} not in cache (not loaded)")
            # Load file in cache and retry
            self.fb_load_file(path)
            return self.get_file_content(path)

    def retrieve_histogram(self, path: str) -> CacheEntry:
        abs_path = self._make_path_absolute(path)
        ext = get_file_ext(abs_path)

        cache_key = abs_path + "/hist"
        if cache_key not in self._file_cache:
            format_handler = self._format_handlers[ext]
            _, data, _ = format_handler.preload(abs_path)
            hist, bins = _compute_histogram(data)

            hist_meta = {
                "bins": bins,
                "shape": hist.shape,
                "max": np.max(hist).item() if hist.size > 0 else 0,
            }
            self._add_to_cache(cache_key, (hist.astype(np.float32), hist_meta))

        return self._file_cache[cache_key]

    def get_histogram_data(self, path: str) -> np.ndarray:
        hist_data, _ = self.retrieve_histogram(path)

        return hist_data

    def get_histogram_meta(self, path: str) -> dict:
        _, hist_meta = self.retrieve_histogram(path)

        return hist_meta


class ContainerHandler:
    EXT: List[str]

    def __init__(self, root_path="", file_types="", show_hidden=False):
        self._root_path = root_path
        self._show_hidden = show_hidden
        self._file_types = file_types

    @abstractmethod
    def list_entry(self, path: str) -> List[dict]:
        raise NotImplementedError


class HDF5ContainerHandler(ContainerHandler):
    EXT = ["hdf5", "h5"]

    def list_entry(self, path) -> List[dict]:
        ext = get_file_ext(path)
        path_relative_to_root = os.path.relpath(path, self._root_path)

        with h5py.File(path, "r") as h5file:
            if "/entry/data/data" not in h5file:
                return []

            data_attrs = _get_dataset_attr(h5file, "/entry/data/data")
            image_nr_low = data_attrs["image_nr_low"]
            assert isinstance(image_nr_low, np.generic)  # nosec

            image_nr_high = data_attrs["image_nr_high"]
            assert isinstance(image_nr_high, np.generic)  # nosec

        return [
            {
                "path": os.path.normpath(
                    os.path.join(path_relative_to_root, f"image_{n}.{ext}.dataset")
                ),
                "name": f"image_{n}.{ext}.dataset",
                "type": "file",
                "ext": ext,
                # Add n to the date of the parent file to simulate the last modification date
                "last_modified": os.path.getmtime(path) + n,
            }
            for n in range(image_nr_low.item(), image_nr_high.item() + 1)
        ]


class FSContainerHandler(ContainerHandler):
    EXT = ["FS"]

    def list_entry(self, path: str) -> List[dict]:
        nodes = []
        path_relative_to_root = os.path.relpath(path, self._root_path)

        with os.scandir(path) as dir_entries:
            for entry in dir_entries:
                if not self._show_hidden and entry.name.startswith("."):
                    continue

                ext = get_file_ext(entry.name) if entry.is_file() else ""
                if entry.is_file() and ext not in self._file_types:
                    continue

                nodes.append(
                    {
                        "path": os.path.normpath(
                            os.path.join(path_relative_to_root, entry.name)
                        ),
                        "name": entry.name,
                        "type": "file"
                        if entry.is_file() and ext not in ["h5", "hdf5"]
                        else "dir",
                        "ext": ext,
                        "last_modified": os.path.getmtime(entry),
                    }
                )

        return nodes


class FormatHandler:
    EXT: List[str]

    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def preload(path: str) -> Tuple[dict, np.ndarray, bytes]:
        raise NotImplementedError()


class CBFFormatHandler(FormatHandler):
    EXT = ["cbf"]

    @staticmethod
    def preload(path: str) -> Tuple[dict, np.ndarray, bytes]:
        if not os.path.isfile(path):
            raise RuntimeError("Files does not exist")

        cbf_image = CbfImage(fname=path)
        float_data = cbf_image.data.astype(np.float32)

        preview_data = CBFFormatHandler._8bit_raw_repr(cbf_image)

        parsed_ext_hdr, braggy_hdr = CBFFormatHandler._parse_header(
            cbf_image, float_data
        )

        img_hdr = {}
        img_hdr["parsed_ext_hdr"] = parsed_ext_hdr
        img_hdr["braggy_hdr"] = braggy_hdr

        return img_hdr, float_data.flatten(), preview_data

    @staticmethod
    def _8bit_raw_repr(raw_data: CbfImage) -> bytes:
        data = raw_data.data.clip(0)
        data = data.astype(np.uint8)

        return data.tobytes()

    @staticmethod
    def _parse_header(cbf_image: CbfImage, np_array: np.ndarray) -> Tuple[dict, dict]:
        height, width = cbf_image.shape

        hdr = cbf_image.header
        parsed_ext_hdr = {}
        braggy_hdr = {}

        _ext_hdr = hdr.get("_array_data.header_contents", "").split("\r\n")
        for data in _ext_hdr:
            # Ignore empty lines coming from multiple line-breaks
            if data == "":
                continue

            key_value = data.strip("#").strip().split()

            key = key_value[0].strip(":").strip()
            value = " ".join(key_value[1:])
            parsed_ext_hdr[key] = value
        try:
            w = float(parsed_ext_hdr.get("Wavelength", "0").strip("A "))
            d = float(parsed_ext_hdr.get("Detector_distance", "0").strip("m "))

            bcx, bcy = parsed_ext_hdr["Beam_xy"].split(",")
            bcx, bcy = float(bcx.strip("pixels() ")), float(bcy.strip("pixels() "))

            px_size_x, px_size_y = parsed_ext_hdr.get("Pixel_size", "0").split("x")
            px_size_x, px_size_y = (
                float(px_size_x.strip("m ")),
                float(px_size_y.strip("m ")),
            )

            dr = math.sqrt((px_size_x * width) ** 2 + (px_size_y * height) ** 2) / 2

            # Remove invalid values (-1)
            clean_np_array = np_array[np_array >= 0]

            braggy_hdr = {
                "wavelength": w,
                "detector_distance": d,
                "beam_cx": bcx,
                "beam_cy": bcy,
                "beam_ocx": (width / 2) - bcx,
                "beam_ocy": (height / 2) - bcy,
                "detector_radius": dr,
                "pixel_size_x": px_size_x,
                "pixel_size_y": px_size_y,
                "img_width": width,
                "img_height": height,
                "pxxpm": 1 / px_size_x,
                "pxypm": 1 / px_size_y,
                **get_array_stats(
                    clean_np_array if clean_np_array.size > 0 else np_array
                ),
            }
        except (KeyError, IndexError):
            logging.info("Could not create Braggy header from CBF header")

        return parsed_ext_hdr, braggy_hdr


class HDF5FormatHandler(FormatHandler):
    EXT = ["dataset"]

    @staticmethod
    def preload(path: str) -> Tuple[dict, np.ndarray, bytes]:
        h5path, img_num = HDF5FormatHandler._interpret_path(path)

        with h5py.File(h5path, "r") as h5file:
            image_nr_low = _get_dataset_attr(h5file, "/entry/data/data")["image_nr_low"]
            assert isinstance(image_nr_low, np.generic)  # nosec

            idx = img_num - image_nr_low.item()
            data = _get_dataset_data(h5file, "/entry/data/data", str(idx))
            assert isinstance(data, np.ndarray)  # nosec

        np_array = data.astype(np.float32)
        preview_data = data.clip(0).astype(np.uint8).tobytes()

        img_hdr = HDF5FormatHandler._get_hdr(path, np_array)

        return img_hdr, np_array, preview_data

    @staticmethod
    def _get_hdr(path: str, np_array: np.ndarray) -> Dict[str, dict]:
        _, ext = os.path.splitext(path.rstrip(".dataset"))
        prefix, _ = path.split("_data")
        mfpath = prefix + "_master" + ext

        with h5py.File(mfpath, "r") as h5file:
            wavelength = _get_instrument_param(h5file, "beam/incident_wavelength")
            detector = _get_instrument_param(h5file, "detector/detector_distance")

            pixel_size_x = _get_instrument_param(h5file, "detector/x_pixel_size")
            pixel_size_y = _get_instrument_param(h5file, "detector/y_pixel_size")
            width = _get_instrument_param(
                h5file, "detector/detectorSpecific/x_pixels_in_detector"
            )
            height = _get_instrument_param(
                h5file, "detector/detectorSpecific/y_pixels_in_detector"
            )

            beam_cx = _get_instrument_param(h5file, "detector/beam_center_x")
            beam_cy = _get_instrument_param(h5file, "detector/beam_center_y")

        # Remove invalid values (SATURATION VALUES)
        clean_np_array = np_array[np_array != np.max(np_array)]

        braggy_hdr = {
            "wavelength": wavelength,
            "detector_distance": detector,
            "beam_cx": beam_cx,
            "beam_cy": beam_cy,
            "beam_ocx": (width / 2) - beam_cx,
            "beam_ocy": (height / 2) - beam_cy,
            "detector_radius": (width * pixel_size_x) / 2,
            "pixel_size_x": pixel_size_x,
            "pixel_size_y": pixel_size_y,
            "img_width": width,
            "img_height": height,
            "pxxpm": 1 / pixel_size_x,
            "pxypm": 1 / pixel_size_y,
            **get_array_stats(clean_np_array if clean_np_array.size > 0 else np_array),
        }

        return {"braggy_hdr": braggy_hdr}

    @staticmethod
    def _interpret_path(path: str) -> Tuple[str, int]:
        h5path, dataset_path = os.path.split(path)
        _, imgnum_suffix = dataset_path.rstrip(".dataset").split("image_")
        imgnum, _ = imgnum_suffix.split(".")

        return h5path, int(imgnum)


def _get_dataset_attr(h5file: h5py.File, dset_path: str):
    dset_content = h5grove.create_content(h5file, dset_path)
    assert isinstance(dset_content, DatasetContent)  # nosec
    return dset_content.attributes()


def _get_dataset_data(
    h5file: h5py.File, dset_path: str, selection: Optional[str] = None
):
    dset_content = h5grove.create_content(h5file, dset_path)
    assert isinstance(dset_content, DatasetContent)  # nosec
    return dset_content.data(selection)


def _get_instrument_param(h5file: h5py.File, param_path: str):
    data = _get_dataset_data(h5file, f"/entry/instrument/{param_path}")
    assert isinstance(data, np.generic)  # nosec
    return data.item()


def _compute_histogram(data: np.ndarray) -> Tuple[np.ndarray, list]:
    std = 3 * np.std(data)
    mean = np.mean(data)
    clean_data = data[data < mean + std]

    if clean_data.size == 0:
        return np.ndarray([]), []

    hist, bins = np.histogram(
        clean_data.flatten(),
        bins=np.arange(np.min(clean_data), np.max(clean_data), 1)
        if np.max(clean_data) <= 300
        else 300,
    )
    return hist, bins.tolist()


def get_file_ext(file_name: str):
    _, ext = os.path.splitext(file_name)
    return ext[1:]  # Remove leading dot
