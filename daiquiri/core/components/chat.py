#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import logging

from marshmallow import Schema, fields
from flask import g

from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.schema.metadata import paginated

logger = logging.getLogger(__name__)


class NewChatSchema(Schema):
    """New Chat Message Schema"""

    message = fields.Str(required=True, metadata={"description": "The message"})


class ChatSchema(NewChatSchema):
    """Chat Message Schema"""

    messageid = fields.Int(metadata={"description": "Unique id of the message"})
    timestamp = fields.Int(metadata={"description": "Epoch timestamp message was sent"})
    sessionid = fields.Str(metadata={"description": "Author sessionid"})
    user = fields.Str(metadata={"description": "User name associated with sessionid"})
    read = fields.List(
        fields.Str(),
        metadata={"description": "List of sessions that have read the message"},
    )


class AllMessagesReadSchema(Schema):
    """All messages read schema"""

    maxid = fields.Int(metadata={"description": "Maximum chat messageid"})


class ChatResource(ComponentResource):
    @marshal(
        inp={"limit": fields.Int(), "offset": fields.Int()},
        out=[[200, paginated(ChatSchema), "List of chat messages"]],
    )
    def get(self, **kwargs):
        """Get the last n chat messages"""
        return self._parent.get_messages(**kwargs)

    @marshal(
        inp=ChatSchema,
        out=[
            [200, ChatSchema(), "Sent message"],
            [400, ErrorSchema(), "Could not send messsage"],
        ],
    )
    def post(self, **kwargs):
        """Add a new chat message"""
        message = self._parent.add_message(**kwargs)
        if message:
            return message

        return {"error": "Could not send message"}, 400

    @marshal(
        out=[
            [200, AllMessagesReadSchema(), "All messages marked as read"],
            [400, ErrorSchema(), "Could not mark all messsages read"],
        ],
    )
    def patch(self, **kwargs):
        """Mark all chat message as read"""
        maxid = self._parent.mark_all(**kwargs)
        if maxid is not None:
            return {"maxid": maxid}

        return {"error": "Could not mark all messages read"}, 400


class ChatMessageResource(ComponentResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Message marked as read"],
            [400, ErrorSchema(), "Could not mark messsage read"],
        ],
    )
    def patch(self, messageid, **kwargs):
        """Mark a chat message as read"""
        read = self._parent.mark_message(messageid, **kwargs)
        if read:
            return {"message": "Message marked as read"}

        return {"error": "Could not mark message read"}, 400


class Chat(Component):
    """Chat Component

    A simple chat component
    """

    _require_blsession = False

    _messageid = 0
    _messages = []

    def setup(self, *args, **kwargs):
        self.register_route(ChatResource, "")
        self.register_route(ChatMessageResource, "/<int:messageid>")

    def add_message(self, **kwargs):
        """Add a new message"""
        self._messageid += 1
        message = {
            "messageid": self._messageid,
            "timestamp": time.time(),
            "sessionid": g.sessionid,
            "user": g.user.get("fullname"),
            "message": kwargs["message"],
            "read": [g.sessionid],
        }

        self._messages.append(message)
        self.emit("new_message", message)

        return message

    def get_messages(self, **kwargs):
        """Get all messages"""
        limit = kwargs.get("limit", 50)
        offset = kwargs.get("offset", 0)

        return {
            "rows": self._messages[::-1][offset : offset + limit],
            "total": len(self._messages),
        }

    def mark_message(self, messageid, **kwargs):
        """Mark a  message as read"""
        message = next(
            (msg for msg in self._messages if msg["messageid"] == messageid), None
        )
        if message:
            if g.sessionid not in message["read"]:
                message["read"].append(g.sessionid)

                self.emit(
                    "message_read",
                    message,
                )

                return True

    def mark_all(self, **kwargs):
        """Mark all messages read"""
        maxid = 0
        for message in self._messages:
            if g.sessionid not in message["read"]:
                message["read"].append(g.sessionid)

            if message["messageid"] > maxid:
                maxid = message["messageid"]

        self.emit(
            "all_message_read",
            {"sessionid": g.sessionid},
        )

        return maxid
