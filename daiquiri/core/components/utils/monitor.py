import logging
import time
from daiquiri.core.hardware.abstract import HardwareObject
import gevent

logger = logging.getLogger(__name__)


def monitor(
    object: HardwareObject, property: str, greenlet: gevent.greenlet, timeout=30
):
    """A monitor function to kill a greenlet if no update is recieved within a timeframe

    This function can be used for example to monitor a running scan and kill the running
    greenlet if a particular axis value has not changed within a certain timeframe.

    Args:
        object (HardwareObject): A daiquiri hardware object
        property (str): The property on the object to monitor
        greenlet (greenlet): The greenlet that should be killed

    Kwargs:
        timeout (int): An optional timeout, default 30s

    """
    last_update_time = time.time()
    last_update_value = 0

    running = True

    def update_pos(obj, prop, value):
        nonlocal last_update_time, last_update_value
        if value != last_update_value:
            last_update_value = value
            last_update_time = time.time()

    object.subscribe(property, update_pos)

    def _monitor():
        nonlocal last_update_time, running, greenlet
        logger.info("Starting monitor")
        while running:
            last_update = round(time.time() - last_update_time, 2)
            if last_update > timeout:
                greenlet.kill()
                raise RuntimeError(
                    f"Greenlet killed as monitor did not recieve an update on property `{property}` for object `{object.name()}` within timeout {timeout}s, last update {last_update}s ago"
                )

            time.sleep(5)

    monitor_greenlet = gevent.spawn(_monitor)

    def kill_monitor():
        nonlocal monitor_greenlet, running
        running = False
        logger.info("Stopping monitor")
        monitor_greenlet.kill()
        object.unsubscribe(property, update_pos)

    return monitor_greenlet, kill_monitor
