#!/usr/bin/env python
# -*- coding: utf-8 -*-
import glob
import os
from flask import g
from marshmallow import fields
import ruamel

from daiquiri.core import marshal
from daiquiri.core.components import (
    Component,
    ComponentActor,
    ComponentActorSchema,
    ComponentResource,
    actor,
)
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.validators import ValidatedRegexp
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.parameteriser import (
    ParameteriserConfigSchema,
    AvailableParametersSchema,
)

try:
    from bliss.scanning.scan_saving import ESRFScanSaving
except ImportError:
    ESRFScanSaving = None


import logging


logger = logging.getLogger(__name__)


class AvailableParametersResource(ComponentResource):
    @marshal(
        inp={
            "type": fields.Str(
                required=True,
                metadata={"description": "The type of parameters to list"},
            )
        },
        out=[
            [200, paginated(AvailableParametersSchema), "List of parameters"],
            [400, ErrorSchema(), "Could not get list of parameters"],
        ],
    )
    def get(self, type: str):
        """Get the list of parameter sets for the selected parameter type"""
        try:
            params = self._parent.get_available_parameters(type)
            return {"total": len(params), "rows": params}, 200
        except Exception:
            message = f"Could not get parameters for type: {type}"
            logger.exception(message)
            return {"error": message}, 400


class Parameteriser(Component):
    _base_url = "parameteriser"
    _config_schema = ParameteriserConfigSchema()
    _config_export = ["parametertypes"]

    def setup(self):
        self._parameters_actors = []
        self.register_route(AvailableParametersResource, "")
        self._generate_parameter_actors()

    def reload(self):
        self._generate_parameter_actors()

    def _generate_parameter_actors(self):
        """Dynamically generate parameter actor resources"""

        def post(self, **kwargs):
            pass

        def preprocess(self, **kwargs):
            kwargs["sessionid"] = g.blsession.get("sessionid")

            return kwargs

        if not self._config.get("actors"):
            self._config["actors"] = {}

        for _, actors in self._config["parametertypes"].items():
            for actor_desc in actors:
                actor_name = actor_desc["actor"]

                self._config["actors"][actor_name] = actor_name

                if actor_name in self._actors:
                    continue

                self._actors.append(actor_name)
                self._parameters_actors.append(actor_name)

                fn = actor(actor_name, spawn=True, preprocess=True)(post)

                act_res = type(
                    actor_name,
                    (ComponentResource,),
                    {"post": fn, "preprocess": preprocess},
                )
                self.register_actor_route(act_res, f"/{actor_name}")

    def set_proposal(self, **data):
        args = self._saving.eval_saving_arguments(
            data, partial_evaulation=True, raise_on_missing=False
        )

        arg_map = {
            "proposal": "proposal_name",
            "sample": "collection_name",
            "dataset": "dataset_name",
        }

        for arg, value in args.items():
            # dont set unevaluated keys (we really only want to set the base path)
            if "{" not in value:
                key = arg

                # For ESRF data policy saving we have do some arg mapping as per:
                # daiquiri/core/saving/bliss_esrf.py
                if arg in arg_map:
                    key = arg_map[arg]

                setattr(self._saving.scan_saving, key, value)

    @property
    def root_directory(self):
        """Return the root proposal directory"""
        # TODO: this is a bit bliss specific
        if isinstance(self._saving.scan_saving, ESRFScanSaving):
            return os.path.join(
                self._saving.scan_saving.base_path,
                self._saving.scan_saving.proposal_name,
                self._saving.scan_saving.beamline,
            )
        else:
            return self._saving.scan_saving.base_path

    def actor_started(self, actid, actor):
        """Callback when an actor starts

        Just set the correct proposal
        """
        if actor.name in self._parameters_actors:
            self.set_proposal(**actor.all_data)
            actor.update(
                root_directory=self.root_directory,
                parameter_root=self._config.get("root", "parameteriser"),
            )

        logger.info(f"Actor '{actor.name}' with id '{actid}' started")

    def get_available_parameters(self, type):
        self.set_proposal(**{"sessionid": g.blsession.get("sessionid")})
        parameter_path = os.path.join(
            self.root_directory, self._config.get("root", "parameteriser"), type
        )
        files = glob.glob(f"{parameter_path}/*.yml")

        parameters = []
        for file in files:
            with open(file) as stream:
                loader = ruamel.yaml.Loader(stream)
                data = loader.get_single_data()
                print("param", file, data)
                parameters.append(
                    {
                        "parameter_type": data.pop("parameter_type"),
                        "instance_type": data.pop("instance_type"),
                        "name": data["name"],
                        "file_name": os.path.basename(file),
                        "directory": os.path.dirname(file),
                        "parameters": data,
                    }
                )

        return parameters


class ParameteriserActorSchema(ComponentActorSchema):
    name = ValidatedRegexp("word-dash", required=True, metadata={"title": "Name"})
    overwrite = fields.Bool()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.Meta.cache = False

        if not hasattr(self.Meta, "uischema"):
            self.Meta.uischema = {}

        self.Meta.uischema["overwrite"] = {
            "classNames": "hidden-row",
            "ui:widget": "hidden",
        }


class ParameteriserActor(ComponentActor):
    def method(self, name, overwrite=False, **kwargs):
        parameter_path = os.path.join(
            self["root_directory"],
            self["parameter_root"],
            self.parameter_type,
            f"{self.name}_{name}.yml",
        )

        if os.path.exists(parameter_path) and not overwrite:
            raise RuntimeError(
                f"Parameter file `{parameter_path}` already exists and not overwriting"
            )

        base_dir = os.path.dirname(parameter_path)
        if not os.path.exists(base_dir):
            os.makedirs(base_dir)

        all_kwargs = {
            "parameter_type": self.parameter_type,
            "instance_type": self.name,
            "name": name,
            **kwargs,
        }

        yaml = ruamel.yaml.YAML()
        yaml.allow_duplicate_keys = True
        yaml.default_flow_style = False
        yaml.indent(mapping=2, sequence=4, offset=2)

        with open(parameter_path, "wt") as parameter_file:
            yaml.dump(all_kwargs, stream=parameter_file)
