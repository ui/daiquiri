#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import h5py
from marshmallow import fields, validate

from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.components.h5grove import MetaSchema, ValueField
from daiquiri.core.utils import worker
from daiquiri.core.responses import gzipped

from h5grove import encode, create_content
from h5grove.content import ResolvedEntityContent, DatasetContent
from h5grove.utils import PathError
from flask import Response

logger = logging.getLogger(__name__)


query_params = {
    "autoprocprogramid": fields.Int(
        metadata={"description": "Auto processing program id"}
    ),
    "autoprocprogramattachmentid": fields.Int(
        metadata={"description": "Auto processing program attachment id"}
    ),
    "datacollectionid": fields.Int(metadata={"description": "Data collection id"}),
    "sampleactionid": fields.Int(metadata={"description": "Sample action id"}),
    "type": fields.String(metadata={"enum": ["processing"]}),
    "path": fields.String(load_default="/"),
}

error_responses = [
    [400, ErrorSchema(), "Could not find HDF5 file"],
    [400, ErrorSchema(), "Could not resolve HDF5 entity"],
]


def make_encoded_response(content, format_arg="json") -> Response:
    """Prepare flask Response according to format"""
    h5grove_response = encode(content, format_arg)

    # Let h5grove deal with non-standard format
    if format_arg in ["tiff", "npy", "csv"]:
        response = Response(h5grove_response.content)
        response.headers.update(h5grove_response.headers)
        return response

    return worker(lambda: gzipped(h5grove_response.content))


class H5GroveResource(ComponentResource):
    def get(self, **kwargs):

        filename = self._parent._get_file(**kwargs)

        if not filename or not os.path.isfile(filename):
            return {"error": "Could not find hdf5 file"}, 400

        with h5py.File(filename, mode="r") as h5file:
            try:
                content = self._get_content(h5file, **kwargs)
            except PathError as e:
                return {"error": f"Could not resolve hdf5 entity: {e}"}, 404

        return self._encode_response(content, **kwargs)

    def _get_content(self, h5file, **kwargs):
        raise NotImplementedError()

    def _encode_response(self, content, **kwargs):
        return make_encoded_response(content)


class AttrResource(H5GroveResource):
    @marshal(
        inp={
            **query_params,
            "attr_keys": fields.List(fields.String(), load_default=None),
        },
        out=[
            [200, fields.Dict(), "Get dict of attributes of HDF5 entity"],
            *error_responses,
        ],
    )
    def get(self, **kwargs):
        return super().get(**kwargs)

    def _get_content(self, h5file, **kwargs):
        content = create_content(h5file, kwargs["path"])
        assert isinstance(content, ResolvedEntityContent)  # nosec
        return content.attributes(kwargs["attr_keys"])


class DataResource(H5GroveResource):
    @marshal(
        inp={
            **query_params,
            "selection": fields.String(load_default=None),
            "format": fields.String(
                validate=validate.OneOf(("json", "bin", "csv", "npy", "tiff")),
                load_default="json",
            ),
            "flatten": fields.Boolean(load_default=False),
            "dtype": fields.String(
                validate=validate.OneOf(("origin", "safe")), load_default="origin"
            ),
        },
        out=[[200, ValueField(), "Get data of a HDF5 dataset"], *error_responses],
    )
    def get(self, **kwargs):
        return super().get(**kwargs)

    def _encode_response(self, content, **kwargs):
        return make_encoded_response(content, kwargs["format"])

    def _get_content(self, h5file, **kwargs):
        content = create_content(h5file, kwargs["path"])
        assert isinstance(content, DatasetContent)  # nosec
        return content.data(kwargs["selection"], kwargs["flatten"], kwargs["dtype"])


class MetaResource(H5GroveResource):
    @marshal(
        inp=query_params,
        out=[
            [200, MetaSchema(), "Get metadata of HDF5 entity"],
            *error_responses,
        ],
    )
    def get(self, **kwargs):
        return super().get(**kwargs)

    def _get_content(self, h5file, **kwargs):
        return create_content(h5file, kwargs["path"]).metadata()


class H5Grove(Component):
    """Generic HDF5 Component

    A component that can read hdf5 files and return json slices
    of data.

    Currently can get files from an
        autoprocprogramid (first rank file)
        autoprocprogramattachmentid
        datacollectionid

    May have other sources in future
    """

    def setup(self, *args, **kwargs):
        self.register_route(AttrResource, "/attr/")
        self.register_route(DataResource, "/data/")
        self.register_route(MetaResource, "/meta/")

    def _file_from_app(self, autoprocprogramid):
        appas = self._metadata.get_autoprocprogram_attachments(
            autoprocprogramid=autoprocprogramid
        )

        rank = 9999
        minr = None
        for app in appas["rows"]:
            app_rank = app["rank"]
            if app_rank is None or app_rank < rank:
                ext = os.path.splitext(app["filename"])[1][1:].strip().lower()
                if app["filetype"] == "Result" and ext in ["h5", "hdf5", "nxs"]:
                    if app_rank is not None:
                        rank = app_rank
                    minr = app

        if minr:
            return os.path.join(minr["filepath"], minr["filename"])

    def _get_file(
        self,
        datacollectionid=None,
        autoprocprogramattachmentid=None,
        autoprocprogramid=None,
        sampleactionid=None,
        type=None,
        **kwargs,
    ):
        """Find the file relevant for the request"""

        # From autoprocprogramid => lowest rank
        if autoprocprogramid is not None:
            return self._file_from_app(autoprocprogramid)

        #  Directly from autoprocprogramattachmentid
        elif autoprocprogramattachmentid is not None:
            appa = self._metadata.get_autoprocprogram_attachments(
                autoprocprogramattachmentid=autoprocprogramattachmentid
            )
            if appa:
                ext = os.path.splitext(appa["filename"])[1][1:].strip().lower()
                if appa["filetype"] == "Result" and ext in ["h5", "hdf5"]:
                    return appa["filefullpath"]

        # From datacollectionid, taking latest related autoprocprogram and lowest
        # rank attachment
        elif datacollectionid is not None and type == "processing":
            apps = self._metadata.get_autoprocprograms(
                datacollectionid=datacollectionid
            )
            logger.debug("Result: %s", apps)
            if apps["total"]:
                autoprocprogramid = apps["rows"][-1]["autoprocprogramid"]
                return self._file_from_app(autoprocprogramid)

        # Direct datacollection hdf5
        elif datacollectionid is not None:
            dc = self._metadata.get_datacollections(datacollectionid=datacollectionid)
            if dc:
                return os.path.join(dc["imagedirectory"], dc["filetemplate"])

        # From a sample action
        elif sampleactionid is not None:
            sampleaction = self._metadata.get_sampleactions(
                sampleactionid=sampleactionid
            )
            if sampleaction:
                return sampleaction["resultfilepath"]
