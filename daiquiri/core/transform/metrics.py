# -*- coding: utf-8 -*-
"""
Vector space metrics
"""

import numpy


class MetricTensor:
    """Defines a metric in R^n"""

    def __init__(self, M):
        if M.shape != M.shape[[0, 0]]:
            raise ValueError("Needs to be (n x n)")
        self.M = M

    @property
    def ndim(self):
        """Dimension of R^n"""
        return self.M.shape[0]

    def inner_product(self, v, w):
        """
        :param num or array-like: row vectors (nv, n)
        :param num or array-like: row vectors (nw, n)
        :returns array: (nv, nw)
        """
        v = numpy.atleast_2d(v)
        w = numpy.atleast_2d(w)
        return v.dot(self.M).dot(w.T)

    def __call__(self, v, w):
        return self.inner_product(v, w)


class EuclideanMetricTensor(MetricTensor):
    """Metric tensor in Euclidean space. Standard representation has the
    identity as the matrix tensor.
    """

    def __init__(self, *basis_vectors):
        """
        :params basis_vectors: vectors with respect to the standard representation
        """
        self.C = basis_vectors

    @property
    def C(self):
        """Columns are the basis vector in the standard representation"""
        return self._C

    @property
    def Ci(self):
        """Columns are the standard vector in this representation"""
        return self._Ci

    @property
    def M(self):
        """Metric tensor"""
        return self._M

    @C.setter
    def C(self, basis_vectors):
        C = numpy.stack(basis_vectors, axis=1)
        if C.shape != (C.shape[0], C.shape[0]):
            raise ValueError("Needs as many vectors as dimensions")
        self._C = C
        self._Ci = numpy.linalg.inv(C)
        self._M = C.T.dot(C)

    @Ci.setter
    def Ci(self, standard_vectors):
        Ci = numpy.stack(standard_vectors, axis=1)
        if Ci.shape != Ci.shape[[0, 0]]:
            raise ValueError("Needs as many vectors as dimensions")
        self._Ci = Ci
        self._C = C = numpy.linalg.inv(Ci)
        self._M = C.T.dot(C)

    def to_standard(self, vb):
        """Convert from this to standard representation

        :param array vb: vectors in this representation (nv, n)
        :param array: vectors in the standard representation (nv, n)
        """
        vb = numpy.atleast_2d(vb)
        return self.C.dot(vb.T).T

    def from_standard(self, ve):
        """Convert from standard to this representation

        :param array ve: vectors in the standard representation (nv, n)
        :param array: vectors in this representation (nv, n)
        """
        ve = numpy.atleast_2d(ve)
        return self.Ci.dot(ve.T).T
