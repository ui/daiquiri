"""Sets based in daiquiri hardware objects
"""

import gevent
import logging
from abc import ABC, abstractproperty
from daiquiri.core.transform import sets


logger = logging.getLogger(__name__)


class AbstractMotorDomain(ABC):
    """A real interval based on hardware motors"""

    @abstractproperty
    def motors(self):
        pass

    @abstractproperty
    def closed(self):
        pass

    @property
    def limits(self):
        # TODO: Cache this?
        # if numpy.isinf(self._limits[0][0]):
        self._limits = self._calc_limits
        return self._limits

    @property
    def _calc_limits(self):
        limits = []
        for m, (clow, chigh) in zip(self.motors, self.closed):
            low, high = m.get("limits")
            if low > high:
                low, high = high, low
            limits.append([low, high, clow, chigh])
        return self._parse_limits(limits)

    @property
    def current_position(self):
        """
        :returns array: shape is (ndomain,)
        """
        return self.as_sequence([m.get("position") for m in self.motors])[0]

    @current_position.setter
    def current_position(self, values):
        self.raiseIfNotIn(values)
        for mot, pos in zip(self.motors, values):
            logger.info(f"Move {mot.name()} to {pos}")
            mot.move(pos)

    @property
    def current_position_dict(self):
        """
        :returns dict: length is ndomain
        """
        result = {}
        for mot, pos in zip(self.motors, self.current_position):
            result[mot.id()] = pos
        return result

    @current_position_dict.setter
    def current_position_dict(self, dic):
        positions = [0] * self.ndomain
        for motname, motpos in dic.items():
            idx = self.motor_index(motname)
            positions[idx] = motpos
        self.current_position = positions

    def wait_motion_done(self, timeout=None):
        """Wait until the motors do not move anymore

        :param num or None timeout: `None` waits indefinitely
        """
        with gevent.Timeout(timeout):
            logger.info("Wait motion done ...")
            for mot in self.motors:
                mot.wait()
            logger.info("Motion done.")

    def move(self, values, wait=True, timeout=None):
        """Set the `current_position` with optional waiting (the default)

        :param array or dict values:
        :param bool wait: wait until motion has finished
        :param num or None timeout: `None` waits indefinitely
        """
        if isinstance(values, dict):
            self.current_position_dict = values
        else:
            self.current_position = values
        if wait:
            self.wait_motion_done(timeout=timeout)

    def rmove(self, values, wait=True, timeout=None):
        """Set the `current_position` with optional waiting (the default)

        :param array or dict values:
        :param bool wait: wait until motion has finished
        :param num or None timeout: `None` waits indefinitely
        """
        if isinstance(values, dict):
            _values = self.current_position_dict
            for motname, rpos in values.items():
                _values[motname] = rpos
            values = _values
        else:
            values = self.current_position + values
        self.move(values, wait=wait, timeout=timeout)

    def motor_limits(self, name):
        """Get motor limits

        :param str name:
        :returns 2-tuple: low, high
        """
        motlim = self.limits[self.motor_index(name)]
        return motlim.low, motlim.high

    def motor_index(self, name):
        """Get the index of the motor name

        :param str name:
        :returns int:
        """
        for i, mot in enumerate(self.motors):
            if mot.id() == name:
                return i
        raise IndexError(name)

    def motor_name(self, i):
        """Get the motor name from the index

        :param int:
        :returns str:
        """
        return self.motors[i].id()


class MotorDomain(AbstractMotorDomain, sets.RealInterval):
    """A real interval based on hardware motors"""

    def __init__(self, motors, closed=None):
        """
        :param list(Motor) motors:
        :param list(2-tuple(bool)) closed: min/max included or not for each motor
        """
        self._motors = motors
        ndim = len(self.motors)
        if closed:
            if ndim != len(closed):
                raise ValueError("Number of motors and number of closed does not match")
            if not all(len(c) == 2 for c in closed):
                raise ValueError("Closed must a a list of 2-tuples")
            self._closed = closed
        else:
            self._closed = [[True, True]] * ndim
        super().__init__(ndim=ndim)

    @property
    def motors(self):
        return self._motors

    @property
    def closed(self):
        return self._closed


class CompositeMotorDomain(AbstractMotorDomain, sets.RealIntervalComposite):
    """A real interval based on hardware motors"""

    @property
    def motors(self):
        lst = []
        for interval in self._intervals:
            lst.extend(interval.motors)
        return lst

    @property
    def closed(self):
        lst = []
        for interval in self._intervals:
            lst.extend(interval.closed)
        return lst
