#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import logging
import ruamel.yaml
from flask import g
from daiquiri.core import CoreBase, CoreResource, marshal
from daiquiri.resources.utils import get_resource_provider, YamlDict
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.layout import LayoutSchema
from daiquiri.core.exceptions import SyntaxErrorYAML

logger = logging.getLogger(__name__)


class LayoutsResource(CoreResource):
    @marshal(out=[[200, paginated(LayoutSchema), "A list of layouts"]])
    def get(self):
        """List of layouts"""
        return self._parent.list(), 200


class Layout(CoreBase):
    """The layouts handler"""

    _require_session = True
    _schemas = {}

    def setup(self):
        self.register_route(LayoutsResource, "")

    def list(self):
        """Get a list of layouts"""
        layouts = []

        provider = get_resource_provider()
        names = provider.list_resource_names("layout", "*.yml")
        for name in sorted(names):
            try:
                layout = YamlDict("layout", name)

                # Make acronym url safe
                layout["acronym"] = "".join(
                    [
                        c
                        for c in layout.get("acronym", layout["name"])
                        if re.match(r"\w", c)
                    ]
                )

                requires = layout.get("require_staff", None)
                if requires:
                    if hasattr(g, "user"):
                        logger.info(f"User login {g.user} require_staff:{requires}")
                        if g.user.staff():
                            layouts.append(layout)
                else:
                    layouts.append(layout)

            except ruamel.yaml.YAMLError as ex:
                layouts.append(
                    {
                        "acronym": name.replace(".yml", ""),
                        "name": name,
                        "error": "Invalid YAML Syntax\n" + str(ex),
                    }
                )

            except SyntaxErrorYAML as ex:
                layouts.append(
                    {
                        "acronym": name.replace(".yml", ""),
                        "name": name,
                        "error": ex.pretty(),
                    }
                )

            except Exception as ex:
                layouts.append(
                    {
                        "acronym": name.replace(".yml", ""),
                        "name": name,
                        "error": "Unexpected error while reading layout: \n" + str(ex),
                    }
                )

        return {"total": len(layouts), "rows": layouts}
