#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import binascii

from mohawk.util import utc_now
from mohawk.base import Resource
from mohawk.bewit import get_bewit, check_bewit, parse_bewit, strip_bewit

import logging

logger = logging.getLogger(__name__)


class SignedUrl:
    """Class for creating tokens to sign urls"""

    _used = {}

    def __init__(self, secret, max_expire=120):
        """Create a signing object

        Args:
            key(str): The encryption secret

        Kwargs:
            max_expire(int): Maximum time a token can live
        """
        self._key = secret
        self._max_expire = max_expire

    @property
    def credentials(self):
        return {"key": self._key, "algorithm": "sha256"}

    def expire_used(self):
        now = time.time()
        to_del = []
        for bewit, tm in self._used.items():
            if now - tm > self._max_expire:
                to_del.append(bewit)

        for d in to_del:
            del self._used[d]

    def sign(self, url, user, ttl=10):
        """Generate a signed url

        Args:
            url(str): The url to sign
            user(str): The user making the request

        Kwargs:
            ttl(int): Time for url to be valid in seconds

        Returns:
            bewit(str): The bewit used to protect the url
        """
        if ttl > self._max_expire:
            raise AttributeError(f"ttl is longer than max time of {self._max_expire}s")

        resource = Resource(
            credentials={"id": user, **self.credentials},
            url=f"http://localhost{url}",
            method="GET",
            nonce="",
            timestamp=utc_now() + ttl,
        )
        return get_bewit(resource)

    def lookup_credentials(self, recipient_id):
        """Lookup a users credentials"""

        # if recipient_id in allowed_recipients:
        #     return allowed_recipients[recipient_id]
        # else:
        #     raise LookupError("unknown recipient_id")

        # TODO: should each user have their own key?
        return {"id": recipient_id, **self.credentials}

    def verify(self, protected_url):
        """Verify a signed url

        Args:
            url(str): The protected url to verify

        Returns
            id(str): The user if the bewit is valid
        """
        self.expire_used()

        raw_bewit, _ = strip_bewit(protected_url)

        try:
            bewit = parse_bewit(raw_bewit)
        except binascii.Error:
            logger.error("Bewit token is not valid base64")
            return {"error": "Invalid Token"}

        if bewit in self._used:
            logger.error("Used bewit token")
            return {"error": "Invalid Token"}
        else:
            self._used[bewit] = time.time()

        try:
            valid = check_bewit(
                protected_url, credential_lookup=self.lookup_credentials
            )
        except Exception as e:
            logger.exception(f"Invalid bewit token: {str(e)}")
            return {"error": "Invalid Token"}
        else:
            if valid:
                return bewit.id
