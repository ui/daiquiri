#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import Union
from io import BytesIO
from datetime import datetime
from functools import update_wrapper, wraps
from flask import Response, make_response
import gzip
import json
import numpy
import struct
from silx.math.combo import min_max


def ndarray_response(data: numpy.ndarray, extra_headers: dict = None):
    """Create a flask response containing a nd-array."""
    msg = data.tobytes()
    resp = Response(
        response=msg,
        mimetype="application/octet-stream",
        status=200,
    )
    resp.headers["Content-Length"] = len(msg)
    resp.headers["Access-Control-Expose-Headers"] = "*"
    if extra_headers:
        for k, v in extra_headers.items():
            resp.headers[k] = v
    resp.headers["DQR-dtype"] = data.dtype.descr[0][1]
    resp.headers["DQR-shape"] = " ".join([str(s) for s in data.shape])
    # FIXME: This could be done with less calls using silx
    resp.headers["DQR-min"] = numpy.nanmin(data)
    resp.headers["DQR-max"] = numpy.nanmax(data)
    resp.headers["DQR-mean"] = numpy.nanmean(data)
    resp.headers["DQR-std"] = numpy.nanstd(data)
    return resp


SUPPORTED_IFF_PROFILES = {"raw", "f16", "u8"}

UINT8_LUT = numpy.arange(256, dtype=numpy.uint8).reshape(-1, 1)


def apply_normalization(
    data,
    norm: str = "linear",
    autoscale: str = "minmax",
    vmin=None,
    vmax=None,
    gamma=1.0,
):
    """Apply normalization to data.

    Arguments:
        data: Data on which to apply the colormap
        norm: Normalization to use
        autoscale: Autoscale mode: "minmax" (default) or "stddev3"
        vmin: Lower bound, None (default) to autoscale
        vmax: Upper bound, None (default) to autoscale
        gamma: Gamma correction parameter (used only for "gamma" normalization)

    Returns:
        Array of colors, vmin, vmax
    """
    from silx.math.colormap import GammaNormalization, _BASIC_NORMALIZATIONS
    from silx.math._colormap import cmap

    if norm == "gamma":
        normalizer = GammaNormalization(gamma)
    else:
        normalizer = _BASIC_NORMALIZATIONS[norm]

    if vmin is None or vmax is None:
        auto_vmin, auto_vmax = normalizer.autoscale(data, autoscale)
        if vmin is None:  # Set vmin respecting provided vmax
            vmin = auto_vmin if vmax is None else min(auto_vmin, vmax)
        if vmax is None:
            vmax = max(auto_vmax, vmin)  # Handle max_ <= 0 for log scale

    norm_data = cmap(data, UINT8_LUT, vmin, vmax, normalization=norm, nan_color=[0])
    norm_data.shape = data.shape
    return norm_data, vmin, vmax


def iff_response(
    data: numpy.ndarray,
    extra_header_type: bytes = b"EXTR",
    extra_headers: dict = None,
    profiles: str = "raw",
    norm: str = None,
    autoscale: str = None,
    vmin: float = None,
    vmax: float = None,
    histogram: bool = False,
):
    """Create a flask response containing a nd-array with extra stuffs encoded
    as IFF blocks.

    It provides a set of profiles for encoding which can be extended.

    - `raw`: Send the raw data if possible
    - `f2`: Send the data as float 16-bits

    Attributes:
        data: The data to encode
        extra_header_type: The type of the extra block if any
        extra_headers: The content of the extra block if any
        profiles: A list of supported profiles with `;` separator. The first one
                  supported will be used. Default is 'raw'
        histogram: If true, join an histogram with the response
    """
    ALIGN_BLOCK = 4

    # For consistency with other image response
    if autoscale == "none":
        autoscale = None

    def fourcc(chunk_id: bytes):
        """Returns an 4 chars represented by string"""
        assert len(chunk_id) == 4  # nosec
        return chunk_id

    def chunk_size(chunk_data: bytes):
        """Returns an 4 chars represented by string"""
        size = len(chunk_data)
        return struct.pack(">I", size)

    def chunk(chunk_id: bytes, chunk_data: bytes):
        return (fourcc(chunk_id), chunk_size(chunk_data), chunk_data, pad(chunk_data))

    def json_chunk(chunk_id: bytes, record: dict):
        raw = json.dumps(record).encode("utf-8")
        return (fourcc(chunk_id), chunk_size(raw), raw, pad(raw))

    def pad(chunk_data: bytes):
        """Pad the data if needed"""
        nb_bytes = len(chunk_data) % ALIGN_BLOCK
        if nb_bytes == 0:
            return b""
        return b"\x00" * (ALIGN_BLOCK - nb_bytes)

    # Select the first available profile
    profiles = [p for p in profiles.split(";") if p in SUPPORTED_IFF_PROFILES]
    profiles.append("raw")
    profile = profiles[0]

    minmax = min_max(data, min_positive=True, finite=True)

    def float_or_none(v):
        return None if v is None else float(v)

    stat = {
        "dtype": data.dtype.descr[0][1],
        "shape": [s for s in data.shape],
        "profile": profile,
        "min": float_or_none(minmax.minimum),
        "min_positive": float_or_none(minmax.min_positive),
        "max": float_or_none(minmax.maximum),
        "mean": float(numpy.nanmean(data)),
        "std": float(numpy.nanstd(data)),
    }

    if profile == "f16":
        pdata = data.astype(numpy.float16)

    elif profile == "u8":
        pdata, vmin, vmax = apply_normalization(
            data, norm=norm, autoscale=autoscale, vmin=vmin, vmax=vmax
        )
        stat["u8_norm"] = norm
        stat["u8_autoscale"] = autoscale
        stat["u8_min"] = float_or_none(vmin)
        stat["u8_max"] = float_or_none(vmax)

    elif profile == "raw":
        pdata = data
    else:
        raise RuntimeError(f"Unsupported profile '{profile}'")

    blocks = [
        *chunk(b"DQR0", b""),
        *json_chunk(b"FORM", stat),
        *chunk(b"DATA", pdata.tobytes()),
    ]

    if histogram:
        # FIXME: Normalize the histogram depending on the normalization
        normalized_array = data[numpy.isfinite(data)]
        count, edges = numpy.histogram(normalized_array, 256)
        count, edges = count.astype(numpy.float32), edges.astype(numpy.float32)
        blocks.extend(chunk(b"HST0", count.tobytes() + edges.tobytes()))

    if extra_headers:
        blocks.extend(json_chunk(extra_header_type, extra_headers))

    msg = b"".join(blocks)
    resp = Response(
        response=msg,
        mimetype="application/octet-stream",
        status=200,
    )
    resp.headers["Content-Length"] = len(msg)
    return resp


def image_response(img, img_format="PNG", extra_headers=None, max_age=None):
    """Create a flask response with an image

    Creates the correct content length so that an XHR request
    can monitor progress correctly

    Args:
        img (Image): A PIL image to send as a flask response

    Kwargs:
        img_format (str): The image type, default png

    Returns
        resp (Response): The flask response
    """
    img_io = BytesIO()
    img.save(img_io, img_format, quality=100)
    img_io.seek(0)

    resp = Response(response=img_io, mimetype=f"image/{img_format.lower()}", status=200)
    resp.headers["Content-Length"] = img_io.getbuffer().nbytes
    if extra_headers:
        resp.headers["Access-Control-Expose-Headers"] = "*"
        for k, v in extra_headers.items():
            resp.headers[k] = v

    if max_age:
        resp.cache_control.max_age = max_age

    return resp


def nocache(view):
    """A response with no-cache

    Stolen from https://arusahni.net/blog/2014/03/flask-nocache.html
    """

    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers["Last-Modified"] = datetime.now()
        response.headers[
            "Cache-Control"
        ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "-1"
        return response

    return update_wrapper(no_cache, view)


def gzipped(
    data: Union[dict, numpy.ndarray, bytes], compress_level: int = 6
) -> Response:
    """A gzipped response

    Args:
        data (dict | numpy.ndarray): An list of data or a dataarray to compress

    Kwargs:
        compress_level (int): The compression level to apply

    Returns:
        resp (Response): The flask response
    """
    if isinstance(data, numpy.ndarray):
        mimetype = "application/octet-stream"
        data_to_bytes = data.tobytes()
    elif isinstance(data, bytes):
        mimetype = "application/octet-stream"
        data_to_bytes = data
    else:
        mimetype = "application/json"
        data_to_bytes = json.dumps(data).encode("utf-8")

    gzip_buffer = BytesIO()
    gzip_file = gzip.GzipFile(
        mode="wb", compresslevel=compress_level, fileobj=gzip_buffer
    )
    gzip_file.write(data_to_bytes)
    gzip_file.close()

    resp = Response(response=gzip_buffer.getvalue(), mimetype=mimetype, status=200)
    resp.headers["Content-Encoding"] = "gzip"
    resp.headers["Content-Length"] = len(resp.get_data())

    return resp
