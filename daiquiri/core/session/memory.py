#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import uuid

from daiquiri.core.session.storage import SessionStorage


class MemorySessionStorage(SessionStorage):
    _sessions = {}

    def create(self, data={}):
        uid = str(uuid.uuid4())
        token = self._generate_token(uid)

        MemorySessionStorage._sessions[uid] = {
            "sessionid": uid,
            "token": token,
            "last_access": time.time(),
            "operator": False,
            "data": data,
        }

        return MemorySessionStorage._sessions[uid]

    def list(self):
        return list(MemorySessionStorage._sessions.values())

    def remove(self, sessionid, token=None):
        if sessionid:
            del MemorySessionStorage._sessions[sessionid]
            return sessionid

        elif token:
            sid = None
            for k, s in MemorySessionStorage._sessions.items():
                if s["token"] == token:
                    sid = k

            if sid:
                del MemorySessionStorage._sessions[sid]
                return sid

    def exists(self, sessionid):
        return sessionid in MemorySessionStorage._sessions

    def get(self, sessionid):
        if sessionid in MemorySessionStorage._sessions:
            return MemorySessionStorage._sessions[sessionid]

    def update(self, sessionid, **kwargs):
        if sessionid in MemorySessionStorage._sessions:
            for k, v in kwargs.items():
                MemorySessionStorage._sessions[sessionid]["data"][k] = v

            MemorySessionStorage._sessions[sessionid]["last_access"] = time.time()

    def take_control(self, sessionid):
        for s in self._sessions.keys():
            MemorySessionStorage._sessions[s]["operator"] = (
                True if s == sessionid else False
            )

    def yield_control(self, sessionid):
        for s in self._sessions.keys():
            if s == sessionid:
                if MemorySessionStorage._sessions[s]["operator"] is True:
                    MemorySessionStorage._sessions[s]["operator"] = False
                    return True

        return False

    def has_control(self, sessionid):
        if sessionid in self._sessions:
            if self._sessions[sessionid]["operator"]:
                return True

        return False

    def no_control(self):
        no_control = True
        for s in self._sessions.values():
            if s["operator"]:
                no_control = False

        return no_control
