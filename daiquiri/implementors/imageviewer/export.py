#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import datetime

from daiquiri.core.components import ComponentActor


class ExportActor(ComponentActor):
    name = "export"
    saving_args = {"dataset": "export_{time}"}

    def method(self, **kwargs):
        for subsample in self["subsamples"]:
            motors = {}
            for _, details in subsample["motors"].items():
                for motor_name, position in details.items():
                    motors[motor_name] = position["destination"]
            motors.update(subsample["additional"])

            file = os.path.join(self["dirname"], f"{subsample['subsampleid']}.json")
            with open(file, "w") as jf:
                jf.write(
                    json.dumps(
                        {
                            "subsampleid": subsample["subsampleid"],
                            "type": subsample["type"],
                            "timestamp": str(datetime.datetime.now()),
                            "comments": subsample["comments"],
                            "extrametadata": subsample["extrametadata"],
                            "motors": motors,
                        },
                        indent=4,
                    )
                )
