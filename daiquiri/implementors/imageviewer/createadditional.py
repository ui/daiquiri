import numpy as np

from daiquiri.core.components import ComponentActor


class CreateadditionalActor(ComponentActor):
    name = "createadditional"

    def method(
        self, datacollectionid, datacollection, selected_scalars, scalars, **kwargs
    ):
        steps_per_patch_x = int(datacollection["steps_x"] / datacollection["patchesx"])
        steps_per_patch_y = int(datacollection["steps_y"] / datacollection["patchesy"])

        maps = []
        for selected_scalar in selected_scalars:
            full_scalar_map = np.full(
                (datacollection["steps_y"], datacollection["steps_x"]), -1.0
            )
            for scan_number, scalar in enumerate(scalars):
                data = scalar["data"][selected_scalar]["data"]
                patch_y, patch_x = np.unravel_index(
                    scan_number,
                    (datacollection["patchesy"], datacollection["patchesx"]),
                )
                scan_map = np.full((steps_per_patch_y * steps_per_patch_x), -1.0)
                for point_id, point in enumerate(data):
                    scan_map[point_id] = point

                scan_map.shape = (steps_per_patch_y, steps_per_patch_x)
                full_scalar_map[
                    patch_y * steps_per_patch_y : patch_y * steps_per_patch_y
                    + steps_per_patch_y,
                    patch_x * steps_per_patch_x : patch_x * steps_per_patch_x
                    + steps_per_patch_x,
                ] = scan_map

            maps.append(
                {"scalar": selected_scalar, "data": full_scalar_map.flatten().tolist()}
            )

        return maps
