#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import fields, validate, validates_schema, ValidationError
import mmh3
import numpy

from bliss.config.static import get_config
from bliss.common.scans import ascan
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel

from daiquiri.core.components import ComponentActor, ComponentActorSchema
from daiquiri.core.utils import to_wavelength
from daiquiri.core.schema.components import RegionSchema
from daiquiri.core.hardware.bliss.session import *
from daiquiri.core.hardware.blissdata.helpers import get_scan_key

from .createmap import CreatemapActor
from .beamlineparams import BeamlineParamsSchema


logger = logging.getLogger(__name__)
cfg = get_config()


class PoiscanSchema(ComponentActorSchema):
    subsampleid = fields.Int(required=True)
    repeats = fields.Int(
        required=True,
        metadata={"title": "No. Repeats"},
        validate=validate.Range(min=1),
        dump_default=1,
    )
    regions = fields.List(
        fields.Nested(RegionSchema),
        metadata={"title": "Energy Regions", "minItems": 1},
        required=True,
    )
    reference = fields.Float(
        metadata={"title": "Energy Reference", "unit": "keV"},
        validate=validate.Range(min=0.1),
        required=True,
    )

    beamlineparams = fields.Nested(
        BeamlineParamsSchema, metadata={"title": "Beamline Parameters"}
    )
    enqueue = fields.Bool(metadata={"title": "Queue Scan"}, dump_default=True)

    @validates_schema
    def schema_validate(self, data, **kwargs):
        objs = data.get("objects")
        if objs:
            if len(objs) > 1 and data.get("enqueue") is False:
                raise ValidationError(
                    f"Can only queue scan when more than one object is selected. {len(objs)} objects selected"
                )

    class Meta:
        uiorder = [
            "subsampleid",
            "reference",
            "regions",
            "repeats",
            "beamlineparams",
            "enqueue",
        ]
        uischema = {
            "subsampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "regions": {"ui:field": "arrayTable"},
            "beamlineparams": {"ui:field": "optionalParams"},
        }


class PoiscanActor(ComponentActor):
    schema = PoiscanSchema
    metatype = "Energy scan"
    name = "poiscan"

    def method(self, **kwargs):
        print("Add poiscan", kwargs)

        print("moving to poi")
        kwargs["absol"]["move_to"](kwargs["absol"])

        print("moving to addiional positions")
        kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])

        print("capture params and image")
        kwargs["before_scan_starts"](self)

        points = int(
            (kwargs["regions"][0]["end_e"] - kwargs["regions"][0]["start_e"])
            / kwargs["regions"][0]["step"]
        )

        print("points", points)

        mca = cfg.get("simu1")
        mca.block_size = 10

        seq = Sequence()
        seq.add_custom_channel(AcquisitionChannel("avg_energy", float, ()))

        rois = kwargs["get_rois"]()["rois"]
        roi_data = {}
        for roi in rois:
            seq.add_custom_channel(AcquisitionChannel(f"avg_{roi['name']}", float, ()))
            roi_data[roi["maproiid"]] = {"name": roi["name"], "data": []}

        diodes = []
        for i in range(4):
            diodes.append(config.get(f"diode{i+2}"))

        with seq.sequence_context() as scan_seq:
            scans = []
            for i in range(kwargs["repeats"]):
                scan = ascan(
                    omega,
                    kwargs["reference"],
                    kwargs["reference"] + kwargs["regions"][0]["end_e"] / 1000,
                    # Intervals vs points !
                    points - 1,
                    kwargs["regions"][0]["dwell"],
                    diode,
                    diodes[0],
                    diodes[1],
                    diodes[2],
                    diodes[3],
                    mca,
                    run=False,
                )
                scans.append(scan)
                scan_seq.add(scan)

            kwargs["update_datacollection"](
                self,
                datacollectionnumber=mmh3.hash(get_scan_key(seq)) & 0xFFFFFFFF,
                imagecontainersubpath="1.1/measurement",
                exposuretime=kwargs["regions"][0]["dwell"],
                numberofimages=points,
                numberofpasses=kwargs["repeats"],
                wavelength=to_wavelength(kwargs["reference"] * 1e3),
                emit_start=True,
            )

            scan_number = None
            for i, scan in enumerate(scans):
                scan.run()

                scan_number = mmh3.hash(get_scan_key(scan)) & 0xFFFFFFFF
                spectra = kwargs["scans"].get_scan_spectra(scan_number, allpoints=True)

                mapsActor = CreatemapActor()
                maps = mapsActor.method(spectra=spectra, rois=rois)

                if maps:
                    for j, mroi in enumerate(maps[0]["maps"]):
                        roi_data[mroi["maproiid"]]["data"].append(mroi["data"])

                        if j == 0:
                            kwargs["add_scanqualityindicators"](
                                self, point=i + 1, total=sum(mroi["data"])
                            )

            avg_len = 0
            avgs = {}
            for roi in roi_data.values():
                avg = numpy.average(roi["data"], axis=0)
                avg_len = len(avg)
                seq.custom_channels[f"avg_{roi['name']}"].emit(avg)
                avgs[roi["name"]] = avg

            energy_counter = "axis:omega"
            energy = []
            scalars = kwargs["scans"].get_scan_data(
                scan_number, per_page=1e10, scalars=["axis:omega"]
            )
            if energy_counter in scalars["data"]:
                energy = scalars["data"][energy_counter]["data"]
            else:
                logger.warning("Cannot find energy counter")
                energy = range(avg_len)

            seq.custom_channels["avg_energy"].emit(energy)

            with kwargs["open_attachment"](
                self, "xy", suffix="_average", ext="asc"
            ) as avgf:
                headers = "\t".join(avgs.keys())
                data = numpy.array([energy] + list(avgs.values())).T
                numpy.savetxt(
                    avgf,
                    data,
                    delimiter="\t",
                    header=f"{energy_counter}\t{headers}",
                )
