import logging
import base64

from bliss import current_session
from daiquiri.core.components import ComponentActor

logger = logging.getLogger(__name__)


class Upload_CanvasActor(ComponentActor):
    name = "upload_canvas"

    def method(self, *, image, sample):
        """This actor will allow external processing of the current canvas
        view in the client

        Kwargs:
            sample(dict): The current sample dictionary
            image(str): Base64 encoded image

        """
        logger.info(f"Saving canvas for {sample['name']}")

        blob = image[image.find("base64") + 7 :]
        blob = base64.b64decode(blob)

        elogbook = current_session.scan_saving.elogbook
        elogbook.send_binary_data(blob, mimetype="image/png")
        elogbook.send_message(
            f"Saved current acquisition image for sample {sample['name']}"
        )
