import math

import numpy as np

from daiquiri.core.components import ComponentActor
from daiquiri.core.utils import worker


class CreatemapActor(ComponentActor):
    name = "createmap"

    def reconstruct_patch(self, datacollection, scalars, spectra, rois):
        steps_per_patch_x = int(datacollection["steps_x"] / datacollection["patchesx"])
        steps_per_patch_y = int(datacollection["steps_y"] / datacollection["patchesy"])

        first = spectra[0]
        evperbin = first["conversion"]["scale"] if first else 1
        offset = first["conversion"]["zero"] if first else 0

        maps = []

        for detector in first["data"]:
            roi_maps = []
            for roi in rois:
                full_roi_map = np.full(
                    (datacollection["steps_y"], datacollection["steps_x"]), -1
                )

                for scan_number, scan in enumerate(spectra):
                    patch_y, patch_x = np.unravel_index(
                        scan_number,
                        (datacollection["patchesy"], datacollection["patchesx"]),
                    )

                    mca = scan["data"][detector]["data"] if scan else []
                    roi_start = max(0, math.floor((roi["start"] - offset) / evperbin))
                    roi_end = min(
                        math.ceil((roi["end"] - offset) / evperbin), len(mca[0])
                    )
                    mask = np.zeros(len(mca[0]))
                    mask[roi_start:roi_end] = 1.0
                    points = np.sum(mca * mask, axis=1)
                    scan_map = np.full((steps_per_patch_y * steps_per_patch_x), -1)
                    scan_map[0 : len(points)] = points
                    scan_map.shape = (steps_per_patch_y, steps_per_patch_x)

                    full_roi_map[
                        patch_y * steps_per_patch_y : patch_y * steps_per_patch_y
                        + steps_per_patch_y,
                        patch_x * steps_per_patch_x : patch_x * steps_per_patch_x
                        + steps_per_patch_x,
                    ] = scan_map

                roi_maps.append(
                    {
                        "maproiid": roi["maproiid"],
                        "data": full_roi_map.flatten().tolist(),
                    }
                )

            maps.append({"det": detector, "maps": roi_maps})

        return maps

    def method(
        self,
        group=False,
        datacollection=None,
        datacollectionid=None,
        datacollectionnumber=None,
        subsampleid=None,
        sum_detectors=False,
        **kwargs
    ):
        if group:
            return self.reconstruct_patch(datacollection, **kwargs)

        evperbin = kwargs["spectra"]["conversion"]["scale"]
        offset = kwargs["spectra"]["conversion"]["zero"]

        spectra = kwargs["spectra"]["data"]

        def generate():
            maps = []
            summed_maps = {}
            for detector in spectra:
                roi_maps = []
                mca = kwargs["spectra"]["data"][detector]["data"]

                for r in kwargs["rois"]:
                    roi_map = np.full((len(mca)), -1)
                    roi_start = max(0, math.floor((r["start"] - offset) / evperbin))
                    roi_end = min(
                        math.ceil((r["end"] - offset) / evperbin), len(mca[0])
                    )

                    mask = np.zeros(len(mca[0]))
                    mask[roi_start:roi_end] = 1.0
                    points = np.sum(mca * mask, axis=1)
                    roi_map[0 : len(points)] = points

                    roi_maps.append(
                        {"maproiid": r["maproiid"], "data": roi_map.tolist()}
                    )

                    if r["maproiid"] not in summed_maps:
                        summed_maps[r["maproiid"]] = np.zeros(roi_map.shape)
                    # Data from different spectra could be of different lengths
                    if len(roi_map) < len(summed_maps[r["maproiid"]]):
                        roi_map.resize(len(summed_maps[r["maproiid"]]))
                    if len(roi_map) > len(summed_maps[r["maproiid"]]):
                        roi_map = roi_map[: len(summed_maps[r["maproiid"]])]
                    summed_maps[r["maproiid"]] += roi_map

                maps.append({"det": detector, "maps": roi_maps})

            config = self.get_config()
            sum_detectors_config = config.get("sum_detectors")
            if sum_detectors or sum_detectors_config:
                return [
                    {
                        "det": "summed",
                        "maps": [
                            {"maproiid": maproiid, "data": roi_map.tolist()}
                            for maproiid, roi_map in summed_maps.items()
                        ],
                    }
                ]

            return maps

        return worker(generate)


if __name__ == "__main__":
    patches_x = 3
    patches_y = 2
    steps_x = 12
    steps_y = 6

    steps_per_patch_x = int(steps_x / patches_x)
    steps_per_patch_y = int(steps_y / patches_y)

    print("spp", steps_per_patch_x, steps_per_patch_y)

    scans = []
    for i in range(patches_x * patches_y):
        offset = (i + 1) * 100
        a = np.array(range(offset, offset + steps_per_patch_x * steps_per_patch_y))
        a.shape = (steps_per_patch_y, steps_per_patch_x)
        # print(i, a)
        scans.append(a)

    full_scan = np.full((steps_y, steps_x), -1)
    for scanid, scan in enumerate(scans):
        scan_pos_x = scanid % patches_x
        scan_pos_y = math.floor(scanid / patches_x)
        offset_x = scan_pos_x * steps_per_patch_x
        offset_y = scan_pos_y * steps_per_patch_y

        print(scanid, scan_pos_x, scan_pos_y, offset_x, offset_y)

        full_scan[
            offset_y : offset_y + steps_per_patch_y,
            offset_x : offset_x + steps_per_patch_x,
        ] = scan

    print(full_scan)

    print(full_scan.flatten())
