#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from decimal import Decimal, ROUND_DOWN
from marshmallow import fields, validate, validates_schema, ValidationError

from bliss.config.static import get_config
from bliss.common.scans import amesh
from bliss.scanning.scan import ScanState
import gevent
import mmh3


from daiquiri.core.components import (
    ComponentActor,
    ComponentActorKilled,
)
from daiquiri.core.utils import to_wavelength
from daiquiri.core.hardware.bliss.session import *
from daiquiri.core.components.utils.monitor import monitor
from daiquiri.core.hardware.blissdata.helpers import get_scan_key
from daiquiri.core.schema.scans.mesh import (
    MeshSchema,
    set_decimal_precision,
)

from .beamlineparams import BeamlineParamsSchema


set_decimal_precision()

cfg = get_config()
logger = logging.getLogger(__name__)


class RoiscanSchema(MeshSchema):
    beamlineparams = fields.Nested(
        BeamlineParamsSchema, metadata={"title": "Beamline Parameters"}
    )
    with_lima = fields.Bool(
        metadata={"title": "Enable Lima Camera"}, dump_default=False
    )

    @validates_schema
    def schema_validate(self, data, **kwargs):
        super().schema_validate(data, **kwargs)

        objs = data.get("objects")
        if objs:
            if len(objs) > 1 and data.get("enqueue") is False:
                raise ValidationError(
                    f"Can only queue scan when more than one object is selected. {len(objs)} objects selected"
                )

    def warnings(self, data, **kwargs):
        warnings = {}
        if data.get("objects"):
            for obj in data.get("objects"):
                size_x = (obj["x2"] - obj["x"]) * 1e-9 / 1e-6
                size_y = (obj["y2"] - obj["y"]) * 1e-9 / 1e-6

                if size_x > 100 or size_y > 100:
                    warnings[
                        obj["subsampleid"]
                    ] = f"Object {obj['subsampleid']} will use stepper rather than piezo as size is {size_x:.0f}x{size_y:.0f} um"

        return warnings

    class Meta:
        uiorder = [
            *MeshSchema.Meta.uiorder,
            "beamlineparams",
            "with_lima",
        ]
        uischema = {
            **MeshSchema.Meta.uischema,
            "continuous": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "fast_axis": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "beamlineparams": {"ui:field": "optionalParams"},
        }
        uigroups = [
            "subsampleid",
            "dwell",
            {"Steps": ["step_size_x", "step_size_y", "steps_x", "steps_y"]},
            "beamlineparams",
            "with_lima",
            "enqueue",
            "estimate",
            "continuous",
            "fast_axis",
        ]


def get_metatype(self, **kwargs):
    return "XRF xrd map" if kwargs.get("with_lima") else "XRF map"


class RoiscanActor(ComponentActor):
    schema = RoiscanSchema
    name = "roiscan"
    metatype = get_metatype
    additional_metadata = {"definition": "XRF_2Dmap"}

    def method(self, **kwargs):
        print("Add roiscan", kwargs)

        print("moving to roi")
        kwargs["absol"]["move_to"](kwargs["absol"])

        print("moving to additional positions")
        kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])

        mca = cfg.get("simu1")
        lima_simulator = cfg.get("lima_simulator")

        axes = kwargs["absol"]["axes"]
        steps_x, steps_y, step_size_x, step_size_y = self.schema().calc_steps_size(
            axes, kwargs["step_size_x"], kwargs["step_size_y"]
        )

        print("calculated steps to be", steps_x, steps_y)

        print("capture params and image")
        kwargs["before_scan_starts"](self)

        detectors = [
            mca,
            diode,
        ]
        if kwargs["with_lima"]:
            detectors.append(lima_simulator)

        mesh = amesh(
            axes["x"]["motor"].object(),
            axes["x"]["destination"][0] + step_size_x / 2,
            axes["x"]["destination"][1] - step_size_x / 2,
            steps_x - 1,
            axes["y"]["motor"].object(),
            axes["y"]["destination"][0] + step_size_y / 2,
            axes["y"]["destination"][1] - step_size_y / 2,
            steps_y - 1,
            kwargs["dwell"],
            *detectors,
            run=False,
            scan_info={"daiquiri_datacollectionid": self["datacollectionid"]},
        )

        greenlet = gevent.spawn(mesh.run)
        mesh.wait_state(ScanState.STARTING)
        kwargs["update_datacollection"](
            self,
            emit_start=True,
            datacollectionnumber=mmh3.hash(get_scan_key(mesh)) & 0xFFFFFFFF,
            imagecontainersubpath="1.1/measurement",
            dx_mm=kwargs["step_size_x"] * 1e-3,
            dy_mm=kwargs["step_size_y"] * 1e-3,
            numberofimages=steps_x * steps_y,
            exposuretime=kwargs["dwell"],
            wavelength=to_wavelength(10000),
            steps_x=steps_x,
            steps_y=steps_y,
            orientation="horizontal",
        )

        print("starting monitor")
        monitor_greenlet, kill_monitor = monitor(
            axes["x"]["motor"], "position", greenlet
        )

        try:
            greenlet.join()
        except ComponentActorKilled:
            greenlet.kill()
            raise
        finally:
            print("stopping monitor")
            kill_monitor()

        monitor_greenlet.get()

        return greenlet.get()
