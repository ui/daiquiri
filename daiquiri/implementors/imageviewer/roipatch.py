#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import gevent
from marshmallow import fields

from bliss.scanning.group import Sequence
from bliss.config.static import get_config
from bliss.common.scans import amesh
from bliss.scanning.scan import ScanState

import mmh3

from daiquiri.core.components import (
    ComponentActor,
    ComponentActorKilled,
)
from daiquiri.core.utils import to_wavelength
from daiquiri.core.hardware.bliss.session import *
from daiquiri.core.hardware.blissdata.helpers import get_scan_key
from daiquiri.core.components.utils.monitor import monitor
from daiquiri.core.schema.scans.mesh import (
    MeshPatchSchema,
    set_decimal_precision,
)

from .beamlineparams import BeamlineParamsSchema

set_decimal_precision()

cfg = get_config()
logger = logging.getLogger(__name__)


class RoipatchSchema(MeshPatchSchema):
    _FINE_RANGE_X = 20
    _FINE_RANGE_Y = 20

    beamlineparams = fields.Nested(
        BeamlineParamsSchema, metadata={"title": "Beamline Parameters"}
    )
    with_lima = fields.Bool(
        metadata={"title": "Enable Lima Camera"}, dump_default=False
    )

    class Meta:
        uiorder = [
            *MeshPatchSchema.Meta.uiorder,
            "with_lima",
            "beamlineparams",
        ]
        uischema = {
            **MeshPatchSchema.Meta.uischema,
            "continuous": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "fast_axis": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "beamlineparams": {"ui:field": "optionalParams"},
        }

        uigroups = [
            "subsampleid",
            "dwell",
            {"Patches": ["patches_x", "patches_y", "patch_size_x", "patch_size_y"]},
            {"Steps": ["step_size_x", "step_size_y", "steps_x", "steps_y"]},
            "with_lima",
            "enqueue",
            "beamlineparams",
            "estimate",
            "continuous",
            "fast_axis",
        ]


def get_metatype(self, **kwargs):
    return "XRF xrd map" if kwargs.get("with_lima") else "XRF map"


class RoipatchActor(ComponentActor):
    schema = RoipatchSchema
    name = "Roipatch"
    metatype = get_metatype

    def method(self, **kwargs):
        print("moving to roi")
        kwargs["absol"]["move_to"](kwargs["absol"])

        print("moving to additional positions")
        kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])

        mca = cfg.get("simu1")
        lima_simulator = cfg.get("lima_simulator")

        axes = kwargs["absol"]["axes"]
        (
            patch_size_x,
            patch_size_y,
            steps_x,
            steps_y,
            offset_x,
            offset_y,
            orientation,
            fast,
            slow,
        ) = self.schema().calc_patch_steps_size(axes, **kwargs)

        print("calculated steps to be", steps_x, steps_y)

        print("capture params and image")
        kwargs["before_scan_starts"](self)

        detectors = [
            mca,
            diode,
        ]
        if kwargs["with_lima"]:
            detectors.append(lima_simulator)

        seq = Sequence()
        with seq.sequence_context() as scan_seq:
            kwargs["update_datacollection"](
                self,
                datacollectionnumber=mmh3.hash(get_scan_key(seq)) & 0xFFFFFFFF,
                imagecontainersubpath="1.1/measurement",
                dx_mm=kwargs["step_size_x"] * 1e-3,
                dy_mm=kwargs["step_size_y"] * 1e-3,
                numberofimages=steps_x
                * steps_y
                * kwargs["patches_x"]
                * kwargs["patches_y"],
                exposuretime=kwargs["dwell"],
                wavelength=to_wavelength(10000),
                steps_x=steps_x * kwargs["patches_x"],
                steps_y=steps_y * kwargs["patches_y"],
                patchesx=kwargs["patches_x"],
                patchesy=kwargs["patches_y"],
                orientation=orientation,
            )

            x_motor = cfg.get("m1")
            y_motor = cfg.get("m2")
            x_motor_fine = cfg.get("s1b")
            y_motor_fine = cfg.get("s1d")
            fine_centre = 0

            try:
                for py in range(kwargs["patches_y"]):
                    for px in range(kwargs["patches_x"]):
                        print(
                            "roipatch: patch index = %s"
                            % (py * kwargs["patches_x"] + px)
                        )

                        x_motor.move(
                            axes[fast]["destination"][0]
                            + (px * patch_size_x)
                            + patch_size_x / 2
                        )
                        y_motor.move(
                            axes[slow]["destination"][0]
                            + (py * patch_size_y)
                            + patch_size_y / 2
                        )
                        print("moved to", x_motor.position, y_motor.position)
                        start_x = fine_centre + ((-patch_size_x / 2) + offset_x) * 1e3
                        end_x = fine_centre + ((patch_size_x / 2) - offset_x) * 1e3
                        start_y = fine_centre + ((-patch_size_y / 2) + offset_y) * 1e3
                        end_y = fine_centre + ((patch_size_y / 2) - offset_y) * 1e3
                        print("scan", start_x, end_x, "y", start_y, end_y)

                        # positions are all in context of m1/2 => mm
                        # convert to um
                        scan = amesh(
                            x_motor_fine,
                            start_x,
                            end_x,
                            steps_x - 1,
                            y_motor_fine,
                            start_y,
                            end_y,
                            steps_y - 1,
                            kwargs["dwell"],
                            *detectors,
                            run=False,
                        )

                        scan_seq.add(scan)

                        greenlet = gevent.spawn(scan.run)
                        scan.wait_state(ScanState.STARTING)

                        monitor_greenlet, kill_monitor = monitor(
                            axes["x"]["motor"], "position", greenlet
                        )

                        try:
                            greenlet.join()
                        except ComponentActorKilled:
                            greenlet.kill()
                            raise
                        finally:
                            print("stopping monitor")
                            kill_monitor()

                        monitor_greenlet.get()
                        greenlet.get()

            finally:
                pass
