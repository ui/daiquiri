#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

from PIL import Image, ImageOps
from marshmallow import fields

from daiquiri.core.components import ComponentActor, ComponentActorSchema


class MosaicSchema(ComponentActorSchema):
    x1 = fields.Int(required=True, metadata={"title": "X Start"})
    y1 = fields.Int(required=True, metadata={"title": "Y Start"})
    x2 = fields.Int(required=True, metadata={"title": "X End"})
    y2 = fields.Int(required=True, metadata={"title": "Y End"})
    sampleid = fields.Int(required=True, metadata={"title": "Sampleid"})
    steps_x = fields.Int(required=True)
    steps_y = fields.Int(required=True)

    def time_estimate(self, data):
        return data["steps_x"] * data["steps_y"] * 5


class MosaicActor(ComponentActor):
    schema = MosaicSchema
    name = "mosaic"
    saving_args = {"dataset": "{sampleid.name}_mosaic{sampleactionid}"}

    def method(self, **kwargs):
        config = self.get_config()
        mirror_parts = config.get("mirror_parts")
        wait_factor = config.get("wait_factor", 1.2)
        sequential = config.get("sequential")

        x = kwargs["absol"]["axes"].get("x")
        if x:
            if kwargs["steps_x"] > 1:
                sizex = (x["destination"][1] - x["destination"][0]) / (
                    kwargs["steps_x"] - 1
                )
            else:
                sizex = x["destination"][1] - x["destination"][0]

        y = kwargs["absol"]["axes"].get("y")
        if y:
            if kwargs["steps_y"] > 1:
                sizey = (y["destination"][1] - y["destination"][0]) / (
                    kwargs["steps_y"] - 1
                )
            else:
                sizey = y["destination"][1] - y["destination"][0]

        kwargs["absol"]["move_to"](kwargs["absol"])

        im_w = None
        im_h = None
        full = None
        for sy in range(kwargs["steps_y"]):
            for sx in range(kwargs["steps_x"]):
                if (sy + 1) % 2 == 0:
                    ssx = kwargs["steps_x"] - sx - 1
                else:
                    ssx = sx

                if x:
                    x["motor"].move(x["destination"][0] + (sizex * ssx))
                    if sequential:
                        x["motor"].wait()

                if y:
                    y["motor"].move(y["destination"][0] + (sizey * sy))
                    if sequential:
                        y["motor"].wait()

                if not sequential:
                    if x:
                        x["motor"].wait()

                    if y:
                        y["motor"].wait()

                time.sleep(kwargs["camera"].get("exposure") * wait_factor)

                resp = kwargs["save"](ssx, sy)

                if sy == 0 and ssx == 0:
                    first = Image.open(resp["path"])
                    im_w = first.size[0]
                    im_h = first.size[1]

                    full = Image.new(
                        "RGB", (im_w * kwargs["steps_x"], im_h * kwargs["steps_y"])
                    )

                part = Image.open(resp["path"])
                full.paste(
                    im=ImageOps.mirror(part) if mirror_parts else part,
                    box=(ssx * im_w, sy * im_h),
                )
                self.update(full=full)
