from daiquiri.core.components import ComponentActor


class MoveActor(ComponentActor):
    name = "move"

    def method(self, *args, **kwargs):
        config = self.get_config()
        sequential = config.get("sequential")
        kwargs["absol"]["move_to"](kwargs["absol"], sequential=sequential)

        if kwargs["absol"].get("positions"):
            kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])
