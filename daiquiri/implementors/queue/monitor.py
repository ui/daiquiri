import logging

from bliss.config.static import get_config

from daiquiri.core.components import ComponentActor

logger = logging.getLogger(__name__)


class MonitorActor(ComponentActor):
    def method(self):
        """Dummy queue monitor for tests"""

        cfg = get_config()
        omega = cfg.get("omega")
        return omega.position != 90
