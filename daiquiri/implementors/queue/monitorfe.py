import logging

import tango

from daiquiri.core.components import ComponentActor

logger = logging.getLogger(__name__)


class MonitorfeActor(ComponentActor):
    def method(self):
        """This actor determines whether the queue is ready to process

        ESRF Specific Version for tango `fe_master`

        Returns:
            ready (boolean): If the queue is ready to process
        """
        config = self.get_config()
        enable = config.get("enable", True)
        if not enable:
            return True

        fe_master = config.get("fe_master")
        if not fe_master:
            logger.warning(
                "Queue monitor is enabled but no `fe_master` is defined, allowing execution"
            )
            return True

        if fe_master:
            fe = tango.DeviceProxy(fe_master)
            return fe.FE_State == "FE open"
