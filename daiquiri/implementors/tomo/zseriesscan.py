#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations

import gevent
import logging
import numpy
from marshmallow import fields, validate

from tomo.zseries import ZSeries
from . import tomo_helper

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)

_logger = logging.getLogger(__name__)


class ZseriesscanSchema(ComponentActorSchema):
    sampleid = fields.Int(
        required=True,
        metadata={"title": "Collection"},
    )
    dataset_name = fields.Str(
        required=True,
        metadata={"title": "Dataset"},
    )
    scan_type = OneOf(
        ["Continuous", "Step"],
        dump_default="Continuous",
        metadata={"title": "Scan type"},
    )
    nb_scans = fields.Int(
        dump_default=None,
        allow_none=True,
        # validate=validate.Range(min=1, max=999999),
        metadata={"title": "Nb scans", "readOnly": True},
    )
    z_height = fields.Float(
        dump_default=None,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Height", "unit": "mm"},
    )
    z_step = fields.Float(
        dump_default=None,
        allow_none=True,
        metadata={"title": "Z step", "unit": "mm", "readOnly": True},
    )
    z_direction = OneOf(
        ["Up", "Down"],
        dump_default="Down",
        metadata={"title": "Direction"},
    )
    z_min_overlap = fields.Float(
        dump_default=10,
        validate=validate.Range(min=0, max=99.99999),
        metadata={"title": "Min overlap", "unit": "%"},
    )
    z_overlap = fields.Float(
        dump_default=10,
        validate=validate.Range(min=0, max=99.99999),
        required=False,
        metadata={"title": "Overlap", "unit": "%", "readOnly": True},
    )
    start_pos = fields.Float(
        dump_default=0,
        metadata={"title": "Start angle", "unit": "deg"},
    )
    end_pos = fields.Float(
        dump_default=360,
        metadata={"title": "End angle", "unit": "deg"},
    )
    tomo_n = fields.Int(
        dump_default=360,
        validate=validate.Range(min=1, max=999999),
        metadata={"title": "Nb steps"},
    )
    n_dark = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb darks"},
    )
    n_flat = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb flats"},
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        dump_default=None,
        metadata={"title": "Exposure time", "unit": "s"},
    )
    use_sinogram = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Sinogram",
            "description": "Generate or not a sinogram during the scan",
        },
    )
    axis_displacement = fields.Int(
        dump_default=0,
        validate=validate.Range(min=-300, max=300),
        metadata={
            "title": "Axis displacement",
            "unit": "%",
            "description": "0% = centered, 100% = half detector width",
        },
    )
    comment = fields.Str(
        dump_default="",
        metadata={
            "title": "Comment",
        },
    )
    enqueue = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Queue Scan",
        },
    )

    class Meta:
        uiorder = [
            "sampleid",
            "dataset_name",
            "scan_type",
            "expo_time",
            "nb_scans",
            "z_step",
            "z_min_overlap",
            "z_overlap",
            "z_height",
            "z_direction",
            "start_pos",
            "end_pos",
            "tomo_n",
            "use_sinogram",
            "n_dark",
            "n_flat",
            "axis_displacement",
            "comment",
            "enqueue",
        ]
        uigroups = [
            {"Data policy": ["sampleid", "dataset_name"], "ui:minwidth": 12},
            "scan_type",
            {
                "Main": ["expo_time", "start_pos", "end_pos", "tomo_n"],
                "ui:minwidth": 12,
            },
            {
                "Z-series": [
                    "z_height",
                    "nb_scans",
                    "z_direction",
                    "z_step",
                    "z_min_overlap",
                    "z_overlap",
                ],
                "ui:minwidth": 6,
            },
            {"Options": ["use_sinogram", "n_dark", "n_flat"], "ui:minwidth": 12},
            {
                "Half acquisition": ["axis_displacement"],
                "ui:minwidth": 12,
            },
            "comment",
            "enqueue",
        ]
        uischema = {
            "sampleid": {"ui:widget": "SampleId"},
            "dataset_name": {"ui:widget": "DatasetName"},
            "comment": {"ui:widget": "textarea", "ui:options": {"rows": 3}},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }

    def calculated(self, data):
        return self._calculated(**data)

    def _calculated(
        self,
        z_direction: str,
        expo_time: float | None = None,
        z_height: None | float = None,
        z_min_overlap: float | None = None,
        **kwargs,
    ):
        """Returns the calculated values

        Arguments:
            data: Dictionary containing the actual parameters of the form
        """
        result: dict[str, object] = {}

        if expo_time is None:
            result["expo_time"] = self.calculated_expo_time()
        vfov = self.calculated_vfov()
        nb_scans = self.calculated_nb_scans(
            z_height=z_height, z_min_overlap=z_min_overlap, vfov=vfov
        )
        if nb_scans is not None:
            result["nb_scans"] = nb_scans
        result["z_step"] = self.calculated_z_step(
            z_height=z_height, nb_scans=nb_scans, vfov=vfov, z_direction=z_direction
        )
        result["z_overlap"] = self.calculated_z_overlap(
            z_height=z_height, nb_scans=nb_scans, vfov=vfov
        )
        print(result, "vfov", vfov, "nb_scans", nb_scans)
        return result

    def calculated_expo_time(self) -> float:
        # Trick to feed the initial expo time based on the tomo imaging device
        tomo_config = tomo_helper.get_active_tomo_config()
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
        imaging = tomo_config.tomo_imaging
        if imaging is None:
            return 1.0
        return imaging.exposure_time

    def calculated_vfov(self) -> None | float:
        """Vertical field of view in milimeter"""
        tomo_config = tomo_helper.get_active_tomo_config()
        tomocam = tomo_config.detectors.active_detector
        if tomocam is None:
            return None
        px = tomocam.sample_pixel_size
        if px is None or px == 0:
            return None
        size = tomocam.actual_size
        if size is None:
            return None
        return px * size[1] * 0.001

    def calculated_nb_scans(
        self, z_height: None | float, vfov: None | float, z_min_overlap: float | None
    ) -> None | int:
        """
        n > height / (vfov * (1 - min_overlap)) - min_overlap / (1 - min_overlap)
        """
        if z_height is None:
            return None
        if z_min_overlap is None:
            return None
        if vfov is None:
            return None
        min_overlap = z_min_overlap * 0.01
        n = z_height / (vfov * (1 - min_overlap)) - min_overlap / (1 - min_overlap)
        return max(1, int(numpy.ceil(n)))

    def calculated_z_overlap(
        self,
        z_height: None | float,
        vfov: None | float,
        nb_scans: int | None,
    ) -> None | float:
        """
        overlap = (vfov * n - height) / (vfov *  (n - 1))
        """
        if z_height is None:
            return None
        if vfov is None:
            return None
        if nb_scans is None:
            return None
        if nb_scans == 1:
            return 0
        return (vfov * nb_scans - z_height) / (vfov * (nb_scans - 1)) * 100

    def calculated_z_step(
        self,
        z_height: None | float,
        vfov: None | float,
        nb_scans: int | None,
        z_direction: str,
    ) -> None | float:
        if z_height is None:
            return None
        if vfov is None:
            return None
        if nb_scans is None:
            return None
        if nb_scans == 1:
            return 0
        step = (z_height - vfov * 0.5) / (nb_scans - 1)
        if z_direction == "Up":
            return -step
        elif z_direction == "Down":
            return step
        else:
            raise ValueError(f"Unexpected value for z_direction. Found: {z_direction}")


class ZseriesscanActor(ComponentActor):
    schema = ZseriesscanSchema
    name = "[tomo] full tomo scan"

    metatype = "tomo"
    saving_args = {"dataset": "{dataset_name}"}

    def method(
        self,
        scan_type,
        dataset_name,
        nb_scans,
        z_step,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        use_sinogram,
        n_dark,
        n_flat,
        axis_displacement,
        comment,
        before_scan_starts,
        update_datacollection,
        **kwargs,
    ):
        if len(kwargs):
            print("Unused params", kwargs)

        tomo_config = tomo_helper.get_active_tomo_config()
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")

        actor_config = self.get_config()
        zseries_object_names = actor_config.get("zseries_object")
        if zseries_object_names is None:
            raise RuntimeError(
                "'fullfield_name' config field was not setup in this samplescan actor description"
            )

        zseries = tomo_helper.get_sequence_from_name(
            tomo_config, zseries_object_names, ZSeries
        )

        mg = tomo_helper.create_mg(tomo_config)
        mg.set_active()

        tomo_helper.setup_main_pars(
            zseries,
            scan_type=scan_type,
            use_sinogram=use_sinogram,
            n_dark=n_dark,
            n_flat=n_flat,
            axis_displacement=axis_displacement,
            comment=comment,
        )
        if use_sinogram:
            zseries.pars.dark_flat_for_each_scan = True

        scan_info = tomo_helper.create_daiquiri_scan_info(
            self, share_data_collection_group=True
        )

        before_scan_starts(self)

        def run():
            zseries.basic_scan(
                collection_name=None,
                dataset_name=dataset_name,
                delta_pos=z_step,
                nb_scans=nb_scans,
                tomo_start_pos=start_pos,
                tomo_end_pos=end_pos,
                tomo_n=tomo_n,
                expo_time=expo_time,
                step_start_pos=None,
                scan_info=scan_info,
                trust_data_policy_location=True,
            )

        greenlet = gevent.spawn(run)
        try:
            greenlet.join()
        except ComponentActorKilled:
            greenlet.kill()
            raise

        greenlet.get()
