#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations

import logging
from marshmallow import fields, validate
from marshmallow import validates_schema, ValidationError
from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
)

from tomo.sequence.tiling import tiling
from tomo.sequence.tiling import tiling_estimation_time
from . import tomo_helper

try:
    from tomo.globals import ACTIVE_TOMOCONFIG
except ImportError:
    ACTIVE_TOMOCONFIG = None

_logger = logging.getLogger(__name__)


class TilingscanwithroiSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)

    scan_type = OneOf(
        ["Continuous", "Step"],
        dump_default="Continuous",
        metadata={"title": "Scan type"},
    )
    speed_ratio = fields.Float(
        validate=validate.Range(min=0.0, max=1.0),
        dump_default=1.0,
        metadata={
            "title": "Stable/speed",
            "multipleOf": 0.01,
        },
    )
    # FIXME: Units have to be read from the motors
    # FIXME: Motor names have to be read from the motor
    start_x = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=-250,
        metadata={"title": "Start", "unit": "㎜"},
    )
    end_x = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=250,
        metadata={"title": "Stop", "unit": "㎜"},
    )
    start_y = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=-250,
        metadata={"title": "Start", "unit": "㎜"},
    )
    end_y = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=250,
        metadata={"title": "Stop", "unit": "㎜"},
    )
    start_z = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=550,
        metadata={"title": "Start", "unit": "㎜"},
    )
    end_z = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=-550,
        metadata={"title": "Stop", "unit": "㎜"},
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        dump_default=None,
        metadata={"title": "Exposure time", "unit": "s"},
    )
    sleep_time = fields.Float(
        validate=validate.Range(min=0.0, max=10),
        dump_default=0,
        metadata={"title": "Sleep time", "unit": "s"},
    )
    n_dark = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb darks"},
    )
    n_flat = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb flats"},
    )
    lateral_motor_mode = OneOf(
        ["over_rot", "under_rot"],
        dump_default="over_rot",
        metadata={
            "title": "Lateral motor",
        },
    )
    pixel_blur = fields.Float(
        dump_default=0,
        metadata={
            "title": "Pixel blur",
            "description": "Displacement during the expo time in pixel",
            "unit": "px",
            "readOnly": True,
        },
    )
    restore_motor_positions = fields.Bool(
        metadata={
            "title": "Move back motors",
            "description": "Move back motors at their initial position when the scan is terminated",
        },
        dump_default=True,
    )
    enqueue = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Queue Scan",
        },
    )

    class Meta:
        uiorder = [
            "sampleid",
            "scan_type",
            "speed_ratio",
            "pixel_blur",
            "expo_time",
            "sleep_time",
            "n_dark",
            "n_flat",
            "lateral_motor_mode",
            "restore_motor_positions",
            "start_x",
            "end_x",
            "start_y",
            "end_y",
            "start_z",
            "end_z",
            "enqueue",
        ]
        uigroups = [
            "sampleid",
            "scan_type",
            "speed_ratio",
            "pixel_blur",
            {
                "Options": [
                    "expo_time",
                    "sleep_time",
                    "n_dark",
                    "n_flat",
                    "lateral_motor_mode",
                    "restore_motor_positions",
                ],
                "ui:minwidth": 12,
            },
            {
                "Y-motor (side)": ["start_x", "end_x"],
                "ui:minwidth": 6,
            },
            {
                "Y-motor (front)": ["start_y", "end_y"],
                "ui:minwidth": 6,
            },
            {
                "Z-motor": ["start_z", "end_z"],
                "ui:minwidth": 6,
            },
            "enqueue",
        ]
        uischema = {
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "speed_ratio": {"ui:widget": "range"},
        }

    @validates_schema
    def schema_validate(self, data, **kwargs):
        if data["scan_type"] == "Continuous":
            if data["lateral_motor_mode"] != "under_rot":
                raise ValidationError(
                    "In continuous mode, the lateral motor have ot be under the rot"
                )

    def calculated(self, data):
        try:
            return self._calculated(**data)
        except Exception:
            _logger.error("Error while calculated", exc_info=True)

    def _calculated(
        self,
        expo_time: float | None = None,
        scan_type: str = None,
        speed_ratio: float = None,
        **kwargs,
    ):
        """Returns the calculated values

        Arguments:
            data: Dictionary containing the actual parameters of the form
        """
        result = {}

        tomo_config = tomo_helper.get_active_tomo_config()
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")

        if expo_time is None:
            # Trick to feed the initial expo time based on the tomo imaging device
            imaging = tomo_config.tomo_imaging
            if imaging is not None:
                expo_time = imaging.exposure_time
            else:
                expo_time = 1.0
            result["expo_time"] = expo_time

        if scan_type == "Step":
            result["pixel_blur"] = 0.0
        elif scan_type == "Continuous" and speed_ratio is not None:
            if expo_time is not None:
                tomo_detector = tomo_config.detectors.active_detector
                px = tomo_detector.sample_pixel_size

                def get_continuous_velocity(z_axis):
                    vmax = z_axis.velocity
                    v1px = 1000 * px / expo_time
                    if v1px < vmax:
                        vmin = v1px
                    else:
                        vmin = vmax * 0.1
                    return vmin + (vmax - vmin) * speed_ratio

                v = get_continuous_velocity(tomo_config.z_axis)
                pixel_blur = (expo_time * v) / (1000 * px)
                result["pixel_blur"] = pixel_blur

        return result

    def time_estimate(self, data):
        result = self._calculated(**data)
        data.update(result)
        tomo_config = (
            ACTIVE_TOMOCONFIG is not None and ACTIVE_TOMOCONFIG.deref_active_object()
        )
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
        tomo_det = tomo_config.detectors.active_detector
        if tomo_det is not None:
            tomo_cam = tomo_det.detector
        else:
            tomo_cam = None

        if tomo_cam is None:
            raise RuntimeError(f"No active detector selected in `{tomo_config.name}`")
        try:
            start_x = data["start_x"]
            end_x = data["end_x"]
            start_y = data["start_y"]
            end_y = data["end_y"]
            start_z = data["start_z"]
            end_z = data["end_z"]
            expo_time = data["expo_time"]
            sleep_time = data.get("sleep_time", 0)  # FIXME: It can be uninitialized...
            n_dark = data["n_dark"]
            n_flat = data["n_flat"]
            lateral_motor_mode = data["lateral_motor_mode"]
            restore_motor_positions = data["restore_motor_positions"]
            scan_type = data["scan_type"]
            speed_ratio = data["speed_ratio"]
        except Exception:
            _logger.debug("Error while reading data", exc_info=True)
            return 0
        try:
            continuous = scan_type == "Continuous"
            return tiling_estimation_time(
                start_x,
                end_x,
                start_y,
                end_y,
                start_z,
                end_z,
                tomo_cam,
                expo_time=expo_time,
                sleep_time=sleep_time,
                n_dark=n_dark,
                n_flat=n_flat,
                lateral_motor_mode=lateral_motor_mode,
                restore_motor_positions=restore_motor_positions,
                continuous=continuous,
                tomoconfig=tomo_config,
                speed_ratio=speed_ratio * 1.0,
            )
        except Exception:
            _logger.error("Error while computing estimation", exc_info=True)
            return 0


class TilingscanwithroiActor(ComponentActor):
    schema = TilingscanwithroiSchema
    name = "[tomo] tiling scan"

    metatype = "tomo"

    def method(
        self,
        start_x: float,
        end_x: float,
        start_y: float,
        end_y: float,
        start_z: float,
        end_z: float,
        expo_time: float,
        sleep_time: float,
        n_dark: int,
        n_flat: int,
        lateral_motor_mode: str,
        scan_type: str,
        restore_motor_positions: bool,
        speed_ratio: float,
        before_scan_starts,
        update_datacollection,
        **kwargs,
    ):
        tomo_config = (
            ACTIVE_TOMOCONFIG is not None and ACTIVE_TOMOCONFIG.deref_active_object()
        )
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
        tomo_det = tomo_config.detectors.active_detector
        if tomo_det is not None:
            tomo_cam = tomo_det.detector
        else:
            tomo_cam = None

        if tomo_cam is None:
            raise RuntimeError(f"No active detector selected in `{tomo_config.name}`")

        tomo_det.sync_hard()
        if not tomo_det.is_online:
            raise RuntimeError(f"{tomo_cam.name} is not online")
        if not tomo_det.check_ready_for_acquisition():
            raise RuntimeError(f"{tomo_cam.name} is not ready")

        continuous = scan_type == "Continuous"
        tiling(
            start_x,
            end_x,
            start_y,
            end_y,
            start_z,
            end_z,
            tomo_cam,
            expo_time=expo_time,
            sleep_time=sleep_time,
            n_dark=n_dark,
            n_flat=n_flat,
            lateral_motor_mode=lateral_motor_mode,
            continuous=continuous,
            speed_ratio=speed_ratio,
            restore_motor_positions=restore_motor_positions,
            tomoconfig=tomo_config,
        )
