#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations

import gevent
import logging
from marshmallow import fields, validate

from tomo.fulltomo import FullFieldTomo
from . import tomo_helper
from bliss.common.standard import mv

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)


_logger = logging.getLogger(__name__)


class TomoscanwithroiSchema(ComponentActorSchema):
    sampleid = fields.Int(
        required=True,
        metadata={"title": "Collection"},
    )
    dataset_name = fields.Str(
        required=True,
        metadata={"title": "Dataset"},
    )
    scan_type = OneOf(
        ["Continuous", "Step"],
        dump_default="Continuous",
        metadata={"title": "Scan type"},
    )
    start_pos = fields.Float(
        dump_default=0,
        metadata={"title": "Start angle", "unit": "deg"},
    )
    range = fields.Float(
        dump_default=360,
        metadata={"title": "Range angle", "unit": "deg"},
    )
    tomo_n = fields.Int(
        dump_default=360,
        validate=validate.Range(min=1, max=999999),
        metadata={"title": "Nb steps"},
    )
    n_dark = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb darks"},
    )
    n_flat = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb flats"},
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        dump_default=None,
        metadata={"title": "Exposure time", "unit": "s"},
    )
    use_sinogram = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Sinogram",
            "description": "Generate or not a sinogram during the scan",
        },
    )
    axis_displacement = fields.Int(
        dump_default=0,
        validate=validate.Range(min=-300, max=300),
        metadata={
            "title": "Axis displacement",
            "unit": "%",
            "description": "0% = centered, 100% = half detector width",
        },
    )
    comment = fields.Str(
        dump_default="",
        metadata={
            "title": "Comment",
        },
    )
    x = fields.Float(
        dump_default=0,
        allow_none=True,
        metadata={"title": "X", "unit": "mm", "readOnly": True},
    )
    y = fields.Float(
        dump_default=0,
        allow_none=True,
        metadata={"title": "Y", "unit": "mm", "readOnly": True},
    )
    z = fields.Float(
        dump_default=0,
        allow_none=True,
        metadata={"title": "Z", "unit": "mm", "readOnly": True},
    )
    enqueue = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Queue Scan",
        },
    )

    class Meta:
        uiorder = [
            "sampleid",
            "dataset_name",
            "scan_type",
            "expo_time",
            "start_pos",
            "range",
            "tomo_n",
            "use_sinogram",
            "n_dark",
            "n_flat",
            "axis_displacement",
            "comment",
            "x",
            "y",
            "z",
            "enqueue",
        ]
        uigroups = [
            {
                "Data policy": [
                    "sampleid",
                    "dataset_name",
                ],
                "ui:minwidth": 12,
            },
            "scan_type",
            {
                "Main": ["expo_time", "start_pos", "range", "tomo_n"],
                "ui:minwidth": 12,
            },
            {
                "Options": ["use_sinogram", "n_dark", "n_flat"],
                "ui:minwidth": 12,
            },
            {
                "Half acquisition": ["axis_displacement"],
                "ui:minwidth": 12,
            },
            "comment",
            {
                "Positioning": ["x", "y", "z"],
                "ui:minwidth": 6,
            },
            "enqueue",
        ]
        uischema = {
            "sampleid": {"ui:widget": "SampleId"},
            "dataset_name": {"ui:widget": "DatasetName"},
            "comment": {"ui:widget": "textarea", "ui:options": {"rows": 3}},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }

    @tomo_helper.patch_actor_validator
    def calculated(self, expo_time: float | None = None, **kwargs):
        """Returns the calculated values

        Arguments:
            data: Dictionary containing the actual parameters of the form
        """
        result = {}

        if expo_time is None:
            # Trick to feed the initial expo time based on the tomo imaging device
            tomo_config = tomo_helper.get_active_tomo_config()
            if tomo_config is None:
                raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
            imaging = tomo_config.tomo_imaging
            if imaging is not None:
                result["expo_time"] = imaging.exposure_time
            else:
                result["expo_time"] = 1.0

        return result

    def time_estimate(self, data):
        try:
            actor = self.get_actor()
            return actor.time_estimate(**data)
        except Exception:
            _logger.error("Error while reading time estimate", exc_info=True)
            return None


class TomoscanwithroiActor(ComponentActor):
    schema = TomoscanwithroiSchema
    name = "[tomo] full tomo scan"

    metatype = "tomo"
    saving_args = {"dataset": "{dataset_name}"}

    def time_estimate(
        self,
        scan_type: str | None = None,
        start_pos: float | None = None,
        range: float | None = None,
        tomo_n: int | None = None,
        expo_time: float | None = None,
        use_sinogram: bool | None = None,
        n_dark: int | None = None,
        n_flat: int | None = None,
        axis_displacement: float | None = None,
        comment: str | None = None,
        **kwargs,
    ) -> float | None:

        if (
            scan_type is None
            or start_pos is None
            or range is None
            or tomo_n is None
            or expo_time is None
            or use_sinogram is None
            or n_dark is None
            or n_flat is None
        ):
            return None

        tomo_config = tomo_helper.get_active_tomo_config()
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")

        actor_config = self.get_config()
        fullfield_object_names = actor_config.get("fullfield_object")
        if fullfield_object_names is None:
            raise RuntimeError(
                "'fullfield_name' config field was not setup in this samplescan actor description"
            )

        fulltomo = tomo_helper.get_sequence_from_name(
            tomo_config, fullfield_object_names, FullFieldTomo
        )

        tomo_helper.setup_main_pars(
            fulltomo,
            scan_type=scan_type,
            use_sinogram=use_sinogram,
            n_dark=n_dark,
            n_flat=n_flat,
            axis_displacement=axis_displacement,
            comment=comment,
        )

        fulltomo.pars.start_pos = start_pos
        fulltomo.pars.range = range
        fulltomo.pars.tomo_n = tomo_n
        fulltomo.pars.exposure_time = expo_time
        fulltomo.prepare()
        # Else it will be considered as already prepared next time
        fulltomo._prepare_done = False
        return fulltomo._inpars.scan_time

    def method(
        self,
        scan_type: str,
        dataset_name: str,
        start_pos: float,
        range: float,
        x: float,
        y: float,
        z: float,
        tomo_n: int,
        expo_time: float,
        use_sinogram: bool,
        n_dark: int,
        n_flat: int,
        axis_displacement: float,
        comment: str,
        before_scan_starts,
        update_datacollection,
        **kwargs,
    ):
        if len(kwargs):
            print("Unused params", kwargs)

        tomo_config = tomo_helper.get_active_tomo_config()
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")

        actor_config = self.get_config()
        fullfield_object_names = actor_config.get("fullfield_object")
        if fullfield_object_names is None:
            raise RuntimeError(
                "'fullfield_name' config field was not setup in this samplescan actor description"
            )

        fulltomo = tomo_helper.get_sequence_from_name(
            tomo_config, fullfield_object_names, FullFieldTomo
        )

        mg = tomo_helper.create_mg(tomo_config)
        mg.set_active()

        tomo_helper.setup_main_pars(
            fulltomo,
            scan_type=scan_type,
            use_sinogram=use_sinogram,
            n_dark=n_dark,
            n_flat=n_flat,
            axis_displacement=axis_displacement,
            comment=comment,
        )

        scan_info = tomo_helper.create_daiquiri_scan_info(
            self, share_data_collection=True
        )

        before_scan_starts(self)

        def run():
            with tomo_config.auto_projection.inhibit():
                sample_stage = tomo_config.sample_stage
                yrot = sample_stage.y_axis.position
                mv(
                    sample_stage.z_axis,
                    z,
                    sample_stage.sample_x_axis,
                    x - yrot,
                    sample_stage.sample_y_axis,
                    y - yrot,
                )
                fulltomo.basic_scan(
                    start_pos, start_pos + range, tomo_n, expo_time, scan_info=scan_info
                )

        greenlet = gevent.spawn(run)
        try:
            greenlet.join()
        except ComponentActorKilled:
            greenlet.kill()
            raise

        greenlet.get()
