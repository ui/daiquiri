from __future__ import annotations

from typing import Any
import contextlib
import logging
from bliss.common import cleanup
from tomo.controllers.tomo_config import TomoConfig
from tomo.utils import configure_shift, ShiftType
from bliss.config.static import get_config
from daiquiri.core.components import (
    ComponentActorSchema,
)
from bliss.common.measurementgroup import MeasurementGroup

_logger = logging.getLogger(__name__)


try:
    from tomo.globals import ACTIVE_TOMOCONFIG
except ImportError:
    ACTIVE_TOMOCONFIG = None


def get_active_tomo_config() -> TomoConfig | None:
    tomo_config = (
        ACTIVE_TOMOCONFIG is not None and ACTIVE_TOMOCONFIG.deref_active_object()
    )
    return tomo_config


def patch_actor_validator(func):
    """Patch the validation function from the actor schema to make is a bit more
    easy to use

    .. code-block::

        @tomo_helper.patch_actor_validator
        def calculated(self, expo_time: float | None = None, **kwargs):
            ...
    """

    def new_func(self, data):
        try:
            try:
                return func(self, **data)
            except TypeError as e:
                if f"{func.name()}" in e.args[0]:
                    # The function signature is not right, it means we expect
                    # params which are not yet part of the function
                    _logger.debug(f"{func.name} failed with: {e}", exc_info=True)
                else:
                    raise
        except Exception as e:
            # Use an error instead of a debug
            _logger.error(f"{func.name} failed with: {e}", exc_info=True)
            raise
        return None

    return new_func


def get_sequence_from_name(
    tomo_config: TomoConfig, object_names: str | list[str], expected_class
):
    if isinstance(object_names, str):
        object_names = [object_names]

    bliss_config = get_config()

    for name in object_names:
        sequence = bliss_config.get(name)
        if not isinstance(sequence, expected_class):
            raise RuntimeError(
                f"'Object {name} is expected to be a {expected_class.name}. Found {type(sequence)}"
            )
        if sequence.tomo_config is tomo_config:
            break
    else:
        raise RuntimeError(
            f"No {expected_class.__name__} object was found for tomo config '{tomo_config.name}'"
        )
    return sequence


def create_daiquiri_scan_info(
    actor: ComponentActorSchema,
    share_data_collection: bool = False,
    share_data_collection_group: bool = False,
) -> dict[str, Any]:
    scan_info = {
        "daiquiri": {"sampleid": actor["sampleid"], "sessionid": actor["sessionid"]}
    }
    if share_data_collection:
        datacollectionid = actor.get("datacollectionid")
        if datacollectionid:
            scan_info["daiquiri"]["datacollectionid"] = datacollectionid
    if share_data_collection_group:
        datacollectiongroupid = actor.get("datacollectiongroupid")
        if datacollectiongroupid is not None:
            scan_info["daiquiri"]["datacollectiongroupid"] = datacollectiongroupid
    return scan_info


@contextlib.contextmanager
def y_axis_at(config: TomoConfig, position: float, enabled: bool = True):
    """
    Move and restore the rotation axis
    """
    if enabled:
        context = cleanup.cleanup(
            config.y_axis,
            restore_list=(cleanup.axis.POS,),
        )
    else:
        context = contextlib.nullcontext

    with context:
        if enabled:
            config.y_axis.move(position)
        yield


def create_mg(tomo_config: TomoConfig):
    tomocam = tomo_config.detectors.active_detector
    if tomocam is None:
        raise RuntimeError(f"No active detector selected in `{tomo_config.name}`")
    cam = tomocam.detector
    mg = MeasurementGroup("daiquiri_mg", {"counters": []})
    try:
        mg.add(cam.image)
    except Exception:  # nosec
        pass
    mg.enable(cam.image.fullname)
    if tomo_config.machinfo is not None:
        try:
            mg.add(tomo_config.machinfo.counters.current)
        except Exception:  # nosec
            pass
        mg.enable(tomo_config.machinfo.counters.current.fullname)
    return mg


def setup_main_pars(
    sequence,
    scan_type: str,
    use_sinogram: bool,
    n_dark: int,
    n_flat: int,
    axis_displacement: float,
    comment: str,
):
    sequence.pars.scan_type = scan_type.upper()
    sequence.pars.activate_sinogram = use_sinogram
    if use_sinogram:
        # A sinogram is expected so a dark will be acquired
        sequence.pars.dark_at_start = True
        sequence.pars.flat_at_start = True
    sequence.pars.flat_n = n_flat
    sequence.pars.dark_n = n_dark
    sequence.pars.comment = comment
    if axis_displacement == 0:
        sequence.pars.half_acquisition = False
        sequence.pars.shift_in_fov_factor = 0
        sequence.pars.shift_in_mm = 0
    else:
        configure_shift(sequence, axis_displacement / 100, ShiftType.FOV_FACTOR)
        sequence.pars.half_acquisition = True
