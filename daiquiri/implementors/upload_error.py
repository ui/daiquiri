import time
import logging
from daiquiri.core.components import ComponentActor

_logger = logging.getLogger(__name__)


class Upload_ErrorActor(ComponentActor):
    name = "upload_error"

    def method(self, *args, **kwargs):
        """Actor to process actor errors externally

        Kwargs:
            actid (str): The actor id
            actor (ComponentActor): The actor raising the exception
            exception (Exception): The exception the actor raised
        """
        _logger.error("Process error (%s %s)", args, kwargs)
        time.sleep(1)
