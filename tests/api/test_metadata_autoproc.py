def test_get_autoprocs(auth_client, with_session):
    res = auth_client.get("/api/metadata/autoprocs?datacollectionid=1")

    assert res.json["total"] == 3
    assert res.status_code == 200


def test_get_autoproc_attachments(auth_client, with_session):
    res = auth_client.get("/api/metadata/autoprocs/attachments?autoprocprogramid=1")
    assert res.json["total"] == 0
    assert res.status_code == 200

    res2 = auth_client.get("/api/metadata/autoprocs/attachments?autoprocprogramid=3")
    assert res2.json["total"] == 1
    assert res2.status_code == 200


def test_get_autoproc_messages(auth_client, with_session):
    res = auth_client.get("/api/metadata/autoprocs/messages?autoprocprogramid=1")
    assert res.json["total"] == 0
    assert res.status_code == 200

    res2 = auth_client.get("/api/metadata/autoprocs/messages?autoprocprogramid=3")
    assert res2.json["total"] == 2
    assert res2.status_code == 200
