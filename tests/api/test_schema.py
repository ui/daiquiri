def test_api_all_schemas_unauthorised(client):
    res = client.get("/api/schema")
    assert res.status_code == 401


def test_api_all_schemas(auth_client):
    res = auth_client.get("/api/schema")
    assert res.status_code == 200


def test_api_schemas_list(auth_client):
    res = auth_client.get("/api/schema/list")
    assert res.status_code == 200


def test_api_schema_roi(auth_client):
    res = auth_client.get("/api/schema/RoiscanSchema")
    assert res.status_code == 200


def test_api_schema_roi_validate(auth_client, with_session, with_control):
    res = auth_client.post("/api/schema/validate/RoiscanSchema", payload={"data": {}})
    assert res.status_code == 200


def test_api_schema_roi_validate_nocontrol(auth_client, with_session):
    res = auth_client.post("/api/schema/validate/RoiscanSchema")
    assert res.status_code == 400
