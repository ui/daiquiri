import time


def test_onetime(auth_client, client):
    res = auth_client.post("/api/session/sign", payload={"url": "/api/session/current"})
    assert res.status_code == 200

    res1 = client.get("/api/session/current", query_string={"bewit": res.json["bewit"]})
    assert res1.status_code == 200


def test_onetime_protected(auth_client, client):
    res = auth_client.post("/api/session/sign", payload={"url": "/api/logging"})
    assert res.status_code == 200

    res1 = client.get("/api/logging", query_string={"bewit": res.json["bewit"]})
    assert res1.status_code == 403


def test_onetime_bogus(auth_client, client):
    res1 = client.get("/api/session/current", query_string={"bewit": "not_a_bewit"})
    assert res1.status_code == 401


def test_onetime_replay(auth_client, client):
    res = auth_client.post("/api/session/sign", payload={"url": "/api/session/current"})
    assert res.status_code == 200

    res1 = client.get("/api/session/current", query_string={"bewit": res.json["bewit"]})
    assert res1.status_code == 200

    res2 = client.get("/api/session/current", query_string={"bewit": res.json["bewit"]})
    assert res2.status_code == 401


def test_onetime_timeout(auth_client, client):
    res = auth_client.post("/api/session/sign", payload={"url": "/api/session/current"})
    assert res.status_code == 200

    time.sleep(11)

    res1 = client.get("/api/session/current", query_string={"bewit": res.json["bewit"]})
    assert res1.status_code == 401
