import numpy
import pytest
from flask import Flask
from daiquiri.core.components.tomo.image_resource import TomoImageResource
from daiquiri.core.components.tomo.datatype import DetectorLiveStorage, Projection


class MockedTomoImageResource(TomoImageResource):
    @property
    def _parent(self):
        class Parent:
            def get_detector(self, detector_id):
                proj = numpy.array([1, 2, 3, 4], dtype=numpy.float32)
                proj.shape = 2, 2
                return DetectorLiveStorage(
                    detector_id=detector_id,
                    tomo_detector_id=detector_id,
                    detector_node_name=detector_id,
                    proj=Projection(scan_id=1, frame_no=2, exposure_time=3, data=proj),
                )

        return Parent()


@pytest.fixture
def flask_app(mocker):
    app = Flask(__name__)
    yield app


def get_iff_block(iff: bytes, name: bytes):
    pos = iff.find(name)
    size = numpy.frombuffer(iff[pos + 4 : pos + 8], dtype=">u4")[0]
    return iff[pos + 8 : pos + 8 + size]


@pytest.fixture
def api(flask_app):
    view = MockedTomoImageResource.as_view("test")
    flask_app.add_url_rule("/data", view_func=view, methods=["GET"])
    with flask_app.test_client() as c:
        yield c


def test_bin(api):
    data = api.get("/data?encoding=bin&process=proj&detectorid=foo").data
    array = numpy.frombuffer(data, dtype=numpy.float32)
    numpy.testing.assert_array_equal(array, numpy.array([1, 2, 3, 4]))


def test_iff(api):
    iff = api.get("/data?encoding=iff&process=proj&detectorid=foo").data
    # FIXME: Create a real IFF parser
    data = get_iff_block(iff, b"DATA")
    array = numpy.frombuffer(data, dtype=numpy.float32)
    numpy.testing.assert_array_equal(array, numpy.array([1, 2, 3, 4]))


def test_iff_u8(api):
    iff = api.get(
        "/data?encoding=iff&profiles=u8&process=proj&detectorid=foo&autoscale=minmax"
    ).data
    # FIXME: Create a real IFF parser
    data = get_iff_block(iff, b"DATA")
    array = numpy.frombuffer(data, dtype=numpy.uint8)
    expected = (numpy.array([1, 2, 3, 4]) - 1) * 255 / 3
    numpy.testing.assert_array_equal(array, expected)
