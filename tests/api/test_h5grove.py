import gzip
import json
import os
import h5py
import numpy as np
import pytest


@pytest.fixture
def datacollection_filepath(auth_client, with_session):
    response = auth_client.get("/api/metadata/datacollections")
    datacollection = response.json["rows"][0]
    datacollection_dir = datacollection["imagedirectory"]

    if not os.path.isdir(datacollection_dir):
        os.makedirs(datacollection_dir)

    return (
        datacollection["datacollectionid"],
        os.path.join(datacollection["imagedirectory"], datacollection["filetemplate"]),
    )


def test_attr(auth_client, datacollection_filepath):
    datacollectionid, filepath = datacollection_filepath

    attributes = {
        "NX_class": "NXRoot",
        "default": "entry",
        "HDF_VERSION": h5py.version.hdf5_version,
    }

    with h5py.File(filepath, mode="w") as h5file:
        for name, value in attributes.items():
            h5file.attrs[name] = value

    response = auth_client.get(
        f"/api/h5grove/attr/?datacollectionid={datacollectionid}&path=/"
    )

    assert response.status_code == 200
    retrieved_attributes = json.loads(gzip.decompress(response.data))
    assert retrieved_attributes == attributes


def test_data_with_bin_format(auth_client, datacollection_filepath):
    datacollectionid, filepath = datacollection_filepath

    tested_h5entity_path = "/entry/image"
    data = np.random.random((128, 128))

    with h5py.File(filepath, mode="w") as h5file:
        h5file[tested_h5entity_path] = data

    response = auth_client.get(
        f"/api/h5grove/data/?datacollectionid={datacollectionid}&path={tested_h5entity_path}&format=bin"
    )
    assert response.status_code == 200

    retrieved_data = np.frombuffer(
        gzip.decompress(response.data), dtype=data.dtype
    ).reshape(data.shape)
    assert np.array_equal(retrieved_data, data)


def test_data_on_slice(auth_client, datacollection_filepath):
    datacollectionid, filepath = datacollection_filepath

    tested_h5entity_path = "/entry/image"
    data = np.random.random((128, 128))

    with h5py.File(filepath, mode="w") as h5file:
        h5file[tested_h5entity_path] = data

    response = auth_client.get(
        f"/api/h5grove/data/?datacollectionid={datacollectionid}&path={tested_h5entity_path}&selection=100,:"
    )
    assert response.status_code == 200

    retrieved_data = json.loads(gzip.decompress(response.data))
    assert np.array_equal(retrieved_data, data[100, :])


def test_meta_on_chunked_compressed_dataset(auth_client, datacollection_filepath):
    datacollectionid, filepath = datacollection_filepath

    tested_h5entity_path = "/data"

    with h5py.File(filepath, mode="w") as h5file:
        h5file.create_dataset(
            tested_h5entity_path,
            data=np.arange(100).reshape(10, 10),
            compression="gzip",
            shuffle=True,
            dtype="<f8",
            chunks=(5, 5),
        )

    response = auth_client.get(
        f"/api/h5grove/meta/?datacollectionid={datacollectionid}&path={tested_h5entity_path}"
    )
    assert response.status_code == 200

    retrieved_metadata = json.loads(gzip.decompress(response.data))
    assert retrieved_metadata == {
        "attributes": [],
        "filters": [{"id": 2, "name": "shuffle"}, {"id": 1, "name": "deflate"}],
        "chunks": [5, 5],
        "name": "data",
        "dtype": "<f8",
        "shape": [10, 10],
        "type": "dataset",
    }


def test_meta_on_group(auth_client, datacollection_filepath):
    datacollectionid, filepath = datacollection_filepath

    tested_h5entity_path = "/"
    attributes = {"NX_class": "NXRoot", "default": "entry"}
    children = ["data", "group"]

    with h5py.File(filepath, mode="w") as h5file:
        for name, value in attributes.items():
            h5file.attrs[name] = value
        h5file.create_group("group")
        h5file["data"] = np.arange(10)

    response = auth_client.get(
        f"/api/h5grove/meta/?datacollectionid={datacollectionid}&path={tested_h5entity_path}"
    )
    assert response.status_code == 200

    retrieved_metadata = json.loads(gzip.decompress(response.data))
    retrieved_attr_name = [attr["name"] for attr in retrieved_metadata["attributes"]]
    retrieved_children_name = [
        child["name"] for child in retrieved_metadata["children"]
    ]

    assert retrieved_attr_name == list(attributes.keys())
    assert retrieved_children_name == children
