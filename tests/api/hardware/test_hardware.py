def test_hardware(auth_client, with_session):
    res = auth_client.get("/api/hardware")
    assert res.status_code == 200


def test_hardware_groups(auth_client, with_session):
    res = auth_client.get("/api/hardware/groups")
    assert res.status_code == 200


def test_hardware_types(auth_client, with_session):
    res = auth_client.get("/api/hardware/types")
    assert res.status_code == 200


def test_hardware_object(auth_client, with_session):
    res = auth_client.get("/api/hardware/omega")
    assert res.status_code == 200


def test_hardware_object_404(auth_client, with_session):
    res = auth_client.get("/api/hardware/omegz")
    assert res.status_code == 404


def test_hardware_object_attr(app, auth_client, with_session, with_control):
    value = 5
    res = auth_client.put(
        "/api/hardware/omega", payload={"property": "acceleration", "value": value}
    )
    assert res.status_code == 200

    omega = app.hardware.get_object("omega")
    assert omega.get("acceleration") == value


def test_hardware_object_attr_invalid_schema(auth_client, with_session, with_control):
    res = auth_client.put(
        "/api/hardware/omega", payload={"property": "acceleration", "value": "string"}
    )
    assert res.status_code == 422
    assert "Not a valid number." in res.json["messages"]["acceleration"]


def test_hardware_object_attr_invalid_value(auth_client, with_session, with_control):
    res = auth_client.put(
        "/api/hardware/omega",
        payload={"property": "acceleration", "value": 100000000000},
    )
    assert res.status_code == 400
    assert "Invalid acceleration" in res.json["error"]


def test_hardware_object_fn(app, auth_client, with_session, with_control):
    value = 5
    res = auth_client.post(
        "/api/hardware/omega", payload={"function": "move", "value": value}
    )
    assert res.status_code == 200

    omega = app.hardware.get_object("omega")
    omega.wait()
    assert omega.get("position") == value


def test_hardware_object_fn_invalid_schema(
    app, auth_client, with_session, with_control
):
    value = "string"
    res = auth_client.post(
        "/api/hardware/omega", payload={"function": "move", "value": value}
    )
    assert res.status_code == 422
    assert "Not a valid number." in res.json["messages"]["move"]


def test_hardware_object_fn_invalid_value(app, auth_client, with_session, with_control):
    value = 20
    res = auth_client.post(
        "/api/hardware/s1b", payload={"function": "move", "value": value}
    )
    assert res.status_code == 400
    assert "Could not call function" in res.json["error"]
