def test_api_layout_unauthorised(client):
    res = client.get("/api/layout")
    assert res.status_code == 401


def test_api_layout(auth_client):
    res = auth_client.get("/api/layout")
    assert res.status_code == 200
