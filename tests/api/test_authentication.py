import time


def test_token_create_decode(client):
    res = client.post(
        "/api/authenticator/login",
        json={"username": "abcd", "password": "abcd"},
    )
    assert res.status_code == 201

    headers = {"Authorization": f"Bearer {res.json['token']}"}
    res2 = client.get("/api/layout", headers=headers)
    assert res2.status_code == 200


def test_token_expired(short_session, client):
    res = client.post(
        "/api/authenticator/login",
        json={"username": "abcd", "password": "abcd"},
    )
    assert res.status_code == 201

    time.sleep(short_session + 1)

    headers = {"Authorization": f"Bearer {res.json['token']}"}
    res2 = client.get("/api/layout", headers=headers)
    assert res2.status_code == 401
    assert "expired" in res2.json["error"].lower()


def test_token_invalid(client):
    headers = {"Authorization": "Bearer asda.asda.asda"}
    res = client.get("/api/layout", headers=headers)
    assert res.status_code == 401
    assert "invalid" in res.json["error"].lower()
