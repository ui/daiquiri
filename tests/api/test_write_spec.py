import os
import pytest

_HERE = os.path.dirname(__file__)
DAIQUIRI_DOC = os.path.join(_HERE, "..", "..", "doc")


@pytest.mark.apispec
def test_write_spec(write_spec):
    assert os.path.exists(f"{DAIQUIRI_DOC}/api/spec.json")
