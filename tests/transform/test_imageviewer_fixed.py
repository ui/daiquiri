import pytest

from daiquiri.core.transform import imageviewer


def test_imageviewer_fine_fixed(motors):
    motordict = {
        "x": motors["simy"],
        "x_fine": motors["simpy"],
        "y": motors["simz"],
        "y_fine": motors["simpz"],
        "z": motors["simx"],
    }
    unitdict = {
        "simy": motors["simy"].get("unit"),
        "simpy": motors["simpy"].get("unit"),
        "simz": motors["simz"].get("unit"),
        "simpz": motors["simpz"].get("unit"),
        "simx": motors["simx"].get("unit"),
    }
    canvas = imageviewer.ImageViewerCanvas(
        motors=motordict,
        units="nm",
        unitdict=unitdict,
        sampleoffset=[0, 0, 0],
        beamoffset=[0, 0, 0],
        focaloffset=[0, 0, 0],
        vlmimageshape=(40, 30),
        zoomlevel="x12",
        zoominfo={
            "x12": {"pixelsize": [1e4, 1e4], "focalpoint": [3, -2]},
        },
        downstream=True,
    )

    motors["simy"].move(0)
    motors["simz"].move(0)
    motors["simy"].wait()
    motors["simz"].wait()

    # Without fixed fine axes, check an ROI of 50um moves fine axes only
    absol = canvas.canvas_to_motor([[0, -0], [50e3, -50e3]])
    assert absol["fixed"]["x"]["destination"] == motors["simy"].get("position")
    assert absol["fixed"]["y"]["destination"] == motors["simz"].get("position")

    canvas.motors_to_world.trnx.smallest_fixed = True
    canvas.motors_to_world.trny.smallest_fixed = True
    canvas.motors_to_world.trnz.smallest_fixed = True

    # With fixed fine axes, check an ROI of 50um moves coarse axes
    absol2 = canvas.canvas_to_motor([[0, -0], [50e3, -50e3]])
    assert absol2["fixed"]["x"]["destination"] != motors["simy"].get("position")
    assert absol2["fixed"]["y"]["destination"] != motors["simz"].get("position")


def test_imageviewer_coarse_fixed(motors):
    motordict = {
        "x": motors["simy"],
        "x_fine": motors["simpy"],
        "y": motors["simz"],
        "y_fine": motors["simpz"],
        "z": motors["simx"],
    }
    unitdict = {
        "simy": motors["simy"].get("unit"),
        "simpy": motors["simpy"].get("unit"),
        "simz": motors["simz"].get("unit"),
        "simpz": motors["simpz"].get("unit"),
        "simx": motors["simx"].get("unit"),
    }
    canvas = imageviewer.ImageViewerCanvas(
        motors=motordict,
        units="nm",
        unitdict=unitdict,
        sampleoffset=[0, 0, 0],
        beamoffset=[0, 0, 0],
        focaloffset=[0, 0, 0],
        vlmimageshape=(40, 30),
        zoomlevel="x12",
        zoominfo={
            "x12": {"pixelsize": [1e4, 1e4], "focalpoint": [3, -2]},
        },
        downstream=True,
    )

    motors["simy"].move(0)
    motors["simz"].move(0)
    motors["simy"].wait()
    motors["simz"].wait()

    assert motors["simy"].get("position") == 0
    assert motors["simpy"].get("limits") == [0, 100]

    # Without fixed coarse axes a move of 150um is reachable
    canvas.canvas_to_motor([[150e3, -150e3]])

    canvas.motors_to_world.trnx.largest_fixed = True
    canvas.motors_to_world.trnx.largest_fixed = True
    canvas.motors_to_world.trnx.largest_fixed = True

    # With fixed coarse axes a move of 150um should raise
    with pytest.raises(ValueError):
        canvas.canvas_to_motor([[150e3, -150e3]])
