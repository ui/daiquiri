import pytest
import numpy
from daiquiri.core.transform import maps
from daiquiri.core.transform import sets


def test_surjection():
    class CustomTransform(maps.Surjection):
        """
        f: D ⊂ R3 -> C ⊂ R2: x->[x[0]^2, cos(a*x[1]+b*x[2])]
        D: ]-inf, inf[, ]-2, 1[, R
        C: ]-inf, inf[, [-1, 1]
        """

        def __init__(self, a=1, b=0):
            limits = [
                [0, numpy.inf, False, False],
                [-2, 1, True, False],
                [-numpy.inf, numpy.inf, True, True],
            ]
            domain = sets.RealInterval(limits=limits)
            limits = [[-numpy.inf, numpy.inf, False, False], [-1, 1, True, True]]
            codomain = sets.RealInterval(limits=limits)
            super().__init__(domain=domain, codomain=codomain)
            self.a = a
            self.b = b

        def _forward(self, coord):
            """
            :param array coord: shape is (npoints, ndomain)
            :returns array-like: shape is (npoints, ncodomain)
            """
            icoord = numpy.zeros((len(coord), self.codomain.ndim), dtype=float)
            icoord[:, 0] = coord[:, 0] ** 2
            icoord[:, 1] = numpy.cos(self.a * coord[:, 1] + self.b * coord[:, 2])
            return icoord

        def _right_inverse(self, icoord):
            """Destination not necessarily unique

            :param array icoord: shape is (npoints, ncodomain)
            :returns array-like: shape is (npoints, ndomain)
            """
            coord = numpy.zeros((len(icoord), self.domain.ndim), dtype=float)
            coord[:, 0] = icoord[:, 0] ** 2
            coord[:, 2] = numpy.arccos(icoord[:, 1]) / self.b
            return coord

    trn = CustomTransform(a=3.2, b=-0.2)

    # Valid domain coordinates
    coords = []
    coords.append([[1, 0.5, -1.2]])
    coords.append([[1, 0.5, -1.2], [10, -1, -100.2]])
    assert_in_domain(trn, coords)

    # Invalid domain coordinates
    coords = []
    coords.append([[0, 10, -1.2]])
    coords.append([[1, 0.5, -1.2], [10, 3, -1.2]])
    assert_notin_domain(trn, coords)

    # Valid codomain coordinates
    icoords = []
    icoords.append([[1, 1]])
    icoords.append([[1, 0.5], [10, -1]])
    assert_in_codomain(trn, icoords)
    assert_in_image(trn, icoords)

    # Invalid codomain coordinates
    icoords = []
    icoords.append([[1, -1.01]])
    icoords.append([[1, 0.5], [10, 12]])
    assert_notin_codomain(trn, icoords)
    assert_notin_image(trn, icoords)

    # Round-trip
    coord = trn.domain.random_uniform(10000)
    assert trn.inverse(trn.forward(coord)) in trn.domain
    icoord = trn.codomain.random_uniform(10000)
    trn.forward(trn.inverse(icoord)) in trn.codomain


def test_injection():
    class CustomTransform(maps.Injection):
        """
        f: D ⊂ R2 -> C ⊂ R2: x->[exp(x[0]), ln(x[1])]
        D: [-inf, inf], [0, inf]
        C: [-inf, inf], [-inf, inf]
        """

        def __init__(self):
            limits = [[-numpy.inf, numpy.inf, True, True], [0, numpy.inf, True, True]]
            domain = sets.RealInterval(limits=limits)
            codomain = sets.RealInterval(ndim=2)
            limits = [[0, numpy.inf, True, True], [-numpy.inf, numpy.inf, True, True]]
            self._image = sets.RealInterval(limits=limits)
            super().__init__(domain=domain, codomain=codomain)

        @property
        def image(self):
            return self._image

        def _forward(self, coord):
            """
            :param array coord: shape is (npoints, ndomain)
            :returns array-like: shape is (npoints, ncodomain)
            """
            icoord = numpy.zeros((len(coord), self.codomain.ndim), dtype=float)
            icoord[:, 0] = numpy.exp(coord[:, 0])
            icoord[:, 1] = numpy.log(coord[:, 1])
            return icoord

        def _left_inverse(self, icoord):
            """
            :param array icoord: shape is (npoints, ncodomain)
            :returns array-like: shape is (npoints, ndomain)
            """
            coord = numpy.zeros((len(icoord), self.domain.ndim), dtype=float)
            coord[:, 0] = numpy.log(icoord[:, 0])
            coord[:, 1] = numpy.exp(icoord[:, 1])
            return coord

    trn = CustomTransform()

    # Valid domain coordinates
    coords = []
    coords.append([[1, 10]])
    coords.append([[1, 10], [-5.2, 2.3]])
    assert_in_domain(trn, coords)

    # Invalid domain coordinates
    coords = []
    coords.append([[1, -10]])
    coords.append([[1, 10], [-5.2, -2.3]])
    assert_notin_domain(trn, coords)

    # Valid codomain coordinates
    icoords = []
    icoords.append([[1, 1]])
    icoords.append([[1, 0.5], [10, -1]])
    assert_in_codomain(trn, icoords)
    assert_in_image(trn, icoords)

    # Invalid image coordinates
    icoords = []
    icoords.append([[-1, -1.01]])
    icoords.append([[-2, 0.5], [10, 12]])
    assert_in_codomain(trn, icoords)
    assert_notin_image(trn, icoords)

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=100)
    assert coord in trn.domain
    numpy.testing.assert_allclose(coord, trn.inverse(trn.forward(coord)))
    icoord = trn.image.random_uniform(10000, maxnum=100)
    numpy.testing.assert_allclose(icoord, trn.forward(trn.inverse(icoord)))


@pytest.mark.flaky(reruns=3)
def test_linearinjection():
    matrix = [
        [1.1 * numpy.cos(0.1), -0.3 * numpy.sin(0.1), 0.1],
        [0.6 * numpy.sin(0.1), 2.1 * numpy.cos(0.1), 0.1],
        [0, 0, 1],
    ]
    limits = [[0, 1], [0, 1], [-numpy.inf, numpy.inf]]
    domain = sets.RealInterval(limits=limits)
    codomain = sets.RealVectorSpace(3)
    trn = maps.LinearInvertibleInjection(
        matrix=matrix, domain=domain, codomain=codomain
    )

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=100)
    assert coord in trn.domain
    numpy.testing.assert_allclose(coord, trn.inverse(trn.forward(coord)))

    # We don't know the image yet
    # icoord = trn.image.random_uniform(10000, maxnum=100)
    # numpy.testing.assert_allclose(icoord, trn.forward(trn.inverse(icoord)))


def test_isomorphism():
    matrix = [
        [1.1 * numpy.cos(0.1), -0.3 * numpy.sin(0.1), 0.1],
        [0.6 * numpy.sin(0.1), 2.1 * numpy.cos(0.1), 0.1],
        [0, 0, 1],
    ]
    domain = sets.RealVectorSpace(3)
    codomain = sets.RealVectorSpace(3)
    trn = maps.Isomorphism(matrix=matrix, domain=domain, codomain=codomain)

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=100)
    assert coord in trn.domain
    numpy.testing.assert_allclose(coord, trn.inverse(trn.forward(coord)))
    icoord = trn.image.random_uniform(10000, maxnum=100)
    numpy.testing.assert_allclose(icoord, trn.forward(trn.inverse(icoord)))


def test_axiscombination():
    class StepperPiezo(maps.AxisLinearCombination):
        """y(nm) = samy(mm) + sampy(um) + y0(nm)"""

        def __init__(self, offset=0):
            coefficients = [1e6, 1e3]
            limits = [[-1, 1], [0, 100]]
            super().__init__(coefficients=coefficients, offset=offset, limits=limits)

        def piezo_limits(self, reference=None):
            return super().codomain_limits(1, reference=reference)

        def stepper_limits(self, reference=None):
            return super().codomain_limits(0, reference=reference)

    offset = 10000
    trn = StepperPiezo(offset=offset)

    # Image limits that can be reached by the motors
    numpy.testing.assert_almost_equal(trn.image.limits.low, -1e6 + offset)
    numpy.testing.assert_almost_equal(trn.image.limits.high, 1e6 + 100e3 + offset)
    off = 30e3 + offset
    numpy.testing.assert_almost_equal(
        trn.stepper_limits([0.1, 30]), [off - 1e6, off + 1e6]
    )
    off = 0.2e6 + offset
    numpy.testing.assert_almost_equal(trn.piezo_limits([0.2, 50]), [off, off + 100e3])

    center = trn.domain.center
    numpy.testing.assert_almost_equal(center, [0, 50])

    # Valid domain coordinates
    coords = []
    coords.append([[0.1, 30]])
    coords.append([[0.1, 30], [-1, 100]])
    assert_in_domain(trn, coords)

    # Invalid domain coordinates
    coords = []
    coords.append([[0.1, 120]])
    coords.append([[-2, 30], [0.1, 30]])
    assert_notin_domain(trn, coords)

    # Valid codomain coordinates
    icoords = []
    icoords.append([[11234]])
    icoords.append([[-11234], [11234]])
    assert_in_codomain(trn, icoords)
    # assert_in_image(trn, icoords)

    # Invalid codomain coordinates
    icoords = []
    icoords.append([[1e7]])
    icoords.append([[1e7], [0]])
    assert_notin_codomain(trn, icoords)
    # assert_notin_image(trn, icoords)

    # Within piezo range
    reference = [0.1, 20]
    icoord = [[0.1e6 + 10e3 + offset], [0.1e6 + 60e3 + offset]]
    expected = [[0.1, 10], [0.1, 60]]
    coord = trn.inverse(icoord, reference=reference)
    numpy.testing.assert_almost_equal(icoord, trn.forward(coord))
    numpy.testing.assert_almost_equal(coord, expected)

    # Within piezo range but steppers are off
    reference = [0.2, 20]
    icoord = [[0.1e6 + 40e3 + offset], [0.1e6 + 60e3 + offset]]
    expected = [[0.1, 40], [0.1, 60]]
    coord = trn.inverse(icoord, reference=reference)
    numpy.testing.assert_almost_equal(icoord, trn.forward(coord))
    numpy.testing.assert_almost_equal(coord, expected)

    # Within piezo range but steppers are off
    reference = [0.2, 20]
    icoord = [[0.1e6 + 40e3 + offset], [0.1e6 + 120e3 + offset]]
    expected = [[0.13, 10], [0.13, 90]]
    coord = trn.inverse(icoord, reference=reference)
    numpy.testing.assert_almost_equal(icoord, trn.forward(coord))
    numpy.testing.assert_almost_equal(coord, expected)

    # Not within piezo range
    reference = [0.2, 20]
    icoord = [[0.1e6 + 40e3 + offset], [0.1e6 + 200e3 + offset]]
    expected = [[0.09, 50], [0.25, 50]]
    coord = trn.inverse(icoord, reference=reference)
    numpy.testing.assert_almost_equal(icoord, trn.forward(coord))
    numpy.testing.assert_almost_equal(coord, expected)


def assert_in_domain(trn, coords):
    for coord in coords:
        assert coord in trn.domain
        trn.forward(coord)


def assert_notin_domain(trn, coords):
    for coord in coords:
        assert coord not in trn.domain
        with pytest.raises(sets.NotInSetError):
            trn.forward(coord)


def assert_in_codomain(trn, icoords):
    for icoord in icoords:
        assert icoord in trn.codomain


def assert_notin_codomain(trn, icoords):
    for icoord in icoords:
        assert icoord not in trn.codomain


def assert_in_image(trn, icoords):
    for icoord in icoords:
        assert icoord in trn.image
        trn.inverse(icoord)


def assert_notin_image(trn, icoords, surjective=True):
    for icoord in icoords:
        assert icoord not in trn.image
        if surjective:
            with pytest.raises(sets.NotInSetError):
                trn.inverse(icoord)
