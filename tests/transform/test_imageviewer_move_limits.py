from daiquiri.core.transform import imageviewer


def test_imageviewer_move_limits(motors):
    motordict = {
        "x": motors["simy"],
        "x_fine": motors["simpy"],
        "y": motors["simz"],
        "y_fine": motors["simpz"],
        "z": motors["simx"],
    }
    unitdict = {
        "simy": motors["simy"].get("unit"),
        "simpy": motors["simpy"].get("unit"),
        "simz": motors["simz"].get("unit"),
        "simpz": motors["simpz"].get("unit"),
        "simx": motors["simx"].get("unit"),
    }
    canvas = imageviewer.ImageViewerCanvas(
        motors=motordict,
        units="nm",
        unitdict=unitdict,
        sampleoffset=[0, 0, 0],
        beamoffset=[0, 0, 0],
        focaloffset=[0, 0, 0],
        vlmimageshape=(40, 30),
        zoomlevel="x12",
        zoominfo={
            "x12": {"pixelsize": [1e4, 1e4], "focalpoint": [3, -2]},
        },
        downstream=True,
    )

    # Check limits are correctly 0 - 100um
    assert motors["simpy"].get("limits") == [0, 100]
    # Inverted motors anyway have limits in the same order
    assert motors["simpz"].get("limits") == [0, 100]

    # Check move of 100um moves the fine motors
    absol = canvas.canvas_to_motor([[0, -0], [100e3, -100e3]])
    assert "x_fine" in absol["variable"]
    assert "y_fine" in absol["variable"]

    # Check move of > 100um moves corse motors
    absol2 = canvas.canvas_to_motor([[0, -0], [101e3, -101e3]])
    assert "x_fine" in absol2["fixed"]
    assert "y_fine" in absol2["fixed"]
