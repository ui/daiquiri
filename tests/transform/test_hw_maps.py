import pytest
import numpy
from daiquiri.core.transform import hw_maps


def test_motor_linearcombination(motors):
    # Transformation: y(nm) = m1(mm) + m2(um) + offset(nm)

    unitdict = {"m1": "mm", "m2": "um"}
    stepperpiezo = [motors["simy"], motors["simpy"]]
    trn = hw_maps.MotorLinearCombination(
        stepperpiezo, offset=1000, units="nm", unitdict=unitdict
    )

    # Check round trip transform
    pos1 = trn.domain.current_position
    pos2 = trn.inverse(trn.forward(pos1))[0]
    numpy.testing.assert_array_equal(pos1, pos2)

    # Check expected positions and limits when centering
    trn.domain.move(trn.domain.center)
    pos1 = trn.domain.current_position
    pos2 = numpy.asarray([1.0, 50])
    numpy.testing.assert_array_equal(pos1, pos2)

    calc = trn.codomain_limits("simy")
    expected = numpy.asarray([-4, 6]) * 1e6 + 50 * 1e3 + 1000
    numpy.testing.assert_allclose(calc, expected)

    calc = trn.codomain_limits("simpy")
    expected = 1 * 1e6 + numpy.asarray([0, 100]) * 1e3 + 1000
    numpy.testing.assert_allclose(calc, expected)

    # Destination not within the motor limits
    destination = trn.domain.current_position
    destination[1] = 200
    with pytest.raises(ValueError):
        trn.domain.move(destination)

    # Destination within the fine range
    trn.domain.move([1, 50])
    y = trn.forward(trn.domain.current_position)
    calc = trn.inverse(y + 10e3)[0]  # + 10 um
    expected = numpy.asarray([1.0, 60])
    numpy.testing.assert_allclose(calc, expected)

    # Destination not within the fine range
    trn.domain.move([1, 0])
    y = trn.forward(trn.domain.current_position)
    calc = trn.inverse(y - 10e3)[0]  # -10 um
    expected = numpy.asarray([1.0 - 0.060, 50])
    numpy.testing.assert_allclose(calc, expected)

    # Destination not within the coarse range
    with pytest.raises(ValueError):
        trn.inverse(1e8)

    # Scan within the fine range
    trn.domain.move([1, 40])
    y = trn.forward(trn.domain.current_position)[0]
    yscan = [y - 40e3, y + 1e3]
    calc = trn.inverse(yscan)
    expected = numpy.asarray([[1, 0], [1, 41]])
    numpy.testing.assert_allclose(calc, expected)

    # Scan not within the fine range
    trn.domain.move([1, 0])
    y = trn.forward(trn.domain.current_position)[0]
    yscan = [y - 10e3, y + 30e3]
    calc = trn.inverse(yscan)
    expected = numpy.asarray([[1 - 0.040, 30], [1 - 0.040, 70]])
    numpy.testing.assert_allclose(calc, expected)
