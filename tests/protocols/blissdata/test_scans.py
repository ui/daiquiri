import pytest
import time
from bliss.common.scans import ascan
from bliss.scanning.group import Sequence


try:
    import blissdata.redis_engine  # noqa
except ImportError:
    pytest.skip("blissdata is not available", allow_module_level=True)


@pytest.fixture
def scans(app_min):
    from daiquiri.core.hardware.blissdata.scans import BlissdataScans

    service = BlissdataScans(
        config={}, app=app_min, source_config={"session": "test_session"}
    )
    yield service
    service.close()


def test_scan(bliss_omega, bliss_diode, bliss_mca, scans):
    previous_scan_list = scans.get_scans()
    nb_previous_scans = len(previous_scan_list["rows"])

    points = 5
    ascan(bliss_omega, 0, 20, points, 0.1, bliss_diode, bliss_mca)

    scan_list = scans.get_scans()
    assert len(scan_list["rows"]) - nb_previous_scans == 1

    last_scan = scan_list["rows"][0]

    data = scans.get_scan_data(last_scan["scanid"])
    xs = data["axes"]["xs"]

    # intervals vs points
    assert len(data["data"][xs[0]]["data"]) == points + 1

    spectra = scans.get_scan_spectra(last_scan["scanid"], point=0)
    assert len(spectra["data"]) == 4

    assert spectra["npoints"] == points + 1

    ys = data["axes"]["ys"]["spectra"]
    spectrum = spectra["data"][ys[0]]

    assert spectrum["name"] == ys[0]
    assert len(spectrum["data"][0]) == 1024


def test_sequence(bliss_omega, bliss_diode, scans):
    previous_scan_list = scans.get_scans()
    nb_previous_scans = len(previous_scan_list["rows"])

    sequence = Sequence(title="Sequence")

    with sequence.sequence_context() as scan_seq:
        scan1 = ascan(bliss_omega, 0, 1, 2, 0.1, bliss_diode, run=False)
        scan_seq.add_and_run(scan1)
        scan2 = ascan(bliss_omega, 0, 1, 3, 0.1, bliss_diode, run=False)
        scan_seq.add_and_run(scan2)

    scan_list = scans.get_scans()
    assert len(scan_list["rows"]) - nb_previous_scans == 3
    last_scan = scan_list["rows"][0]
    data = scans.get_scan_data(last_scan["scanid"])
    assert data is not None


def test_lima(bliss_omega, bliss_lima, scans):
    previous_scan_list = scans.get_scans()
    nb_previous_scans = len(previous_scan_list["rows"])

    # centered roi
    size = 2
    bliss_lima.image.roi = 1024 // 2 - size // 2, 1024 // 2 - size // 2, size, size
    ascan(bliss_omega, 0, 1, 2, 0.1, bliss_lima)

    scan_list = scans.get_scans()
    assert len(scan_list["rows"]) - nb_previous_scans == 1

    last_scan = scan_list["rows"][0]

    data = scans.get_scan_data(last_scan["scanid"])
    ys = data["axes"]["ys"]
    assert ys["images"] == ["lima_simulator:image"]
    assert data["data"]["lima_simulator:image"] == {
        "data": [],
        "dtype": "<u4",
        "name": "lima_simulator:image",
        "shape": [size, size],
        "size": 3,
    }

    frame0 = scans.get_scan_image(
        last_scan["scanid"], "lima_simulator:image", image_no=0
    )
    assert frame0.shape == (size, size)

    frame1 = scans.get_scan_image(
        last_scan["scanid"], "lima_simulator:image", image_no=1
    )
    frame2 = scans.get_scan_image(
        last_scan["scanid"], "lima_simulator:image", image_no=2
    )

    assert frame0[0, 0] >= 80, "Wrong frame0"
    assert frame1[0, 0] >= 180, "Wrong frame1"
    assert frame2[0, 0] >= 280, "Wrong frame2"


def test_scan_watch(bliss_omega, bliss_diode, scans):
    class ScanWatch:
        def __init__(self):
            self._new_scan = False
            self._new_data = False
            self._end_scan = False

        def new_scan(self, *args, **kwargs):
            self._new_scan = True

        def new_data(self, *args, **kwargs):
            self._new_data = True

        def end_scan(self, *args, **kwargs):
            self._end_scan = True

        @property
        def all(self):
            return [self._new_scan, self._new_data, self._end_scan]

    sw = ScanWatch()

    scans.watch_new_scan(sw.new_scan)
    scans.watch_end_scan(sw.end_scan)
    scans.watch_new_data(sw.new_data)

    ascan(bliss_omega, 0, 20, 5, 0.1, bliss_diode)

    # TODO: Rubbish synchronisation :(
    # Message is sometimes emitted after scan finishes
    time.sleep(1)

    assert all(sw.all)
