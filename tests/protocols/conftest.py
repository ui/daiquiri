import pytest
from bliss.config.static import get_config


@pytest.fixture
def bliss_omega(app_min):
    return get_config().get("omega")


@pytest.fixture
def bliss_objectref1(app_min):
    return get_config().get("objectref1")


@pytest.fixture
def bliss_diode(app_min):
    return get_config().get("diode")


@pytest.fixture
def bliss_tomo_detectors(app_min):
    return get_config().get("tomo_detectors")


@pytest.fixture
def bliss_tomo_detector(app_min):
    return get_config().get("tomo_detector")


@pytest.fixture
def bliss_shutter1(app_min):
    shutter = get_config().get("shutter1")
    # reach the attribute so what it is disabled
    # FIXME: This is a hack to use an abstract BLISS shutter as a mock
    try:
        shutter.mode = None
    except Exception:
        pass
    return shutter


@pytest.fixture
def bliss_mca(app_min):
    return get_config().get("simu1")


@pytest.fixture
def bliss_lima(app_min, lima_simulator):
    return get_config().get("lima_simulator")
