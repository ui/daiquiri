import pytest

from daiquiri.core.exceptions import SyntaxErrorYAML
from daiquiri.resources import utils


def test_include(mock_resource_provider):
    mock_resource_provider.create_resource("layout", "include.yml", "include: true")
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include include.yml"
    )

    obj = utils.YamlDict("layout", "test.yml")
    assert obj["value"]["include"] is True


def test_include_ref(mock_resource_provider):
    mock_resource_provider.create_resource("layout", "include.yml", "include: true")
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include include.yml#include"
    )

    obj = utils.YamlDict("layout", "test.yml")
    assert obj["value"] is True


def test_include_ref_no_key(mock_resource_provider):
    mock_resource_provider.create_resource("layout", "include.yml", "include: true")
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include include.yml#second"
    )

    with pytest.raises(SyntaxErrorYAML) as e:
        utils.YamlDict("layout", "test.yml")
    assert "include.yml" in str(e.value.args[0]["error"])
    assert "no such key" in str(e.value.args[0]["error"])


def test_include_ref_wrong_extension(mock_resource_provider):
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include include.txt"
    )

    with pytest.raises(SyntaxErrorYAML) as e:
        utils.YamlDict("layout", "test.yml")

    assert "test.yml" in str(e.value.args[0]["error"])
    assert "must have extension" in str(e.value.args[0]["error"])


def test_include_syntax_error(mock_resource_provider):
    mock_resource_provider.create_resource(
        "layout", "include.yml", "include: true\nsecond moo"
    )
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include include.yml#include"
    )

    with pytest.raises(SyntaxErrorYAML) as e:
        utils.YamlDict("layout", "test.yml")

    assert "include.yml" in str(e.value.args[0]["error"])


def test_include_ref_none_object(mock_resource_provider):
    # This is valid yaml but not an object
    mock_resource_provider.create_resource("layout", "include.yml", "include true")
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include include.yml#include"
    )

    with pytest.raises(SyntaxErrorYAML) as e:
        utils.YamlDict("layout", "test.yml")

    assert "include.yml" in str(e.value.args[0]["error"])
    assert "not an object" in str(e.value.args[0]["error"])
