from warnings import warn

import pytest

from daiquiri.resources.utils import ConfigDict
from daiquiri.resources.utils import get_resource_provider
from daiquiri.core.exceptions import SyntaxErrorYAML
from daiquiri.core.resources.resource_provider import ResourceNotAvailable


def test_resource_loader(mock_resource_provider):
    mock_resource_provider.create_resource("config", "test.yml", "value: true")

    conf = ConfigDict("test.yml")
    assert conf["value"] is True


def test_list_resources(mock_resource_provider):
    mock_resource_provider.create_resource("config", "test.yml", "value: true")
    mock_resource_provider.create_resource("config", "test1.yml", "value: true")
    mock_resource_provider.create_resource("config", "toto.yml", "value: true")

    provider = get_resource_provider()
    names = provider.list_resource_names("config", "test*.yml")
    input_names = ["test.yml", "test1.yml"]
    if names != input_names:
        warn("Resource list not in order")
    assert set(names) == set(input_names)


def test_no_such_resource():
    with pytest.raises(ResourceNotAvailable):
        ConfigDict("none.yml")


def test_synax_error_resource(mock_resource_provider):
    content = "value: true\nsecond true"
    mock_resource_provider.create_resource("config", "test.yml", content)

    with pytest.raises(SyntaxErrorYAML):
        ConfigDict("test.yml")
