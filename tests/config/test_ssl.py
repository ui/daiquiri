import os.path
from daiquiri.cli.server import get_ssl_context
from daiquiri.core.options import ServerOptions


def test_ssl_context(mock_resource_provider, mocker):
    """Create a SSL configuration

    Make sure the SSL load_cert_chain was called with the right files from the config
    """
    mock_resource_provider.create_resource(
        "config",
        "app.yml",
        """
ssl: true
ssl_cert: cert.txt
ssl_key: key.txt
    """,
    )

    mock_resource_provider.create_resource("certs", "cert.txt", "SECRET CERT")
    mock_resource_provider.create_resource("certs", "key.txt", "SECRET KEY")

    mocker.patch("ssl.SSLContext.load_cert_chain")
    context = get_ssl_context(ServerOptions())
    assert context is not None

    assert context.load_cert_chain.call_count == 1
    certfile, keyfile = context.load_cert_chain.call_args[0]
    assert certfile.endswith("cert.txt")
    assert keyfile.endswith("key.txt")
    assert os.path.exists(certfile)
    assert os.path.exists(keyfile)


def test_absolute_ssl_context(mock_resource_provider, mocker):
    """Create a SSL configuration

    Make sure the SSL load_cert_chain was called with the right files from the config
    """
    mock_resource_provider.create_resource("certs", "cert.txt", "SECRET CERT")
    mock_resource_provider.create_resource("certs", "key.txt", "SECRET KEY")
    certpath = mock_resource_provider.abs_path("certs", "cert.txt")
    keypath = mock_resource_provider.abs_path("certs", "key.txt")

    mock_resource_provider.create_resource(
        "config",
        "app.yml",
        f"""
ssl: true
ssl_cert: {certpath}
ssl_key: {keypath}
    """,
    )

    mocker.patch("ssl.SSLContext.load_cert_chain")
    context = get_ssl_context(ServerOptions())
    assert context is not None

    assert context.load_cert_chain.call_count == 1
    certfile, keyfile = context.load_cert_chain.call_args[0]
    assert certfile == certpath
    assert keyfile == keypath
    assert os.path.exists(certfile)
    assert os.path.exists(keyfile)


def test_ssl_context_from_options(mock_resource_provider, mocker):
    """Create a SSL configuration

    Make sure the SSL load_cert_chain was called with the right files from the config
    """
    mock_resource_provider.create_resource(
        "config",
        "app.yml",
        """
ssl: false
ssl_cert: cert2.txt
ssl_key: key2.txt
    """,
    )

    mock_resource_provider.create_resource("certs", "cert.txt", "SECRET CERT")
    mock_resource_provider.create_resource("certs", "key.txt", "SECRET KEY")

    mocker.patch("ssl.SSLContext.load_cert_chain")
    options = ServerOptions(ssl=True, ssl_cert="cert.txt", ssl_key="key.txt")
    context = get_ssl_context(options)
    assert context is not None

    assert context.load_cert_chain.call_count == 1
    certfile, keyfile = context.load_cert_chain.call_args[0]
    assert certfile.endswith("cert.txt")
    assert keyfile.endswith("key.txt")
    assert os.path.exists(certfile)
    assert os.path.exists(keyfile)
