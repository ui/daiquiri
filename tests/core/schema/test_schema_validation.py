from daiquiri.core.schema import Schema
from daiquiri.core.components import ComponentActorSchema
from marshmallow import fields
from marshmallow import ValidationError, validates_schema


class MockedSchema(Schema):
    def __init__(self, mocker):
        self._hardware = mocker.Mock()
        self._hardware.get_objects = lambda: []
        self._session = mocker.Mock()
        app = mocker.Mock()
        socketio = mocker.Mock()
        docs = mocker.Mock()
        super(MockedSchema, self).__init__({}, app=app, socketio=socketio, docs=docs)


def test_valid(mocker):
    """Nothing special append for valid data"""
    schema = MockedSchema(mocker)

    class TestSchema(ComponentActorSchema):
        DEBUG = True
        number = fields.Int()

    schema._schemas["test"] = TestSchema()
    result = schema.validate("test", {"number": 1})
    assert result["errors"] == {}


def test_invalid(mocker):
    """Nothing special append for invalid data"""
    schema = MockedSchema(mocker)

    class TestSchema(ComponentActorSchema):
        DEBUG = True
        number = fields.Int()

    schema._schemas["test"] = TestSchema()
    result = schema.validate("test", {"number": None})
    assert result["errors"] == {}


def test_schema_validation(mocker):
    """
    Validate a valid data.

    If ValidationError is raised by `validates_schema`, the message is returned"""
    schema = MockedSchema(mocker)

    class TestSchema(ComponentActorSchema):
        DEBUG = True
        number = fields.Int()

        @validates_schema
        def schema_validate(self, data, **kwargs):
            raise ValidationError("Oupsi")

    schema._schemas["test"] = TestSchema()
    result = schema.validate("test", {"number": 1})
    assert result["errors"] == {"schema": ["Oupsi"]}


def test_invalid_field_initialized_by_calculated(mocker):
    """
    Even if data is not valid, calculated is called.
    """
    schema = MockedSchema(mocker)

    class TestSchema(ComponentActorSchema):
        DEBUG = True
        number = fields.Int(dump_default=None)

        def calculated(self, data, **kwargs):
            return {"number": 1}

    schema._schemas["test"] = TestSchema()
    result = schema.validate("test", {"number": None})
    assert result["calculated"]["number"] == 1


def test_invalid_field_with_estimation_time(mocker):
    """
    Even if data is not valid, you can have an estimation time.
    """
    schema = MockedSchema(mocker)

    class TestSchema(ComponentActorSchema):
        DEBUG = True
        number = fields.Int(dump_default=None)

        def time_estimate(self, data, **kwargs):
            return 10

    schema._schemas["test"] = TestSchema()
    result = schema.validate("test", {"number": None})
    assert result["time_estimate"] == 10


def test_invalid_field_with_warnings(mocker):
    """
    If data is invalid, you can have warnings.
    """
    schema = MockedSchema(mocker)

    class TestSchema(ComponentActorSchema):
        DEBUG = True
        number = fields.Int(dump_default=None)

        def warnings(self, data, **kwargs):
            return {"number": "Argggg"}

    schema._schemas["test"] = TestSchema()
    result = schema.validate("test", {"number": None})
    assert result["warnings"] == {"number": "Argggg"}
