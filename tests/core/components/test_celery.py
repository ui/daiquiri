from celery.result import AsyncResult


def test_celery_basics(celery_app, reloaded_celery_worker):
    future = celery_app.send_task("sleep")
    result = future.get()

    assert result is True


def test_celery_mimas(app, celery_app, reloaded_celery_worker):
    @celery_app.task(name="sidecar.celery.mimas.task.mimas")
    def mimas(event: str, dataCollectionId: int, **kwargs):
        return "mimas"

    reloaded_celery_worker.reload()

    celery_component = app.components.get_component("celery")
    task_id = celery_component.send_event(1, "start")

    future = AsyncResult(task_id)
    result = future.get(timeout=5)

    assert result == "mimas"
