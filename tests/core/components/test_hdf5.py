import h5py
from daiquiri.core.components.hdf5 import hdf5_to_dict
from daiquiri.core.responses import gzipped


def feed_h5_data(h5):
    h5["integer_0d"] = 10
    h5["float_0d"] = 3.14
    h5["string_0d"] = "foo"
    h5["bool_0d"] = True
    h5["integer_1d"] = [10]
    h5["float_1d"] = [3.14]
    h5["string_1d"] = ["foo"]
    h5["bool_1d"] = [True]
    h5["group/a"] = 10
    h5["attr"] = 10
    h5["attr"].attrs["integer_0d"] = 10
    h5["attr"].attrs["float_0d"] = 3.14
    h5["attr"].attrs["string_0d"] = "foo"
    h5["attr"].attrs["bool_0d"] = True
    h5["attr"].attrs["integer_1d"] = [10]
    h5["attr"].attrs["float_1d"] = [3.14]
    h5["attr"].attrs["string_1d"] = ["foo"]
    h5["attr"].attrs["bool_1d"] = [True]


def test_hdf5_to_dict_with_data(
    tmpdir,
):
    filename = tmpdir / "test.h5"
    with h5py.File(filename, mode="w") as h5:
        feed_h5_data(h5)

    result = hdf5_to_dict(filename, "/", load_data=True)
    assert result["children"]["integer_0d"]["dtype"] == "int64"
    assert result["children"]["integer_0d"]["data"] == 10
    assert result["children"]["string_0d"]["dtype"] == "string"
    assert result["children"]["string_0d"]["data"] == "foo"
    gzipped(result)
