from ruamel.yaml import YAML
from daiquiri.core.components.tomo import TomoComponent


class TomoComponentMock(TomoComponent):
    def __init__(self, mocker, config):
        self._session = mocker.Mock()
        app = mocker.Mock()
        docs = mocker.Mock()
        super(TomoComponentMock, self).__init__(config, app=app, docs=docs)


class ComponentsMock:
    def __init__(self, mocker):
        self.__components = {}
        self.__components["proxy"] = mocker.Mock()

    def get_component(self, component_type):
        return self.__components.get(component_type)


CONFIG = """
component: tomo
sources:
  - type: bliss
    session: test_session
    tomo_config: tomo_config

tomovis:
    url: http://localhost:8888/
    create_proxy: true
"""


def test_tomovis_proxy(beacon, tomo_installed, mocker):
    """Check that the proxy component is called when the tomo component is setup
    to proxy the tomovis"""
    yaml = YAML()
    config = yaml.load(CONFIG)
    tomo = TomoComponentMock(mocker, config)
    components = ComponentsMock(mocker)
    tomo.after_all_setup(components)
    proxy = components.get_component("proxy")
    assert proxy.setup_proxy.call_count == 1
    description = proxy.setup_proxy.call_args[0][0]
    assert description["name"] == "tomovis"
    assert description["target"] == "http://localhost:8888/"
