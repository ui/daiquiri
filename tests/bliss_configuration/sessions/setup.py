import os

from bliss import current_session
from bliss.common.standard import *
from bliss.common.event import dispatcher
from bliss.scanning.scan_display import ScanDisplay

# deactivate automatic Flint startup
ScanDisplay().auto = False

load_script("simple")

# SESSION_NAME = get_current_session().name

scan_saving = current_session.scan_saving
if os.environ.get("USE_NEXUS_WRITER"):
    if scan_saving.writer != "nexus":
        scan_saving.writer = "nexus"


def is_tomo_env():
    try:
        import tomo  # noqa
    except Exception:
        return False
    return True


if is_tomo_env():
    tomo_config = config.get("tomo_config")  # noqa
    tomo_config.set_active()

beamstop.move("IN")  # noqa: F405

# Do not remove this print (used in tests)
print("TEST_SESSION INITIALIZED")
