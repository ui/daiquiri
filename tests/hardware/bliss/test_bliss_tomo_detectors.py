def test_tomo_detectors(tomo_installed, tomo_detectors):
    """Get a tomo_detectors.

    Make sure the relationships are there
    """
    assert tomo_detectors.get("detectors") == ["hardware:tomo_detector"]
