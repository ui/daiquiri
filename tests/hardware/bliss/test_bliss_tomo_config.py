def test_tomo_config(tomo_installed, tomo_config):
    """Get a tomo config.

    Make sure the relationships are there
    """
    assert tomo_config.get("detectors") == "hardware:tomo_detectors"
    assert "hardware:tomo_config__" in tomo_config.get("sample_stage")
