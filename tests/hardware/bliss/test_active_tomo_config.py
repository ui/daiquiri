def test_state(tomo_installed, active_tomo_config):
    """Get the active tomo config.

    Make sure the relationships are there
    """
    assert type(active_tomo_config).__name__ == "Activetomoconfig"
    assert active_tomo_config.get("ref") == "hardware:tomo_config"
