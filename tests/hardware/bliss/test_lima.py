import gevent


def test_lima_simulator(tomo_installed, lima_simulator, lima1, bliss_lima1):
    """Get a lima detector.

    Make sure some content is there
    """
    # Make sure everything is synchronized
    for i in range(10):
        if lima1.get("state") != "OFFLINE":
            break
        gevent.sleep(0.5)

    assert lima1.get("state") == "READY"
    assert lima1.get("static")["lima_type"] == "Simulator"
    assert lima1.get("static")["image_max_dim"] == (1024, 1024)

    # Make sure everything is synchronized
    for i in range(10):
        if lima1.get("acc_max_expo_time") is not None:
            break
        gevent.sleep(0.5)

    assert lima1.get("acc_max_expo_time") == 1.0
    bliss_lima1.accumulation.max_expo_time = 2.0
    assert lima1.get("acc_max_expo_time") == 2.0
