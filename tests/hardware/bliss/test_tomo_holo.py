def test_tomo_holo(tomo_installed, tomo_holo):
    """Get a tomo_detectors.

    Make sure the relationships are there
    """
    assert tomo_holo.get("settle_time") == 0.0
