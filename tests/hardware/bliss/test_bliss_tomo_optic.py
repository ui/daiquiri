def test_tomo_optic(tomo_installed, fixedmag_optic):
    """Get a fixed tomo_optic.

    Check the parameters
    """
    assert fixedmag_optic.get("magnification") == 6.5
    assert fixedmag_optic.get("available_magnifications") is None


def test_tomo_detector_without_optic(tomo_installed, triplemic_optic):
    """Get a triple tomo optic

    Check the parameters
    """
    assert triplemic_optic.get("magnification") == 5
    magnifications = triplemic_optic.get("available_magnifications")
    assert set(magnifications) == {5, 7.5, 10}
