import gevent
import daiquiri


def test_procedure1(tomo_installed, bliss_procedure1, procedure1):
    """Get a procedure.

    Make sure the object is reachable.
    """
    assert procedure1.get("state") == "STANDBY"


def test_procedure1_start(tomo_installed, bliss_procedure1, procedure1):
    """Get a procedure.

    Make sure the object is reachable.
    """
    procedure1.call("start", None)
    for _ in range(20):
        if procedure1.get("state") == "STANDBY":
            break
        gevent.sleep(0.1)
    else:
        assert False, "Procedure is still not in STANDBY"
    assert daiquiri._test_procedure1 == "done"
