def test_bliss_omega(omega, bliss_omega):
    bliss_omega.move(20)
    assert bliss_omega.position == omega.get("position")


def test_bliss_ref(objectref1, bliss_objectref1, bliss_omega):
    """Get an objectref device and change the referred object

    Check that the new object is properly referred by Daiquiri as object name
    """
    assert objectref1.get("ref") == "hardware:roby"
    bliss_objectref1.ref = bliss_omega
    assert objectref1.get("ref") == "hardware:omega"


def test_bliss_none_ref(objectref1, bliss_objectref1):
    """Get an objectref device and set the referred object to None

    Check that the new object is properly referred by Daiquiri as a none ref
    """
    bliss_objectref1.ref = None
    assert objectref1.get("ref") == "hardware:"
