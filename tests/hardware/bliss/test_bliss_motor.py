import logging
import gevent
import pytest


def test_initialization(motor1, bliss_motor1):
    """Load the motor1 from bliss

    Check that the static state is right
    """
    assert motor1.user_tags() == ["anticlockwise"]


def test_move(motor1, bliss_motor1):
    """Move the daiquiri motor.

    Check that the BLISS axis is at the expected location.
    """
    motor1.move(1.0)
    bliss_motor1.wait_move()
    assert bliss_motor1.position == 1.0


def test_exception_at_motion_start(motor1, bliss_motor1):
    """Try to move the an unreachable position"""
    with pytest.raises(ValueError):
        motor1.move(9999999.0)


def test_exception_at_motion_stop(motor_with_weird_tolerance, caplog):
    """Move a daiquiri axis which fail at motion stop.

    Check that daiquiri is aware of such error.
    """
    caplog.set_level(logging.ERROR, logger="user")
    motor_with_weird_tolerance.move(1.33333333333)
    bliss_axis = motor_with_weird_tolerance._object
    try:
        bliss_axis.wait_move()
    except RuntimeError:
        gevent.sleep(0.1)
    assert len(caplog.records) == 1
    msg = caplog.records[0].getMessage()
    assert "motor_with_weird_tolerance" in msg
