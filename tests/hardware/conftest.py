import pytest

# TODO: parameterize to reuse a fixture for multiple backends
# test shutter (exists in bliss and tango)


@pytest.fixture
def omega(app):
    omega = app.hardware.get_object("omega")
    pos = omega.get("position")

    omega.move(100)
    omega.wait()

    yield omega

    omega.move(pos)
    omega.wait()


@pytest.fixture
def objectref1(app):
    objectref1 = app.hardware.get_object("objectref1")
    yield objectref1


@pytest.fixture
def shutter1(app):
    obj = app.hardware.get_object("shutter1")
    yield obj


@pytest.fixture
def motor1(app):
    obj = app.hardware.get_object("motor1")
    yield obj


@pytest.fixture
def motor_with_weird_tolerance(app):
    obj = app.hardware.get_object("motor_with_weird_tolerance")
    yield obj


@pytest.fixture
def robz_limits(app):
    robz = app.hardware.get_object("robz")
    limits = robz.get("limits")
    pos = robz.get("position")

    robz.set("limits", [-5, 5])
    robz.move(0)
    robz.wait()

    yield robz

    robz.set("limits", limits)
    robz.move(pos)
    robz.wait()


@pytest.fixture
def tomo_config(app):
    obj = app.hardware.get_object("tomo_config")
    yield obj


@pytest.fixture
def active_tomo_config(app):
    obj = app.hardware.get_object("active_tomo_config")
    yield obj


@pytest.fixture
def tomo_imaging(app):
    obj = app.hardware.get_object("tomo_imaging")
    yield obj


@pytest.fixture
def tomo_detectors(app):
    obj = app.hardware.get_object("tomo_detectors")
    yield obj


@pytest.fixture
def tomo_detector(app):
    obj = app.hardware.get_object("tomo_detector")
    yield obj


@pytest.fixture
def tomo_flat_motion(app):
    obj = app.hardware.get_object("tomo_flat_motion")
    yield obj


@pytest.fixture
def tomo_holo(app):
    obj = app.hardware.get_object("tomo_holo")
    yield obj


@pytest.fixture
def fixedmag_optic(app):
    obj = app.hardware.get_object("fixedmag_optic")
    yield obj


@pytest.fixture
def triplemic_optic(app):
    obj = app.hardware.get_object("triplemic_optic")
    yield obj


@pytest.fixture
def tomo_detector_without_optic(app):
    obj = app.hardware.get_object("tomo_detector_without_optic")
    yield obj


@pytest.fixture
def lima1(app):
    obj = app.hardware.get_object("lima1")
    yield obj


@pytest.fixture
def procedure1(app):
    obj = app.hardware.get_object("procedure1")
    yield obj
