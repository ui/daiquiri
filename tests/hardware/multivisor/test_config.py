import pytest
from ruamel.yaml import YAML

PROTOCOLS = """
component: hardware

protocols:
  - id:  multivisor
    type: multivisor
    address: http://localhost:22000

groups: []

objects:
  - url: multivisor://mainstation:id00:metadata
"""


@pytest.fixture
def multivisor_empty(requests_mock):
    data = """
    {
        "supervisors": {
            "mainstation": {
                "processes": {},
                "running": true
            }
        }
    }
    """
    requests_mock.get("http://localhost:22000/api/data", text=data)
    requests_mock.get("http://localhost:22000/api/stream")


def test_protocol(hardware_mock, multivisor_empty):
    yaml = YAML()
    config = yaml.load(PROTOCOLS)
    hardware_mock.read_config(config)
    o = hardware_mock.get_object("mainstation:id00:metadata")
    assert o is not None
