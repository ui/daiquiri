import pytest
from daiquiri.core.hardware import Hardware


@pytest.fixture
def hardware_mock(mocker):
    app = mocker.Mock()

    class HardwareMock(Hardware):
        def setup(self):
            pass

    h = HardwareMock(app)
    h._app = app
    h._base_config = {"versions": []}
    h._schema = mocker.Mock()
    try:
        yield h
    finally:
        h.disconnect()
