import glob
import numpy
import os
import pytest
import shutil
import subprocess
from PIL import Image

from daiquiri.core.hardware.tango.lima import parse_video_frame

try:
    import cv2
except ImportError:
    cv2 = None


DIR_PATH = os.path.dirname(os.path.realpath(__file__))
VIDEO_STREAMER = os.path.join(DIR_PATH, "video-streamer-mpeg")


def assert_most_pixels_close(result_path, reference_path, atol=5, validity=0.9):
    """
    Assert image comparaison pixels per pixels per composante (RGBA).

    Arguemnts:
        result_path: File path the test
        reference_path: File path to use as reference
        atol: The absolute tolerance per pixel composante `a - b < atol`
        validity: The percentage of pixel composantes which have to be valid
    """
    assert 0.0 < validity <= 1.0
    with Image.open(reference_path) as reference:
        with Image.open(result_path) as result:
            result = numpy.array(result)
            reference = numpy.array(reference)

            diff = numpy.abs(reference.astype(float) - result) <= atol
            nb_points = numpy.prod(diff.shape)
            assert numpy.count_nonzero(diff) >= nb_points * validity, numpy.vstack(
                (result[numpy.logical_not(diff)], reference[numpy.logical_not(diff)])
            ).T


def create_jpeg(data, path):
    if data.dtype == numpy.uint16:
        data = (data / 255).astype(numpy.uint8)

    im = Image.fromarray(data)
    im.save(path, "jpeg", quality=60)


def setup_module(module):
    if not os.path.exists(VIDEO_STREAMER):
        subprocess.check_call(
            [
                "git",
                "clone",
                "https://gitlab.esrf.fr/ui/video-streamer-mpeg",
                VIDEO_STREAMER,
            ]
        )


def teardown_module(module):
    shutil.rmtree(VIDEO_STREAMER)


@pytest.mark.skipif(not cv2, reason="opencv not available")
def test_frame_decoding():
    frames = glob.glob(os.path.join(DIR_PATH, "video-streamer-mpeg/tests/frames/*.raw"))
    for frame in frames:
        result = os.path.basename(frame).replace("raw", "jpg")
        reference = os.path.join(DIR_PATH, "results", result)

        if os.path.exists(reference):
            with open(frame, "rb") as f:
                data = parse_video_frame(f.read())
                create_jpeg(data, result)
                assert_most_pixels_close(result, reference)
                os.unlink(result)


@pytest.mark.skipif(not cv2, reason="opencv not available")
def test_frame_decoding_bayer_rescale():
    frame = os.path.join(
        DIR_PATH,
        "video-streamer-mpeg/tests/frames/basler_aca1920-40gc_11_1936x1216_frame0.raw",
    )
    result = os.path.basename(frame).replace(".raw", "_12bpp.jpg")
    reference = os.path.join(DIR_PATH, "results", result)
    with open(frame, "rb") as f:
        data = parse_video_frame(f.read(), 12)
        create_jpeg(data, result)
        assert_most_pixels_close(result, reference)
        os.unlink(result)


@pytest.mark.skipif(not cv2, reason="opencv not available")
def test_frame_rotation():
    frames = glob.glob(os.path.join(DIR_PATH, "video-streamer-mpeg/tests/frames/*.raw"))
    for frame in frames:
        for rot in [90, 270]:
            result = os.path.basename(frame).replace(".raw", f"_{rot}.jpg")
            reference = os.path.join(DIR_PATH, "results", result)

            if os.path.exists(reference):
                with open(frame, "rb") as f:
                    data = parse_video_frame(f.read(), rotation=rot)
                    create_jpeg(data, result)
                    assert_most_pixels_close(result, reference)
                    os.unlink(result)
