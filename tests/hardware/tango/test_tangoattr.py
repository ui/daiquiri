import pytest
from daiquiri.core.hardware.tango.tangoattr import TangoAttr


@pytest.fixture(scope="module")
def tangoattr_last_image_saved(module_mocker, module_lima_simulator):
    tango_url = "id00/limaccds/simulator1"
    app = module_mocker.Mock()
    tangoattr = TangoAttr(tango_url=tango_url, attrname="last_image_saved", app=app)
    yield tangoattr


@pytest.fixture(scope="module")
def tangoattr_user_detector_name(module_mocker, module_lima_simulator):
    tango_url = "id00/limaccds/simulator1"
    app = module_mocker.Mock()
    tangoattr = TangoAttr(tango_url=tango_url, attrname="user_detector_name", app=app)
    yield tangoattr


def test_tangoattr_state(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("state") == "ON"


def test_tangoattr_status(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("status") == "The device is in ON state."


def test_tangoattr_number(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("value") == -1
    assert tangoattr_last_image_saved.get("data_type") == "DevLong"


def test_tangoattr_string(tangoattr_user_detector_name):
    assert tangoattr_user_detector_name.get("value") == "Simulator"
    assert tangoattr_user_detector_name.get("data_type") == "DevString"


def test_tangoattr_unit(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("unit") == ""


def test_tangoattr_quality(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("quality") == "ATTR_VALID"


def test_tangoattr_format(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("format") == "%d"


def test_tangoattr_display_unit(tangoattr_last_image_saved):
    assert tangoattr_last_image_saved.get("display_unit") == ""
