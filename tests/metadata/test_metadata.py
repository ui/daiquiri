import datetime
from pytest import approx

# Test functions that are used within the application without a flask app_context


def test_add_datacollection(metadata):
    data = {
        "sessionid": 1,
        "sampleid": 1,
        "subsampleid": 1,
        "starttime": datetime.datetime.now() - datetime.timedelta(seconds=600),
        "endtime": datetime.datetime.now() - datetime.timedelta(seconds=400),
        "experimenttype": "experiment",
    }

    dc = metadata.add_datacollection(no_context=True, **data)

    for k in ["sampleid", "subsampleid"]:
        assert data[k] == dc[k]


def test_update_datacollection(metadata):
    dcs = metadata.get_datacollections(no_context=True)
    last = dcs["rows"][-1]["datacollectionid"]

    runstatus = "Failure"
    dc = metadata.update_datacollection(
        datacollectionid=last, no_context=True, runstatus=runstatus
    )

    assert dc["runstatus"] == runstatus


def test_update_datacollection_gridinfo(metadata):
    dcs = metadata.get_datacollections(no_context=True)
    last = dcs["rows"][-1]["datacollectionid"]

    snaked = 1
    dc = metadata.update_datacollection(
        datacollectionid=last, no_context=True, snaked=snaked
    )

    assert dc["snaked"] == snaked


def test_add_sampleimage(metadata):
    data = {
        "sampleid": 1,
        "offsetx": 10,
        "offsety": 20,
        "scalex": 1500,
        "scaley": 1510,
        "file": "/data/sampleimage.jpg",
    }

    image = metadata.add_sampleimage(no_context=True, **data)

    for k in ["offsetx", "offsety", "scalex", "scaley"]:
        assert data[k] == approx(image[k])


# def test_queue_subsample(metadata):
#     cqsid, dcpid = metadata.queue_subsample(subsampleid=1)

#     assert cqsid, dcpid
#     assert metadata.unqueue_subsample(1, containerqueuesampleid=cqsid)


def test_add_xrf_map_large(metadata):
    import random

    data = {
        "datacollectionid": 1,
        "maproiid": 1,
        "data": [random.randrange(1e4, 1e6) for i in range(512 * 512)],
    }

    xrfmap = metadata.add_xrf_map(no_context=True, return_data=True, **data)

    for k in ["datacollectionid", "maproiid", "data"]:
        assert data[k] == xrfmap[k]


def test_add_xrf_map(metadata):
    data = {"datacollectionid": 1, "maproiid": 1, "data": list(range(20 * 20))}

    xrfmap = metadata.add_xrf_map(no_context=True, return_data=True, **data)

    for k in ["datacollectionid", "maproiid", "data"]:
        assert data[k] == xrfmap[k]


def test_update_xrf_map(metadata):
    maps = metadata.get_xrf_maps(no_context=True)
    last = maps["rows"][-1]["mapid"]

    opacity = 0.5
    xrfmap = metadata.update_xrf_map(mapid=last, no_context=True, opacity=opacity)

    assert xrfmap["opacity"] == opacity


def test_add_xrf_map_roi_scalar(metadata):
    data = {"scalar": "fx2_det0"}

    xrfmaproiscalar = metadata.add_xrf_map_roi_scalar(no_context=True, **data)
    assert xrfmaproiscalar["maproiid"]


def test_add_datacollection_attachment(metadata):
    data = {
        "datacollectionid": 1,
        "filetype": "params",
        "filepath": "/data/params.json",
    }

    att = metadata.add_datacollection_attachment(no_context=True, **data)

    assert data["filetype"] == att["filetype"]
    assert data["filepath"] == att["filefullpath"]


def test_sampleaction(metadata):
    data = {
        "sessionid": 1,
        "sampleid": 1,
        "actiontype": "MOSAIC",
        "status": "SUCCESS",
        "starttime": datetime.datetime.now() - datetime.timedelta(seconds=40),
        "endtime": datetime.datetime.now(),
        "xtalsnapshotafter": "/data/snapshot.jpeg",
    }

    action = metadata.add_sampleaction(no_context=True, **data)

    for k in ["sessionid", "sampleid", "actiontype", "status", "xtalsnapshotafter"]:
        assert data[k] == action[k]

    actions = metadata.get_sampleactions(no_context=True)
    action1 = actions["rows"][-1]
    for k in ["sessionid", "sampleid", "actiontype", "status", "xtalsnapshotafter"]:
        assert data[k] == action1[k]

    status = "ERROR"
    actionu = metadata.update_sampleaction(
        action1["sampleactionid"], no_context=True, status=status
    )
    assert actionu["status"] == status


def test_add_scanqualityindicator(metadata):
    data = {"datacollectionid": 1, "total": 12, "point": 1}

    sqi_id = metadata.add_scanqualityindicators(no_context=True, **data)
    assert sqi_id == "1-1"

    sqis = metadata.get_scanqualityindicators(datacollectionid=1, no_context=True)
    for iid, point in enumerate(sqis["point"]):
        if point == 1:
            assert sqis["total"][iid] == 12
