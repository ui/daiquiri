import argparse
import time

from celery import Celery
from celery.exceptions import Reject


def main():
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(description="Start a simple celery worker")
    parser.add_argument(
        "--broker", help="Broker address", default="redis://localhost:6379/1"
    )
    parser.add_argument(
        "--backend", help="Backend storage address", default="redis://localhost:6379/2"
    )
    args = parser.parse_args()

    app = Celery(
        "tasks",
        broker=args.broker,
        backend=args.backend,
    )

    @app.task(name="sleep")
    def sleep(
        sleep_time: int = 0.1,
        raise_exception: bool = False,
        reject: bool = False,
    ):
        print("sleeping")
        if raise_exception:
            raise RuntimeError("Task raising")
        if reject:
            raise Reject("Task rejecting", requeue=False)

        time.sleep(sleep_time)
        print("sleep done")

        return True

    argv = ["worker"]
    app.worker_main(argv)


if __name__ == "__main__":
    main()
