#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Example client of the REST server using PyQt5
"""

import time
import sys
import logging

from PyQt5 import QtCore, QtWidgets
from silx.gui import plot as silx_plot

from daiquiri.cli.client_requests import RestClient
from daiquiri.cli.client_sio import SocketIOClient, BaseNameSpace


logger = logging.getLogger(__name__)

for k in ["engineio.client", "socketio.client", "urllib3.connectionpool"]:
    log = logging.getLogger(k)
    log.disabled = True


def debounce(s):
    """Decorator ensures function that can only be called once every 's' seconds.
    Stolen from: https://gist.github.com/kylebebak/ee67befc156831b3bbaa88fb197487b0
    """

    def decorate(f):
        t = None

        def wrapped(*args, **kwargs):
            nonlocal t
            t_ = time.time()
            if t is None or t_ - t >= s:
                result = f(*args, **kwargs)
                t = time.time()
                return result

        return wrapped

    return decorate


class HardwareNameSpace(BaseNameSpace):
    def on_change(self, data):
        self._signal.emit({"type": "hardware", "response": data})


class ScanNameSpace(BaseNameSpace):
    def on_new_scan(self, data):
        print("on_new_scan", data)
        self._signal.emit({"type": "scan_new", "response": data})

    def on_new_data(self, data):
        print("on_new_data", data)
        self._signal.emit({"type": "scan_data", "response": data})


class SessionNameSpace(BaseNameSpace):
    def on_message(self, data):
        self._signal.emit({"type": "session", "response": data})


class Worker(QtCore.QThread):
    signal = QtCore.pyqtSignal(dict)
    _running = False

    def __init__(self, parent=None, **kwargs):
        super(Worker, self).__init__(parent)
        self._mutex = QtCore.QMutex()
        self._client = kwargs.pop("client", None)

    def run(self):
        self._running = True

        self._socketio = SocketIOClient(token=self._client.token())
        self._socketio.register_namespace(
            HardwareNameSpace("/hardware", signal=self.signal)
        )
        self._socketio.register_namespace(ScanNameSpace("/scans", signal=self.signal))
        self._socketio.register_namespace(
            SessionNameSpace("/session", signal=self.signal)
        )
        self._socketio.connect()

        while self._running:
            self.sleep(5)
            # self.signal.emit({ 'message': 'moo '})

    def stop(self):
        self._mutex.lock()
        self._running = True
        self._mutex.unlock()


class Motor(QtWidgets.QWidget):
    def __init__(self, obj, client=None):
        self._client = client
        super().__init__()

        self._state = obj

        self._layout = QtWidgets.QVBoxLayout()
        self._group = QtWidgets.QGroupBox(obj["name"])
        self._group.setLayout(self._layout)

        self._widgets = {
            "position": QtWidgets.QLineEdit(str(obj["properties"]["position"]))
        }

        self._status = QtWidgets.QLabel(str(obj["properties"]["state"][0]))
        self._widgets["position"].textChanged.connect(self.position_changed)

        self._layout.addWidget(self._widgets["position"])

        self._stop = QtWidgets.QPushButton("Stop")
        self._stop.clicked.connect(self._do_stop)

        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(self._status)
        hbox.addWidget(self._stop)

        self._layout.addLayout(hbox)

    def container(self):
        return self._group

    def _do_stop(self):
        resp = self._client.req(
            "/hardware/{id}".format(id=self._state["id"]),
            method="post",
            data={"function": "stop"},
        )
        print("_do_stop", resp.json())
        if resp.status_code == 200:
            logger.info("Motor Stopped")

    def position_changed(self, value):
        print("Motor position_changed", value)
        # return
        if "MOVING" in self._state["properties"]["state"]:
            logger.warning("Axis already moving")
            return

        resp = self._client.req(
            "/hardware/{id}".format(id=self._state["id"]),
            method="post",
            data={"function": "move", "value": value},
        )
        if resp.status_code != 200:
            logger.warning("Could not move to position")
            print(resp, resp.json())

        logger.debug("Sent position")

    def set_enabled(self, enabled=True, internal=False):
        if not internal:
            self._enabled = enabled
        for w in self._widgets.values():
            w.setDisabled((not enabled))

        if not internal:
            self._stop.setEnabled(enabled)

    def update(self, data):
        vals = data["response"]
        if vals["id"] == self._state["id"]:
            for k, v in vals["data"].items():
                if k == "state":
                    if "MOVING" in v:
                        self.set_enabled(False, internal=True)
                    else:
                        if self._enabled:
                            self.set_enabled(True, internal=True)

                    self._status.setText(str(v[0]))

                if k in self._widgets:
                    self._widgets[k].blockSignals(True)
                    self._widgets[k].setText(str(v))
                    self._widgets[k].blockSignals(False)

                self._state["properties"][k] = v


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        self._hwr = []
        self._hardware = {}
        super().__init__()

        self._status = QtWidgets.QStatusBar()
        self.setStatusBar(self._status)

        self._client = RestClient()
        self._client.login("efgh", "5678", "qt")

        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)

        self._vmain = QtWidgets.QHBoxLayout(self._main)

        self._layout = QtWidgets.QVBoxLayout()
        self._layoutr = QtWidgets.QVBoxLayout()

        self._vmain.addLayout(self._layout)
        self._vmain.addLayout(self._layoutr)

        self.populate_session()
        self.populate_hardware()
        self.populate_scans()

        self._worker = Worker(client=self._client)
        self._worker.signal.connect(self.handleDataSent)
        self._worker.start()

    def logout(self):
        self._client.logout()

    def closeEvent(self, event):
        self._client.logout()

    def handleDataSent(self, data):
        print(data)

        if "type" in data:
            if data["type"] == "hardware":
                for hw in self._hwr:
                    hw.update(data)

            if data["type"] == "session":
                self.update_session()
                print("session event", data)

            if data["type"] == "scan_new":
                self.update_scans()
                self.show_scan(data["response"]["scanid"], new=True)

            if data["type"] == "scan_data":
                self.show_scan(data["response"]["scanid"])

    def populate_scans(self):
        layout = QtWidgets.QVBoxLayout()
        self._scan_table = QtWidgets.QTableWidget()
        self._scan_table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self._scan_table.setColumnCount(7)
        self._scan_table.setHorizontalHeaderLabels(
            ["#", "title", "start", "end", "points", "count time", ""]
        )
        self._scan_table.itemSelectionChanged.connect(self._row_selected)

        self.silx_plot = silx_plot.Plot1D(self)

        layout.addWidget(self._scan_table)
        layout.addWidget(self.silx_plot)

        group = QtWidgets.QGroupBox("Scans")
        group.setLayout(layout)

        self._layoutr.addWidget(group)
        self.update_scans()

    def _row_selected(self):
        item = self._scan_table.selectedItems()
        scanid = item[0].text()
        self.show_scan(scanid, new=True)

    def update_scans(self):
        resp = self._client.req("/scans")
        if resp.status_code == 200:
            self._scan_table.setRowCount(len(resp.json()["scans"]))

            for i, s in enumerate(resp.json()["scans"]):
                print(s)
                self._scan_table.setItem(
                    i, 0, QtWidgets.QTableWidgetItem(str(s["scanid"]))
                )
                self._scan_table.setItem(i, 1, QtWidgets.QTableWidgetItem(s["title"]))
                self._scan_table.setItem(
                    i,
                    2,
                    QtWidgets.QTableWidgetItem(
                        time.strftime(
                            "%Y-%m-%d %H:%M:%S", time.localtime(s["start_timestamp"])
                        )
                    ),
                )
                self._scan_table.setItem(
                    i,
                    3,
                    QtWidgets.QTableWidgetItem(
                        time.strftime(
                            "%Y-%m-%d %H:%M:%S", time.localtime(s["end_timestamp"])
                        )
                    ),
                )
                self._scan_table.setItem(
                    i, 4, QtWidgets.QTableWidgetItem(str(s["npoints"]))
                )
                self._scan_table.setItem(
                    i, 5, QtWidgets.QTableWidgetItem(str(s["count_time"]))
                )

            if len(resp.json()):
                self.show_scan(resp.json()["scans"][-1]["scanid"])

    @debounce(0.5)
    def show_scan(self, scanid, new=False):
        resp = self._client.req("/scans/data/{id}".format(id=scanid))
        if resp.status_code == 200:
            if new:
                self.silx_plot.clearCurves()

            obj = resp.json()
            xaxis = obj["axes"]["xs"][0]
            x = obj["data"][xaxis]

            yaxes = obj["axes"]["ys"]
            for s in yaxes["scalars"]:
                y = obj["data"][s]

                self.silx_plot.addCurve(
                    x["data"][0 : len(y["data"])], y["data"], legend=y["name"]
                )

    def populate_session(self):
        layout = QtWidgets.QVBoxLayout()
        self._session_table = QtWidgets.QTableWidget()
        self._session_table.setColumnCount(5)
        self._session_table.setHorizontalHeaderLabels(
            ["id", "user", "client", "last access", ""]
        )

        layout.addWidget(self._session_table)

        button = QtWidgets.QPushButton("Request Control")
        button.clicked.connect(self._request_control)
        layout.addWidget(button)

        group = QtWidgets.QGroupBox("Sessions")
        group.setLayout(layout)

        self._layout.addWidget(group)
        self.update_session()

    def _request_control(self):
        resp = self._client.req("/session/current/control", method="post")
        self._status.showMessage("Requesting Control")
        if resp.status_code == 200:
            self.update_session()
            self._status.showMessage("Control Granted")

    def change_session_status(self):
        has_control = self._client.session("operator")
        for hw in self._hwr:
            hw.set_enabled(has_control)

    def update_session(self):
        resp = self._client.req("/session")
        if resp.status_code == 200:
            self._session_table.setRowCount(len(resp.json()))

            for i, s in enumerate(resp.json()):
                self._session_table.setItem(
                    i, 0, QtWidgets.QTableWidgetItem(s["sessionid"])
                )
                self._session_table.setItem(
                    i, 1, QtWidgets.QTableWidgetItem(s["data"]["username"])
                )
                self._session_table.setItem(
                    i, 2, QtWidgets.QTableWidgetItem(s["data"]["client"])
                )
                control = ""
                if s["operator"]:
                    control = "[Control]"
                self._session_table.setItem(i, 4, QtWidgets.QTableWidgetItem(control))

                you = ""
                if s["sessionid"] == self._client.session("sessionid"):
                    self._client.set_session("operator", s["operator"])
                    you = "[You]"

                self._session_table.setItem(
                    i, 4, QtWidgets.QTableWidgetItem(control + " " + you)
                )

        self.change_session_status()

    def populate_hardware(self):
        resp = self._client.req("/hardware")
        if resp.status_code == 200:
            resp2 = self._client.req("/hardware/groups")
            if resp2.status_code == 200:
                hw = resp.json()
                groups = resp2.json()

                hwvbox = QtWidgets.QVBoxLayout()
                hwbox = QtWidgets.QGroupBox("Hardware")
                hwbox.setLayout(hwvbox)

                for g in groups:
                    hbox = QtWidgets.QHBoxLayout()
                    group = QtWidgets.QGroupBox(g["name"])
                    group.setLayout(hbox)

                    for oid in g["objects"]:
                        for obj in hw:
                            if obj["id"] == oid:
                                if obj["type"] == "motor":
                                    inst = Motor(obj, client=self._client)
                                    inst.set_enabled(False)
                                    hbox.addWidget(inst.container())

                                    self._hwr.append(inst)

                    hwvbox.addWidget(group)

                self._layout.addWidget(hwbox)


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    # window.setGeometry(500, 100, 400, 400)
    window.show()
    try:
        sys.exit(app.exec_())
    finally:
        window.logout()
