#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import argparse
import logging
from datetime import datetime

from flask import Flask
from flask_apispec import FlaskApiSpec
from flask_socketio import SocketIO

from daiquiri.core.metadata import MetaData
from daiquiri.resources import utils

app = Flask(__name__)
docs = FlaskApiSpec(app)
socketio = SocketIO(app)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class MapFromFile:
    def __init__(self, metadata, session):
        self._metadata = metadata
        self._session = metadata.get_sessions(session=session, no_context=True)

    def create_xrf_map(
        self, subsampleid=None, datacollectionid=None, steps_x=0, steps_y=0, **kwargs
    ):
        if not subsampleid and not datacollectionid:
            raise Exception("One of subsampleid or datacollectionid must be specified")

        points = steps_x * steps_y
        datacollection = self.ensure_datacollection(
            subsampleid, datacollectionid, steps_x, steps_y
        )

        for i in range(kwargs["nmaps"]):
            roi = self._metadata.add_xrf_map_roi_scalar(scalar=f"testmap{i}")
            xmap = self._metadata.add_xrf_map(
                maproiid=roi["maproiid"],
                datacollectionid=datacollection["datacollectionid"],
                data=[i + random.random() * 1000 for i in range(points)],
                points=points,
                no_context=True,
            )

        logger.info(
            f"Created xrf map {xmap['mapid']} from scalar {kwargs.get('scalar')}"
        )

        if datacollectionid is None:
            self._metadata.update_datacollection(
                datacollectionid=datacollection["datacollectionid"],
                endtime=datetime.now(),
                runstatus="Successful",
                comments="Import from mapfromhdf5",
                no_context=True,
            )

    def ensure_datacollection(
        self, subsampleid=None, datacollectionid=None, steps_x=None, steps_y=None
    ):
        if subsampleid:
            subsample = self._metadata.get_subsamples(
                subsampleid=subsampleid, no_context=True
            )

            if subsample["type"] != "roi":
                logger.error(
                    f"Can only import from rois, subsample {subsample['subsampleid']} is {subsample['type']}"
                )
                exit()

            datacollection = self._metadata.add_datacollection(
                imagedirectory="path",
                filetemplate="images",
                sessionid=self._session["sessionid"],
                sampleid=subsample["sampleid"],
                subsampleid=subsample["subsampleid"],
                starttime=datetime.now(),
                endtime=datetime.now(),
                experimenttype="XRF map",
                steps_x=steps_x,
                steps_y=steps_y,
                numberofimages=steps_x * steps_y,
                dx_mm=(subsample["x2"] - subsample["x"]) / steps_x * 1e-6,
                dy_mm=(subsample["y2"] - subsample["y"]) / steps_y * 1e-6,
                no_context=True,
            )

            logger.info(f"Created datacollection {datacollection['datacollectionid']}")

        else:
            datacollection = self._metadata.get_datacollections(
                datacollectionid=datacollectionid, no_context=True
            )

        return datacollection


def main():
    args = parse_args()
    utils.add_resource_root(args.resource_folder)

    config = utils.ConfigDict("app.yml")
    metadata = MetaData(
        config, app=app, schema=None, docs=docs, socketio=socketio
    ).init()

    mff = MapFromFile(metadata, args.session)
    mff.create_xrf_map(
        args.subsampleid,
        args.datacollectionid,
        steps_x=args.steps_x,
        steps_y=args.steps_y,
    )


def parse_args():
    parser = argparse.ArgumentParser(description="Create XRF maps for tests")
    parser.add_argument(
        "--resource-folder",
        dest="resource_folder",
        help="Server resource directories",
        required=True,
    )
    parser.add_argument(
        "--session", help="The session to import the map in to", default="blc00001-1"
    )

    parser.add_argument(
        "--steps-x", type=int, dest="steps_x", default=200, help="Number of steps in x"
    )

    parser.add_argument(
        "--steps-y", type=int, dest="steps_y", default=200, help="Number of steps in y"
    )

    parser.add_argument(
        "--nmaps", type=int, dest="nmaps", default=1, help="Number of maps to create"
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--subsampleid", type=int, help="Subsampleid to import map to")
    group.add_argument(
        "--datacollectionid", type=int, help="Datacollectionid to import map to"
    )

    return parser.parse_args()


if __name__ == "__main__":
    main()
