# Scans
The scans component tells Daiquiri how to interact with the scanning system. Currently only a [Bliss](https://gitlab.esrf.fr/bliss/bliss) handler is implemented

## scans.yml

The `scans.yml` specifies the `type` and any parameters for that handler

### Data Conversion

For 1d data (spectra) the `zero` and `scale` factor should be provided to allow conversion to units that daiquiri understands. `zero` is the zero offset of the data, and `scale` is the energy (eV) per bin

```yaml
mca:
  conversion:
    zero: 0
    scale: 25
```

### Bliss Handler

The Bliss handler should be told which session to connect to:

```yaml
sources:
  - type: bliss
    session: test_session
```

The Bliss handler will watch this session and send events on scan start, new data, and end. This allows Daiquiri to follow scans interactively.

## Blissdata Handler

In addition to the core bliss handler there is also a standalone [blissdata](https://gitlab.esrf.fr/bliss/blissdata) handler. This handler does not require a bliss installation and should be directed to the blissdata `redis_url`:

```yaml
sources:
  - type: blissdata
    session: test_session
    redis_url: redis://localhost:6379
```

## Additional Handlers

Additional handlers can be implemented through the [`ScanSource`](https://gitlab.esrf.fr/ui/daiquiri/-/blob/main/daiquiri/core/hardware/abstract/scansource.py) interface.
