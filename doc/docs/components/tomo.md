# Tomography

The tomography component provides shared configuration for services around
BLISS tomography.

If defined it:
- Monitors tomo scans to extract few metadata
- Exposes a service to reach metadata from tomo scans
- Exposes a service to reach images from Lima detector
- Exposes a service to move tomo sample stage motors

## Common configuration

The link to the BLISS session have to be provided.

Then the name of the BLISS object holding the tomography configuration.

```yaml
component: tomo
sources:
  - type: bliss
    session: !include bliss.yml#session

    # Bliss name of the related tomo config
    # It's a BLISS object containing most of the setup related to a tomo beamlines
    tomo_config: ACTIVE_TOMOCONFIG

# If defined a tomovis reconstruction service will be exposed and used in the GUI
tomovis:
    url: http://localhost:8888/
    create_proxy: true
```

## Tomovis

A tomovis service can be specified.

For now it is used to process and to store the state of the tiling
reconstruction.

If defined:

- Daiquiri backend is able to inform this service when a new tiling
  scan was started, to start the data processing.
- Daiquiri frontend is able to monitor the service to select and display the
  available tiling reconstruction.

If `create_proxy` is set to true, a proxy for this service is handled by
Daiquiri. This allow to enforce https protocol for the client, or provides
a single entry for external users. But this increase the latency of the
service.

## Avatar

A dedicated avatar can be setup the following way.

```yaml
# Setup a custom avatar
avatar:
  sprite_sheet: 'resources/tomo/fox-yellow.png'
```

The image file contain tiles in a grid of 4x10. Each tile is used for
a specific context.

## Image format

An optional field `image_format` can be defined to specify the way to transfer
image data.

If undefined the value is `png`.

```yaml
# Way to share the detector data
# One of png / jpeg
image_format: png
```
