# File Editor

The File Editor component provides a way to read and write a whitelisted group of files. This is useful for writing and debugging [Actors](../actors.md) in real-time. By default the file editor component will whitelist the current `resources` and `implementors` folder.

Extra paths may be specified in the `fileeditor.yml`

```yaml
directories:
  - /extra/path
```
