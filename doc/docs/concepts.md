# Concepts

## Login
Because Daiquiri uses web technologies it must enforce user login in order to know who is using the application.

## Sessions
In order to restrict access to Daiquiri the relevant beamline session information is used to verify whether a particular user (or proposal login) currently has access to the beamline. This is important when Daiquiri is made available remotely to stop random users interfering with the beamline. Staff members can always log in to the beamline.

The added benifit of making the user select a session is that the data policy is automatically implemented. 

## Control
Only one session within Daiquiri can control the beamline at once. This stops multiple acquisitions being launched simultanously. Users must request and grant control. Staff members can login and take control at any time.

## Queue
Daiquiri provides the possibility to queue [Actors](actors.md). The means a series of measurements can be queued to allow automated data collection. The queue can wait for a status flag before processing, for example to check if beam is available. For this to work an `implementors/queue/monitor.py` actor should be defined that returns a boolean value defining if the queue is ready to process. An example is provided in the example `implementors`
