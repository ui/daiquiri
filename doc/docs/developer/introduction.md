# Developing

## Code Style

Code linting uses [flake8](https://flake8.pycqa.org/en/latest/index.html) and style is enforced using [black](https://github.com/psf/black). To get the right versions, Install the packages with an optional `dev` dependency:

- _Pip:_

```
pip install .[dev]
```

CI on the server will automatically enforce style, but not linting. See the [CI](./ci.md) section for more details.

## Setup with docker

The easiest way to set-up a dev environment is to use the docker images provided in [mimosa-database](https://gitlab.esrf.fr/ui/mimosa/mimosa-database) and [daiquiri-docker](https://gitlab.esrf.fr/ui/daiquiri-docker). These will provide a prepopulated local database and all needed dependencies (including `bliss`). These images are availabe on docker hub so can be installed as follows:

```
docker run -d --name mariadb esrfbcu/mimosa-database:main
docker run -d -v "/path/to/daiquiri:/daiquiri" --rm --link mariadb -p 9032:9032 -p 9030:9030 -p 8080:8080 esrfbcu/daiquiri:main
```

Thanks to the option `-v "/path/to/daiquiri:/daiquiri"` that mounts the local project in the docker, local changes to `daiquiri` are propagated to the docker image for development.

Congratulations, you now have a running instance of `daiquiri` that can be accessed on `http://localhost:8080` and be modified at your will !

## Local installation setup

### Deal with dependencies

The **true** lists of dependencies are located in `requirements*.txt`. They include `conda` requirements for the project, the tests and the doc to be installed when installing `daiquiri` for development. These files are also used to set up the project in the CI tasks.

Dependencies in `setup.cfg` only serve as runtime checks when running `daiquiri-server`.

### Local Database

A local copy of the mimosa database can be installed for development and testing using the 
[mimosa-database](https://gitlab.esrf.fr/ui/mimosa/mimosa-database) image:

```bash
docker run --name mariadb -p 3306:3306 esrfbcu/mimosa-database:main
```

Modify the database credentials to connect to the test database, and set the
`meta_beamline` and `meta_staff`, in app.yml:

```yaml
meta_beamline: bl
meta_staff: bl_staff
meta_user: test
meta_url: localhost:4406/test
```

and set the password in secrets.yml:

```yaml
meta_pass: test
```

The local database provides two users:
| User | Password | Type |
| ---- | ---- | ---- |
| abcd | anything | normal user |
| efgh | anything | admin user |

## Testing

Testing uses [pytest](https://docs.pytest.org/). As daiquiri has a database dependency, CI based testing
will generate a new database on each run. For running tests locally a local
empty database is required. Tests will drop the existing database and create
a new one each time tests are run.

### With docker containers

By using the [docker-based setup](#setup-with-docker), the script [connect.sh](https://gitlab.esrf.fr/ui/daiquiri-docker/-/blob/master/connect.sh) of `daiquiri-docker` can be used to have access to a console in the docker container running `daiquiri` with all needed dependencies.

Tests can then be launched by navigating to the `/daiquiri` folder and by running

```
pytest
```
