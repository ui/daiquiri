site_name: Daiquiri
site_url: https://ui.gitlab-pages.esrf.fr/daiquiri
repo_name: 'ui/daiquiri'
repo_url: 'https://gitlab.esrf.fr/ui/daiquiri'
theme:
  name: material
  favicon: favicon.png
  language: en
  custom_dir: 'theme'
  palette:
    primary: deep purple
    accent: cyan
  font:
    text: Poppins
    code: 'source-code-pro'
  logo: daiquiri.png

nav:
  - About: index.md
  - Installation: installation.md
  - Configuration: configuration.md
  - In Use:
    - Concepts: concepts.md
    - Resources: resources.md
    - Actors: actors.md
    - Layouts: layouts.md
    - Local Code: local.md
    - Components:
      - Introduction: components/introduction.md
      - Celery: components/celery.md
      - Console: components/console.md
      - File Browser: components/filebrowser.md
      - File Editor: components/fileeditor.md
      - Hardware: components/hardware.md
      - H5Grove: components/h5grove.md
      - Image Viewer: components/imageviewer.md
      - Parameteriser: components/parameteriser.md
      - Proxy: components/proxy.md
      - Scans: components/scans.md
      - Synoptic: components/synoptic.md
      - Sample Scan: components/samplescan.md
      - Tomo: components/tomo.md
  - API Reference: api.md
  - Developers:
    - Introduction: developer/introduction.md
    - Continuous Integration: developer/ci.md
    - Hardware Objects: developer/hardware.md
    - BLISS Controllers: developer/bliss.md
    - Components: developer/components.md
    - Reference:
      - Core Classes:
        - daiquiri.core.session: reference/session.md
        - daiquiri.core.metadata: reference/metadata.md
        - daiquiri.core.hardware: 
          - abstract: reference/hardware/abstract.md
          - bliss: reference/hardware/bliss.md
          - tango: reference/hardware/tango.md
      - Schema:
        - daiquiri.core.schema.validators: reference/validators.md
        - daiquiri.core.schema.hardware: reference/hardware/schema.md

plugins:
  - search
  - mkdocstrings:
      default_handler: python
      custom_templates: templates
      watch:
        - ../daiquiri

markdown_extensions:
  - codehilite
  - attr_list
  - admonition
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences

extra_css:
  - stylesheets/extra.css
